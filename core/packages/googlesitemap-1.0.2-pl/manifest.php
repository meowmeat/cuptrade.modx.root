<?php return array (
  'manifest-version' => '1.1',
  'manifest-attributes' => 
  array (
    'license' => 'The MIT License
--------------------------
Copyright 2019 - levpro.ru

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.',
    'readme' => '--------------------
Google sitemap generator
--------------------
Author: LevPro Development Agency <info@levpro.ru>
--------------------
Small php library for generation xml sitemap
',
    'changelog' => 'Changelog for Google sitemap generator.

1.0.0
==============
- Initial release.

1.0.1
==============
- Fixed perfomance problems.

1.0.2
==============
- Added ignore list.
- Added option: save as file.
- Added option: save file path.
- added time limit.',
    'setup-options' => 'googlesitemap-1.0.2-pl/setup-options.php',
  ),
  'manifest-vehicles' => 
  array (
    0 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modNamespace',
      'guid' => '5f4db5631de224e5270c3ab17ac4f868',
      'native_key' => 'googlesitemap',
      'filename' => 'modNamespace/763686a740810df3ed2dac8058127318.vehicle',
      'namespace' => 'googlesitemap',
    ),
    1 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modCategory',
      'guid' => 'f88cfdc6bae991559bd93fc4b0cd3c61',
      'native_key' => 1,
      'filename' => 'modCategory/4027d64fbe501afb34427210e88ca970.vehicle',
      'namespace' => 'googlesitemap',
    ),
    2 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modCategory',
      'guid' => '0ded06f16a7ea7bde32902bc270fdf17',
      'native_key' => 1,
      'filename' => 'modCategory/c4ecd91b77ef70fc7e3aed00a7854e43.vehicle',
      'namespace' => 'googlesitemap',
    ),
    3 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modCategory',
      'guid' => 'f256a805aba8db884ad7d316ccfe5736',
      'native_key' => 1,
      'filename' => 'modCategory/eabc26f83bbf11a2663da837eda5a650.vehicle',
      'namespace' => 'googlesitemap',
    ),
  ),
);