//== hide gallery-elements for tablet
//== this function called in a file: script-closing
function gallery_hideElementsTablet() {
  let galleryElement_toHide = $('.gallery').find(
    '.gallery-preview--hidetablet'
  );
  if ($(window).width() <= 860) {
    galleryElement_toHide
      .find('.gallery-preview__img')
      .addClass('gallery-preview__img-hidden');
  } else {
    galleryElement_toHide
      .find('.gallery-preview__img')
      .removeClass('gallery-preview__img-hidden');
  }
}

//== Gallery Toggle hidden previews
$('.js-galleryToggleHidden').on('click touch', function (e) {
  // e.preventDefault();
  let galleryPreviewImg_Hidden = $(this)
    .closest('.section-wrapper')
    .find('.gallery-section')
    .find('.gallery-preview__img-hidden');
  let galleryPreviewImages = $(this)
    .closest('.section-wrapper')
    .find('.gallery-preview')
    .find('.gallery-preview__img');
  if ($(window).width() > 860) {
    if (galleryPreviewImg_Hidden.length >= 1) {
      // $(this).text() == 'Показать ещё';
      galleryPreviewImg_Hidden
        .slice(0, 4)
        .slideDown()
        .removeClass('gallery-preview__img-hidden');
    } else if (galleryPreviewImg_Hidden.length == 0) {
      // $(this).text('Скрыть');
      galleryPreviewImages.slice(8).slideUp();
      setTimeout(function () {
        galleryPreviewImages.slice(8).addClass('gallery-preview__img-hidden');
      }, 600);
    }
  }

  if ($(window).width() > 560 && $(window).width() <= 860) {
    if (galleryPreviewImg_Hidden.length >= 1) {
      // $(this).text() == 'Показать ещё';
      galleryPreviewImg_Hidden
        .slice(0, 3)
        .slideDown()
        .removeClass('gallery-preview__img-hidden');
    } else if (galleryPreviewImg_Hidden.length == 0) {
      // $(this).text('Скрыть');
      galleryPreviewImages.slice(6).slideUp();
      setTimeout(function () {
        galleryPreviewImages.slice(6).addClass('gallery-preview__img-hidden');
      }, 600);
    }
  }

  if ($(window).width() >= 320 && $(window).width() <= 560) {
    if (galleryPreviewImg_Hidden.length >= 1) {
      // $(this).text() == 'Показать ещё';
      galleryPreviewImg_Hidden
        .slice(0, 2)
        .slideDown()
        .removeClass('gallery-preview__img-hidden');
    } else if (galleryPreviewImg_Hidden.length == 0) {
      // $(this).text('Скрыть');
      galleryPreviewImages.slice(6).slideUp();
      setTimeout(function () {
        galleryPreviewImages.slice(6).addClass('gallery-preview__img-hidden');
      }, 600);
    }
  }
});
