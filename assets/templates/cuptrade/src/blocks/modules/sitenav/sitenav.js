//== Sitenav Mobile: slide toggle On Tablet
$(document).on('click touch', '.js-sitenavMob_Call', function () {
  $('body').addClass('no-scroll');
  $('#sitenavMob').removeClass('slideOutRight').addClass('slideInRight');
  $('#sitenavMob').show();
  if ($(window).width() > 560) {
    $('#sitenavOverlay').addClass('sitenav-overlay--active');
    $('.sitenav-overlay--active').css('height', $(document).height());
    if ($('#sitenavOverlay').hasClass('sitenav-overlay--active') != true) {
      $('#sitenavOverlay').css('height', '');
    }
  }
});
$(document).on('click touch', '#sitenavOverlay', function () {
  $('body').removeClass('no-scroll');
  $('#sitenavMob').removeClass('slideInRight').addClass('slideOutRight');
  $('#sitenavCatalog_Mob')
    .removeClass('slideInRight')
    .addClass('slideOutRight');
  $(this).removeClass('sitenav-overlay--active');
  $(this).css('height', '');
});

//== Sitenav Mobile Close
$(document).on('click touch', '.js-sitenavClose', function () {
  $('body').removeClass('no-scroll');
  $('#sitenavMob').removeClass('slideInRight').addClass('slideOutRight');
  $('#sitenavOverlay').removeClass('sitenav-overlay--active').css('height', '');
});

//== Sitenav Set of inner links togle
$(document).on('click touch', '.js-sitenavSet_Toggle', function (e) {
  e.preventDefault();
  // e.stopPropagation();
  $(this)
    .closest('.sitenav-menu__module-heading')
    .toggleClass('sitenav-menu__module-heading--expanded');
  $(this).closest('.sitenav-menu__module').find('.sitenav-set').slideToggle();
});

//== Sitenav Mobile: toggle menu set
// function sitenav_toggleSet() {
//   const sitenav = $('#sitenavMob');
//   if (!sitenav.length) return;
//   sitenav.on('click', '.js-sitenavChildSet_Call', function () {
//     let sitenav_module = $(this);
//     let sitenav_set = sitenav_module.siblings('.sitenav-set');
//     sitenav_set.slideToggle();
//     $('.js-sitenavChildSet_Call')
//       .not(sitenav_module)
//       .siblings('.sitenav-set')
//       .slideUp();
//   });
// }
// sitenav_toggleSet();
