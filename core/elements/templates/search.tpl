{extends 'file:templates/index.tpl'}
{block 'class'}search-page{/block}
{block 'main'}
{set $limit = $.get.limit?:21}
  {'!mFilter2Custom' | snippet : [
	'ajaxMode' => 0,
	'ajax' => 1,
	'class' => 'msProduct',
  'element' => 'msProducts',
  'returnIds' => 0,
  'fastMode' => 1,
  'setMeta' => 0,
	'toPlaceholders' => 'my.',
  'tpl' => '@FILE chunks/all/tpl.goods_card.tpl',
	'parents' => 2,
	'queryVar' => 'query',
	'orderby' => 'default',
	'sort' => 'default:asc',
	'limit' => $limit,
	'tplPage' => '@INLINE <a href="{$href}" class="pagination__item">{$pageNo}</a>',
  'tplPageWrapper' => '@INLINE <div class="pagination__items mse2_pagination">{$prev}{$pages}{$next}</div>',
	'tplPageActive' => '@INLINE <span class="pagination__item pagination__item-current">{$pageNo}</span>',
	'tplPagePrev' => '@INLINE <a href="{$href}" style="background-color: #df312f;" class="pagination__control pagination__prev"><svg width="8" height="12" viewBox="0 0 8 12" fill="#fff" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M6.079 11.693l1.442-1.386L3.386 6l4.135-4.307L6.08.307.614 6l5.465 5.693z" />
                    </svg></a>',
	'tplPageNext' => '@INLINE <a href="{$href}" class="pagination__control pagination__next"><svg width="8" height="12" viewBox="0 0 8 12" fill="#fff" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M1.721 11.693L.28 10.307 4.414 6 .279 1.693 1.72.307 7.186 6l-5.465 5.693z" />
                    </svg></a>',
	'tplPageSkip' => '@INLINE <div class="pagination__dots">...</div>',
	'tplPagePrevEmpty' => '@INLINE <a href="javascript:void(0)" class="pagination__control pagination__prev"><svg width="8" height="12" viewBox="0 0 8 12" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M6.079 11.693l1.442-1.386L3.386 6l4.135-4.307L6.08.307.614 6l5.465 5.693z" />
                    </svg></a>',
	'tplPageNextEmpty' => '@INLINE <a href="javascript:void(0)" style="background-color: #faf7f7;" class="pagination__control pagination__next"><svg width="8" height="12" viewBox="0 0 8 12" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M1.721 11.693L.28 10.307 4.414 6 .279 1.693 1.72.307 7.186 6l-5.465 5.693z" />
                    </svg></a>',
	'cache' => '1',
	

'filters' => '
resource|parent:categories,
ms|value
',

'aliases' => '
        resource|parent==category,
        resource|menuindex==default,
        ms|value==value
    ',

    'tplFilter.outer.category' => 'tpl.mFilter2.filter.CATEGORYouter',
    'tplFilter.row.category' => 'tpl.mFilter2.filter.CATEGORYcheckbox',
    'tplFilter.outer.value' => 'tpl.mFilter2.filter.VALUEouter',
    'tplFilter.row.value' => 'tpl.mFilter2.filter.VALUEcheckbox',

    'showZeroPrice' => 0,
    'includeContent' => 0,
    'showLog' => 0
]}
  <div class="search-section section js-yes-search" id="mse2_mfilter">
    <div class="section-wrapper">
      <div class="title search__title">По запросу «{$.get.query | esc}» найдено {'my.total' | placeholder | decl : 'совпадение|совпадения|совпадений' : true}</div>
      {set $form}
      <form action="{25 | url}" method="GET" class="search-page-form">
        <input type="search" name="query" value="{$.get.query}" id="searchPageFormField" class="search-page-form__input" placeholder="Я ищу...">
        <input type="submit" name="searchPageForm_submit" id="searchPageFormButton" class="search-page-form__submit" value="">
        <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-search.svg" alt="" class="search-page-form__submit-ico">
      </form>
      {/set}
      {$form}

      <div class="section-twin">
        <div class="section-twin__side">
          
              {'my.filters' | placeholder}
          
          {*<div class="sidebar-mob-section">
            <div class="sidebar-mob-select js-sidebarMob_Call">
              <span class="sidebar-mob-select__text">Cовпадения в каталоге</span>
              <svg width="16" height="9" viewBox="0 0 12 8" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg sidebar-mob-select__ico">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M.04 1.989L1.425.546l4.307 4.135L10.04.546l1.385 1.443-5.693 5.465L.04 1.989z" />
              </svg>
            </div>
            <div class="sidebar-mob">
              <div class="sidebar-mob__items">
                <a href="" class="sidebar-mob__item">Однослойные стаканы</a>
                <a href="" class="sidebar-mob__item">Двухслойные стаканы</a>
                <a href="" class="sidebar-mob__item">Крышки для стаканов</a>
              </div>
            </div>
          </div>*}

        </div>

        <div class="section-twin__main">
          <div class="catalog">

            {include 'file:chunks/all/sort.tpl'}

            <div class="catalog-items catalog-items-white" id="mse2_results">
            {'my.results' | placeholder}
            </div> <!-- end catalog-items -->
            <div class="pagination-section">
              <div class="section-wrapper">
                <div class="pagination">
                  {$_modx->getPlaceholder('page.nav')}
                  {set $pages = ($_modx->getPlaceholder('page.total')/$limit) | ceil}
                  <script>TOTAL_PAGES = {$pages?:1}</script>
                  <form method="get" class="pagination-switch__form js-form-pages">
                    {if $.get.limit}<input type="hidden" name="limit" value="{$.get.limit}">{/if}
                    {if $.get.query}<input type="hidden" name="query" value="{$.get.query}">{/if}
                    <div class="pagination-switch__text">Перейти на страницу</div>
                    <div class="pagination-switch__controls">
                      <input type="number" name="page" value="{(($.get.page?($.get.page+1):2)>$pages)?1:($.get.page?($.get.page+1):2)}" id="paginationForm_switch" onkeypress="return checkingNumberFormat(event);" class="pagination-switch__input">
                      <input type="submit" id="paginationForm_submit" value="Перейти" class="pagination-switch__button">
                    </div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>

  <div class="search-section section js-not-search">
    <div class="section-wrapper">
      <div class="title search__title">По запросу «{$.get.query | esc}» ничего не найдено</div>

      {$form}
      <div class="search-empty__text">Попробуйте уточнить запрос или перейдите на главную страницу</div>
      <div class="flex-centered"><a href="/" class="button button-primary search-empty__button">Перейти на главную</a></div>
    </div>
  </div>
{/block}