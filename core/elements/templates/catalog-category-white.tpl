{extends 'file:templates/index.tpl'}
{block 'class'}catalog-category-page{/block}
{block 'main'}
{$_modx->getChunk('@FILE chunks/all/breadcrumbs.tpl')}
{set $menu_new = 'pdoMenu' | snippet : [
      'showLog' => 0,
      'templates' => '11',
      'parents' => $_modx->resource.parent,
      'level' => 1,
      'hereClass' => '_open',
      'selfClass' => 'self',
      'tplOuter' => '@INLINE {$wrapper}<div class="sidebar-list__more js-sidebarList_More"><span class="sidebar-list__more-text">Ещё</span> <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_down.svg" class="sidebar-list__more-ico"></div>',
      'tplHere' => '@INLINE <span class="sidebar-list__item sidebar-list__item--active">{$menutitle}</span>{$wrapper}',
      'tpl' => '@INLINE <a href="{$link}" class="sidebar-list__item">{$menutitle}</a>{$wrapper}',
      'tplInner' => '@INLINE {$wrapper}',
      'tplInnerRow' => '@INLINE <a href="{$link}" class="sidebar-list__item">{$menutitle}</a>{$wrapper}'
  ]}
  
  {set $limit = $.get.limit?:45}
  {'!mFilter2Custom' | snippet : [
	'ajaxMode' => 0,
	'ajax' => 1,
	'class' => 'msProduct',
  'element' => 'msProducts',
  'returnIds' => 0,
  'fastMode' => 1,
  'setMeta' => 0,
	'toPlaceholders' => 'my.',
  'tpl' => '@FILE chunks/all/tpl.goods_card.tpl',
	'parents' => $_modx->resource.id,
	'queryVar' => 'query',
	'orderby' => 'default',
	'sort' => 'default:asc',
	'limit' => $limit,
	'tplPage' => '@INLINE <a href="{$href}" class="pagination__item">{$pageNo}</a>',
  'tplPageWrapper' => '@INLINE <div class="pagination__items mse2_pagination">{$prev}{$pages}{$next}</div>',
	'tplPageActive' => '@INLINE <span class="pagination__item pagination__item-current">{$pageNo}</span>',
	'tplPagePrev' => '@INLINE <a href="{$href}" style="background-color: #df312f;" class="pagination__control pagination__prev"><svg width="8" height="12" viewBox="0 0 8 12" fill="#fff" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M6.079 11.693l1.442-1.386L3.386 6l4.135-4.307L6.08.307.614 6l5.465 5.693z" />
                    </svg></a>',
	'tplPageNext' => '@INLINE <a href="{$href}" class="pagination__control pagination__next"><svg width="8" height="12" viewBox="0 0 8 12" fill="#fff" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M1.721 11.693L.28 10.307 4.414 6 .279 1.693 1.72.307 7.186 6l-5.465 5.693z" />
                    </svg></a>',
	'tplPageSkip' => '@INLINE <div class="pagination__dots">...</div>',
	'tplPagePrevEmpty' => '@INLINE <a href="javascript:void(0)" class="pagination__control pagination__prev"><svg width="8" height="12" viewBox="0 0 8 12" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M6.079 11.693l1.442-1.386L3.386 6l4.135-4.307L6.08.307.614 6l5.465 5.693z" />
                    </svg></a>',
	'tplPageNextEmpty' => '@INLINE <a href="javascript:void(0)" style="background-color: #faf7f7;" class="pagination__control pagination__next"><svg width="8" height="12" viewBox="0 0 8 12" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M1.721 11.693L.28 10.307 4.414 6 .279 1.693 1.72.307 7.186 6l-5.465 5.693z" />
                    </svg></a>',
	'cache' => '1',
	

'filters' => '
resource|parent:categories,
ms|value
',

'aliases' => '
        resource|parent==category,
        resource|menuindex==default,
        ms|value==value
    ',

    'tplFilter.outer.category' => 'tpl.mFilter2.filter.CATEGORYouter',
    'tplFilter.row.category' => 'tpl.mFilter2.filter.CATEGORYcheckbox',
    'tplFilter.outer.value' => 'tpl.mFilter2.filter.VALUEouter',
    'tplFilter.row.value' => 'tpl.mFilter2.filter.VALUEcheckbox',

    'showZeroPrice' => 0,
    'includeContent' => 0,
    'showLog' => 0
]}
  <div class="catalog-section section">
    <div class="section-wrapper">
      <div class="section-twin">
        <div class="section-twin__side">
          <div class="sidebar js-not-more">
            <div class="sidebar-list">
              {set $parentObj = $_modx->resource.parent|resource}
              <a href="{$parentObj.uri}" class="sidebar-section">{$parentObj.menutitle?:$parentObj.pagetitle}</a>
              {$menu_new}
            </div>
            <div class="swiper-button-lock">
            {'my.filters' | placeholder}
            </div>
          </div>
          <div class="sidebar-mob-section">
            <div class="sidebar-mob-select js-sidebarMob_Call">
              <span class="sidebar-mob-select__text">{$_modx->resource.menutitle?:$_modx->resource.pagetitle}</span>
              <svg width="16" height="9" viewBox="0 0 12 8" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg sidebar-mob-select__ico">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M.04 1.989L1.425.546l4.307 4.135L10.04.546l1.385 1.443-5.693 5.465L.04 1.989z" />
              </svg>
            </div>
            <div class="sidebar-mob">
              <div class="sidebar-mob__items">
                {$menu_new | replace:"sidebar-section":"sidebar-mob__item" | replace:"sidebar-list__item":"sidebar-mob__item"}
              </div>
            </div>
          </div>
        </div>

        <div class="section-twin__main catalog" id="mse2_mfilter">
          <h1>{$_modx->resource.pagetitle}</h1>
          {if $.get.query}<div class="title search__title">по запросу «{$.get.query | esc}»</div>{/if}

          {include 'file:chunks/all/sort.tpl'}

          <div class="catalog-items catalog-items-white" id="mse2_results">
            {'my.results' | placeholder}
          </div> <!-- end catalog-items -->
          <div class="pagination-section">
            <div class="section-wrapper">
              <div class="pagination">
                {$_modx->getPlaceholder('page.nav')}
                {set $pages = ($_modx->getPlaceholder('page.total')/$limit) | ceil}
                <script>TOTAL_PAGES = {$pages?:1}</script>
                <form method="get" class="pagination-switch__form js-form-pages">
                  {if $.get.limit}<input type="hidden" name="limit" value="{$.get.limit}">{/if}
                  {if $.get.query}<input type="hidden" name="query" value="{$.get.query}">{/if}
                  <div class="pagination-switch__text">Перейти на страницу</div>
                  <div class="pagination-switch__controls">
                    <input type="number" name="page" value="{(($.get.page?($.get.page+1):2)>$pages)?1:($.get.page?($.get.page+1):2)}" id="paginationForm_switch" onkeypress="return checkingNumberFormat(event);" class="pagination-switch__input">
                    <input type="submit" id="paginationForm_submit" value="Перейти" class="pagination-switch__button">
                  </div>
                </form>
              </div>
            </div>
          </div>

        </div><!-- end section-twin-main -->
      </div> <!-- end section-twin -->

    </div>
  </div>
  {if $content = $_modx->resource.content}
    <div class="catalog-info-section section">
      <div class="section-wrapper">
        <div class="section-info">
          {$content | lazy:true}
        </div>
      </div>
    </div>
  {/if}
{/block}