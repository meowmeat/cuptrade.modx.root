<?php

require "../../_functions.php";

$output='';

//

$result=query("select id,header,route from content where status=1 and cat_id=3 and id not in(16,17) order by position desc");

$amount=rows($result);

if($amount==0) return '';

$i=0;

while($f=fetch($result)){

  $sresult=query("select id,header,route,price,price2,art,prv_pic_file_name,prv_pic_file_type,route from catalog_products where cat_id='{$f['id']}' and status=1 order by position desc");

  $products='';

  while($sf=fetch($sresult)){

$products.=<<< EndOfThis
        <tr>
          <td class="table__cell table__cell--img">
            <img src="/pics/products/{$sf['prv_pic_file_name']}.{$sf['prv_pic_file_type']}" alt="" class="product__img">
            <!--<img src="img/new.jpg" alt="" class="product__img">-->
          </td>
          <td class="table__cell table__cell--info">
            <div class="product__name"><a href="https://www.cuptrade.ru/product/{$sf['route']}/">{$sf['header']}</a></div>
            <!--<div class="product__note">(120 мл полный)</div>-->
            <div class="product__art">{$sf['art']}</div>
          </td>
          <!--
          <td colspan="4" class="table__cell table__cell--size">
            <div class="size-list">
              <div class="size-list__in">
                <div class="size-list__item">62</div>
                <div class="size-list__item">57</div>
                <div class="size-list__item">45</div>
                <div class="size-list__item">63</div>
              </div>
            </div>
          </td>
          -->
          <td>
            <div class="product__value">65</div>
          </td>
          <!--
          <td>
            <div class="product__value">2520</div>
            <div class="product__subvalue">0,065 м3</div>
            <div class="product__signature">обьем короба</div>
            <div class="product__subvalue">8 кг</div>
            <div class="product__signature">вес короба</div>
          </td>
          <td>
            <div class="product__value">90</div>
          </td>
          -->
          <td>
            <div class="product__value">{$sf['price']} <span class="rub">₽</span></div>
          </td>
          <td>
            <div class="product__value">{$sf['price2']} <span class="rub">₽</span></div>
          </td>
        </tr>
EndOfThis;

  }

$f['header']=strip_tags($f['header']);

$output.=<<< EndOfOutput
      <table class="table table--main table--with-caption product">
        <caption class="table__caption table__caption--red">{$f['header']}</caption>
        <tr>
          <th></th>
          <th></th>
          <!--
          <th colspan="4">
            <div class="size-list">
              <div class="size-list__title">Размеры (мм)</div>
              <div class="size-list__in">
                <div class="size-list__item"><img src="img/size1.png" alt=""></div>
                <div class="size-list__item"><img src="img/size2.png" alt=""></div>
                <div class="size-list__item"><img src="img/size3.png" alt=""></div>
                <div class="size-list__item"><img src="img/size4.png" alt=""></div>
              </div>
            </div>
          </th>
          -->
          <th>В тубе<br> (шт.)</th>
          <!--<th>В коробе<br> (шт.)</th>-->
          <!--<th>Мин. партия Private Label (шт.)</th>-->
          <th>Сумма < 5000<br> цена <span class="rub">₽</span></th>
          <th>Сумма > 5000<br> цена <span class="rub">₽</span></th>
        </tr>
{$products}
      </table>
      <br>
EndOfOutput;

}

//

$title=date("dmy")."-cuptrade.ru-pricelist.pdf";

//

echo <<< EndOfThis

<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <title>{$title}</title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
  <link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="css/main.css">
</head>
<body>
  <!-- BEGIN page 1 -->
  <div class="page page--img">
    <img src="img/page1-2.jpg" alt="" class="page__img">
  </div>
  <!-- END page 1 -->

  <!-- BEGIN page 2 -->
  <div class="page page--img">
    <img src="img/page2-2.jpg" alt="" class="page__img">
  </div>
  <!-- END page 2 -->

  <!-- BEGIN page 3 -->
  <div class="page page--img">
    <img src="img/page2-design.jpg" alt="" class="page__img">
  </div>
  <!-- END page 3 -->

  <!-- BEGIN page 4 -->
  <div class="page">
    <!-- BEGIN header -->
    <header></header>
    <!-- END header -->
    <!-- BEGIN section -->
    <div class="section">
      <h1>Прайс-лист</h1>
      <div class="note">Действует система скидок в зависимости от суммы заказа. Оптовые цены предоставляются по запросу.</div>
{$output}
    </div>
    <!-- END section -->
  </div>
  <!-- END page 3 -->

</body>
</html>

EndOfThis;

?>