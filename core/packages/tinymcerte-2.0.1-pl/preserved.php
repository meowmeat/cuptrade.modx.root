<?php return array (
  '0785dfed1f731a27735c2cc3a82741e5' => 
  array (
    'criteria' => 
    array (
      'name' => 'tinymcerte',
    ),
    'object' => 
    array (
      'name' => 'tinymcerte',
      'path' => '{core_path}components/tinymcerte/',
      'assets_path' => '{assets_path}components/tinymcerte/',
    ),
  ),
  'b46d0c40d512e83b380978f94682f2a9' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.plugins',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.plugins',
      'value' => 'advlist autolink lists charmap print preview anchor visualblocks searchreplace code fullscreen insertdatetime media table paste modxlink modximage',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  'a7b0a8dee8e8be3197b6a017a465afd4' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.menubar',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.menubar',
      'value' => 'file edit insert view format table tools',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  'ebe37e484643e633333e41467a7b8ad8' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.statusbar',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.statusbar',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  '13c804d5a370c1551de59911a0c8b0aa' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.image_advtab',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.image_advtab',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  'd7ca6f13194ab45b750a9e82871e93c9' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.object_resizing',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.object_resizing',
      'value' => '1',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  '5009d246f9651408f7786d615d4b25ff' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.paste_as_text',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.paste_as_text',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  '35bcd561550680ac4a1e003cab7693d3' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.link_class_list',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.link_class_list',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  'e8ddb422ec161a0d9564ec6aeef92f48' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.browser_spellcheck',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.browser_spellcheck',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  '6815f358c2636b01692946a352791917' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.content_css',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.content_css',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  'b6be7b873584cbe5f94b34fe62d4e51c' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.image_class_list',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.image_class_list',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  'fc76229bb53fc629556794ba0e1079f5' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.external_config',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.external_config',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  '69849e24bd63dbcf13769e03f0e56d91' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.skin',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.skin',
      'value' => 'modx',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  'bc146ad9e1e2a631863630af0ed13ff9' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.relative_urls',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.relative_urls',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  '788cf00f17a44c5e9541853607dac151' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.remove_script_host',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.remove_script_host',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  '0e2a85f973148541d5bc0914cbf61ad1' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.valid_elements',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.valid_elements',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  'ddf909193aef84716f1968c417f7553a' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.settings',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.settings',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  '30d4a64015e0b87bbe89a58fb46f7b78' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.enable_link_list',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.enable_link_list',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  'a76b8bbf96dab6a5fe6b2c5e15966abf' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.links_across_contexts',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.links_across_contexts',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.default',
      'editedon' => NULL,
    ),
  ),
  '07a06a4cd6f510a063195144689c05ed' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.toolbar1',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.toolbar1',
      'value' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.toolbar',
      'editedon' => NULL,
    ),
  ),
  '4dec64c76363076d748e131fd83c7b79' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.toolbar2',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.toolbar2',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.toolbar',
      'editedon' => NULL,
    ),
  ),
  'fce1646cfc60677212868a00a96d8abd' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.toolbar3',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.toolbar3',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.toolbar',
      'editedon' => NULL,
    ),
  ),
  '11ebbcb295e0ac2bbe19f46be9458405' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.style_formats',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.style_formats',
      'value' => '[{"title": "Headers", "items": "headers_format"},{"title": "Inline", "items": "inline_format"},{"title": "Blocks", "items": "blocks_format"},{"title": "Alignment", "items": "alignment_format"}]',
      'xtype' => 'textarea',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => NULL,
    ),
  ),
  'f3b2ec004f1b85cc59f9b372a2d8b595' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.headers_format',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.headers_format',
      'value' => '[{"title": "Header 1", "format": "h1"},{"title": "Header 2", "format": "h2"},{"title": "Header 3", "format": "h3"},{"title": "Header 4", "format": "h4"},{"title": "Header 5", "format": "h5"},{"title": "Header 6", "format": "h6"}]',
      'xtype' => 'textarea',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => NULL,
    ),
  ),
  '422f39022ed8c200e368908983a45974' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.inline_format',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.inline_format',
      'value' => '[{"title": "Bold", "icon": "bold", "format": "bold"},{"title": "Italic", "icon": "italic", "format": "italic"},{"title": "Underline", "icon": "underline", "format": "underline"},{"title": "Strikethrough", "icon": "strike-through", "format": "strikethrough"},{"title": "Superscript", "icon": "superscript", "format": "superscript"},{"title": "Subscript", "icon": "subscript", "format": "subscript"},{"title": "Code", "icon": "sourcecode", "format": "code"}]',
      'xtype' => 'textarea',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => NULL,
    ),
  ),
  '4c9514e45f8a079190117697b62ce3ab' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.blocks_format',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.blocks_format',
      'value' => '[{"title": "Paragraph", "format": "p"},{"title": "Blockquote", "format": "blockquote"},{"title": "Div", "format": "div"},{"title": "Pre", "format": "pre"}]',
      'xtype' => 'textarea',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => NULL,
    ),
  ),
  '77d33fa29cb81d933efcab796c87371d' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.alignment_format',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.alignment_format',
      'value' => '[{"title": "Left", "icon": "align-left", "format": "alignleft"},{"title": "Center", "icon": "align-center", "format": "aligncenter"},{"title": "Right", "icon": "align-right", "format": "alignright"},{"title": "Justify", "icon": "align-justify", "format": "alignjustify"}]',
      'xtype' => 'textarea',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => NULL,
    ),
  ),
  '13c64700dfe10e3f297d5d313dbbf183' => 
  array (
    'criteria' => 
    array (
      'key' => 'tinymcerte.style_formats_merge',
    ),
    'object' => 
    array (
      'key' => 'tinymcerte.style_formats_merge',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'tinymcerte',
      'area' => 'tinymcerte.style_formats',
      'editedon' => NULL,
    ),
  ),
  '22f9dab395598bddbf23f0491804c329' => 
  array (
    'criteria' => 
    array (
      'category' => 'TinyMCE Rich Text Editor',
    ),
    'object' => 
    array (
      'id' => 9,
      'parent' => 0,
      'category' => 'TinyMCE Rich Text Editor',
      'rank' => 0,
    ),
  ),
  '5a45a2465ee7d8cbc11ebd10529b44e5' => 
  array (
    'criteria' => 
    array (
      'name' => 'TinyMCE Rich Text Editor',
    ),
    'object' => 
    array (
      'id' => 10,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'TinyMCE Rich Text Editor',
      'description' => '',
      'editor_type' => 0,
      'category' => 9,
      'cache_type' => 0,
      'plugincode' => '/**
 * TinyMCE Rich Tech Editor Plugin
 *
 * @package tinymcerte
 * @subpackage pluginfile
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$className = \'TinyMCERTE\\Plugins\\Events\\\\\' . $modx->event->name;

$corePath = $modx->getOption(\'tinymcerte.core_path\', null, $modx->getOption(\'core_path\') . \'components/tinymcerte/\');
/** @var TinyMCERTE $tinymcerte */
$tinymcerte = $modx->getService(\'tinymcerte\', \'TinyMCERTE\', $corePath . \'model/tinymcerte/\', [
    \'core_path\' => $corePath
]);

if ($tinymcerte) {
    if (class_exists($className)) {
        $handler = new $className($modx, $scriptProperties);
        if (get_class($handler) == $className) {
            $handler->run();
        } else {
            $modx->log(xPDO::LOG_LEVEL_ERROR, $className. \' could not be initialized!\', \'\', \'TinyMCE RTE Plugin\');
        }
    } else {
        $modx->log(xPDO::LOG_LEVEL_ERROR, $className. \' was not found!\', \'\', \'TinyMCE RTE Plugin\');
    }
}

return;',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'disabled' => 0,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/**
 * TinyMCE Rich Tech Editor Plugin
 *
 * @package tinymcerte
 * @subpackage pluginfile
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$className = \'TinyMCERTE\\Plugins\\Events\\\\\' . $modx->event->name;

$corePath = $modx->getOption(\'tinymcerte.core_path\', null, $modx->getOption(\'core_path\') . \'components/tinymcerte/\');
/** @var TinyMCERTE $tinymcerte */
$tinymcerte = $modx->getService(\'tinymcerte\', \'TinyMCERTE\', $corePath . \'model/tinymcerte/\', [
    \'core_path\' => $corePath
]);

if ($tinymcerte) {
    if (class_exists($className)) {
        $handler = new $className($modx, $scriptProperties);
        if (get_class($handler) == $className) {
            $handler->run();
        } else {
            $modx->log(xPDO::LOG_LEVEL_ERROR, $className. \' could not be initialized!\', \'\', \'TinyMCE RTE Plugin\');
        }
    } else {
        $modx->log(xPDO::LOG_LEVEL_ERROR, $className. \' was not found!\', \'\', \'TinyMCE RTE Plugin\');
    }
}

return;',
    ),
  ),
  'ad38d85a1dc2800d947011efc2c3d113' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 10,
      'event' => 'OnManagerPageBeforeRender',
    ),
    'object' => 
    array (
      'pluginid' => 10,
      'event' => 'OnManagerPageBeforeRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '447b733b0e1b5b31aed5c95e53240107' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextEditorRegister',
    ),
    'object' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextEditorRegister',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'aa427a16bbc4326601d453b8ade5d20e' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextEditorInit',
    ),
    'object' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextEditorInit',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'd83a925956ac602120917c5a33b251c9' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextBrowserInit',
    ),
    'object' => 
    array (
      'pluginid' => 10,
      'event' => 'OnRichTextBrowserInit',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
);