{extends 'file:templates/index.tpl'}
{block 'class'}manufacture-page{/block}
{block 'main'}
  {set $about = json_decode($_modx->resource.manufacture, true)[0]}
  {set $bread = $_modx->getChunk('@FILE chunks/all/breadcrumbs.tpl')}
  {if $about.slide}
    {set $slide = json_decode($about.slide, true)[0]}
    <div class="manufacture-promo-section section" {if $slide.img}style="background:url('{$slide.img}') 50% 50% no-repeat;background-size: cover;"{/if}>
      <img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_lines_square_small_white.svg" alt="" class="manufacture-promo__shape">
      {$bread | replace:"class=\"breadcrumbs\"":"class=\"breadcrumbs breadcrumbs--white section\"" | replace:"grey":"white"}
      <div class="section-wrapper">
        <div class="manufacture-promo">
          <h1 class="title promo-title">{$slide.title}</h1>
          <div class="promo-subtitle">{$slide.desc | strip_tags:"<br><b><i><em><s><u><strong>"}</div>
        </div>
      </div>
      <div class="manufacture-promo__video-scaffold"></div>
      <video autoplay loop playsinline muted class="manufacture-promo__video-bg">
        <source src="assets/templates/cuptrade/public/assets/cuptrade/video/cuptrade_579424105.mp4" type="video/mp4">
        <source src="assets/templates/cuptrade/public/assets/cuptrade/video/cuptrade_579424105.webm" type="video/webm">
      </video>
    </div>
  {else}
    {$bread}
  {/if}
  {if $blocks = json_decode($about.block1, true)}
    <div class="manufacture-components-section section">
      <img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_lines_square_wide_red.svg" alt="" class="manufacture-components__shape">
      <div class="section-wrapper">
        {foreach $blocks as $block}
          <div class="section-info">
            <div class="title section-info__title">{$block.title}</div>
            {$block.desc}
          </div>
          {if $items = json_decode($block.items, true)}
            <div class="manufacture-components">
              {foreach $items as $item}
                <div class="manufacture-component">
                  <div class="manufacture-component__number">{$item@index+1}</div>
                  <div class="manufacture-component__title">{$item.title}</div>
                  <div class="manufacture-component__text">{$item.desc}</div>
                </div>
              {/foreach}
            </div>
          {/if}
        {/foreach}
        {if $about.btn_text}<div class="flex-centered"><button class="button button-primary js-modalCall" data-modal-called="modalOrder">{$about.btn_text}</button></div>{/if}
      </div>
    </div>
  {/if}
  {if $blocks = json_decode($about.block2, true)}
    <div class="manufacture-produces-section section">
      <div class="section-wrapper">
        {foreach $blocks as $block}
        <div class="section-info">
          <div class="title section-info__title">{$block.title}</div>
          {$block.desc}
        </div>
        {if $items = json_decode($block.items, true)}
        <div class="manufacture-produces">
          {foreach $items as $item}
          <div class="manufacture-produce">
            <img src="{$item.img}" alt="" class="manufacture-produce__ico">
            <div class="manufacture-produce__info">
              <div class="manufacture-produce__title">{$item.title}</div>
              <div class="manufacture-produce__text">{$item.desc}</div>
            </div>
          </div>
          {/foreach}
        </div>
        {/if}
        {/foreach}
      </div>
    </div>
  {/if}
  {if $blocks = json_decode($about.block3, true)}
    {set $block3}
    <div class="manufacture-processing-section section">
      <div class="section-wrapper">
        <div class="section-heading">
          <div class="section-heading__title">{$about.block3_title}</div>
        </div>
        <div class="manufacture-processings">
          {foreach $blocks as $block}
            <div class="manufacture-processing" href="{$block.big_img}" data-fancybox="manufacture" data-caption="{$block.desc}">
              <div class="manufacture-processing__title">{$block.title}</div>
              <div class="manufacture-processing__text">{$block.desc}</div>
              <img src="{$block.img}" alt="" class="manufacture-processing__bg">
            </div>
          {/foreach}
        </div>
      </div>
    </div>
    {/set}
    {$block3 | lazy:true}
  {/if}
  
  
  <div class="main-gallery-section section" id="pdopage" style="margin-top: 35px">
    <div class="section-wrapper">
      <div class="section-info">
        <div class="section-heading__title">Немного кейсов</div>
      </div>
      <div class="gallery-section">
        <div class="section-wrapper">
          <div class="gallery js-gallery-wrap">
      {'!pdoPageCustom' | snippet : [
          'frontend_css' => 0,
          'frontend_js' => 0,
          'ajaxTplMore' => '@INLINE <div class="flex-centered"><button class="button button-primary design-section__button js-more-btn">Показать ещё</button></div>',
          'ajaxElemMore' => '#pdopage .js-more-btn',
          'ajaxElemRows' => '#pdopage .js-gallery-wrap',
          'element' => 'ms2Gallery@ms2Gallery',
          'resources' => 1,
          'limit' => 8,
          'ajax' => 1,
          'ajaxMode' => 'button',
          'setMeta' => 0,
          'tplPageWrapper' => '@INLINE <div class="pagination"><ul class="pagination">{$pages}</ul></div>',
          'tpl' => '@INLINE {if $files?}
            {foreach $files as $file}
                <a href="{$file.url}" data-caption="{$file.alt|htmlent}" data-fancybox="gallery" class="gallery-preview">
                  <img src="assets/templates/cuptrade/public/assets/cuptrade/img/preloader.gif" data-src="{$file.gallery_origin}" alt="{$file.alt|htmlent}" class="gallery-preview__img lazy">
                </a>
            {/foreach}
          {/if}'
      ]}
          </div>
        </div>
      </div>
        {$_modx->getPlaceholder('page.nav')}
    </div>
  </div>
  
  
  {if $blocks = json_decode($about.block4, true)}
    {set $block4}
    <div class="manufacture-certs-section section">
      <div class="section-wrapper">
        {foreach $blocks as $block}
          <div class="section-info">
            <div class="title section-info__title">{$block.title}</div>
            {$block.desc}
          </div>
          {if $items = json_decode($block.items, true)}
            <div class="manufacture-certs">
              {foreach $items as $item}
                <div class="manufacture-cert">
                  <img src="{$item.img}" alt="{$item.title|htmlent}" class="manufacture-cert__img">
                </div>
              {/foreach}
            </div>
          {/if}
        {/foreach}
      </div>
    </div>
    {/set}
    {$block4 | lazy:true}
  {/if}
{/block}