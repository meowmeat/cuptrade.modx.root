<?php return array (
  'f160b78120ccc136907ff3acd3b85b1f' => 
  array (
    'criteria' => 
    array (
      'name' => 'colorpicker',
    ),
    'object' => 
    array (
      'name' => 'colorpicker',
      'path' => '{core_path}components/colorpicker/',
      'assets_path' => '{assets_path}components/colorpicker/',
    ),
  ),
  '9a1953448f29f217d9bd3ae8c3858c66' => 
  array (
    'criteria' => 
    array (
      'category' => 'ColorPicker',
    ),
    'object' => 
    array (
      'id' => 11,
      'parent' => 0,
      'category' => 'ColorPicker',
      'rank' => 0,
    ),
  ),
  '465ad78a88ac275333ff99479492159d' => 
  array (
    'criteria' => 
    array (
      'name' => 'ColorPicker',
    ),
    'object' => 
    array (
      'id' => 12,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'ColorPicker',
      'description' => 'ColorPicker runtime hooks - registers custom TV input & output types and includes javascripts on document edit pages.',
      'editor_type' => 0,
      'category' => 11,
      'cache_type' => 0,
      'plugincode' => '/**
 * ColorPicker Runtime Hooks
 *
 * Registers custom TV input & output types and includes javascripts on document
 * edit pages so that the TV can be used from within other extras (i.e. MIGX,
 * Collections)
 *
 * @package colorpicker
 * @subpackage plugin
 *
 * @event OnManagerPageBeforeRender
 * @event OnTVInputRenderList
 * @event OnTVOutputRenderList
 * @event OnTVInputPropertiesList
 * @event OnTVOutputRenderPropertiesList
 * @event OnDocFormRender
 *
 * @var modX $modx
 */

$eventName = $modx->event->name;

$corePath = $modx->getOption(\'colorpicker.core_path\', null, $modx->getOption(\'core_path\') . \'components/colorpicker/\');
/** @var ColorPicker $colorpicker */
$colorpicker = $modx->getService(\'colorpicker\', \'ColorPicker\', $corePath . \'model/colorpicker/\', array(
    \'core_path\' => $corePath
));

switch ($eventName) {
    case \'OnManagerPageBeforeRender\':
        $modx->controller->addLexiconTopic(\'colorpicker:default\');
        $colorpicker->includeScriptAssets();
        break;
    case \'OnTVInputRenderList\':
        $modx->event->output($corePath . \'elements/tv/input/\');
        break;
    case \'OnTVOutputRenderList\':
        $modx->event->output($corePath . \'elements/tv/output/\');
        break;
    case \'OnTVInputPropertiesList\':
        $modx->event->output($corePath . \'elements/tv/input/options/\');
        break;
    case \'OnTVOutputRenderPropertiesList\':
        $modx->event->output($corePath . \'elements/tv/output/options/\');
        break;
    case \'OnDocFormRender\':
        $colorpicker->includeScriptAssets();
        break;
};',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'disabled' => 1,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/**
 * ColorPicker Runtime Hooks
 *
 * Registers custom TV input & output types and includes javascripts on document
 * edit pages so that the TV can be used from within other extras (i.e. MIGX,
 * Collections)
 *
 * @package colorpicker
 * @subpackage plugin
 *
 * @event OnManagerPageBeforeRender
 * @event OnTVInputRenderList
 * @event OnTVOutputRenderList
 * @event OnTVInputPropertiesList
 * @event OnTVOutputRenderPropertiesList
 * @event OnDocFormRender
 *
 * @var modX $modx
 */

$eventName = $modx->event->name;

$corePath = $modx->getOption(\'colorpicker.core_path\', null, $modx->getOption(\'core_path\') . \'components/colorpicker/\');
/** @var ColorPicker $colorpicker */
$colorpicker = $modx->getService(\'colorpicker\', \'ColorPicker\', $corePath . \'model/colorpicker/\', array(
    \'core_path\' => $corePath
));

switch ($eventName) {
    case \'OnManagerPageBeforeRender\':
        $modx->controller->addLexiconTopic(\'colorpicker:default\');
        $colorpicker->includeScriptAssets();
        break;
    case \'OnTVInputRenderList\':
        $modx->event->output($corePath . \'elements/tv/input/\');
        break;
    case \'OnTVOutputRenderList\':
        $modx->event->output($corePath . \'elements/tv/output/\');
        break;
    case \'OnTVInputPropertiesList\':
        $modx->event->output($corePath . \'elements/tv/input/options/\');
        break;
    case \'OnTVOutputRenderPropertiesList\':
        $modx->event->output($corePath . \'elements/tv/output/options/\');
        break;
    case \'OnDocFormRender\':
        $colorpicker->includeScriptAssets();
        break;
};',
    ),
  ),
  '8fb86134fcf5c8b7905d2dfca36f82b6' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 12,
      'event' => 'OnManagerPageBeforeRender',
    ),
    'object' => 
    array (
      'pluginid' => 12,
      'event' => 'OnManagerPageBeforeRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'ae640355c88ae6e0f4bf5c41284e5fbf' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 12,
      'event' => 'OnTVInputPropertiesList',
    ),
    'object' => 
    array (
      'pluginid' => 12,
      'event' => 'OnTVInputPropertiesList',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '3cf52b0c0773ae706d3a0885ac4f9978' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 12,
      'event' => 'OnTVInputRenderList',
    ),
    'object' => 
    array (
      'pluginid' => 12,
      'event' => 'OnTVInputRenderList',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '68bf8a1f95105ed081999aeef40d050e' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 12,
      'event' => 'OnTVOutputRenderList',
    ),
    'object' => 
    array (
      'pluginid' => 12,
      'event' => 'OnTVOutputRenderList',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '95f4b04f04873643323a7ffac2a03c7a' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 12,
      'event' => 'OnTVOutputRenderPropertiesList',
    ),
    'object' => 
    array (
      'pluginid' => 12,
      'event' => 'OnTVOutputRenderPropertiesList',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'c64a2b838745465cf103b4a2bb6545ca' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 12,
      'event' => 'OnDocFormRender',
    ),
    'object' => 
    array (
      'pluginid' => 12,
      'event' => 'OnDocFormRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
);