{extends 'file:templates/index.tpl'}
{block 'class'}content-page{/block}
{block 'main'}
  <div class="content-section section">
    {$_modx->getChunk('@FILE chunks/all/breadcrumbs.tpl')}

{set $parent_ids = $_modx->getParentIds($_modx->resource.id)}
{set $parent_menu = (11 in $parent_ids)?11:2}
{if !$_modx->getChildIds($_modx->resource.id) && !$_modx->resource.isfolder}
  {set $menu_new = 'pdoCrumbs'|snippet:[
  'from' => $parent_menu,
  'exclude' => $parent_menu,
  'fastMode' => 1,
  'tplWrapper' => '@INLINE {$output}',
  'wrapIfEmpty' => 0,
  'showHome' => 0,
  'showCurrent' => 1,
  'tplHome' => '@INLINE ',
  'tpl' => '@INLINE <a href="{$link}" class="sidebar-section"><svg width="7" height="11" viewBox="0 0 8 12" fill="#989798" xmlns="http://www.w3.org/2000/svg" class="ico-svg sidebar-category-ico">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M6.079 11.693l1.442-1.386L3.386 6l4.135-4.307L6.08.307.614 6l5.465 5.693z" />
                </svg>{$menutitle}</a>',
  'tplCurrent' => '@INLINE <span class="sidebar-list__item sidebar-list__item--active">{$menutitle}</span>
  {if $child_res = \'pdoResources\' | snippet:[
  \'where\' => \'{"hidemenu":false}\',
  \'tplWrapper\' => \'@INLINE {$output}\',
  \'tpl\' => \'@INLINE <a href="{$uri}" class="sidebar-list__item">{$menutitle?:$pagetitle}</a>\',
  \'parents\' => $_modx->resource.id,
  \'limit\' => 0,
  \'sortby\' => \'menuindex\',
  \'depth\' => 0,
  \'sortdir\' => \'ASC\'
  ]}
  {$child_res}
  {else}
  {\'pdoResources\' | snippet:[
  \'where\' => \'{"hidemenu":false}\',
  \'resources\' => \'-\'~$_modx->resource.id,
  \'tplWrapper\' => \'@INLINE {$output}<div class="sidebar-list__more js-sidebarList_More"><span class="sidebar-list__more-text">Ещё</span> <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_down.svg" class="sidebar-list__more-ico"></div>\',
  \'tpl\' => \'@INLINE <a href="{$uri}" class="sidebar-list__item">{$menutitle?:$pagetitle}</a>\',
  \'parents\' => $_modx->resource.parent?:2,
  \'limit\' => 0,
  \'sortby\' => \'menuindex\',
  \'depth\' => 0,
  \'sortdir\' => \'ASC\'
  ]}
  {/if}
  '
  ]}
{else}
  {set $menu_new = 'pdoMenu' | snippet : [
      'showLog' => 0,
      'templates' => '9',
      'parents' => $_modx->resource.parent,
      'level' => 2,
      'hereClass' => '_open',
      'selfClass' => 'self',
      'tplOuter' => '@INLINE {$wrapper}',
      'tpl' => '@INLINE <a href="{$link}" class="sidebar-section">{$menutitle}</a>{$wrapper}',
      'tplInner' => '@INLINE {$wrapper}',
      'tplInnerRow' => '@INLINE <a href="{$link}" class="sidebar-list__item">{$menutitle}</a>{if $last}<div class="sidebar-list__more js-sidebarList_More"><span class="sidebar-list__more-text">Ещё</span> <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_down.svg" class="sidebar-list__more-ico"></div>{/if}{$wrapper}'
  ]}
{/if}

    <div class="section-wrapper">
      <div class="section-twin">
        <div class="section-twin__side">
          <div class="sidebar">
            <div class="sidebar-list">
              {$menu_new}
              {*<a href="" class="sidebar-section">Материалы и компоненты</a>
              <a href="" class="sidebar-section">Одноразовая посуда</a>
              <a href="" class="sidebar-section">Упаковка</a>
              <a href="" class="sidebar-section">Сертифицированный пластик</a>*}
            </div>
          </div>

          <div class="sidebar-mob-section">
            <div class="sidebar-mob-select js-sidebarMob_Call">
              <span class="sidebar-mob-select__text">{$_modx->resource.menutitle?:$_modx->resource.pagetitle}</span>
              <svg width="16" height="9" viewBox="0 0 12 8" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg sidebar-mob-select__ico">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M.04 1.989L1.425.546l4.307 4.135L10.04.546l1.385 1.443-5.693 5.465L.04 1.989z" />
              </svg>
            </div>
            <div class="sidebar-mob">
              <div class="sidebar-mob__items">
                {$menu_new | replace:"sidebar-section":"sidebar-mob__item" | replace:"sidebar-list__item":"sidebar-mob__item"}
                {*<a href="" class="sidebar-mob__item">Материалы и компоненты</a>
                <a href="" class="sidebar-mob__item">Одноразовая посуда</a>
                <a href="" class="sidebar-mob__item">Упаковка</a>
                <a href="" class="sidebar-mob__item">Сертифицированный пластик</a>*}
              </div>
            </div>
          </div>
        </div>

        <div class="section-twin__main">
          <div class="content">
            <h1>{$_modx->resource.pagetitle}</h1>
            {$_modx->resource.content | lazy:true | getgallery}
          </div> <!-- Content -->
        </div> <!-- end section-twin-main -->

      </div> <!-- end section-twin -->
    </div>
  </div>
{/block}