<?php return array (
  '16f264377cd27fe7ef17c46834496c77' => 
  array (
    'criteria' => 
    array (
      'name' => 'frontendmanager',
    ),
    'object' => 
    array (
      'name' => 'frontendmanager',
      'path' => '{core_path}components/frontendmanager/',
      'assets_path' => '',
    ),
  ),
  '7570b748eae2cccaa0999cc97c88dcf4' => 
  array (
    'criteria' => 
    array (
      'key' => 'frontendmanager_frontend_css',
    ),
    'object' => 
    array (
      'key' => 'frontendmanager_frontend_css',
      'value' => 'myfront.css',
      'xtype' => 'textfield',
      'namespace' => 'frontendmanager',
      'area' => 'frontendmanager_frontend',
      'editedon' => '2021-07-21 19:18:11',
    ),
  ),
  'd02226091705063c272bdb949199a94d' => 
  array (
    'criteria' => 
    array (
      'key' => 'frontendmanager_frontend_js',
    ),
    'object' => 
    array (
      'key' => 'frontendmanager_frontend_js',
      'value' => 'frontend.js',
      'xtype' => 'textfield',
      'namespace' => 'frontendmanager',
      'area' => 'frontendmanager_frontend',
      'editedon' => NULL,
    ),
  ),
  '1d522c488180c6671a1e878c6b6d785f' => 
  array (
    'criteria' => 
    array (
      'key' => 'frontendmanager_frontend_tpl',
    ),
    'object' => 
    array (
      'key' => 'frontendmanager_frontend_tpl',
      'value' => 'tpl.frontendmanager',
      'xtype' => 'textfield',
      'namespace' => 'frontendmanager',
      'area' => 'frontendmanager_frontend',
      'editedon' => NULL,
    ),
  ),
  '9e9c0b02815e28d7f55b7b208016b4b9' => 
  array (
    'criteria' => 
    array (
      'key' => 'frontendmanager_contenttypes',
    ),
    'object' => 
    array (
      'key' => 'frontendmanager_contenttypes',
      'value' => '1',
      'xtype' => 'textfield',
      'namespace' => 'frontendmanager',
      'area' => 'frontendmanager_manager',
      'editedon' => NULL,
    ),
  ),
  '0390cd2c3237058bfee5c5ebe1b077e8' => 
  array (
    'criteria' => 
    array (
      'key' => 'frontendmanager_manager_css',
    ),
    'object' => 
    array (
      'key' => 'frontendmanager_manager_css',
      'value' => 'manager.css',
      'xtype' => 'textfield',
      'namespace' => 'frontendmanager',
      'area' => 'frontendmanager_manager',
      'editedon' => NULL,
    ),
  ),
  'a8891e6905ec9ef032d6bd0c4212fc77' => 
  array (
    'criteria' => 
    array (
      'key' => 'frontendmanager_manager_js',
    ),
    'object' => 
    array (
      'key' => 'frontendmanager_manager_js',
      'value' => 'manager.js',
      'xtype' => 'textfield',
      'namespace' => 'frontendmanager',
      'area' => 'frontendmanager_manager',
      'editedon' => NULL,
    ),
  ),
  'b7f17b139c7c670020a70a7e6304def9' => 
  array (
    'criteria' => 
    array (
      'category' => 'frontendManager',
    ),
    'object' => 
    array (
      'id' => 18,
      'parent' => 0,
      'category' => 'frontendManager',
      'rank' => 0,
    ),
  ),
  '49562e3d6eceb71b0e8d097c15ff7051' => 
  array (
    'criteria' => 
    array (
      'name' => 'tpl.frontendmanager',
    ),
    'object' => 
    array (
      'id' => 52,
      'source' => 1,
      'property_preprocess' => 0,
      'name' => 'tpl.frontendmanager',
      'description' => '',
      'editor_type' => 0,
      'category' => 18,
      'cache_type' => 0,
      'snippet' => '<div id="frontendManager" class="fm-panel" >
	<a href="[[++manager_url]]?a=resource/update&id={$_modx->resource.id}" target="_blank" class="fm-logo"><img src="[[++manager_url]]templates/default/images/modx-icon-color.svg"></a>
	<a href="[[++manager_url]]" target="_blank" class="fm-mode"><span class="fm-icon-hide"><img src="[[++manager_url]]templates/default/images/modx-icon-color.svg"></span></a>
	<a href="[[++manager_url]]?a=resource/update&id={$_modx->resource.id}" data-action="iframe"><span class="fm-icon-edit"></span> <span class="fm-link-text">{\'frontendmanager_btn_edit\' | lexicon}</span></a>
	<a href="[[++manager_url]]?a=security/user" data-action="iframe"><span class="fm-icon-user"></span> <span class="fm-link-text">{\'frontendmanager_btn_users\' | lexicon}</span></a>
	<a href="[[++manager_url]]?a=mgr/orders&namespace=minishop2" data-action="iframe"><span class="fm-icon-ms2"></span> <span class="fm-link-text">{\'frontendmanager_btn_ms2\' | lexicon}</span></a>
	<a href="[[++manager_url]]?id=0&a=context/update&key={$_modx->context.key}" data-action="iframe"><span class="fm-icon-context"></span> <span class="fm-link-text">{\'frontendmanager_btn_context\' | lexicon}</span></a>
	<a href="[[++manager_url]]?a=system/settings" data-action="iframe"><span class="fm-icon-settings"></span> <span class="fm-link-text">{\'frontendmanager_btn_settings\' | lexicon}</span></a>
	<a href="[[++manager_url]]?a=system/event" data-action="iframe"><span class="fm-icon-log"></span><span class="fm-link-text">{\'frontendmanager_btn_log\' | lexicon}</span></a>
	<a href="[[++manager_url]]?a=system/refresh_site" data-action="iframe"><span class="fm-icon-cache"></span><span class="fm-link-text">{\'frontendmanager_btn_cache\' | lexicon}</span></a>
</div>
',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => 'core/components/frontendmanager/elements/chunks/chunk.frontendmanager.tpl',
      'content' => '<div id="frontendManager" class="fm-panel" >
	<a href="[[++manager_url]]?a=resource/update&id={$_modx->resource.id}" target="_blank" class="fm-logo"><img src="[[++manager_url]]templates/default/images/modx-icon-color.svg"></a>
	<a href="[[++manager_url]]" target="_blank" class="fm-mode"><span class="fm-icon-hide"><img src="[[++manager_url]]templates/default/images/modx-icon-color.svg"></span></a>
	<a href="[[++manager_url]]?a=resource/update&id={$_modx->resource.id}" data-action="iframe"><span class="fm-icon-edit"></span> <span class="fm-link-text">{\'frontendmanager_btn_edit\' | lexicon}</span></a>
	<a href="[[++manager_url]]?a=security/user" data-action="iframe"><span class="fm-icon-user"></span> <span class="fm-link-text">{\'frontendmanager_btn_users\' | lexicon}</span></a>
	<a href="[[++manager_url]]?a=mgr/orders&namespace=minishop2" data-action="iframe"><span class="fm-icon-ms2"></span> <span class="fm-link-text">{\'frontendmanager_btn_ms2\' | lexicon}</span></a>
	<a href="[[++manager_url]]?id=0&a=context/update&key={$_modx->context.key}" data-action="iframe"><span class="fm-icon-context"></span> <span class="fm-link-text">{\'frontendmanager_btn_context\' | lexicon}</span></a>
	<a href="[[++manager_url]]?a=system/settings" data-action="iframe"><span class="fm-icon-settings"></span> <span class="fm-link-text">{\'frontendmanager_btn_settings\' | lexicon}</span></a>
	<a href="[[++manager_url]]?a=system/event" data-action="iframe"><span class="fm-icon-log"></span><span class="fm-link-text">{\'frontendmanager_btn_log\' | lexicon}</span></a>
	<a href="[[++manager_url]]?a=system/refresh_site" data-action="iframe"><span class="fm-icon-cache"></span><span class="fm-link-text">{\'frontendmanager_btn_cache\' | lexicon}</span></a>
</div>
',
    ),
  ),
  '5922507cc40774b2c98b49da3dccf5d4' => 
  array (
    'criteria' => 
    array (
      'name' => 'frontendmanager',
    ),
    'object' => 
    array (
      'id' => 19,
      'source' => 1,
      'property_preprocess' => 0,
      'name' => 'frontendmanager',
      'description' => '',
      'editor_type' => 0,
      'category' => 18,
      'cache_type' => 0,
      'plugincode' => 'if (!$modx->user->hasSessionContext(\'mgr\') || !$modx->user->isMember(\'Administrator\')) return;
switch ($modx->event->name) {
    case \'OnWebPagePrerender\':
        $frontendManager = $modx->getService(\'frontendmanager\',\'frontendManager\', MODX_CORE_PATH . \'components/frontendmanager/model/frontendmanager/\', array());
        if(!$frontendManager) return;
		$contentTypes = explode(\',\', $modx->getOption(\'frontendmanager_contenttypes\'));
        if (in_array($modx->resource->content_type, $contentTypes)) {
			$modx->resource->_output .=  $frontendManager->initialize($modx->context->key);
        }
        break;
    case \'OnBeforeManagerPageInit\':
        if ($_GET[\'frame\']) {
			$modx->regClientCSS(MODX_ASSETS_URL.\'components/frontendmanager/css/mgr/\'.$modx->getOption(\'frontendmanager_manager_css\', NULL, \'manager.css\'));
			$modx->regClientStartupScript(MODX_ASSETS_URL.\'components/frontendmanager/js/mgr/\'.$modx->getOption(\'frontendmanager_manager_js\', NULL, \'manager.js\'));
        }
        break;
    default:
        break;
}
return;',
      'locked' => 0,
      'properties' => NULL,
      'disabled' => 0,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => 'core/components/frontendmanager/elements/plugins/plugin.frontendmanager.php',
      'content' => 'if (!$modx->user->hasSessionContext(\'mgr\') || !$modx->user->isMember(\'Administrator\')) return;
switch ($modx->event->name) {
    case \'OnWebPagePrerender\':
        $frontendManager = $modx->getService(\'frontendmanager\',\'frontendManager\', MODX_CORE_PATH . \'components/frontendmanager/model/frontendmanager/\', array());
        if(!$frontendManager) return;
		$contentTypes = explode(\',\', $modx->getOption(\'frontendmanager_contenttypes\'));
        if (in_array($modx->resource->content_type, $contentTypes)) {
			$modx->resource->_output .=  $frontendManager->initialize($modx->context->key);
        }
        break;
    case \'OnBeforeManagerPageInit\':
        if ($_GET[\'frame\']) {
			$modx->regClientCSS(MODX_ASSETS_URL.\'components/frontendmanager/css/mgr/\'.$modx->getOption(\'frontendmanager_manager_css\', NULL, \'manager.css\'));
			$modx->regClientStartupScript(MODX_ASSETS_URL.\'components/frontendmanager/js/mgr/\'.$modx->getOption(\'frontendmanager_manager_js\', NULL, \'manager.js\'));
        }
        break;
    default:
        break;
}
return;',
    ),
  ),
  '912a9efc78e8106d46d9c2381a4fef36' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnWebPagePrerender',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnWebPagePrerender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'f567dbb8f998f28c40f6251895ef6931' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnBeforeManagerPageInit',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnBeforeManagerPageInit',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
);