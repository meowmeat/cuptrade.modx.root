{extends 'file:templates/index.tpl'}
{block 'class'}catalog-page{/block}
{block 'main'}
  <div class="catalog-section section">
    <div class="section-wrapper">
      <div class="catalog-main">
        <div class="section-heading">
          <h1 class="section-heading__title">{$_modx->resource.pagetitle}</h1>
        </div>
{set $catalog_block}
        <div class="catalog-navs">
          {'pdoResources' | snippet:[
          'where' => '{"hidemenu":false}',
          'tplWrapper' => '@INLINE {$output}',
          'includeTVs' => 'category',
          'tpl' => '@INLINE {if $_pls[\'tv.category\']}{set $cat = $_pls[\'tv.category\'][0]}{/if}
                            <a href="{$uri}" title="{($cat.title?:$pagetitle)|htmlent}" class="catalog-nav {$cat.color?:\'catalog-nav-singlecup\'}">
                              <h2 class="catalog-nav__title">{$cat.title?:$pagetitle}</h2>
                              <img src="{$cat.img_catalog?:$cat.img_menu}" alt="{($cat.title?:$pagetitle)|htmlent}" class="catalog-nav__image">
                            </a>',
          'parents' => $_modx->resource.id,
          'limit' => 0,
          'sortby' => 'menuindex',
          'depth' => 0,
          'sortdir' => 'ASC'
          ]}
          {if $adds = json_decode($_modx->resource.add_card, true)}
            {foreach $adds as $add}
              {if $add.link}
                <a href="{$add.link | url}" title="{set $title = ($add.title?:($add.link|resource:"pagetitle"))}{$title|htmlent}" class="catalog-nav {$add.color?:'catalog-nav-singlecup'}">
                  <h2 class="catalog-nav__title">{$title}</h2>
                  <img src="{$add.img?:'assets/templates/cuptrade/public/assets/cuptrade/img/logo-cuptrade.svg'}" alt="{$title|htmlent}" class="catalog-nav__image">
                </a>
              {/if}
            {/foreach}
          {/if}
        </div>
{/set}
{$catalog_block | lazy:true}
      {'!msProductsCustom' | snippet : [
          'wrapIfEmpty' => 0,
          'parents' => 2,
          'limit' => 8,
          'includeContent' => 1,
          'tplWrapper' => '@INLINE <div class="catalog-items">{$output}</div> <!-- end catalog-items -->',
          'tpl' => '@FILE chunks/all/tpl.goods_card.tpl'
      ]}
      </div>
    </div>
  </div>
  {if $content = $_modx->resource.content}
  <div class="catalog-info-section section">
    <div class="section-wrapper">
      <div class="">
        {$content | lazy:true}
      </div>
    </div>
  </div>
  {/if}
{/block}