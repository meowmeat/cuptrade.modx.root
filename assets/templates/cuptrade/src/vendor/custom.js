var miniShop2Config = miniShop2Config || {};
(function (window, document, $, miniShop2Config) {
    var miniShop2 = miniShop2 || {};
    miniShop2Config.callbacksObjectTemplate = function () {
        return {
            // return false to prevent send data
            before: [],
            response: {
                success: [],
                error: []
            },
            ajax: {
                done: [],
                fail: [],
                always: []
            }
        }
    };
    miniShop2.Callbacks = miniShop2Config.Callbacks = {
        Cart: {
            add: miniShop2Config.callbacksObjectTemplate(),
            remove: miniShop2Config.callbacksObjectTemplate(),
            change: miniShop2Config.callbacksObjectTemplate(),
            clean: miniShop2Config.callbacksObjectTemplate()
        },
        Order: {
            add: miniShop2Config.callbacksObjectTemplate(),
            getcost: miniShop2Config.callbacksObjectTemplate(),
            clean: miniShop2Config.callbacksObjectTemplate(),
            submit: miniShop2Config.callbacksObjectTemplate(),
            getrequired: miniShop2Config.callbacksObjectTemplate()
        },
    };
    miniShop2.Callbacks.add = function (path, name, func) {
        if (typeof func != 'function') {
            return false;
        }
        path = path.split('.');
        var obj = miniShop2.Callbacks;
        for (var i = 0; i < path.length; i++) {
            if (obj[path[i]] == undefined) {
                return false;
            }
            obj = obj[path[i]];
        }
        if (typeof obj != 'object') {
            obj = [obj];
        }
        if (name != undefined) {
            obj[name] = func;
        }
        else {
            obj.push(func);
        }
        return true;
    };
    miniShop2.Callbacks.remove = function (path, name) {
        path = path.split('.');
        var obj = miniShop2.Callbacks;
        for (var i = 0; i < path.length; i++) {
            if (obj[path[i]] == undefined) {
                return false;
            }
            obj = obj[path[i]];
        }
        if (obj[name] != undefined) {
            delete obj[name];
            return true;
        }
        return false;
    };
    miniShop2.ajaxProgress = false;
    miniShop2.setup = function () {
        // selectors & $objects
        this.actionName = 'ms2_action';
        this.action = ':submit[name=' + this.actionName + ']';
        this.form = '.ms2_form';
        this.$doc = $(document);

        this.sendData = {
            $form: null,
            action: null,
            formData: null
        };

        this.timeout = 300;
    };
    miniShop2.initialize = function () {
        miniShop2.setup();
        // Indicator of active ajax request

        //noinspection JSUnresolvedFunction
        miniShop2.$doc
            .ajaxStart(function () {
                miniShop2.ajaxProgress = true;
            })
            .ajaxStop(function () {
                miniShop2.ajaxProgress = false;
            })
            .on('submit', miniShop2.form, function (e) {
                e.preventDefault();
                var $form = $(this);
                var action = $form.find(miniShop2.action).val();

                if (action) {
                    var formData = $form.serializeArray();
                    formData.push({
                        name: miniShop2.actionName,
                        value: action
                    });
                    miniShop2.sendData = {
                        $form: $form,
                        action: action,
                        formData: formData
                    };
                    miniShop2.controller();
                }
            });
        miniShop2.Cart.initialize();
        miniShop2.Message.initialize();
        miniShop2.Order.initialize();
    };
    miniShop2.controller = function () {
        var self = this;
        switch (self.sendData.action) {
            case 'cart/add':
                miniShop2.Cart.add();
                break;
            case 'cart/remove':
                miniShop2.Cart.remove();
                break;
            case 'cart/change':
                miniShop2.Cart.change();
                break;
            case 'cart/clean':
                miniShop2.Cart.clean();
                break;
            case 'order/submit':
                miniShop2.Order.submit();
                break;
            case 'order/clean':
                miniShop2.Order.clean();
                break;
            default:
                return;
        }
    };
    miniShop2.send = function (data, callbacks, userCallbacks) {
        var runCallback = function (callback, bind) {
            if (typeof callback == 'function') {
                return callback.apply(bind, Array.prototype.slice.call(arguments, 2));
            }
            else if (typeof callback == 'object') {
                for (var i in callback) {
                    if (callback.hasOwnProperty(i)) {
                        var response = callback[i].apply(bind, Array.prototype.slice.call(arguments, 2));
                        if (response === false) {
                            return false;
                        }
                    }
                }
            }
            return true;
        };
        // set context
        if ($.isArray(data)) {
            data.push({
                name: 'ctx',
                value: miniShop2Config.ctx
            });
        }
        else if ($.isPlainObject(data)) {
            data.ctx = miniShop2Config.ctx;
        }
        else if (typeof data == 'string') {
            data += '&ctx=' + miniShop2Config.ctx;
        }

        // set action url
        var formActionUrl = (miniShop2.sendData.$form)
            ? miniShop2.sendData.$form.attr('action')
            : false;
        var url = (formActionUrl)
            ? formActionUrl
            : (miniShop2Config.actionUrl)
                      ? miniShop2Config.actionUrl
                      : document.location.href;
        // set request method
        var formMethod = (miniShop2.sendData.$form)
            ? miniShop2.sendData.$form.attr('method')
            : false;
        var method = (formMethod)
            ? formMethod
            : 'post';

        // callback before
        if (runCallback(callbacks.before) === false || runCallback(userCallbacks.before) === false) {
            return;
        }
        // send
        var xhr = function (callbacks, userCallbacks) {
            return $[method](url, data, function (response) {
                if (response.success) {
                    if (response.message) {
                        miniShop2.Message.success(response.message);
                    }
                    runCallback(callbacks.response.success, miniShop2, response);
                    runCallback(userCallbacks.response.success, miniShop2, response);
                }
                else {
                    miniShop2.Message.error(response.message);
                    runCallback(callbacks.response.error, miniShop2, response);
                    runCallback(userCallbacks.response.error, miniShop2, response);
                }
            }, 'json').done(function () {
                runCallback(callbacks.ajax.done, miniShop2, xhr);
                runCallback(userCallbacks.ajax.done, miniShop2, xhr);
            }).fail(function () {
                runCallback(callbacks.ajax.fail, miniShop2, xhr);
                runCallback(userCallbacks.ajax.fail, miniShop2, xhr);
            }).always(function () {
                runCallback(callbacks.ajax.always, miniShop2, xhr);
                runCallback(userCallbacks.ajax.always, miniShop2, xhr);
            });
        }(callbacks, userCallbacks);
    };

    miniShop2.Cart = {
        callbacks: {
            add: miniShop2Config.callbacksObjectTemplate(),
            remove: miniShop2Config.callbacksObjectTemplate(),
            change: miniShop2Config.callbacksObjectTemplate(),
            clean: miniShop2Config.callbacksObjectTemplate()
        },
        setup: function () {
            miniShop2.Cart.cart = '#msCart';
            miniShop2.Cart.miniCart = '#msMiniCart';
            miniShop2.Cart.miniCartNotEmptyClass = 'full';
            miniShop2.Cart.countInput = 'input[name=count]';
            miniShop2.Cart.totalWeight = '.ms2_total_weight';
            miniShop2.Cart.totalCount = '.ms2_total_count';
            miniShop2.Cart.totalCost = '.ms2_total_cost';
        },
        initialize: function () {
            miniShop2.Cart.setup();
            if (!$(miniShop2.Cart.cart).length) {
                return;
            }
            miniShop2.$doc.on('change', miniShop2.Cart.cart + ' ' + miniShop2.Cart.countInput, function () {
                if (!!$(this).val()) {
                    $(this).closest(miniShop2.form).submit();
                }
            });
        },
        add: function () {
            var callbacks = miniShop2.Cart.callbacks;
            callbacks.add.response.success = function (response) {
                this.Cart.status(response.data);
            };
            miniShop2.send(miniShop2.sendData.formData, miniShop2.Cart.callbacks.add, miniShop2.Callbacks.Cart.add);
        },
        remove: function () {
            var callbacks = miniShop2.Cart.callbacks;
            callbacks.remove.response.success = function (response) {
                this.Cart.remove_position(miniShop2.Utils.getValueFromSerializedArray('key'));
                this.Cart.status(response.data);
            };
            miniShop2.send(miniShop2.sendData.formData, miniShop2.Cart.callbacks.remove, miniShop2.Callbacks.Cart.remove);
        },
        change: function () {
            var callbacks = miniShop2.Cart.callbacks;
            callbacks.change.response.success = function (response) {
                if (typeof(response.data.key) == 'undefined') {
                    this.Cart.remove_position(miniShop2.Utils.getValueFromSerializedArray('key'));
                }
                else {
                    $('#' + miniShop2.Utils.getValueFromSerializedArray('key')).find('');
                }
                this.Cart.status(response.data);
            };
            miniShop2.send(miniShop2.sendData.formData, miniShop2.Cart.callbacks.change, miniShop2.Callbacks.Cart.change);
        },
        status: function (status) {
            if (status['total_count'] < 1) {
                location.reload();
            }
            else {
                //var $cart = $(miniShop2.Cart.cart);
                var $miniCart = $(miniShop2.Cart.miniCart);
                if (status['total_count'] > 0 && !$miniCart.hasClass(miniShop2.Cart.miniCartNotEmptyClass)) {
                    $miniCart.addClass(miniShop2.Cart.miniCartNotEmptyClass);
                }
                $(miniShop2.Cart.totalWeight).text(miniShop2.Utils.formatWeight(status['total_weight']));
                $(miniShop2.Cart.totalCount).text(status['total_count']);
                $(miniShop2.Cart.totalCost).text(miniShop2.Utils.formatPrice(status['total_cost']));
                if ($(miniShop2.Order.orderCost, miniShop2.Order.order).length) {
                    miniShop2.Order.getcost();
                }
            }
        },
        clean: function () {
            var callbacks = miniShop2.Cart.callbacks;
            callbacks.clean.response.success = function (response) {
                this.Cart.status(response.data);
            };

            miniShop2.send(miniShop2.sendData.formData, miniShop2.Cart.callbacks.clean, miniShop2.Callbacks.Cart.clean);
        },
        remove_position: function (key) {
            $('#' + key).remove();
        }
    };



    miniShop2.Order = {
        callbacks: {
            add: miniShop2Config.callbacksObjectTemplate(),
            getcost: miniShop2Config.callbacksObjectTemplate(),
            clean: miniShop2Config.callbacksObjectTemplate(),
            submit: miniShop2Config.callbacksObjectTemplate(),
            getrequired: miniShop2Config.callbacksObjectTemplate()
        },
        setup: function () {
            miniShop2.Order.order = '#checkout__final';
            miniShop2.Order.deliveries = '#deliveries';
            miniShop2.Order.payments = '#payments';
            miniShop2.Order.deliveryInput = 'input[name="delivery"]';
            miniShop2.Order.inputParent = '.input-parent';
            miniShop2.Order.paymentInput = 'input[name="payment"]';
            miniShop2.Order.paymentInputUniquePrefix = 'input#payment_';
            miniShop2.Order.deliveryInputUniquePrefix = 'input#delivery_';
            miniShop2.Order.orderCost = '#ms2_order_cost'
        },
        initialize: function () {
            miniShop2.Order.setup();
            if ($(miniShop2.Order.order).length) {
                miniShop2.$doc
                    .on('click', miniShop2.Order.order + ' [name="' + miniShop2.actionName + '"][value="order/clean"]', function (e) {
                        miniShop2.Order.clean();
                        e.preventDefault();
                    })
                    .on('change', miniShop2.Order.order + ' input,' + miniShop2.Order.order + ' textarea', function () {
                        var $this = $(this);
                        var key = $this.attr('name');
                        var value = $this.val();
                        miniShop2.Order.add(key, value);
                    });
                var $deliveryInputChecked = $(miniShop2.Order.deliveryInput + ':checked', miniShop2.Order.order);
                $deliveryInputChecked.trigger('change');
            }
        },
        updatePayments: function (payments) {
            var $paymentInputs = $(miniShop2.Order.paymentInput, miniShop2.Order.order);
            $paymentInputs.attr('disabled', true).prop('disabled', true).closest(miniShop2.Order.inputParent).hide();
            if (payments.length > 0) {
                for (var i in payments) {
                    if (payments.hasOwnProperty(i)) {
                        $paymentInputs.filter(miniShop2.Order.paymentInputUniquePrefix + payments[i]).attr('disabled', false).prop('disabled', false).closest(miniShop2.Order.inputParent).show();
                    }
                }
            }
            if ($paymentInputs.filter(':visible:checked').length == 0) {
                $paymentInputs.filter(':visible:first').trigger('click');
            }
        },
        add: function (key, value) {
            var callbacks = miniShop2.Order.callbacks;
            var old_value = value;
            callbacks.add.response.success = function (response) {
                (function (key, value, old_value) {
                    var $field = $('[name="' + key + '"]', miniShop2.Order.order);
                    switch (key) {
                        case 'delivery':
                            $field = $(miniShop2.Order.deliveryInputUniquePrefix + response.data[key]);
                            if (response.data[key] != old_value) {
                                $field.trigger('click');
                            }
                            else {
                                miniShop2.Order.getrequired(value);
                                miniShop2.Order.updatePayments($field.data('payments'));
                                miniShop2.Order.getcost();
                            }
                            break;
                        case 'payment':
                            $field = $(miniShop2.Order.paymentInputUniquePrefix + response.data[key]);
                            if (response.data[key] != old_value) {
                                $field.trigger('click');
                            }
                            else {
                                miniShop2.Order.getcost();
                            }
                            break;
                        case 'address':
                        case 'phone':
                          return true;
                          break;
                        //default:
                    }
                    $field.val(response.data[key]).removeClass('error').closest(miniShop2.Order.inputParent).removeClass('error');
                })(key, value, old_value);
            };
            callbacks.add.response.error = function () {
                (function (key) {
                    var $field = $('[name="' + key + '"]', miniShop2.Order.order);
                    if ($field.attr('type') == 'checkbox' || $field.attr('type') == 'radio') {
                        $field.closest(miniShop2.Order.inputParent).addClass('error');
                    }
                    else {
                        $field.addClass('error');
                    }
                })(key);
            };

            var data = {
                key: key,
                value: value
            };
            data[miniShop2.actionName] = 'order/add';
            miniShop2.send(data, miniShop2.Order.callbacks.add, miniShop2.Callbacks.Order.add);
        },
        getcost: function () {
            var callbacks = miniShop2.Order.callbacks;
            callbacks.getcost.response.success = function (response) {
                $(miniShop2.Order.orderCost, miniShop2.Order.order).text(miniShop2.Utils.formatPrice(response.data['cost']));
            };
            var data = {};
            data[miniShop2.actionName] = 'order/getcost';
            miniShop2.send(data, miniShop2.Order.callbacks.getcost, miniShop2.Callbacks.Order.getcost);
        },
        clean: function () {
            var callbacks = miniShop2.Order.callbacks;
            callbacks.clean.response.success = function () {
                location.reload();
            };

            var data = {};
            data[miniShop2.actionName] = 'order/clean';
            miniShop2.send(data, miniShop2.Order.callbacks.clean, miniShop2.Callbacks.Order.clean);
        },
        submit: function () {
            miniShop2.Message.close();

            // Checking for active ajax request
            if (miniShop2.ajaxProgress) {
                //noinspection JSUnresolvedFunction
                miniShop2.$doc.ajaxComplete(function () {
                    miniShop2.ajaxProgress = false;
                    miniShop2.$doc.unbind('ajaxComplete');
                    miniShop2.Order.submit();
                });
                return false;
            }

            var callbacks = miniShop2.Order.callbacks;
            callbacks.submit.before = function () {
                $(':button, a, button[value="order/submit"]', miniShop2.Order.order).attr('disabled', true).prop('disabled', true);
                $('input, textarea', miniShop2.Order.order).attr('readonly', true).prop('readonly', true);
            };
            callbacks.submit.response.success = function (response) {
                if (response.data['redirect']) {
                    document.location.href = response.data['redirect'];
                }
                else if (response.data['msorder']) {
                    document.location.href = document.location.origin + document.location.pathname
                        + (document.location.search ? document.location.search + '&' : '?')
                        + 'msorder=' + response.data['msorder'];
                }
                else {
                    location.reload();
                }
            };
            callbacks.submit.response.error = function (response) {
                setTimeout((function () {
                    $(':button, a, button[value="order/submit"]', miniShop2.Order.order).attr('disabled', false).prop('disabled', false);
                    $('input, textarea', miniShop2.Order.order).attr('readonly', false).prop('readonly', false);
                }.bind(this)),3 * miniShop2.timeout);
                $('[name]', miniShop2.Order.order).removeClass('error').closest(miniShop2.Order.inputParent).removeClass('error');
                for (var i in response.data) {
                    if (response.data.hasOwnProperty(i)) {
                        var key = response.data[i];
                        //var $field = $('[name="' + response.data[i] + '"]', miniShop2.Order.order);
                        //$field.addClass('error').closest(miniShop2.Order.inputParent).addClass('error');
                        var $field = $('[name="' + key + '"]', miniShop2.Order.order);
                        if ($field.attr('type') == 'checkbox' || $field.attr('type') == 'radio') {
                            $field.closest(miniShop2.Order.inputParent).addClass('error');
                        }
                        else {
                            $field.addClass('error');
                        }
                    }
                }
            };
            return miniShop2.send(miniShop2.sendData.formData, miniShop2.Order.callbacks.submit, miniShop2.Callbacks.Order.submit);
        },
        getrequired: function (value) {
            var callbacks = miniShop2.Order.callbacks;
            callbacks.getrequired.response.success = function (response) {
                $('[name]', miniShop2.Order.order).removeClass('required').closest(miniShop2.Order.inputParent).removeClass('required');
                var requires = response.data['requires'];
                for (var i = 0, length = requires.length; i < length; i++) {
                    $('[name=' + requires[i] + ']', miniShop2.Order.order).addClass('required').closest(miniShop2.Order.inputParent).addClass('required');
                }
            };
            callbacks.getrequired.response.error = function () {
                $('[name]', miniShop2.Order.order).removeClass('required').closest(miniShop2.Order.inputParent).removeClass('required');
            };

            var data = {
                id: value
            };
            data[miniShop2.actionName] = 'order/getrequired';
            miniShop2.send(data, miniShop2.Order.callbacks.getrequired, miniShop2.Callbacks.Order.getrequired);
        }
    };

    miniShop2.Message = {
        initialize: function () {
            miniShop2.Message.close = function () {
            };
            miniShop2.Message.show = function (message) {
                if (message != '') {
                    alert(message);
                }
            };
        },
        success: function (message) {
            if (message) {
                console.log("<h4>"+message+"</h4>", "&nbsp;")
            }
        },
        error: function (message) {
            if (message) {
                console.error("<h4>"+message+"</h4>", "&nbsp;")
            }
        },
        info: function (message) {
            if (message) {
                console.info("<h4>"+message+"</h4>", "&nbsp;")
            }
        },
        warning: function (message) {
            if (message) {
                console.error("<h4>"+message+"</h4>", "&nbsp;")
            }
        },
        close: function () {
            //toastr.clear()
        },
    };

    miniShop2.Utils = {
        empty: function (val) {
            return (typeof(val) == 'undefined' || val == 0 || val === null || val === false || (typeof(val) == 'string' && val.replace(/\s+/g, '') == '') || (typeof(val) == 'object' && val.length == 0));
        },
        formatPrice: function (price) {
            var pf = miniShop2Config.price_format;
            price = this.number_format(price, pf[0], pf[1], pf[2]);

            if (miniShop2Config.price_format_no_zeros && pf[0] > 0) {
                price = price.replace(/(0+)$/, '');
                price = price.replace(/[^0-9]$/, '');
            }

            return price;
        },
        formatWeight: function (weight) {
            var wf = miniShop2Config.weight_format;
            weight = this.number_format(weight, wf[0], wf[1], wf[2]);

            if (miniShop2Config.weight_format_no_zeros && wf[0] > 0) {
                weight = weight.replace(/(0+)$/, '');
                weight = weight.replace(/[^0-9]$/, '');
            }

            return weight;
        },
        // Format a number with grouped thousands,
        number_format: function (number, decimals, dec_point, thousands_sep) {
            // original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
            // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // bugfix by: Michael White (http://crestidg.com)
            var i, j, kw, kd, km;

            // input sanitation & defaults
            if (isNaN(decimals = Math.abs(decimals))) {
                decimals = 2;
            }
            if (dec_point == undefined) {
                dec_point = ',';
            }
            if (thousands_sep == undefined) {
                thousands_sep = '.';
            }

            i = parseInt(number = (+number || 0).toFixed(decimals)) + '';

            if ((j = i.length) > 3) {
                j = j % 3;
            }
            else {
                j = 0;
            }

            km = j
                ? i.substr(0, j) + thousands_sep
                : '';
            kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
            kd = (decimals
                ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, '0').slice(2)
                : '');

            return km + kw + kd;
        },
        getValueFromSerializedArray: function (name, arr) {
            if (!$.isArray(arr)) {
                arr = miniShop2.sendData.formData;
            }
            for (var i = 0, length = arr.length; i < length; i++) {
                if (arr[i].name == name) {
                    return arr[i].value;
                }
            }
            return null;
        }
    };

    $(document).ready(function ($) {
        miniShop2.initialize();
        var html = $('html');
        html.removeClass('no-js');
        if (!html.hasClass('js')) {
            html.addClass('js');
        }

        //функции обратного вызова после добавления товара в корзину
        miniShop2.Callbacks.add('Cart.add.response.success', 'cart_add', function(resp, r) {
            if( $(miniShop2.Cart.cart).length ){
                document.location.reload();
            }
            else{
                if(typeof BTN_ACTION != 'undefined'){
                    BTN_ACTION.attr('onclick', 'window.location="'+CART_LINK+'"');
                    BTN_ACTION.attr('type', 'button');
                    if(BTN_ACTION.find('>svg').length){
                        let svg = BTN_ACTION.find('>svg');
                            BTN_ACTION.html('В корзине! Оформить заказ?').append(svg);
                            BTN_ACTION.addClass('button-green')
                            BTN_ACTION.removeClass('button-primary')
                    }
                    else{
                        BTN_ACTION.addClass('button-green')
                        BTN_ACTION.removeClass('button-primary')
                        BTN_ACTION.text('В корзине! Оформить заказ?');
                    }
                }
                //$('div[data-modal-form="modalShop_AddToCart"]').trigger('click');
                //$('#modalShop_AddToCart .js-count-this').text(resp.message);
            }
            $('.js-minicart').each(function(){
               $(this).text(resp.data.total_count);
            });

            // Запуск метода для яндекс коммерции
            if (typeof dataLayerPushCart === 'function') {
                dataLayerPushCart(resp.data.total_cost, resp.data.total_count); // находится в шаблоне product.tpl
            }
        });
        miniShop2.Callbacks.add('Cart.change.response.success', 'cart_change', function(resp) {
            $('.js-minicart').each(function(){
               $(this).text(resp.data.total_count);
            });
            if( $('#msCart').length ){
              getCostPosition(resp.data.key);
            }
        });
        miniShop2.Callbacks.add('Cart.remove.response.success', 'cart_remove', function(resp) {
            $('.js-minicart').each(function(){
               $(this).text(resp.data.total_count);
            });
            if( $('#msCart').length ){
              getCostPosition(null, 'remove');
            }
        });
        //отключение кнопки создания заказа, если кликнули по ней
        miniShop2.Callbacks.add('Order.submit.ajax.done', 'order_submit_request_done', function(response) {
            $('button[value="order/submit"]').attr('disabled', false).prop('disabled', false);
            return true;
        });
        miniShop2.Callbacks.add('Order.getcost.response.success', 'order_getcost', function(response) {
          if(response.success){
            if(response.data.delivery_cost){
              $('.js-delivery_cost').text((response.data.delivery_cost).toLocaleString("ru-RU"));
            }
            else{
              $('.js-delivery_cost').text(0);
            }
            $('.js-total_cost').text((response.data.cost).toLocaleString("ru-RU"));
            frmtPrice();
          }
        });
        //функция обратного вызова после успеха или ошибки добавления поля в заказ
        miniShop2.Callbacks.add('Order.add.response.success', 'order_add_field_success', function(response) {
            if(typeof response.data.delivery != 'undefined'){
                /*$('select[name="payment"]').html('');
                let paymvalid = $('input[name="delivery"][value="'+response.data.delivery+'"]').data('payments');
                paymvalid.forEach(function(item){
                    $('select[name="payment"]').append( $('select[name="payment_origin"] option[value="'+item+'"]').clone(true) );
                });*/
                if(response.data.delivery > 1){
                    $('*[name="address"]').parents('.js-address-wrap').show();
                    $('*[name="address"]').attr('data-req', true);
                    $('*[name="address"]').attr('required', true);
                }
                else{
                    $('*[name="address"]').parents('.js-address-wrap').hide();
                    $('*[name="address"]').removeAttr('data-req');
                    $('*[name="address"]').removeAttr('required');
                }
                if(!CHECK_COST){
                    let optioncheck = $('#checkout__final').find('input[name="delivery"]:checked');
                    let price = optioncheck.data('price'),
                        distance_price = optioncheck.data('distance_price');
                    CHECK_COST = true;
                    getMKADDistance(price, distance_price, $('.js-delivery_cost'));
                }
            }
            if(typeof response.data.address != 'undefined'){
                $(miniShop2.Order.order).find('input[name="street"]').val( response.data.address );
                if(!CHECK_COST){
                    let optioncheck = $('#checkout__final').find('input[name="delivery"]:checked');
                    let price = optioncheck.data('price'),
                        distance_price = optioncheck.data('distance_price');
                    CHECK_COST = true;
                    getMKADDistance(price, distance_price, $('.js-delivery_cost'));
                }
                setTimeout(function(){
                    miniShop2.Order.add('street', response.data.address);
                }, 350);
            }
            return true;
        });
    });

    window.miniShop2 = miniShop2;
})(window, document, jQuery, miniShop2Config);
////////////////////////////////////////////////////////
var mse2Config = mse2Config || {};
var mSearch2 = {
    initialized: false,
    loading: false,
    reached: false,
    options: {
        wrapper: '#mse2_mfilter',

        results: '#mse2_results',
        total: '#mse2_total',
        limit: '#mse2_limit',
        slider: '.mse2_number_slider',

        filters: '#mse2_filters',
        filter_title: '.filter_title',
        filter_wrapper: 'fieldset',

        pagination: '.mse2_pagination',
        pagination_link: '.mse2_pagination a',

        sort: '#mse2_sort',
        sort_link: '#mse2_sort a',

        tpl: '#mse2_tpl',
        tpl_link: '#mse2_tpl a',

        selected: '#mse2_selected',
        selected_tpl: '<a href="#" data-id="_id_" class="mse2_selected_link"><em>_title_</em><sup>x</sup></a>',
        selected_wrapper_tpl: '<strong>_title_:</strong>',
        selected_filters_delimeter: '; ',
        selected_values_delimeter: ' ',

        more: '.btn_more',
        more_tpl: '<button class="btn btn-default btn_more">' + mse2Config['moreText'] + '</button>',

        active_class: 'active',
        disabled_class: 'disabled',
        disabled_class_fieldsets: 'disabled_fieldsets',
        loading_class: 'loading',
        reset_skip: ['limit', 'sort', 'tpl'],
        prefix: 'mse2_',
        suggestion: 'sup', // inside filter item, e.g. #mse2_filters
        autoLoad: true,
    },
    sliders: {},
    numbers: {},
    selections: {},
    elements: ['filters', 'results', 'pagination', 'total', 'sort', 'selected', 'limit', 'tpl'],
    initialize: function (selector) {
        if (this.initialized) {
            return false;
        }
        var i;
        if (mse2Config['filterOptions'] != undefined && Object.keys(mse2Config['filterOptions']).length > 0) {
            for (i in mse2Config['filterOptions']) {
                if (mse2Config['filterOptions'].hasOwnProperty(i)) {
                    this.options[i] = mse2Config['filterOptions'][i];
                }
            }
        }
        for (i in this.elements) {
            if (this.elements.hasOwnProperty(i)) {
                var elem = this.elements[i];
                this[elem] = $(selector).find(this.options[elem]);
            }
        }
        // Get ready for old chunk
        if (this['pagination'].length == 0) {
            this.options['pagination'] = '#mse2_pagination';
            this.options['pagination_link'] = '#mse2_pagination a';
            this['pagination'] = $(selector).find(this.options['pagination']);
        }
        this.handlePagination();
        this.handleSort();
        this.handleTpl();
        this.handleNumbers();
        this.handleSlider();
        this.handleLimit();

        var submit = $(this.options.filters + ' [type="submit"]');
        if (this.options.autoLoad) {
            $(document).on('change', this.options.filters, function () {
                return mSearch2.submit();
            });
            submit.addClass('hidden');
        }
        else {
            submit.removeClass('hidden');
        }

        $(document).on('submit', this.options.filters, function () {
            return mSearch2.submit();
        });

        this.btn_reset = $(this.options.filters + ' [type="reset"]');
        $(document).on('reset', this.options.filters, function () {
            return mSearch2.reset();
        });

        for (i in this.startParams) {
            if (this.startParams.hasOwnProperty(i) && this.options.reset_skip.indexOf(i) === -1) {
                this.btn_reset.removeClass('hidden');
                break;
            }
        }

        if (this.selected) {
            this.selections[this.selected.text().replace(/\s+$/, '').replace(/:$/, '')] = [];
            var selectors = [
                this.options.filters + ' input[type="checkbox"]',
                this.options.filters + ' input[type="radio"]',
                this.options.filters + ' select'
            ];
            $(document).on('change', selectors.join(', '), function () {
                mSearch2.handleSelected($(this));
            });

            selectors = [
                'input[type="checkbox"]:checked',
                'input[type="radio"]:checked',
                'select'
            ];
            this.filters.find(selectors.join(', ')).each(function () {
                mSearch2.handleSelected($(this));
            });

            $(document).on('click', this.options.selected + ' a', function () {
                var id = $(this).data('id').replace(mse2Config['filter_delimeter'], "\\" + mse2Config['filter_delimeter']);
                var elem = $('#' + id);
                if (elem[0]) {
                    switch (elem[0].tagName) {
                        case 'INPUT':
                            elem.prop('checked', false).trigger('change');
                            break;
                        case 'SELECT':
                            elem.val(elem.find('option:first').prop('value')).trigger('change');
                            break;
                    }
                }
                return false;
            });
        }
        mSearch2.setEmptyFieldsets();
        mSearch2.setTotal(this.total.text());

        if (!mSearch2.Hash.oldbrowser()) {
            history.replaceState({mSearch2: window.location.href}, '', document.location.href);
        }
        window.setTimeout(function() {
            $(window).on('popstate', function (e) {
                if (e.originalEvent.state && e.originalEvent.state['mSearch2']) {
                    var params = {};
                    var tmp = e.originalEvent.state['mSearch2'].split('?');
                    if (tmp[1]) {
                        tmp = tmp[1].split('&');
                        for (var i in tmp) {
                            if (tmp.hasOwnProperty(i)) {
                                var tmp2 = tmp[i].split('=');
                                params[tmp2[0]] = tmp2[1];
                            }
                        }
                    }
                    mSearch2.setFilters(params);
                    mSearch2.load(params);
                }
            });
        }, 1000);

        this.initialized = true;
    },

    handlePagination: function () {
        var pcre = new RegExp(mse2Config['pageVar'] + '[=|\/|-](\\d+)');
        switch (mse2Config['mode']) {
            case 'button':
                this.pagination.hide();
                // Add more button
                this.results.after(this.options['more_tpl']);
                var more = $(this.options['more']);

                var has_results = false;
                $(this.options['pagination_link']).each(function () {
                    var href = $(this).prop('href');
                    var match = href.match(pcre);
                    var page = !match ? 1 : match[1];
                    if (page > mse2Config['page']) {
                        has_results = true;
                        return false;
                    }
                });
                if (!has_results) {
                    more.hide();
                }
                if (mse2Config['page'] > 1) {
                    mse2Config['page'] = '';
                    mSearch2.Hash.remove('page');
                    mSearch2.load();
                }

                $(document).on('click', this.options['wrapper'] + ' ' + this.options['more'], function (e) {
                    e.preventDefault();
                    mSearch2.addPage();
                });
                break;

            case 'scroll':
                this.pagination.hide();
                var wrapper = $(this.options['wrapper']);
                var $window = $(window);
                $window.on('scroll', function () {
                    if (!mSearch2.reached && $window.scrollTop() > wrapper.height() - $window.height()) {
                        mSearch2.reached = true;
                        mSearch2.addPage();
                    }
                });

                if (mse2Config['page'] > 1) {
                    mse2Config['page'] = '';
                    mSearch2.Hash.remove('page');
                    mSearch2.load();
                }
                break;

            default:
                $(document).on('click', this.options.pagination_link, function () {
                    if (!$(this).hasClass(mSearch2.options.active_class)) {
                        $(mSearch2.options.pagination).removeClass(mSearch2.options.active_class);
                        $(this).addClass(mSearch2.options.active_class);

                        var tmp = $(this).prop('href').match(pcre);
                        var page = tmp && tmp[1] ? Number(tmp[1]) : 1;
                        mse2Config['page'] = (page != mse2Config['start_page']) ? page : '';

                        var params = mSearch2.getFilters();
                        mSearch2.Hash.set(params);
                        mSearch2.load(params, function () {
                            $('html, body').animate({
                                scrollTop: $(mSearch2.options.wrapper).position().top || 0
                            }, 0);
                        });
                    }

                    return false;
                });
        }
    },

    handleSort: function () {
        var params = this.Hash.get();
        if (params.sort) {
            var sorts = params.sort.split(mse2Config['values_delimeter']);
            for (var i = 0; i < sorts.length; i++) {
                var tmp = sorts[i].split(mse2Config['method_delimeter']);
                if (tmp[0] && tmp[1]) {
                    $(this.options.sort_link + '[data-sort="' + tmp[0] + '"]').data('dir', tmp[1]).attr('data-dir', tmp[1]).addClass(this.options.active_class);
                }
            }
        }

        $(document).on('click', this.options.sort_link, function () {
            if ($(this).hasClass(mSearch2.options.active_class) && $(this).data('dir') == '') {
                return false;
            }
            $(mSearch2.options.sort_link).removeClass(mSearch2.options.active_class);
            $(this).addClass(mSearch2.options.active_class);
            var dir;
            if ($(this).data('dir') == '') {
                dir = $(this).data('default');
            }
            else {
                dir = $(this).data('dir') == 'desc'
                    ? 'asc'
                    : 'desc';
            }
            $(mSearch2.options.sort_link).data('dir', '').attr('data-dir', '');
            $(this).data('dir', dir).attr('data-dir', dir);

            var sort = $(this).data('sort');
            if (dir) {
                sort = sort.replace(
                    new RegExp(mse2Config['method_delimeter'] + '.*?' + mse2Config['values_delimeter']),
                    mse2Config['method_delimeter'] + dir + mse2Config['values_delimeter']
                );
                sort += mse2Config['method_delimeter'] + dir;
            }
            mse2Config['sort'] = (sort != mse2Config['start_sort']) ? sort : '';
            var params = mSearch2.getFilters();
            if (mse2Config['page'] > 1 && (mse2Config['mode'] == 'scroll' || mse2Config['mode'] == 'button')) {
                mse2Config['page'] = '';
                delete(params['page']);
            }
            mSearch2.Hash.set(params);
            mSearch2.load(params);

            return false;
        });
    },

    handleTpl: function () {
        $(document).on('click', this.options.tpl_link, function () {
            if (!$(this).hasClass(mSearch2.options.active_class)) {
                $(mSearch2.options.tpl_link).removeClass(mSearch2.options.active_class);
                $(this).addClass(mSearch2.options.active_class);

                var tpl = $(this).data('tpl');
                mse2Config['tpl'] = (tpl != mse2Config['start_tpl'] && tpl != 0) ? tpl : '';

                var params = mSearch2.getFilters();
                if (mse2Config['page'] > 1 && (mse2Config['mode'] == 'scroll' || mse2Config['mode'] == 'button')) {
                    mse2Config['page'] = '';
                    delete(params['page']);
                }
                mSearch2.Hash.set(params);
                mSearch2.load(params);
            }

            return false;
        });
    },

    handleSlider: function () {
        if (!$(mSearch2.options.slider).length) {
            return false;
        }
        else if (!$.ui || !$.ui.slider) {
            return mSearch2.loadJQUI(mSearch2.handleSlider);
        }
        $(mSearch2.options.slider).each(function () {
            var $this = $(this);
            var fieldset = $(this).parents('fieldset');
            var imin = fieldset.find('input:first');
            var imax = fieldset.find('input:last');
            var vmin = Number(imin.attr('value'));
            var vmax = Number(imax.attr('value'));
            var cmin = Number(imin.data('current-value'));
            var cmax = Number(imax.data('current-value'));
            // Check for decimals
            var ival = imin.val();
            var decimal = ival.indexOf('.') != -1;
            var decimals = decimal
                ? Number(ival.substr(ival.indexOf('.') + 1).length)
                : 0;
            var delimiter = 1;
            for (var i = 1; i <= decimals; i++) {
                delimiter *= 10;
            }

            var name = imin.prop('name');
            $this.slider({
                min: vmin,
                max: vmax,
                values: [vmin, vmax],
                range: true,
                step: 1 / delimiter,
                stop: function (e, ui) {
                    imin.val(ui.values[0].toFixed(decimals));
                    imax.val(ui.values[1].toFixed(decimals));
                    imin.add(imax).trigger('change');
                    mSearch2.sliders[name]['user_changed'] = true;
                },
                change: function (e, ui) {
                    if (mSearch2.sliders[name] != undefined && mSearch2.sliders[name]['values'] != undefined) {
                        mSearch2.sliders[name]['changed'] = mSearch2.sliders[name]['values'][0] != ui.values[0].toFixed(decimals) ||
                            mSearch2.sliders[name]['values'][1] != ui.values[1].toFixed(decimals);
                    }
                },
                slide: function (e, ui) {
                    if (decimal) {
                        imin.val(ui.values[0].toFixed(decimals));
                        imax.val(ui.values[1].toFixed(decimals));
                    } else {
                        imin.val(ui.values[0]);
                        imax.val(ui.values[1]);
                    }
                }
            });

            var changed = mSearch2.Hash.get()[name] !== undefined;
            mSearch2.sliders[name] = {
                changed: changed,
                user_changed: changed
            };

            var values = mSearch2.Hash.get();
            if (values[name]) {
                var tmp = values[name].split(mse2Config['values_delimeter']);
                if (tmp[0].match(/(?!^-)[^0-9\.]/g)) {
                    tmp[0] = tmp[0].replace(/(?!^-)[^0-9\.]/g, '');
                }
                if (tmp.length > 1) {
                    if (tmp[1].match(/(?!^-)[^0-9\.]/g)) {
                        tmp[1] = tmp[1].replace(/(?!^-)[^0-9\.]/g, '');
                    }
                }
                imin.val(tmp[0]);
                imax.val(tmp.length > 1 ? tmp[1] : tmp[0]);
            }

            //imin.attr('readonly', true);
            imin.attr('data-decimal', decimals);
            imin.on('change keyup input click', function (e) {
                if (this.value.match(/(?!^-)[^0-9\.]/g)) {
                    this.value = this.value.replace(/(?!^-)[^0-9\.]/g, '');
                }
                if (e.type != 'keyup' && e.type != 'input') {
                    if (this.value > vmax) {
                        this.value = vmax;
                    }
                    else if (this.value < vmin) {
                        this.value = vmin;
                    }
                }
                if (e.type == 'change') {
                    mSearch2.sliders[name]['user_changed'] = true;
                }
                $this.slider('values', 0, this.value);
            });
            //imax.attr('readonly', true);
            imax.attr('data-decimal', decimals);
            imax.on('change keyup input click', function (e) {
                if (this.value.match(/(?!^-)[^0-9\.]/g)) {
                    this.value = this.value.replace(/(?!^-)[^0-9\.]/g, '');
                }
                if (e.type != 'keyup' && e.type != 'input') {
                    if (this.value > vmax) {
                        this.value = vmax;
                    }
                    else if (this.value < vmin) {
                        this.value = vmin;
                    }
                }
                if (e.type == 'change') {
                    mSearch2.sliders[name]['user_changed'] = true;
                }
                $this.slider('values', 1, this.value);
            });

            if (values[name]) {
                imin.add(imax).trigger('click');
            }

            mSearch2.sliders[name]['values'] = [vmin, vmax];
            if (!isNaN(cmin) && !isNaN(cmax)) {
                if (cmin != 0 && cmin != vmin) {
                    $this.slider('values', 0, cmin);
                    imin.val(cmin);
                }
                if (cmax != 0 && cmax != vmax) {
                    $this.slider('values', 1, cmax);
                    imax.val(cmax);
                }
                mSearch2.sliders[name]['changed'] = mSearch2.Hash.get()[name] !== undefined;
            }
        });
        return true;
    },

    handleLimit: function () {
        $(document).on('change', this.options.limit, function () {
            var limit = $(this).val();
            mse2Config['page'] = '';
            if (limit == mse2Config['start_limit']) {
                mse2Config['limit'] = '';
            }
            else {
                mse2Config['limit'] = limit;
            }
            var params = mSearch2.getFilters();
            if (mse2Config['page'] > 1 && (mse2Config['mode'] == 'scroll' || mse2Config['mode'] == 'button')) {
                mse2Config['page'] = '';
                delete(params['page']);
            }
            mSearch2.Hash.set(params);
            mSearch2.load(params);
        });
    },

    handleSelected: function (input) {
        if (!input[0]) {
            return;
        }
        var id = input.prop('id');
        var title = '';
        var elem;

        var filter = input.parents(this.options['filter_wrapper']);
        var filter_title = '';
        var tmp;
        if (filter.length) {
            tmp = filter.find(this.options['filter_title']);
            if (tmp.length > 0) {
                filter_title = tmp.text();
            }
        }
        if (filter_title == '') {
            //noinspection LoopStatementThatDoesntLoopJS
            for (filter_title in this.selections) {
                break;
            }
        }

        switch (input[0].tagName) {
            case 'INPUT':
                var label = mSearch2.filters.find('label[for="' + input.prop('id') + '"]');
                var sup = label.find('sup').text();
                var text = label.text().trim();
                if (sup) {
                    title = text.replace(new RegExp(sup.replace('+', '\\+') + '$'), '');
                } else {
                    title = text;
                }
                $('[data-id="' + id + '"]', this.selected).remove();
                if (input.is(':checked')) {
                    elem = this.options['selected_tpl']
                        .replace('[[+id]]', id).replace('[[+title]]', title)
                        .replace('_id_', id).replace('_title_', title);
                }
                break;

            case 'SELECT':
                var option = input.find('option:selected');
                $('[data-id="' + id + '"]', this.selected).remove();
                if (input.val() != '') {
                    title = ' ' + option.text().replace(/(\(.*\)$)/, '');
                    elem = this.options['selected_tpl']
                        .replace('[[+id]]', id).replace('[[+title]]', title)
                        .replace('_id_', id).replace('_title_', title);
                }
                break;
        }

        if (elem != undefined) {
            if (this.selections[filter_title] == undefined || input[0].type == 'radio') {
                this.selections[filter_title] = {};
            }
            this.selections[filter_title][id] = elem;
        }
        else if (this.selections[filter_title] != undefined && this.selections[filter_title][id] != undefined) {
            delete this.selections[filter_title][id];
        }

        this.selected.html('');
        var count = 0;
        var selected = [];
        for (var i in this.selections) {
            if (!this.selections.hasOwnProperty(i) || !Object.keys(this.selections).length) {
                continue;
            }
            if (Object.keys(this.selections[i]).length) {
                tmp = [];
                for (var i2 in this.selections[i]) {
                    if (!this.selections[i].hasOwnProperty(i2)) {
                        continue;
                    }
                    tmp.push(this.selections[i][i2]);
                    count++;
                }
                title = this.options['selected_wrapper_tpl']
                    .replace('[[+title]]', i)
                    .replace('_title_', i);
                selected.push(title + tmp.join(this.options['selected_values_delimeter']));
            }
        }

        if (count) {
            this.selected.append(selected.join(this.options['selected_filters_delimeter'])).show();
        }
        else {
            this.selected.hide();
        }
    },

    handleNumbers: function () {
        var groups = {};
        $('input[type="text"],input[type="number"]', mSearch2.filters).each(function() {
            var $this = $(this);
            var name = $this.attr('name');
            if (groups[name] == undefined) {
                groups[name] = [$this];
            } else {
                groups[name].push($this);
            }
        });

        var data = this.Hash.get();
        for (var name in groups) {
            if (!groups.hasOwnProperty(name)) {
                continue;
            }
            var group = groups[name];
            if (group.length == 2) {
                var $min = group[0];
                var $max = group[1];
                var vmin = $min.val();
                var vmax = $max.val();
                $min.attr('data-original-value', vmin);
                $max.attr('data-original-value', vmax);
                this.numbers[name] = {
                    values: [vmin, vmax],
                };
                if (data[name] != undefined) {
                    var values = data[name].split(mse2Config['values_delimeter']);
                    if (values.length == 2) {
                        $min.val(values[0]);
                        $max.val(values[1]);
                    }
                }
            }
        }
    },

    load: function (params, callback, append) {
        if (this.loading) {
            return false;
        } else {
            this.loading = true;
        }

        this.btn_reset.addClass('hidden');
        for (var i in params) {
            if (params.hasOwnProperty(i) && this.options.reset_skip.indexOf(i) === -1) {
                this.btn_reset.removeClass('hidden');
                break;
            }
        }

        if (!params || !Object.keys(params).length) {
            params = this.getFilters();
        }
        params.action = 'filter';
        params.pageId = mse2Config['pageId'];

        this.beforeLoad();
        params.key = mse2Config['key'];
        //noinspection JSUnresolvedFunction
        $.post(mse2Config['actionUrl'], params, function (response) {
            mSearch2.loading = false;
            mSearch2.afterLoad();
            if (response['success']) {
                mSearch2.Message.success(response['message']);
                mSearch2.pagination.html(response['data']['pagination']);
                if (append) {
                    mSearch2.results.append(response['data']['results']);
                }
                else {
                    mSearch2.results.html(response['data']['results']);
                }

                if (mse2Config['mode'] == 'button') {
                    if (response['data']['pages'] == response['data']['page']) {
                        $(mSearch2.options['more']).hide();
                    }
                    else {
                        $(mSearch2.options['more']).show();
                    }
                }
                else if (mse2Config['mode'] == 'scroll') {
                    mSearch2.reached = response['data']['pages'] == response['data']['page'];
                }

                mSearch2.setTotal(response['data']['total']);
                if (callback && $.isFunction(callback)) {
                    callback.call(this, response, params);
                }
                mSearch2.setSuggestions(response['data']['suggestions']);
                mSearch2.setEmptyFieldsets();
                if (response['data'].log) {
                    $('.mFilterLog').html(response['data'].log);
                }
                mSearch2.updateTitle(response['data']);
                $(document).trigger('mse2_load', response);
            }
            else {
                mSearch2.Message.error(response['message']);
            }
        }, 'json');
    },

    getFilters: function () {
        var params = {};
        // Disabled friendly urls
        var hash = this.Hash.get();
        if (hash[mse2Config['idVar']] != undefined) {
            params[mse2Config['idVar']] = hash[mse2Config['idVar']];
        }
        // Other params
        if (mse2Config[mse2Config['queryVar']] != '') {
            params[mse2Config['queryVar']] = mse2Config[mse2Config['queryVar']];
        }
        if (mse2Config[mse2Config['parentsVar']] != '') {
            params[mse2Config['parentsVar']] = mse2Config[mse2Config['parentsVar']];
        }
        if (mse2Config['sort'] != '') {
            params.sort = mse2Config['sort'];
        }
        if (mse2Config['tpl'] != '') {
            params.tpl = mse2Config['tpl'];
        }
        if (mse2Config['page'] > 0) {
            params.page = mse2Config['page'];
        }
        if (mse2Config['limit'] > 0) {
            params.limit = mse2Config['limit'];
        }
        // Filters
        $.map(this.filters.serializeArray(), function (n) {
            if (n.value === '') {
                return;
            }
            if (params[n.name]) {
                params[n.name] += mse2Config['values_delimeter'] + n.value;
            }
            else {
                params[n.name] = n.value;
            }
        });
        var i;
        for (i in this.sliders) {
            if (this.sliders.hasOwnProperty(i) && params[i]) {
                if (!this.sliders[i]['changed']) {
                    delete params[i];
                }
            }
        }
        for (i in this.numbers) {
            if (this.numbers.hasOwnProperty(i) && params[i]) {
                if (this.numbers[i]['values'].join(mse2Config['values_delimeter']) == params[i]) {
                    delete params[i];
                }
            }
        }

        return params;
    },

    setFilters: function (params) {
        if (!params) {
            params = {};
        }
        for (var i in this.elements) {
            if (!this.elements.hasOwnProperty(i)) {
                continue;
            }
            var elem = this.elements[i];
            if (typeof(this[elem]) == 'undefined') {
                continue;
            }
            var item, name, values, val, type;
            switch (elem) {
                case 'limit':
                    if (params['limit'] == undefined) {
                        this.limit.val(mse2Config['start_limit']);
                        mse2Config['limit'] = '';
                    }
                    else {
                        this.limit.val(params['limit']);
                    }
                    break;
                case 'pagination':
                    mse2Config['page'] = params['page'] == undefined
                        ? ''
                        : params['page'];
                    break;
                case 'sort':
                    var sorts = {};
                    values = params['sort'];
                    if (values == undefined) {
                        values = mse2Config['start_sort'];
                        mse2Config['sort'] = '';
                    }
                    if (typeof(values) != 'object' && values != '') {
                        values = values.split(mse2Config['values_delimeter']);
                        for (i in values) {
                            if (!values.hasOwnProperty(i)) {
                                continue;
                            }
                            name = values[i].split(mse2Config['method_delimeter']);
                            if (name[0] && name[1]) {
                                sorts[name[0]] = name[1];
                            }
                        }
                    }
                    $(document).find(this.options['sort_link']).each(function () {
                        item = $(this);
                        name = item.data('sort');
                        if (sorts[name]) {
                            item.data('dir', sorts[name]).attr('data-dir', sorts[name]);
                            item.addClass(mSearch2.options['active_class']);
                        }
                        else {
                            item.data('dir', '').attr('data-dir', '');
                            item.removeClass(mSearch2.options['active_class']);
                        }
                    });
                    break;
                case 'tpl':
                    values = params['tpl'] != undefined
                        ? params['tpl']
                        : 0;
                    $(document).find(this.options['tpl_link']).each(function () {
                        item = $(this);
                        if (item.data('tpl') == values) {
                            item.addClass(mSearch2.options['active_class']);
                            mse2Config['tpl'] = values;
                        }
                        else {
                            item.removeClass(mSearch2.options['active_class']);
                        }
                    });
                    break;
                case 'filters':
                    this['filters'].find('input').each(function () {
                        item = $(this);
                        name = item.prop('name');
                        type = item.prop('type');
                        values = params[name];
                        if (values != undefined && typeof(values) != 'object') {
                            values = values.split(mse2Config['values_delimeter']);
                        }
                        switch (type) {
                            case 'checkbox':
                            case 'radio':
                                var checked = item.is(':checked');
                                if (params[name] != undefined) {
                                    item.prop('checked', values.indexOf(String(item.val())) != -1);
                                }
                                else {
                                    item.prop('checked', false);
                                }
                                if (item.is(':checked') != checked) {
                                    mSearch2.handleSelected(item);
                                }
                                break;
                            default:
                                if (mSearch2.sliders[name]) {
                                    if (item.prop('id').match(/.*?_0$/)) {
                                        val = (values != undefined && values[0] != undefined)
                                            ? values[0]
                                            : mSearch2.sliders[name]['values'][0];
                                    }
                                    else {
                                        val = (values != undefined && values[1] != undefined)
                                            ? values[1]
                                            : mSearch2.sliders[name]['values'][1];
                                    }
                                    if (val != item.val()) {
                                        item.val(val).trigger('click');
                                    }
                                } else {
                                    var original = item.data('original-value');
                                    if (original != undefined) {
                                        item.val(original);
                                    }
                                }
                        }
                    });
                    this['filters'].find('select').each(function () {
                        item = $(this);
                        name = item.prop('name');
                        type = item.prop('type');
                        values = params[name];
                        if (values != undefined) {
                            if (typeof(values) != 'object') {
                                values = values.split(mse2Config['values_delimeter']);
                            }
                            item.find('option').each(function () {
                                var option = $(this);
                                val = option.prop('value');
                                var selected = option.is(':selected');
                                $(this).prop('selected', values.indexOf(String(val)) != -1);
                                if (option.is(':selected') != selected) {
                                    mSearch2.handleSelected(item);
                                }
                            });
                        }
                        else {
                            item.val('');
                        }
                        /*
                         if (params[name] != undefined) {
                         item.prop('checked', values.indexOf(String(item.val())) != -1);
                         }
                         else {
                         item.prop('checked', false);
                         }
                         */
                    });
                    break;
            }
        }
    },

    setSuggestions: function (suggestions) {
        var aliases = mse2Config['aliases'] || {};
        var alias, value;
        for (var filter in suggestions) {
            if (!suggestions.hasOwnProperty(filter)) {
                continue;
            }
            var is_slider = typeof(this.sliders[filter]) != 'undefined';
            var is_number = typeof(this.numbers[filter]) != 'undefined';
            var arr = suggestions[filter];
            alias = filter;
            if (typeof(aliases[filter]) != 'undefined') {
                filter = aliases[filter];
            }
            var count, selector, input;
            if (is_slider && !this.sliders[alias]['user_changed']) {
                var vmin = null;
                var vmax = null;
                for (value in arr) {
                    if (!arr.hasOwnProperty(value)) {
                        continue;
                    }
                    count = arr[value];
                    if (typeof(count) != 'number') {
                        continue;
                    }
                    if (count > 0 && (vmin === null || vmin > value)) {
                        vmin = Number(value);
                    }
                    if (count > 0 && vmax < value) {
                        vmax = Number(value);
                    }
                }
                selector = filter.replace(mse2Config['filter_delimeter'], "\\" + mse2Config['filter_delimeter']);
                var imin = $('#' + mSearch2.options.prefix + selector + '_0', mSearch2.filters);
                if (imin.length) {
                    if (vmin == null) {
                        vmin = Number(imin.data('original-value'));
                    }
                    imin.val(vmin.toFixed(imin.data('decimal'))).trigger('click');
                }
                var imax = $('#' + mSearch2.options.prefix + selector + '_1', mSearch2.filters);
                if (imax.length) {
                    if (vmax == null) {
                        vmax = Number(imax.data('original-value'));
                    }
                    imax.val(vmax.toFixed(imax.data('decimal'))).trigger('click');
                }
                this.sliders[alias]['values'] = [vmin, vmax];
                this.sliders[alias]['changed'] = false;
            }
            else if (is_number) {
                // Do nothing
            }
            else {
                for (value in arr) {
                    if (!arr.hasOwnProperty(value)) {
                        continue;
                    }
                    count = arr[value];
                    selector = filter.replace(mse2Config['filter_delimeter'], "\\" + mse2Config['filter_delimeter']);
                    input = $('#' + mSearch2.options.prefix + selector, mSearch2.filters).find('[value="' + value.replace(/&quot;/g, '\\"') + '"]');
                    if (!input[0]) {
                        continue;
                    }

                    switch (input[0].tagName) {
                        case 'INPUT':
                            var proptype = input.prop('type');
                            if (proptype != 'checkbox' && proptype != 'radio') {
                                continue;
                            }
                            var label = $('#' + mSearch2.options.prefix + selector, mSearch2.filters).find('label[for="' + input.prop('id') + '"]');
                            var elem = input.parent().find(mSearch2.options.suggestion);
                            elem.text(count);

                            if (count == 0) {
                                if (input.is(':not(:checked)')) {
                                    input.prop('disabled', true);
                                    label.addClass(mSearch2.options.disabled_class);
                                    mSearch2.handleSelected(input);
                                }
                            }
                            else {
                                input.prop('disabled', false);
                                label.removeClass(mSearch2.options.disabled_class);
                            }

                            if (input.is(':checked')) {
                                elem.hide();
                            }
                            else {
                                elem.show();
                            }
                            break;

                        case 'OPTION':
                            var text = input.text();
                            var matches = text.match(/\([^\)]+\)$/);
                            var src = matches
                                ? matches[0]
                                : '';
                            var dst = '';

                            if (!count) {
                                input.prop('disabled', true).addClass(mSearch2.options.disabled_class);
                                /*
                                 if (input.is(':selected')) {
                                 input.prop('selected', false);
                                 mSearch2.handleSelected(input);
                                 }
                                 */
                            }
                            else {
                                dst = ' (' + count + ')';
                                input.prop('disabled', false).removeClass(mSearch2.options.disabled_class);
                            }

                            if (input.is(':selected')) {
                                dst = '';
                            }

                            if (src) {
                                text = text.replace(src, dst);
                            }
                            else {
                                text += dst;
                            }
                            input.text(text);

                            mSearch2.handleSelected(input.parent());
                            break;
                    }
                }
            }
        }
    },

    setEmptyFieldsets: function () {
        this.filters.find('fieldset').each(function () {
            var all_children_disabled = $(this).find('label:not(.' + mSearch2.options.disabled_class + ')').length == 0;
            if (all_children_disabled) {
                $(this).addClass(mSearch2.options.disabled_class_fieldsets);
            }
            if (!all_children_disabled && $(this).hasClass(mSearch2.options.disabled_class_fieldsets)) {
                $(this).removeClass(mSearch2.options.disabled_class_fieldsets);
            }
        });
    },

    setTotal: function (total) {
        if (this.total.length != 0) {
            if (!total || total == 0) {
                this.total.parent().hide();
                this.limit.parent().hide();
                this.sort.hide();
                this.total.text(0);
            }
            else {
                this.total.parent().show();
                this.limit.parent().show();
                this.sort.show();
                this.total.text(total);
            }
        }
    },

    beforeLoad: function () {
        $(this.options['wrapper']).addClass(this.options['loading_class']);
        this.results.css('opacity', .5);
        $(this.options.pagination_link).addClass(this.options.active_class);
        this.filters.find('input, select').prop('readonly', true).addClass(this.options.disabled_class);
    },

    afterLoad: function () {
        $(this.options['wrapper']).removeClass(this.options['loading_class']);
        this.results.css('opacity', 1);
        this.filters.find('.' + this.options.disabled_class).prop('readonly', false).removeClass(this.options.disabled_class);
    },

    loadJQUI: function (callback, parameters) {
        $('<link/>', {
            rel: 'stylesheet',
            type: 'text/css',
            href: mse2Config['cssUrl'] + 'jquery-ui/jquery-ui.min.css'
        }).appendTo('head');

        return $.getScript(mse2Config['jsUrl'] + 'lib/jquery-ui.min.js', function () {
            if (typeof callback == 'function') {
                callback(parameters);
            }
        });
    },

    submit: function (params) {
        if (this.loading) {
            return false;
        }
        else if (!params || !Object.keys(params).length) {
            params = this.getFilters();
        }
        else {
            delete(params['action']);
            delete(params['key']);
            delete(params['pageId']);
        }
        delete(params['page']);

        var action = $(this.options.filters).attr('action');
        if (!mSearch2.options.autoLoad) {
            var vars = '';
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    vars += '&' + i + '=' + params[i];
                }
            }
            if (!action.match(/\?/)) {
                document.location = action + vars.replace(/^&/, '?');
            }
            else {
                document.location = action + vars;
            }
            return false;
        }
        else {
            mse2Config['page'] = '';
            mSearch2.Hash.set(params);
            mSearch2.load(params);
            return false;
        }
    },

    reset: function () {
        if (this.loading) {
            return false;
        }
        var hash = this.Hash.get();
        var data = {};
        this.Hash.clear();
        for (var i in this.options.reset_skip) {
            if (!this.options.reset_skip.hasOwnProperty(i)) {
                continue;
            }
            var item = this.options.reset_skip[i];
            if (hash[item] != undefined) {
                data[item] = hash[item];
            }
        }
        this.setFilters(data);
        for (var value in this.sliders) {
            if (this.sliders.hasOwnProperty(value)) {
                this.sliders[value]['changed'] =
                this.sliders[value]['user_changed'] = false;
            }
        }

        return this.submit();
    },

    addPage: function () {
        var pcre = new RegExp(mse2Config['pageVar'] + '[=|\/|-](\\d+)');
        var current = mse2Config['page'] || 1;
        $(this.options['pagination_link']).each(function () {
            var href = $(this).prop('href');
            var match = href.match(pcre);
            var page = !match ? 1 : Number(match[1]);
            if (page > current) {
                mse2Config['page'] = (page != mse2Config['start_page']) ? page : '';
                var tmp = mSearch2.getFilters();
                delete(tmp['page']);
                mSearch2.Hash.set(tmp);

                var params = mSearch2.getFilters();
                mSearch2.load(params, null, true);
                return false;
            }
        });
    },

    updateTitle: function (response) {
        if (typeof(pdoTitle) == 'undefined') {
            return;
        }
        var $title = $('title');
        var separator = pdoTitle['separator'] || ' / ';
        var tpl = pdoTitle['tpl'];

        var title = [];
        var items = $title.text().split(separator);
        var pcre = new RegExp('^' + tpl.split(' ')[0] + ' ');
        for (var i = 0; i < items.length; i++) {
            if (i === 1 && response['page'] && response['page'] > 1) {
                title.push(tpl.replace('{page}', response['page']).replace('{pageCount}', response['pages']));
            }
            if (!items[i].match(pcre)) {
                title.push(items[i]);
            }
        }
        $title.text(title.join(separator));
    },

};

mSearch2.Form = {
    initialized: false,
    initialize: function (selector) {
        if (this.initialized || typeof(mse2FormConfig) == 'undefined') {
            return false;
        }
        $(selector).each(function () {
            var form = $(this);
            var config = mse2FormConfig[form.data('key')];
            var cache = {};

            if (config.autocomplete == '0' || config.autocomplete == 'false') {
                return false;
            }
            else if (!$.ui || !$.ui.autocomplete) {
                return mSearch2.loadJQUI(mSearch2.Form.initialize, selector);
            }

            form.find('input[name="' + config['queryVar'] + '"]').autocomplete({
                source: function (request, callback) {
                    if (request.term in cache) {
                        callback(cache[request.term]);
                        return;
                    }
                    var data = {
                        action: 'search',
                        key: form.data('key'),
                        pageId: config.pageId
                    };
                    data[config['queryVar']] = request.term;
                    //noinspection JSUnresolvedFunction
                    $.post(mse2Config['actionUrl'], data, function (response) {
                        if (response['data']['log']) {
                            $('.mSearchFormLog').html(response['data']['log']);
                        }
                        else {
                            $('.mSearchFormLog').html('');
                        }
                        cache[request.term] = response['data']['results'];
                        callback(response['data']['results'])
                    }, 'json');
                },
                minLength: config['minQuery'] || 3,
                select: function (event, ui) {
                    if (ui.item.url) {
                        document.location.href = ui.item.url;
                    }
                    else {
                        setTimeout(function () {
                            form.submit();
                        }, 100);
                    }
                }
            }).data("ui-autocomplete")._renderItem =
                function (ul, item) {
                    return $('<li>')
                        .data("item.autocomplete", item)
                        .addClass("mse2-ac-wrapper")
                        .append($("<div>").addClass('mse2-ac-link').html(item.label))
                        //.append("<a class=\"mse2-ac-link\">" + item.label + "</a>")
                        .appendTo(ul);
                };

            //noinspection JSPotentiallyInvalidConstructorUsage,JSUnresolvedVariable
            jQuery.ui.autocomplete.prototype._resizeMenu = function () {
                var ul = this.menu.element;
                ul.outerWidth(this.element.outerWidth());
            };

            return true;
        });
        this.initialized = true;
    }
};

mSearch2.Message = {
    success: function (message) {
    },
    error: function (message) {
        alert(message);
    }
};

mSearch2.Hash = {
    get: function () {
        var vars = {}, hash, splitter, hashes;
        if (!this.oldbrowser()) {
            var pos = window.location.href.indexOf('?');
            hashes = (pos != -1) ? decodeURIComponent(window.location.href.substr(pos + 1)) : '';
            splitter = '&';
        }
        else {
            hashes = decodeURIComponent(window.location.hash.substr(1));
            splitter = '/';
        }

        if (hashes.length == 0) {
            return vars;
        }
        else {
            hashes = hashes.split(splitter);
        }

        for (var i in hashes) {
            if (hashes.hasOwnProperty(i)) {
                hash = hashes[i].split('=');
                if (typeof hash[1] == 'undefined') {
                    vars['anchor'] = hash[0];
                }
                else {
                    vars[hash[0]] = hash[1];
                }
            }
        }
        return vars;
    },
    set: function (vars) {
        var hash = '';
        var i;
        for (i in vars) {
            if (vars.hasOwnProperty(i)) {
                hash += '&' + i + '=' + vars[i];
            }
        }
        if (!this.oldbrowser()) {
            if (hash.length != 0) {
                hash = '?' + hash.substr(1);
                var specialChars = {"%": "%25", "+": "%2B"};
                for (i in specialChars) {
                    if (specialChars.hasOwnProperty(i) && hash.indexOf(i) !== -1) {
                        hash = hash.replace(new RegExp('\\' + i, 'g'), specialChars[i]);
                    }
                }
            }
            window.history.pushState({mSearch2: document.location.pathname + hash}, '', document.location.pathname + hash);
        }
        else {
            window.location.hash = hash.substr(1);
        }
    },
    add: function (key, val) {
        var hash = this.get();
        hash[key] = val;
        this.set(hash);
    },
    remove: function (key) {
        var hash = this.get();
        delete hash[key];
        this.set(hash);
    },
    clear: function () {
        this.set({});
    },
    oldbrowser: function () {
        return !(window.history && history.pushState);
    }
};

mSearch2.startParams = mSearch2.Hash.get();
////////////////////////////////////////////////////////
var lazyLoadImg = function(){
    var lazyImages = [].slice.call(document.querySelectorAll("img.lazy, div.lazy, section.lazy, iframe.lazy"));
  if ("IntersectionObserver" in window) {
    let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let lazyImage = entry.target;
          lazyImage.src = lazyImage.dataset.src;
          if(lazyImage.dataset.background){
              $(lazyImage).css('background-image', 'url("'+lazyImage.dataset.background+'")');
          }
          //lazyImage.srcset = lazyImage.dataset.srcset;
          lazyImage.classList.remove("lazy");
          lazyImageObserver.unobserve(lazyImage);
        }
      });
    });

    lazyImages.forEach(function(lazyImage) {
      lazyImageObserver.observe(lazyImage);
    });
  } else {
    // Possibly fall back to a more compatible method here
  }
}
window.addEventListener('load', (event) => {
  lazyLoadImg();
});
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined
}
function setCookie(name, value, props) {
    props = props || {}
    var exp = props.expires
    if (typeof exp == "number" && exp) {
        var d = new Date()
        d.setTime(d.getTime() + exp*1000)
        exp = props.expires = d
    }
    if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }
    value = encodeURIComponent(value)
    var updatedCookie = name + "=" + value
    props.path = '/';
    props.domain = HTTP_HOST;
    for(var propName in props){
        updatedCookie += "; " + propName
        var propValue = props[propName]
        if(propValue !== true){ updatedCookie += "=" + propValue }
    }
    document.cookie = updatedCookie
}
function deleteCookie(name) {
    setCookie(name, null, { expires: -1 })
}
var getValueSortable = function(){
  if($('[id="mse2_ms|value"]').find('>span[id^="mse2_ms|value"]').length > 1){
    $('.js-value-wrapper').show();
  }
  else{
    $('.js-value-wrapper').hide();
  }
}
getValueSortable();
$(document).on('mse2_load', function(e, data) {
    lazyLoadImg();
    getValueSortable();
    if(data.data.page >= data.data.pages){
      $('.js-form-pages input[name="page"]').val(1);
    }
    else{
      $('.js-form-pages input[name="page"]').val(data.data.page+1);
    }
    if(!data.data.pages || data.data.pages <= 1){
      $('.js-form-pages').hide();
    }
    else{
      $('.js-form-pages').show();
    }

    /*if( $('#mse2_results').find('.js-not-result').length ){
      $('#mse2_results').addClass('catalog-list');
    }
    else{
      if( $('.js-catalog-view.active').attr('data-type') == 'list' ){
        $('#mse2_results').addClass('catalog-list');
      }
      else{
        $('#mse2_results').removeClass('catalog-list');
      }
    }*/
});
if(typeof TOTAL_PAGES != 'undefined' && TOTAL_PAGES <= 1){
  $('.js-form-pages').hide();
}
$(document).on('pdopage_load', function(e, config, response) {
    lazyLoadImg();
});
$(document).on('af_complete', function(event, response) {
    var form = response.form;
    if (response.success /*&& form.attr('id') == 'sign_up'*/) {
      window.location.href = SUCCESS_SUMBIT_URL;
    }
});
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

//закрытие верхнего баннера
$('.js-offerTop_Close').click(function () {
    setCookie('offerTop', 1, {'expires': 604800});
});
if(getCookie('offerTop')){
    $('.js-offerTop_Close').closest('.offer-top-section').hide();
}

//слайд в шапке страницы О нас
if(typeof slide != 'undefined' && slide){
  $('.js-promo-section').css({'background':'url('+slide+') 50% 50% no-repeat', 'background-size':'cover'});
}

//переменная с кнопкой, через которую добавили товар в корзину
BTN_ACTION = undefined;
$(document).on('click touch', '[value="cart/add"]', function(){
   BTN_ACTION = $(this);
});

if( $('.js-discount').length && typeof DISCOUNT != 'undefined' ){
  $('.js-discount').text(DISCOUNT);
}
// if( $('.js-discount-summ').length && typeof DISCOUNT_SUMM != 'undefined' ){
//   $('.js-discount-summ').text(DISCOUNT_SUMM);
// }

//активная услуга в сайдбаре левом
  var pathname = window.location.pathname;
  var path_arr = pathname.split('/');
  path_arr = path_arr.filter(function (el) {
    return el != '';
  });
  var is_true_service = false;
  //console.log(path_arr);
  //console.log(pathname);
  jQuery('.header-nav a').each(function(){
     href = String(jQuery(this).attr('href'));
     href = '/' + href;
     if(pathname == href){
         jQuery(this).addClass('header-nav__item_active');
         //jQuery(this).find('span').addClass('services-nav__item-text--active');
         is_true_service = true;
         //return false;
     }
  });
  if(!is_true_service){
      jQuery('.header-nav a').each(function(){
         href = String(jQuery(this).attr('href'));
         href = '/' + href;
         href_arr = href.split('/');
         thiss = jQuery(this);

         href_arr = href_arr.filter(function (el) {
            return el != '';
          });

          if(!is_true_service){
             href_arr.forEach(function(element) {
                 if(element != ''){
                    if(path_arr.includes(element) && element != 'catalog' && href_arr.length < 3 && path_arr.length > 1){
                         thiss.addClass('header-nav__item_active');
                         //thiss.find('span').addClass('services-nav__item-text--active');
                         is_true_service = true;
                         return false;
                      }
                 }
             });
          }
      });
  }

  //таблицы
  if($('.content table').length>0){
      $('.content table').each(function(){
          $(this).wrap('<div class="table-wrapper"></div>');
      });
  }

  //картинки в контенте
  $('.content p > img').each(function(){
    let $this = $(this),
        p = $this.parent();
    $this.unwrap();
  });

  //прячем лишние пункты меню сайдбара
  $('.sidebar:not([id="mse2_resource|parent"]):not(.js-not-more) .sidebar-list').each(function(){
    if( !$(this).find('>*').length ){
      $(this).remove();
      return true;
    }
    $(this).children('a.sidebar-list__item:gt(3)').wrapAll('<div class="sidebar-list--hidden">');
  });
  var total_items = $('.sidebar .sidebar-list').length;
  $('.sidebar .sidebar-list').each(function(idx){
    if (idx === total_items - 1) {
      $(this).addClass('sidebar-list--last');
    }
    if(!$(this).find('.sidebar-list--hidden').length){
        $(this).find('.js-sidebarList_More').remove();
    }
  });
  $('.sidebar-mob__items .js-sidebarList_More').remove();
  //форматирование цен
  var frmtPrice = function(){
    $('.js-price').each(function(){
      let price = parseFloat($(this).text().replace(',', '.').replace(' ', '').replace(' ', '')).toFixed(2);
      $(this).text( price );
    });
  }
  frmtPrice();

  //цена товара на странице товара
  $(document).on('change', '.js-type-pack', function () {
    if(typeof BTN_ACTION != 'undefined'){
        BTN_ACTION.attr('onclick', false);
        BTN_ACTION.attr('type', 'submit');
        if(BTN_ACTION.find('>svg').length){
            let svg = BTN_ACTION.find('>svg');
                BTN_ACTION.html('В корзину').append(svg);
        }
        else{
            BTN_ACTION.text('В корзину');
        }
    }
    let price = +$('.js-productCount_Number').attr('data-price'),
        price_discount = +$('.js-productCount_Number').attr('data-discount'),
        cost = $(this).attr('data-price'),
        pack_count = $(this).attr('data-pack-count'),
        count = $('.js-productCount_Number').val()?$('.js-productCount_Number').val():1;

        if ($(this).val() == 2) {
          cost = pack_count * price_discount;
          $('.js-cost').attr('data-product-price', cost);
          $('.js-cost').find('[data-product-cost]').text((cost * count).toLocaleString('ru', {minimumFractionDigits: 2,maximumFractionDigits: 2}));
          $('.js-price-current').text(price_discount.toLocaleString('ru', {minimumFractionDigits: 2,maximumFractionDigits: 2}));
        } else {
          cost = pack_count * price;
          $('.js-cost').attr('data-product-price', cost);
          $('.js-cost').find('[data-product-cost]').text((cost * count).toLocaleString('ru', {minimumFractionDigits: 2,maximumFractionDigits: 2}));
          $('.js-price-current').text(price.toLocaleString('ru', {minimumFractionDigits: 2,maximumFractionDigits: 2}));
        }

        // if( $('.js-opts:checked').length ){
        //   $('.js-opts').each(function(){
        //     let $this = $(this);
        //     $this.find('+label > .js-price-opts').text($this.find('+label > .js-price-opts').data('opt-price'));
        //   });
        //   $('.js-opts:checked').each(function(){
        //     let $this = $(this),
        //         val = +$(this).val(),
        //         price_opt = 0;
        //     if(typeof OPTS != 'undefined'){
        //       OPTS[val].forEach(function(price_opts, count_opts){
        //         // console.log(pack_count, count, price, price_opt);
        //         // console.log(pack_count*count);
        //         if((pack_count*count) >= count_opts){
        //           price_opt = +price_opts;
        //           //console.log(price_opts);
        //           $this.find('+label > .js-price-opts').text(price_opts);
        //         }
        //       });
        //     }

        //     if(price_opt){
        //       price = price + price_opt;
        //       price_discount = price_discount + price_opt;
        //     }

        //   });
        // }
        // else{
        //   $('.js-opts').each(function(){
        //     let $this = $(this);
        //     $this.find('+label > .js-price-opts').text($this.find('+label > .js-price-opts').data('opt-price'));
        //   });
        // }

        //console.log(pack_count, price);

        // if(typeof DISCOUNT_SUMM != 'undefined' && (cost * count) >= DISCOUNT_SUMM){
        //   cost = pack_count * price_discount;
        //   $('.js-cost').attr('data-product-price', cost);
        //   $('.js-cost').find('[data-product-cost]').text((cost * count).toLocaleString('ru', {minimumFractionDigits: 2,maximumFractionDigits: 2}));
        //   $('.js-price-current').text(price_discount.toLocaleString('ru', {minimumFractionDigits: 2,maximumFractionDigits: 2}));
        // }
        // else{
        //   cost = pack_count * price;
        //   $('.js-cost').attr('data-product-price', cost);
        //   $('.js-cost').find('[data-product-cost]').text((cost * count).toLocaleString('ru', {minimumFractionDigits: 2,maximumFractionDigits: 2}));
        //   $('.js-price-current').text(price.toLocaleString('ru', {minimumFractionDigits: 2,maximumFractionDigits: 2}));
        // }
  });
  $('.js-type-pack:checked').trigger('change');
  $(document).on('change', '.js-opts', function () {
    $('.js-type-pack:checked').trigger('change');
  });

  //сортировка
  $(document).on('change', 'input[name="sort"]', function () {
    let params = mSearch2.getFilters(),
        tmp = [];
        delete(params['sort']);
        mSearch2.Hash.remove('sort');

        $('input[name="sort"]').each(function(){
          if($(this).val()){
            tmp.push($(this).val());
          }
        });

        params['sort'] = tmp.join(mse2Config['values_delimeter']);
        mse2Config['sort'] = tmp.join(mse2Config['values_delimeter']);
        mSearch2.Hash.set(params);
        mSearch2.load(params);

        if(mse2Config['sort'].toLowerCase() == mse2Config['start_sort'].toLowerCase()){
            mse2Config['sort'] = '';
            mSearch2.Hash.remove('sort');
        }
  });
  //установка селекта сортировки при загрузке страницы
  if( $('input[name="sort"]').length ){
      if (getUrlParameter('sort') != 'undefined' && getUrlParameter('sort')){
          let sort = getUrlParameter('sort');
          let sorts = sort.split(mse2Config['values_delimeter']);
            for (let i = 0; i < sorts.length; i++) {
                let tmp = sorts[i].split(mse2Config['method_delimeter']);
                if (tmp[0] && tmp[1]) {
                  let $this = $('input[value="'+tmp[0]+':'+tmp[1]+'"]+label');
                  $('#' + $this.attr('for')).attr('checked', true);
                  $this
                    .closest('.catalog-sorting__control')
                    .find('.catalog-sorting__control-text')
                    .text($('#' + $this.attr('for')).attr('data-title'));
                  $this
                    .closest('.catalog-sorting__control')
                    .find('.catalog-sorting__control-value')
                    .attr('value', $('#' + $this.attr('for')).attr('value'));
                }
            }
      }
  }
  if( $('#mse2_results').find('.js-not-result').length ){
    $('.js-sorting').hide();
    $('.js-yes-search').hide();
    $('.js-not-search').show();
  }
  else{
    $('.js-yes-search').show();
    $('.js-not-search').hide();
  }

  var getCostPosition = function(product_key, act){
    $.post('/assets/components/cuptrade/change.php', { action: 'getcost'/*, product_key: resp.data.key*/ }, function(data) {
        if(data.success){
            let prop = data.data.data;
            $('.js-total_cart').text((prop.total_cart).toLocaleString("ru-RU"));
            $('.js-total_cost').text((prop.total_cost).toLocaleString("ru-RU"));
            $('.js-total_discount').text((prop.total_discount).toLocaleString("ru-RU"));
            if(product_key && data.data.html){
              //console.log(data.data.html);
              //$('#msCart').find('#' + product_key).replaceWith($(data.data.html).find('#' + product_key));
              $('#msCart').replaceWith(data.data.html);
            }
            else if((act == 'remove') && data.data.html){
              $('#msCart').replaceWith(data.data.html);
            }
            frmtPrice();
        }
        else{
            document.location.reload();
        }
    })
    .fail(function() {
        console.error('Не удаётся запросить стоимость товаров в корзине');
    });
  }
  // удаление из корзины
  // выбранной позиции
  $(document).on('click touch', '.js-cart-delete', function(e, prop){
      let key = $(this).parents('.cart-position').attr('id'),
          $this = $(this);
      let $form = $this.parents('form.ms2_form'),
      formData = $form.serializeArray(),
        action = 'cart/remove';
        formData.push({"name":"action", "value":action});
        miniShop2.sendData = {
            $form: $form,
            action: action,
            formData: formData
        };

        $.post('/assets/components/cuptrade/delete.php', { action: 'remove', product_key: key }, function(data) {
              if(data.success){
                  if(typeof data.data.html != 'undefined'){
                      $('body').find('.cart-position:last').parent().append(data.data.html);
                  }
                  miniShop2.Cart.remove();
              }
              else{
                  miniShop2.Cart.remove();
              }
          })
          .fail(function() {
              console.error('Не удаётся удалить позицию');
          });
  });
  //отмена удаления из корзины
  $(document).on('click touch', '.js-cancel-remove', function(){
      let key = $(this).attr('data-key'),
          $this = $(this);
        $.post('/assets/components/cuptrade/delete.php', { action: 'cancel', product_key: key }, function(data) {
            if(data.success){
              document.location.reload();
            }
            else{
              $this.parents('.cart-position__cancel').remove();
              alert(data.message);
            }
          })
          .fail(function() {
              console.error('Не удаётся восстановить позицию');
          });
  });

  // корзина: изменение кол-ва товаров в корзине
  $(document).on('change', '#msCart .js-productCount_Number', function(e) {
    if(!$(this).val() || $(this).val() < 1){
      return false;
    }
    let $form = $(this).parents('form.ms2_form'),
        formData = $form.serializeArray(),
        name = miniShop2.actionName;
          action = formData.name;
      miniShop2.sendData = {
          $form: $form,
          action: action,
          formData: formData
      };
    miniShop2.Cart.change();
  });

  // корзина: изменение опции товара
  $(document).on('change', '#msCart .js-opts', function(e) {
    if(!$(this).val()){
      return false;
    }
    if(!$(this).parents('.cart-position').attr('id')){
      return false;
    }

    let $this = $(this),
        key = $this.parents('.cart-position').attr('id'),
        value = $this.val(),
        action = ($this.is(':checked')?'addopts':'removeopts');
    $.post('/assets/components/cuptrade/change.php', { action: action, product_key: key, value: value }, function(data) {
        if(data.success){
          if(data.data.html && data.data.data.new_key){
            $('#msCart').find('#' + data.data.data.product_key).replaceWith($(data.data.html).find('#' + data.data.data.new_key));
          }
          getCostPosition(key);
        }
        else{
          alert(data.message);
        }
      })
      .fail(function() {
          console.error('Не удаётся добавить опцию');
      });
  });

  //для форм
  $("form:not(.pagination-switch__form):not(.search-form)").each(function(index, el) {
    setTimeout(function() {
        $(el).prepend('<input type="hidden" name="idPage" value="'+ID_PAGE+'">');
    }, 500);
    setTimeout(function() {
        $(el).prepend('<input type="hidden" name="toolsss" value="'+HTTP_HOST+'">');
    }, 8000);
	});

//для промокода WhatsApp
function replaceQueryParam(url, param, value) {
    var explodedUrl = url.split('?');
    var baseUrl = explodedUrl[0] || '';
    var query = '?' + (explodedUrl[1] || '');
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
    var queryWithoutParameter = query.replace(regex, "$1").replace(/&$/, '');
    return baseUrl + (queryWithoutParameter.length > 2 ? queryWithoutParameter  + '&' : '?') + (value ? param + "=" + value : '');
}

window.onRoistatAllModulesLoaded = function () {
    window.roistat.registerOnVisitProcessedCallback(function() {
        var message = 'Мой промокод на скидку: {roistat_visit}';
      	var text = message.replace(/{roistat_visit}/g, window.roistat.getVisit());
        var linkElements = document.querySelectorAll('[href*="//api.whatsapp.com/send"]');
        for (var elementKey in linkElements) {
            if (linkElements.hasOwnProperty(elementKey)) {
                var element = linkElements[elementKey];
                element.href = replaceQueryParam(element.href, 'text', text);
            }
        }
    });
};
// $(function () {
// miniShop2.Callbacks.Order.submit.response.success = function (res) {
//   // console.log(res)
// }
// });
