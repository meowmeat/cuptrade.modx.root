{extends 'file:templates/index.tpl'}
{block 'class'}delivery-page{/block}
{block 'main'}
<body class="delivery-page">
  {set $about = json_decode($_modx->resource.deliverypayment, true)[0]}
  {set $bread = $_modx->getChunk('@FILE chunks/all/breadcrumbs.tpl')}
  {if $about.slide}
    {set $slide = json_decode($about.slide, true)[0]}
    <div class="delivery-promo-section section" {if $slide.img}style="background:url('{$slide.img}') 50% 50% no-repeat;background-size: cover;"{/if}>
      <img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_lines_square_white.svg" alt="" class="delivery-promo__shape">
      <img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_lines_square_white.svg" alt="" class="delivery-promo__shape2">
      {$bread | replace:"class=\"breadcrumbs\"":"class=\"breadcrumbs breadcrumbs--white section\"" | replace:"grey":"white"}
      <div class="section-wrapper">
        <div class="delivery-promo">
          <h1 class="title promo-title">{$slide.title?:$_modx->resource.pagetitle}</h1>
          <div class="promo-subtitle">{$slide.desc | strip_tags:"<br><b><i><em><s><u><strong>"}</div>
        </div>
      </div>
    </div>
  {else}
    {$bread}
  {/if}

  {if $deliveries = json_decode($about.delivery, true)}
    <div class="delivery-section section">
      <img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_linnes_red_about.svg" alt="" class="delivery-section__shape">
      <div class="section-wrapper">
        <div class="delivery-title"><span class="delivery-title__text">Доставка</span></div>
        <div class="delivery-wrap">
          {foreach $deliveries as $deliv}
          <div class="delivery-item">
            {if $deliv.img}<img src="{$deliv.img}" alt="{$deliv.title | htmlent}" class="delivery-item__ico">{/if}
            <div class="delivery-item__info">
              <div class="delivery-item__title">{$deliv.title}</div>
              <div class="delivery-item__text">{$deliv.desc}</div>
              {if $deliv.subdesc}<div class="delivery-item__note">{$deliv.subdesc}</div>{/if}
            </div>
          </div>
          {/foreach}
        </div>
      </div>
    </div>
  {/if}

  {if $payments = json_decode($about.payment, true)}
    <div class="delivery-section section">
      <div class="section-wrapper">
        <div class="delivery-title"><span class="delivery-title__text">Оплата</span></div>
        <div class="delivery-wrap">
          {foreach $payments as $pay}
            <div class="delivery-item">
              <img src="{$pay.img}" alt="{$pay.title | htmlent}" class="delivery-item__ico">
              <div class="delivery-item__info">
                <div class="delivery-item__title">{$pay.title}</div>
                <div class="delivery-item__text">{$pay.desc}</div>
                {if $pay.subdesc}<div class="delivery-item__note">{$pay.subdesc}</div>{/if}
              </div>
            </div>
          {/foreach}
        </div>
      </div>
    </div>
  {/if}

  {set $contacts = $_modx->getChunk('@FILE chunks/all/contacts.tpl')}
  {$contacts | lazy:true}
{/block}