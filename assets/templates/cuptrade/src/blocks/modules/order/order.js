//==== order ====//
//== Switching tabs for order delivery
$(document).on('click touch', '.js-tab_Switch', function () {
  let switcher_Active = $(this);
  let switchingTab = $(this).attr('data-switching-tab');
  let switchingTab_Active = $(this)
    .closest('.order-delivery__switcher')
    .siblings('#' + switchingTab);
  if (!switcher_Active.hasClass('order-delivery__switch--active')) {
    $('.order-delivery__switch')
      .not(switcher_Active)
      .removeClass('order-delivery__switch--active');
    switcher_Active.addClass('order-delivery__switch--active');
    $('.order-delivery__tab')
      .not(switchingTab_Active)
      .removeClass('order-delivery__tab--active');
    switchingTab_Active.addClass('order-delivery__tab--active');
  }
});
