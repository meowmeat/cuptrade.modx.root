//== this function called in a file: script-closing
function currentScreenHeight() {
  let screenHeight = $(window).innerHeight();
  return screenHeight;
}

function changeLogo(th) {
  filepath = th.value.split('\\').pop().split('/').pop();
  const lastDot = filepath.lastIndexOf('.');

  const fileName = filepath.substring(0, lastDot);
  const ext = filepath.substring(lastDot + 1);
  
  $('#file_upl_value')[0].value = fileName;

  $(th).prev().hide() // hide button
  $(th).next().show()
}
function clearLogo(th) {
  $('[for="file_upl"]').show();
  $('#uploaded-logo').hide();
  $('#file_upl_value')[0].value = null;
}


//== Phone Links: remove phone attribute on desktop
// if (screen.width > 1030) $('a[href^="tel:"]').removeAttr('href');

//== Masked Phone input
$('input[type="tel"], .js-phone').inputmask({
  mask: '+7 999 999 99 99',
  clearMaskOnLostFocus: true,
});

/*=include form-validation.js */

//== Check number format for input - calling in input
function checkingNumberFormat(e) {
  e = e || window.event;
  var charCode = e.which ? e.which : e.keyCode;
  return /\d/.test(String.fromCharCode(charCode));
}

$.fancybox.defaults.loop = true;

/*=include modal-forms.js */
/*=include offer-top.js */
/*=include header.js */
/*=include sitenav.js */
/*=include main-page.js */
/*=include gallery.js */
/*=include catalog.js */
/*=include product.js */
/*=include manufacture.js */
/*=include content.js */
/*=include sidebar.js */
/*=include sidebar-mob.js */
/*=include about.js */
/*=include contacts.js */
/*=include main.js */

/*
 *
 *
 */
/*=include script-closing.js */
