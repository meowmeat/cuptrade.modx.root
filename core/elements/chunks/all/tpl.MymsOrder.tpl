<div class="section-twin__main">
  <a name="form"></a>
          <div class="order">
            <form id="checkout__final" method="post" enctype="multipart/form-data" class="order-form js-validateForm_Policy">
              <div class="order-form__person">
                <div class="order-form__title">Персональные данные</div>
                <div class="form-field form-field--invalid">
                  <input type="text" name="receiver" value="{$form['receiver']}" id="formOrder_name" class="form-input order-form__input" placeholder="Ваше имя*" data-req>
                  <div class="form-field__prompt">Заполните поле</div>
                </div>
                <div class="form-field form-field--invalid">
                  <input type="email" name="email" value="{$form['email']}" id="formOrder_mail" class="form-input order-form__input" placeholder="E-mail*" data-req>
                  <div class="form-field__prompt">Заполните поле</div>
                </div>
                <div class="form-field form-field--invalid">
                  <input type="tel" name="phone" value="{$form['phone']}" id="formOrder_phone" class="form-input order-form__input" placeholder="Телефон*" data-req>
                  <div class="form-field__prompt">Заполните поле</div>
                </div>
              </div>

              <div class="order-form__delivery">
                <div class="order-form__title">Доставка</div>
                {foreach $deliveries as $delivery index=$index}
                    {*$delivery | print*}
                    {var $checked = !($order.delivery in keys $deliveries) && $index == 0 || $delivery.id == $order.delivery}
                    {if $checked}{set $delivery_id = $delivery.id}{/if}
                    <div class="form-option__switcher-position">
                      <input data-distance_price="{$delivery.distance_price?:0}" data-price="{$delivery.price?:0}" type="radio" class="form-option__switcher" name="delivery" value="{$delivery.id}" id="delivery_{$delivery.id}" data-payments="{$delivery.payments | json_encode}"
                                                {$checked ? 'checked' : ''}>
                      <label for="delivery_{$delivery.id}" class="form-option__switcher-label">{$delivery.name} {if $delivery.description}({$delivery.description}){/if}</label>
                    </div>
                {/foreach}
              </div>

              <div class="js-address-wrap">
                <div class="order-form__title--borderless">Адрес доставки</div>
                <div class="form-field">
                  <textarea required name="address" id="address" rows="10" placeholder="Адрес" class="form-input form-input__textarea order-form__textarea">{$form.address}</textarea>
                  <input type="hidden" name="street" value="{$form.street}">
                </div>
              </div>

              <div class="order-form__payment">
                <div class="order-form__title">Оплата</div>
                {foreach $payments as $payment index=$index}
                    {*$payment | print*}
                    {var $checked = !($order.payment in keys $payments) && $index == 0 || $payment.id == $order.payment}
                    {if $checked}{set $payment_id = $payment.id}{/if}
                    <div class="form-option__switcher-position">
                      <input type="radio" class="form-option__switcher" name="payment" value="{$payment.id}" id="payment_{$payment.id}" {$checked ? 'checked' : ''}>
                      <label for="payment_{$payment.id}" class="form-option__switcher-label">{$payment.name}</label>
                    </div>
                {/foreach}
              </div>

              <div class="order-form__title--borderless">Комментарии к заказу</div>
              <div class="form-field">
                <textarea name="comment" id="formOrder_comment" rows="10" placeholder="Комментарии" class="form-input form-input__textarea order-form__textarea">{$form.comment}</textarea>
              </div>

              <div class="form-policy">
                <input type="checkbox" name="policy_check" id="formOrder_policy" class="form-policy__checkbox" value="1" required checked data-req>
                <label for="formOrder_policy" class="form-policy__checkbox-label">
                  Я согласен с <a href="{5|url}" class="form-policy__link">обработкой персональных данных</a>
                </label>
              </div>

              <input type="hidden" name="formOrder_hidden" class="form-input--hide">
              <input type="submit" name="formOrder_submit" class="button button-primary order-form__submit" value="Отправить">
              <input type="hidden" name="ms2_action" value="order/submit">
              <input type="hidden" name="roistat_visit" value="{$.cookie.roistat_visit}">
            </form>
          </div>
        </div>