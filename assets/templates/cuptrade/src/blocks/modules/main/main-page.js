//== Change images in main nav menu
$('.main-nav-wide__link').on({
  mouseenter: function () {
    let mainNavLink_Image = this.getAttribute('data-mainnav-img');
    let mainNavLink_Image_Active = $('#' + mainNavLink_Image);
    $('.main-nav-wide__image-main').removeClass('main-nav-wide__image--active');
    $(this).addClass('main-nav-wide__link--active');
    mainNavLink_Image_Active.toggleClass('main-nav-wide__image--active');
    // mainNavLink_Image_Active
    //   .addClass('slideInRight');
  },
  mouseleave: function () {
    let mainNavLink_Image = this.getAttribute('data-mainnav-img');
    let mainNavLink_Image_Active = $('#' + mainNavLink_Image);
    $(this).removeClass('main-nav-wide__link--active');
    // mainNavLink_Image_Active
    //   .removeClass('slideInRight');
    mainNavLink_Image_Active.removeClass('main-nav-wide__image--active');
    setTimeout(function () {
      if (!$('.main-nav-wide__link').hasClass('main-nav-wide__link--active')) {
        $('.main-nav-wide__image-main').addClass(
          'main-nav-wide__image--active'
        );
      }
    }, 200);
  },
});

//== Carousel for Product Novelty
var mainNoveltyCarousel = new Swiper('.main-novelty > .swiper-container', {
  loop: false,
  slidesPerView: 4,
  spaceBetween: 30,
  navigation: {
    nextEl: '.main-novelty__next',
    prevEl: '.main-novelty__prev',
  },
  breakpoints: {
    // when window width is >= value px etc
    1025: {
      grid: {
        rows: 2,
        fill: 'row',
      },
      enabled: false
    },
    761: {
      slidesPerView: 3,
      spaceBetween: 20,
    },
    421: {
      slidesPerView: 2,
      spaceBetween: 14,
    },
    319: {
      slidesPerView: 1,
      spaceBetween: 10,
    },
  },
});

//== Main About section - remove accented-class items
//== this function called in a file: script-closing
function mainAboutAccent_remove() {
  let mainAboutItem = $('.main-about').find('.main-about__item');
  if ($(window).width() <= 560) {
    if (mainAboutItem.hasClass('main-about__item--accent')) {
      mainAboutItem.removeClass('main-about__item--accent');
    } else {
      mainAboutItem.addClass('main-about__item--accent');
    }
  }
}

//== Main page Callback Form switchers toggle
$(document).on('click touch', '.js-callbackFormSwitcher_Toggle', function () {
  let callbackFormSwitcher = $(this)
    .closest('.main-callback-form__switchers')
    .find('.main-callback-form__switcher');
  let callbackFormSwitcher_Active = $(this);
  if (callbackFormSwitcher.hasClass('main-callback-form__switcher--active')) {
    callbackFormSwitcher
      .not(callbackFormSwitcher_Active)
      .removeClass('main-callback-form__switcher--active');
    callbackFormSwitcher_Active.addClass(
      'main-callback-form__switcher--active'
    );
  } else {
    callbackFormSwitcher.removeClass('main-callback-form__switcher--active');
  }

  let callbackFormField_mail = callbackFormSwitcher_Active
    .closest('.main-callback-form')
    .find('.main-callback-form__field-mail');
  let callbackFormField_phone = callbackFormSwitcher_Active
    .closest('.main-callback-form')
    .find('.main-callback-form__field-phone');
  if (
    callbackFormSwitcher_Active.data('contacts-fieldhide') ===
    'contasts-form-mail'
  ) {
    callbackFormField_mail.addClass('main-callback-form__field--hidden');
    callbackFormField_mail
      .find('.main-callback-form__input')
      .removeAttr('data-req');
    callbackFormField_phone.removeClass('main-callback-form__field--hidden');
    callbackFormField_phone
      .find('.main-callback-form__input')
      .attr('data-req', '');
  }
  if (
    callbackFormSwitcher_Active.data('contacts-fieldhide') ===
    'contasts-form-phone'
  ) {
    callbackFormField_phone.addClass('main-callback-form__field--hidden');
    callbackFormField_phone
      .find('.main-callback-form__input')
      .removeAttr('data-req');
    callbackFormField_mail.removeClass('main-callback-form__field--hidden');
    callbackFormField_mail
      .find('.main-callback-form__input')
      .attr('data-req', '');
  }

  if (callbackFormSwitcher_Active.data('switcher-note') === 'telephone') {
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('.main-callback-form__switchers-note')
      .text('Свяжемся по телефону.');
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('input[name="type"]')
      .val('Свяжемся по телефону.');
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('input[name="name_form"]')
      .val('Связаться по телефону');
  }
  if (callbackFormSwitcher_Active.data('switcher-note') === 'email') {
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('.main-callback-form__switchers-note')
      .text('Отправим электронное письмо.');
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('input[name="type"]')
      .val('Отправим электронное письмо.');
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('input[name="name_form"]')
      .val('Связаться по почте');
  }
  if (callbackFormSwitcher_Active.data('switcher-note') === 'whatsapp') {
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('.main-callback-form__switchers-note')
      .text('Напишем в WhatsApp, никаких звонков!');
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('input[name="type"]')
      .val('Напишем в WhatsApp, никаких звонков!');
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('input[name="name_form"]')
      .val('Связаться по WhatsApp');
  }
});
