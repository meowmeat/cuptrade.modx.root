<?php
$input = $modx->getChildIds(2, 1, array('context' => $modx->context->key));
$rand_keys = array_rand($input, 1);
$cat = $modx->getObject('modResource', array('id' => $input[$rand_keys], 'published' => 1, 'deleted' => 0));
$products = $modx->getChildIds($input[$rand_keys], 3, array('context' => $modx->context->key));
while(!$cat || !$products){
  $rand_keys = array_rand($input, 1);
  $cat = $modx->getObject('modResource', array('id' => $input[$rand_keys], 'published' => 1, 'deleted' => 0));
  $products = $modx->getChildIds($input[$rand_keys], 3, array('context' => $modx->context->key));
}
return ['parent' => $input[$rand_keys], 'title' => $cat->pagetitle];