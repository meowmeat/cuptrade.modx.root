{extends 'file:templates/index.tpl'}
{block 'class'}thanks-page{/block}
{block 'main'}
  <div class="thanks-section section">
    <div class="section-wrapper">
      <div class="thanks">
        <div class="thanks__title">
          {if $.get.msorder}
            <span class="thanks__title-first">Спасибо,</span>&nbsp;заказ&nbsp;
            <span class="thanks__title-order">{'!msGetOrder' | snippet : [
    'tpl' => '@INLINE {$order.num}'
    ]}</span>&nbsp;оформлен!
          {else}
            <span class="thanks__title-first">Спасибо,</span>&nbsp;мы&nbsp;свяжемся с вами!
          {/if}
        </div>
        {if $.session.cuptrade_code}
          <div class="thanks-order">
            <div class="thanks-order__text">Ваш уникальный код:</div>
            <div class="thanks-order__number">{$.session.cuptrade_code}</div>
          </div>
        {/if}
        <div class="thanks__text">В рабочее время мы отвечаем в течение часа, но если вы совсем не хотите ждать, позвоните нам по номеру <a href="tel:{$_modx->config['phone']}" class="js-phone">{$_modx->config['phone'] | replace:"+7":""}</a>{if $_modx->config['whatsapp']} или напишите в <a href="https://api.whatsapp.com/send?phone={$_modx->config['whatsapp']}">WhatsApp</a>{/if}, назвав сотруднику свой уникальный код, и получите расчёт ещё быстрее!</div>

        <div class="thanks__buttons">
          <a href="/" class="button button-primary thanks__button">
            <svg width="6" height="11" viewBox="0 0 8 12" fill="#fff" xmlns="http://www.w3.org/2000/svg" class="ico-svg thanks__button-ico">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M6.079 11.693l1.442-1.386L3.386 6l4.135-4.307L6.08.307.614 6l5.465 5.693z" />
            </svg>
            Вернуться на главную
          </a>
          {if $_modx->config['insta']}
          <a href="{$_modx->config['insta']}" target="_blank" class="button button-outlined thanks__button-inst">
            <svg width="21" height="21" viewBox="0 0 27 26" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg thanks__button-ico">
              <path d="M4.487.682a6.376 6.376 0 00-2.304 1.501 6.357 6.357 0 00-1.501 2.3c-.323.824-.54 1.77-.603 3.155C.016 9.023 0 9.467 0 12.998c0 3.53.016 3.974.08 5.359.063 1.385.285 2.33.602 3.155.333.857.777 1.58 1.501 2.305a6.375 6.375 0 002.304 1.501c.825.323 1.771.54 3.156.603 1.385.063 1.829.079 5.36.079 3.53 0 3.974-.016 5.36-.08 1.384-.063 2.33-.285 3.155-.602a6.375 6.375 0 002.304-1.501 6.374 6.374 0 001.501-2.305c.323-.824.54-1.77.603-3.155.063-1.385.08-1.829.08-5.36 0-3.53-.017-3.974-.08-5.36-.063-1.384-.285-2.33-.603-3.155a6.41 6.41 0 00-1.495-2.299A6.376 6.376 0 0021.523.682c-.825-.323-1.77-.54-3.155-.603C16.983.016 16.538 0 13.008 0c-3.53 0-3.975.016-5.36.08-1.39.057-2.336.28-3.16.602zm13.77 1.734c1.268.058 1.955.27 2.415.449a4.05 4.05 0 011.496.972c.454.455.735.888.973 1.496.18.46.39 1.147.449 2.416.063 1.369.074 1.781.074 5.254 0 3.472-.016 3.885-.074 5.254-.058 1.268-.27 1.955-.45 2.415a4.05 4.05 0 01-.972 1.496 4.05 4.05 0 01-1.496.973c-.46.18-1.147.39-2.415.449-1.37.063-1.782.074-5.254.074-3.473 0-3.885-.016-5.254-.074-1.269-.058-1.956-.27-2.416-.45a4.05 4.05 0 01-1.496-.972 4.05 4.05 0 01-.972-1.496c-.18-.46-.391-1.147-.45-2.415-.063-1.37-.073-1.782-.073-5.254 0-3.473.015-3.885.074-5.254.058-1.269.27-1.956.449-2.416a4.05 4.05 0 01.972-1.496 4.05 4.05 0 011.496-.972c.46-.18 1.147-.391 2.416-.45 1.369-.063 1.781-.073 5.254-.073 3.472 0 3.885.01 5.254.074z" />
              <path d="M6.327 13.003a6.676 6.676 0 1013.352 0 6.676 6.676 0 00-13.352 0zm11.01 0a4.333 4.333 0 01-4.334 4.334 4.333 4.333 0 01-4.334-4.334 4.333 4.333 0 014.334-4.335 4.333 4.333 0 014.334 4.335zM19.948 7.622a1.56 1.56 0 100-3.119 1.56 1.56 0 000 3.119z" />
            </svg>
            Подписаться в Instagram
          </a>
          {/if}
        </div>
      </div>
    </div>
  </div>
{/block}