<?php
require_once 'pdopage.class.php';
class pdoPageCustom extends pdoPage
{
    public function loadJsCss()
    {
        $assetsUrl = !empty($this->pdoTools->config['assetsUrl'])
            ? $this->pdoTools->config['assetsUrl']
            : MODX_ASSETS_URL . 'components/pdotools/';
        if ($css = trim($this->pdoTools->config['frontend_css'])) {
            $this->modx->regClientCSS(str_replace('[[+assetsUrl]]', $assetsUrl, $css));
        }
        if ($js = trim($this->pdoTools->config['frontend_js'])) {
            $this->modx->regClientScript(str_replace('[[+assetsUrl]]', $assetsUrl, $js));
        }
        $ajaxHistory = $this->pdoTools->config['ajaxHistory'] === ''
            ? !in_array($this->pdoTools->config['ajaxMode'], array('scroll', 'button'))
            : !empty($this->pdoTools->config['ajaxHistory']);
        $limit = $this->pdoTools->config['limit'] > $this->pdoTools->config['maxLimit']
            ? $this->pdoTools->config['maxLimit']
            : $this->pdoTools->config['limit'];
        $moreChunk = $this->modx->getOption('ajaxTplMore', $this->pdoTools->config,
            '@INLINE <button class="btn btn-default btn-more">[[%pdopage_more]]</button>'
        );
        $moreTpl = $this->pdoTools->getChunk($moreChunk, array('limit' => $limit));

        $hash = sha1(json_encode($this->pdoTools->config));
        $_SESSION['pdoPage'][$hash] = $this->pdoTools->config;

        $config = array(
            'wrapper' => $this->modx->getOption('ajaxElemWrapper', $this->pdoTools->config, '#pdopage'),
            'rows' => $this->modx->getOption('ajaxElemRows', $this->pdoTools->config, '#pdopage .rows'),
            'pagination' => $this->modx->getOption('ajaxElemPagination', $this->pdoTools->config,
                '#pdopage .pagination'),
            'link' => $this->modx->getOption('ajaxElemLink', $this->pdoTools->config, '#pdopage .pagination a'),
            'more' => $this->modx->getOption('ajaxElemMore', $this->pdoTools->config, '#pdopage .btn-more'),
            'moreTpl' => $moreTpl,
            'mode' => $this->pdoTools->config['ajaxMode'],
            'history' => (int)$ajaxHistory,
            'pageVarKey' => $this->pdoTools->config['pageVarKey'],
            'pageLimit' => $limit,
            'assetsUrl' => $assetsUrl,
            'connectorUrl' => rtrim($assetsUrl, '/') . '/connector.php',
            'pageId' => $this->modx->resource->id,
            'hash' => $hash,
            'scrollTop' => (bool)$this->modx->getOption('scrollTop', $this->pdoTools->config, true),
        );

        if (empty($this->pdoTools->config['frontend_startup_js'])) {
            $this->modx->regClientStartupScript(
                '<script type="text/javascript">pdoPage = {callbacks: {}, keys: {}, configs: {}};</script>', true
            );
        } else {
            $this->modx->regClientStartupScript(
                $this->pdoTools->getChunk($this->pdoTools->config['frontend_startup_js']), true
            );
        }

        if (empty($this->pdoTools->config['frontend_init_js'])) {
            // $idx = $this->modx->getPlaceholder('_AjaxFormInit');
            // $this->modx->regClientScript(
            //     '<script>function initPdoPage(){ pdoPage.initialize(' . json_encode($config) . ');}</script>', true
            // );
            // $script = '';
            // for ($i = 1; $i <= $idx; $i++) {
            //     $script .= "initAjaxForm{$i}();";
            // }
            // $script .= "initPdoPage();";
            // $this->modx->regClientScript(
            //     "<script>function initAjaxForm(){ {$script}}</script>", true
            // );
            /** @noinspection Annotator */
            $this->modx->regClientScript(
                '<script type="text/javascript">window.onload = function() { pdoPage.initialize(' . json_encode($config) . '); }</script>', true
            );
        } else {
            $this->modx->regClientScript(
                $this->pdoTools->getChunk($this->pdoTools->config['frontend_init_js'], array(
                    'key' => $config['pageVarKey'],
                    'wrapper' => $config['wrapper'],
                    'config' => json_encode($config),
                )), true
            );
        }
    }
}
