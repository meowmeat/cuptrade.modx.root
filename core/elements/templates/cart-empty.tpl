{extends 'file:templates/index.tpl'}
{block 'class'}cart-empty-page{/block}
{block 'main'}
  <div class="cart-empty section">
    <div class="cart-empty-wrapper">
      <img src="assets/templates/cuptrade/public/assets/cuptrade/img/cups.png" alt="" class="cart-empty__img">
      <div class="cart-empty__title">В корзине пока ничего нет</div>
      <div class="cart-empty__text">Вы можете найти товары на главной, либо воспользоваться поиском, если ищете что-то конкретное.</div>
      <a href="{2 | url}" class="button button-primary cart-empty__button">Выбрать товары</a>
    </div>
  </div>

  {set $similar = $_modx->runSnippet('@FILE snippets/random.php')}
  {if $similar}
  <div class="product-similar-section section">
    <img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_lines_square.svg" alt="" class="product-similar__shape">
    <div class="section-wrapper">
      <h2 class="product-section__title">{$similar.title}</h2>
      {'!msProductsCustom' | snippet : [
      'wrapIfEmpty' => 0,
      'parents' => $similar.parent,
      'limit' => 8,
      'includeContent' => 1,
      'tplWrapper' => '@INLINE <div class="product-similar">
        <div class="swiper-container swiper-container-similar">
          <div class="swiper-wrapper swiper-wrapper-similar">{$output}</div>
          <div class="product-similar__controls">
            <div class="product-similar__nav product-similar__prev"><img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_left.svg" alt="" class="product-similar__prev-ico"></div>
            <div class="product-similar__nav product-similar__next"><img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_right.svg" alt="" class="product-similar__next-ico"></div>
          </div>
        </div>
      </div>',
      'tpl' => '@FILE chunks/all/tpl.goods_card.tpl'
  ] | replace:"catalog-card\"":"catalog-card swiper-slide\""}
    </div>
  </div>
  {/if}
{/block}