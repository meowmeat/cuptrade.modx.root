<?php return array (
  '4a42b27e73423e822e03ab67cf7c6feb' => 
  array (
    'criteria' => 
    array (
      'name' => 'seosuite',
    ),
    'object' => 
    array (
      'name' => 'seosuite',
      'path' => '{core_path}components/seosuite/',
      'assets_path' => '{assets_path}components/seosuite/',
    ),
  ),
  '90f6edc0d5b4239bac07f289b3073a20' => 
  array (
    'criteria' => 
    array (
      'name' => 'seosuite',
    ),
    'object' => 
    array (
      'id' => 6,
      'name' => 'seosuite',
      'description' => 'SEO Suite dashboard widget',
      'type' => 'file',
      'content' => '[[++core_path]]components/seosuite/elements/widgets/seosuite.widget.php',
      'namespace' => 'seosuite',
      'lexicon' => 'seosuite:default',
      'size' => 'half',
      'name_trans' => 'seosuite',
      'description_trans' => 'SEO Suite dashboard widget',
    ),
  ),
  'a0edeb7ad78b4c3df2169449be28552d' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.blocked_words',
    ),
    'object' => 
    array (
      'key' => 'seosuite.blocked_words',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite',
      'editedon' => NULL,
    ),
  ),
  '731977d13579ba5917841b9cbcbf12cb' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.disabled_templates',
    ),
    'object' => 
    array (
      'key' => 'seosuite.disabled_templates',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite',
      'editedon' => NULL,
    ),
  ),
  '54cc4567308804538785460e7e7a054b' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.exclude_words',
    ),
    'object' => 
    array (
      'key' => 'seosuite.exclude_words',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite',
      'editedon' => NULL,
    ),
  ),
  '619d4c0206af181f1e4acb224934e1e8' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.placeholder_plugin_enabled',
    ),
    'object' => 
    array (
      'key' => 'seosuite.placeholder_plugin_enabled',
      'value' => '0',
      'xtype' => 'combo-boolean',
      'namespace' => 'seosuite',
      'area' => 'seosuite',
      'editedon' => '2021-06-29 02:58:08',
    ),
  ),
  '63fb323eccd93c05bb6695b0b3404788' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.migration_finished',
    ),
    'object' => 
    array (
      'key' => 'seosuite.migration_finished',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'seosuite',
      'area' => 'seosuite',
      'editedon' => '2021-06-28 17:18:39',
    ),
  ),
  'ce249114e5018680fd20a380184f0775' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.user_name',
    ),
    'object' => 
    array (
      'key' => 'seosuite.user_name',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite',
      'editedon' => NULL,
    ),
  ),
  '566626717b25848504617492f0687bf6' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.user_email',
    ),
    'object' => 
    array (
      'key' => 'seosuite.user_email',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite',
      'editedon' => NULL,
    ),
  ),
  '3d1ad69c699c2f6039b3af5bd0200049' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.meta.field_counters',
    ),
    'object' => 
    array (
      'key' => 'seosuite.meta.field_counters',
      'value' => 'longtitle:30|120,description:120|255',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_meta',
      'editedon' => '2021-07-22 16:00:11',
    ),
  ),
  '578902e0a8fc62be980fd75021569b35' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.meta.default_meta_description',
    ),
    'object' => 
    array (
      'key' => 'seosuite.meta.default_meta_description',
      'value' => '[[+description]]',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_meta',
      'editedon' => NULL,
    ),
  ),
  'e9b83a4482aed13b700ac28ac7e021e2' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.meta.default_meta_title',
    ),
    'object' => 
    array (
      'key' => 'seosuite.meta.default_meta_title',
      'value' => '[[+longtitle]] | [[++site_name]]',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_meta',
      'editedon' => NULL,
    ),
  ),
  '38d75b2d5a235012a19d99e16eb67eae' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.meta.default_alternate_context',
    ),
    'object' => 
    array (
      'key' => 'seosuite.meta.default_alternate_context',
      'value' => 'web',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_meta',
      'editedon' => NULL,
    ),
  ),
  '1e68028c0170255014a78067c0d16d65' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.meta.keywords_field_counters',
    ),
    'object' => 
    array (
      'key' => 'seosuite.meta.keywords_field_counters',
      'value' => 'longtitle:4,description:8,content',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_meta',
      'editedon' => NULL,
    ),
  ),
  'e1b9418e6572cb3244417a1e2ffe061b' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.meta.preview.length_desktop_description',
    ),
    'object' => 
    array (
      'key' => 'seosuite.meta.preview.length_desktop_description',
      'value' => '160',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_meta',
      'editedon' => NULL,
    ),
  ),
  '444169960767459ef8745e91761099c5' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.meta.preview.length_desktop_title',
    ),
    'object' => 
    array (
      'key' => 'seosuite.meta.preview.length_desktop_title',
      'value' => '70',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_meta',
      'editedon' => NULL,
    ),
  ),
  'a0335d8c1706cf3c3fe43f69c8f33723' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.meta.preview.length_mobile_description',
    ),
    'object' => 
    array (
      'key' => 'seosuite.meta.preview.length_mobile_description',
      'value' => '130',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_meta',
      'editedon' => NULL,
    ),
  ),
  'a528a02e2cf661d0858799805a31ef1b' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.meta.preview.length_mobile_title',
    ),
    'object' => 
    array (
      'key' => 'seosuite.meta.preview.length_mobile_title',
      'value' => '78',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_meta',
      'editedon' => NULL,
    ),
  ),
  '583f933a7ecccdd3533a240a0aa6c4ab' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.meta.searchengine',
    ),
    'object' => 
    array (
      'key' => 'seosuite.meta.searchengine',
      'value' => 'yandex',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_meta',
      'editedon' => '2021-06-28 17:20:37',
    ),
  ),
  'edcfa6279a191bb7150285e19b21b1cb' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.meta.searchmode',
    ),
    'object' => 
    array (
      'key' => 'seosuite.meta.searchmode',
      'value' => 'desktop',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_meta',
      'editedon' => NULL,
    ),
  ),
  '819979a5b2f1ac9d3daa2ab92ab75c0a' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.sitemap.default_changefreq',
    ),
    'object' => 
    array (
      'key' => 'seosuite.sitemap.default_changefreq',
      'value' => 'weekly',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_sitemap',
      'editedon' => NULL,
    ),
  ),
  'c6f5f1b4109bccb822cf0de1438ce672' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.sitemap.default_priority',
    ),
    'object' => 
    array (
      'key' => 'seosuite.sitemap.default_priority',
      'value' => '0.5',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_sitemap',
      'editedon' => NULL,
    ),
  ),
  'ad861bb2ec4d5447eae649c74cda6e6f' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.sitemap.dependent_ultimateparent',
    ),
    'object' => 
    array (
      'key' => 'seosuite.sitemap.dependent_ultimateparent',
      'value' => '0',
      'xtype' => 'combo-boolean',
      'namespace' => 'seosuite',
      'area' => 'seosuite_sitemap',
      'editedon' => NULL,
    ),
  ),
  'eda69036582df185d948286805e80702' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.sitemap.babel.add_alternate_links',
    ),
    'object' => 
    array (
      'key' => 'seosuite.sitemap.babel.add_alternate_links',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'seosuite',
      'area' => 'seosuite_sitemap',
      'editedon' => NULL,
    ),
  ),
  '5e8b7e985215c4e8c35fade24fab4b66' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.tab_social.og_types',
    ),
    'object' => 
    array (
      'key' => 'seosuite.tab_social.og_types',
      'value' => 'website',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_social',
      'editedon' => NULL,
    ),
  ),
  'e587b6fccc03d069e888674ffc29dcfd' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.tab_social.twitter_cards',
    ),
    'object' => 
    array (
      'key' => 'seosuite.tab_social.twitter_cards',
      'value' => 'summary,summary_large_image,app,player',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_social',
      'editedon' => NULL,
    ),
  ),
  'a6c4ced7c4ff01be13e317248e7a0d4f' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.tab_social.twitter_creator_id',
    ),
    'object' => 
    array (
      'key' => 'seosuite.tab_social.twitter_creator_id',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_social',
      'editedon' => NULL,
    ),
  ),
  '3740448bff8283dcc5dcdc8672d38a7a' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.tab_social.default_og_image',
    ),
    'object' => 
    array (
      'key' => 'seosuite.tab_social.default_og_image',
      'value' => 'assets/templates/cuptrade/public/assets/cuptrade/img/cups.png',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_social',
      'editedon' => '2021-06-29 03:14:50',
    ),
  ),
  '0ec0a26aba1b02848613cffb23757524' => 
  array (
    'criteria' => 
    array (
      'key' => 'seosuite.tab_social.default_twitter_image',
    ),
    'object' => 
    array (
      'key' => 'seosuite.tab_social.default_twitter_image',
      'value' => 'assets/templates/cuptrade/public/assets/cuptrade/img/cups.png',
      'xtype' => 'textfield',
      'namespace' => 'seosuite',
      'area' => 'seosuite_tab_social',
      'editedon' => '2021-06-29 03:14:51',
    ),
  ),
  'aa511d704bae24f3ff56d0635f1920b1' => 
  array (
    'criteria' => 
    array (
      'category' => 'SEO Suite',
    ),
    'object' => 
    array (
      'id' => 8,
      'parent' => 0,
      'category' => 'SEO Suite',
      'rank' => 0,
    ),
  ),
  '6a000d7d05b6c0eabcd870d29c3ba97e' => 
  array (
    'criteria' => 
    array (
      'name' => 'tplMetaTitle',
    ),
    'object' => 
    array (
      'id' => 23,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'tplMetaTitle',
      'description' => 'The default chunk used for the title.',
      'editor_type' => 0,
      'category' => 8,
      'cache_type' => 0,
      'snippet' => '<title>[[+value]]</title>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<title>[[+value]]</title>',
    ),
  ),
  '72bf72bda5f7f8c2a9e177ef6d088984' => 
  array (
    'criteria' => 
    array (
      'name' => 'tplMeta',
    ),
    'object' => 
    array (
      'id' => 24,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'tplMeta',
      'description' => 'The default chunk used for the meta.',
      'editor_type' => 0,
      'category' => 8,
      'cache_type' => 0,
      'snippet' => '<meta name="[[+name]]" content="[[+value:escape]]" />',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<meta name="[[+name]]" content="[[+value:escape]]" />',
    ),
  ),
  '14847633f0b741bbe0fc2bf4912bf6ab' => 
  array (
    'criteria' => 
    array (
      'name' => 'tplMetaSocial',
    ),
    'object' => 
    array (
      'id' => 25,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'tplMetaSocial',
      'description' => 'The default chunk used for the social meta.',
      'editor_type' => 0,
      'category' => 8,
      'cache_type' => 0,
      'snippet' => '<meta property="[[+name]]" content="[[+value:escape]]" />',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<meta property="[[+name]]" content="[[+value:escape]]" />',
    ),
  ),
  'bb6b98541e46ade52e2f458be784fef6' => 
  array (
    'criteria' => 
    array (
      'name' => 'tplLink',
    ),
    'object' => 
    array (
      'id' => 26,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'tplLink',
      'description' => 'The default chunk used for the link.',
      'editor_type' => 0,
      'category' => 8,
      'cache_type' => 0,
      'snippet' => '<link rel="[[+name]]" href="[[+value]]" [[+hreflang:notempty=`hreflang="[[+hreflang]]"`]]/>',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '<link rel="[[+name]]" href="[[+value]]" [[+hreflang:notempty=`hreflang="[[+hreflang]]"`]]/>',
    ),
  ),
  '38c0a34c6b668bb06038deda1162d775' => 
  array (
    'criteria' => 
    array (
      'name' => 'tplAlternateWrapper',
    ),
    'object' => 
    array (
      'id' => 27,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'tplAlternateWrapper',
      'description' => 'The default chunk used for wrapping alternate links',
      'editor_type' => 0,
      'category' => 8,
      'cache_type' => 0,
      'snippet' => '[[+output]]',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'static' => 0,
      'static_file' => '',
      'content' => '[[+output]]',
    ),
  ),
  '97034b791a7080e97f3b90a6598fa7da' => 
  array (
    'criteria' => 
    array (
      'name' => 'SeoSuiteMeta',
    ),
    'object' => 
    array (
      'id' => 51,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'SeoSuiteMeta',
      'description' => 'Snippet for adding the title and meta description in your website.',
      'editor_type' => 0,
      'category' => 8,
      'cache_type' => 0,
      'snippet' => '$modelPath = $modx->getOption(
        \'seosuite.core_path\',
        null,
        $modx->getOption(\'core_path\', null, MODX_CORE_PATH) . \'components/seosuite/\'
    ) . \'model/seosuite/snippets/\';
$modx->loadClass(\'SeoSuiteSnippets\', $modelPath, true, true);
$ssSnippets = new SeoSuiteSnippets($modx);

if (!$ssSnippets instanceof SeoSuiteSnippets) {
    $modx->log(xPDO::LOG_LEVEL_ERROR, \'[SeoSuiteMeta] Failed to initialize class SeoSuiteSnippets.\');
}

return $ssSnippets->seosuiteMeta($scriptProperties);',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '$modelPath = $modx->getOption(
        \'seosuite.core_path\',
        null,
        $modx->getOption(\'core_path\', null, MODX_CORE_PATH) . \'components/seosuite/\'
    ) . \'model/seosuite/snippets/\';
$modx->loadClass(\'SeoSuiteSnippets\', $modelPath, true, true);
$ssSnippets = new SeoSuiteSnippets($modx);

if (!$ssSnippets instanceof SeoSuiteSnippets) {
    $modx->log(xPDO::LOG_LEVEL_ERROR, \'[SeoSuiteMeta] Failed to initialize class SeoSuiteSnippets.\');
}

return $ssSnippets->seosuiteMeta($scriptProperties);',
    ),
  ),
  '0a69bb5a6ed2e12c80541e859c183531' => 
  array (
    'criteria' => 
    array (
      'name' => 'SeoSuiteSitemap',
    ),
    'object' => 
    array (
      'id' => 52,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'SeoSuiteSitemap',
      'description' => 'Snippet for adding a google sitemap to your website.',
      'editor_type' => 0,
      'category' => 8,
      'cache_type' => 0,
      'snippet' => '$modelPath = $modx->getOption(
        \'seosuite.core_path\',
        null,
        $modx->getOption(\'core_path\', null, MODX_CORE_PATH) . \'components/seosuite/\'
    ) . \'model/seosuite/snippets/\';
$modx->loadClass(\'SeoSuiteSnippets\', $modelPath, true, true);
$ssSnippets = new SeoSuiteSnippets($modx);

if (!$ssSnippets instanceof SeoSuiteSnippets) {
    $modx->log(xPDO::LOG_LEVEL_ERROR, \'[SeoSuiteMeta] Failed to initialize class SeoSuiteSnippets.\');
}

return $ssSnippets->seosuiteSitemap($scriptProperties);',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '$modelPath = $modx->getOption(
        \'seosuite.core_path\',
        null,
        $modx->getOption(\'core_path\', null, MODX_CORE_PATH) . \'components/seosuite/\'
    ) . \'model/seosuite/snippets/\';
$modx->loadClass(\'SeoSuiteSnippets\', $modelPath, true, true);
$ssSnippets = new SeoSuiteSnippets($modx);

if (!$ssSnippets instanceof SeoSuiteSnippets) {
    $modx->log(xPDO::LOG_LEVEL_ERROR, \'[SeoSuiteMeta] Failed to initialize class SeoSuiteSnippets.\');
}

return $ssSnippets->seosuiteSitemap($scriptProperties);',
    ),
  ),
  'dd245525a00f1e3bc2d0ef0ad7735250' => 
  array (
    'criteria' => 
    array (
      'name' => 'SEO Suite',
    ),
    'object' => 
    array (
      'id' => 9,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'SEO Suite',
      'description' => 'Plugin to handle the SEO Suite redirects.',
      'editor_type' => 0,
      'category' => 8,
      'cache_type' => 0,
      'plugincode' => '/**
 * SeoSuite
 *
 * Copyright 2019 by Sterc <modx@sterc.com>
 */
$instance = $modx->getService(\'seosuite\', \'SeoSuite\', $modx->getOption(\'seosuite.core_path\', null, $modx->getOption(\'core_path\') . \'components/seosuite/\') . \'model/seosuite/\');
if ($instance instanceof SeoSuite) {
    $instance->firePlugins($modx->event, $scriptProperties);
}',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'disabled' => 0,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/**
 * SeoSuite
 *
 * Copyright 2019 by Sterc <modx@sterc.com>
 */
$instance = $modx->getService(\'seosuite\', \'SeoSuite\', $modx->getOption(\'seosuite.core_path\', null, $modx->getOption(\'core_path\') . \'components/seosuite/\') . \'model/seosuite/\');
if ($instance instanceof SeoSuite) {
    $instance->firePlugins($modx->event, $scriptProperties);
}',
    ),
  ),
  '96f4106b04cad624d966ebdda3abe7bd' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 9,
      'event' => 'OnDocFormPrerender',
    ),
    'object' => 
    array (
      'pluginid' => 9,
      'event' => 'OnDocFormPrerender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '9d34862e33628fc6b0a785159c0863e8' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 9,
      'event' => 'OnDocFormRender',
    ),
    'object' => 
    array (
      'pluginid' => 9,
      'event' => 'OnDocFormRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '216dfea8c8e741495eb6caf17d4bae24' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 9,
      'event' => 'OnDocFormSave',
    ),
    'object' => 
    array (
      'pluginid' => 9,
      'event' => 'OnDocFormSave',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'e8a5ced34206c0135cb7b24e0f152adf' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 9,
      'event' => 'onEmptyTrash',
    ),
    'object' => 
    array (
      'pluginid' => 9,
      'event' => 'onEmptyTrash',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '726a3d6ba25545b1de9dd5dd17d7efa7' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 9,
      'event' => 'OnLoadWebDocument',
    ),
    'object' => 
    array (
      'pluginid' => 9,
      'event' => 'OnLoadWebDocument',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '42680196013c417d4a5f18bc33306e73' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 9,
      'event' => 'OnMODXInit',
    ),
    'object' => 
    array (
      'pluginid' => 9,
      'event' => 'OnMODXInit',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'ebd1438428a55a500e3f1da343fa861a' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 9,
      'event' => 'OnPageNotFound',
    ),
    'object' => 
    array (
      'pluginid' => 9,
      'event' => 'OnPageNotFound',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '181bd7f9699dc8b7d745e469b5fb5940' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 9,
      'event' => 'OnResourceBeforeSort',
    ),
    'object' => 
    array (
      'pluginid' => 9,
      'event' => 'OnResourceBeforeSort',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'ec58b420542bc33e45ae5be352713196' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 9,
      'event' => 'OnResourceDuplicate',
    ),
    'object' => 
    array (
      'pluginid' => 9,
      'event' => 'OnResourceDuplicate',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '2a6f93e9ed0a471e993c1e79d670c819' => 
  array (
    'criteria' => 
    array (
      'text' => 'seosuite.menu.seosuite',
    ),
    'object' => 
    array (
      'text' => 'seosuite.menu.seosuite',
      'parent' => 'components',
      'action' => 'home',
      'description' => 'seosuite.menu.seosuite_desc',
      'icon' => '',
      'menuindex' => 5,
      'params' => '',
      'handler' => '',
      'permissions' => '',
      'namespace' => 'seosuite',
    ),
  ),
);