//== Modal Call
$(document).on('click touch', '.js-modalCall', function () {
  $('body').addClass('no-scroll');
  let modalCalled = this.getAttribute('data-modal-called');
  let modalActive = $('#' + modalCalled);
  let modalActive_Content = modalActive.find('.modal-content');
  let modalActive_Overlay = modalActive.find('.modal-overlay');
  $('#sitenavMob').hide();
  $('#sitenavMob').removeClass('sitenav-overlay--active');
  $('#sidenavOverlay').removeClass('sitenav-overlay--active');

  modalActive.show();

  // console.log( currentScreenHeight() );
  // console.log( 'Height of modalActive '+modalActive.height() );
  modalActive_Overlay.css('height', currentScreenHeight());
  // console.log( modalActive_Content.height() );
  if (modalActive_Content.height() >= currentScreenHeight()) {
    modalActive.css('height', currentScreenHeight());
    modalActive_Overlay.css('height', 'auto');
  }
});

$(document).on('click touch', '.js-modalCall_Mob', function () {
  $('#sitenavMob').removeClass('slideInRight').addClass('slideOutRight');
  setTimeout(function () {
    $('#sitenavMob').css('height', '');
  }, 500);
  $('#sitenavOverlay').removeClass('sitenav-overlay--active').css('height', '');
  $('body').addClass('no-scroll');
  let modalCalled = this.getAttribute('data-modal-called');
  let modalActive = $('#' + modalCalled);
  let modalActive_Content = modalActive.find('.modal-content');
  let modalActive_Overlay = modalActive.find('.modal-overlay');
  modalActive.show();
  // console.log( currentScreenHeight() );
  // console.log( 'Height of modalActive '+modalActive.height() );
  modalActive_Overlay.css('height', currentScreenHeight());
  // console.log( modalActive_Content.height() );
  if (modalActive_Content.height() >= currentScreenHeight()) {
    modalActive.css('height', currentScreenHeight());
    modalActive_Overlay.css('height', 'auto');
  }
});

//== this function called in a file: script-closing
function modalOverlayHeight_OnResize() {
  let modalInstance = $('body').find('.modal');
  let modalInstance_Overlay = modalInstance.find('.modal-overlay');
  let modalInstance_Content = modalInstance.find('.modal-content');
  if (
    modalInstance.is(':visible') &&
    modalInstance_Content.outerHeight() < currentScreenHeight()
  ) {
    modalInstance.css('height', 'auto');
    modalInstance_Overlay.css('height', currentScreenHeight());
  }
  if (
    modalInstance.is(':visible') &&
    modalInstance_Content.outerHeight() >= currentScreenHeight()
  ) {
    modalInstance.css('height', currentScreenHeight());
    modalInstance_Overlay.css('height', 'auto');
  }
}

//== Call Nested Modals forms
// $('.js-modalFormNested_Call').click(function () {
//   $('#sitenavMob').hide();
//   $('#sitenavOverlay').removeClass('sitenav-overlay--active');
//   $('.modal').hide();
//   $('body').addClass('no-scroll');
//   let modalFormNested = this.getAttribute('data-modal-called');
//   $('#' + modalFormNested).show();
// });

//== Modal Close
$(document).on('click touch', '.js-modalClose', function () {
  $(this).closest('.modal').hide().css('height', '');
  $(this).closest('.modal-overlay').css({ height: '' });
  $('body').removeClass('no-scroll');
});
$(document).on('click touch', '.modal-overlay', function (e) {
  let modalContent = $('.modal-content');
  if (!modalContent.is(e.target) && modalContent.has(e.target).length === 0) {
    $(this).closest('.modal').hide().css('height', '');
    $(this).css({ height: '' });
    $('body').removeClass('no-scroll');
  }
});
