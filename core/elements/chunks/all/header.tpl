{set $modals}
  <div id="modalOrder" class="modal">
    <div class="modal-overlay">
      <div class="modal-content">
        <div class="modal-wrap">
          <div class="modal-close js-modalClose">
            <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cross.svg" alt="" class="modal-close__ico">
          </div>
          <div class="modal-form__image">
            <img src="{$_modx->resource.id == 81 ? $_modx->config['img_order_logo'] : $_modx->config['img_order']}" alt="Форма заказа" class="modal-form__img">
          </div>
          {'!AjaxFormCustom' | snippet : [
            'frontend_js' => 0,
            'frontend_css' => 0,
            'snippet' => 'FormIt',
            'hooks' => 'nospam,FormItSaveForm,email,amoCRMAddContact',
            'form' => '@INLINE <form enctype="multipart/form-data" action="" method="post" class="modal-form js-validateForm_Policy"><input type="hidden" name="cf-page" value="'~$_modx->resource.id~'"><input type="hidden" name="workemail" value="">
            <div class="modal-form__title">{$_modx->resource.id == 81 ? "Расчет стаканов с логотипом" : "Расчет стаканов с индивидуальным дизайном"}</div>
            <div class="modal-form__subtitle">Оставьте ваши данные и мы тут же подготовим и отправим вам коммерческое предложение</div>
            <div class="form-field">
              <input type="text" name="name" id="formModalOrder_name" class="form-input modal-form__input" placeholder="Ваше имя" data-req>
              <div class="form-field__prompt">Заполните поле</div>
            </div>
            <div class="form-fields-2x">
              <div class="form-field">
                <input type="tel" name="phone" id="formModalOrder_phone" class="form-input modal-form__input" placeholder="+7 ___ ___ __ __" data-req data-req-or="email">
                <div class="form-field__prompt">Заполните поле</div>
              </div>
              <div class="form-field">
                <input type="email" name="email" id="formModalOrder_email" class="form-input modal-form__input" placeholder="Почта" data-req data-req-or="phone">
                <div class="form-field__prompt">Заполните поле</div>
              </div>
            </div>
            <div class="form-field">
              <textarea name="comment" id="formModalOrder_comment" rows="10" placeholder="{$_modx->resource.id == 81 ? "Опишите задачу! Например: нужно 2000 белых однослойный стаканов 250мл с лого (прилагаю) к среде!" : "Опишите задачу! Например: нужно 1000 стаканов с нашим дизайном (пример прикрепляю) дэдлайн – конец недели!"}" class="form-input form-input__textarea modal-form__textarea"></textarea>
            </div>
            {if $_modx->resource.id != 8}
            <div class="form-field">
              <label for="file_upl" class="button button-primary">
                  {$_modx->resource.id == 81 ? "Прикрепить логотип" : "Прикрепить дизайн-макет"}
              </label>
              <input name="files" id="file_upl" type="file" style="display: none;" onchange="changeLogo(this)">
              <div id="uploaded-logo" style="display: none;position: relative;">
                <input id="file_upl_value" class="form-input modal-form__input" disabled readonly>
                <i onclick="clearLogo()" style="position: absolute; top: 14px; right: 21px;cursor: pointer;background-color: #e5e5e5;">
                  <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                      <g fill="none" stroke="#FF1818" stroke-linejoin="round" stroke-width="2">
                          <path d="M9 19.8L9-1.8M-1.8 9L19.8 9" transform="rotate(45 9 9)"/>
                      </g>
                  </svg>
                </i>
              </div>
              <div class="form-field__prompt error_files" style="display: block; position: unset; font-size: 15px; margin-top: 15px;"></div>
            </div>
            {/if}
            <div class="form-policy">
              <input type="checkbox" name="policy_check" id="formModalOrder_policy" class="form-policy__checkbox" value="1" checked data-req>
              <label for="formModalOrder_policy" class="form-policy__checkbox-label">
                Я согласен с <a href="{5|url}" target="_blank" class="form-policy__link">обработкой персональных данных</a>
              </label>
            </div>
            <input type="hidden" name="formModalOrder_hidden" class="form-input--hide">
            <button type="submit" name="frm_modalorderservice_submit" class="button button-primary modal-form__submit">Отправить заявку</button>
          </form>',
            'formSelector' => 'frm_modalorderservice_submit',
            'emailTpl' => 'cf-contacts',
            'emailSubject' => $_modx->resource.id == 81 ? "Расчет стаканов с логотипом" : "Расчет стаканов с индивидуальным дизайном",
            'emailTo' => $_modx->config['email_form']?:$_modx->config['email'],
            'emailFrom' => ('emailsender'|option),
            'emailUseFieldForSubject' => 1,
            'amoCRMmodxAmoFieldsEq' => 'name||phone||email==contact_email||email||comment||files||cf-page',
            'formFields' => 'name,phone,email,comment,cf-page',
      	    'fieldNames' => 'name==Имя,phone==Телефон,email==Email,comment==Комментарии,cf-page==ID страницы',
      	    'formName' => $_modx->resource.id == 81 ? "Расчет стаканов с логотипом" : "Расчет стаканов с индивидуальным дизайном",
      	    'validationErrorMessage' => 'Проверьте правильность заполнения формы',
      	    'customValidators' => 'formit2checkfile',
      	    'validate' => 'files:formit2checkfile',
            'validationErrorMessage' => 'Пожалуйста, исправьте ошибки!'
          ]}
        </div>
      </div>
    </div>
  </div>
  <div id="modalCallback" class="modal">
    <div class="modal-overlay">
      <div class="modal-content">
        <div class="modal-wrap">
          <div class="modal-close js-modalClose">
            <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cross.svg" alt="" class="modal-close__ico">
          </div>
          <div class="modal-form__image">
            <img src="{$_modx->config['img_callback']?:'assets/templates/cuptrade/public/assets/cuptrade/img/modal_form_img.jpg'}" alt="" class="modal-form__img">
          </div>
          {'!AjaxFormCustom' | snippet : [
            'frontend_js' => 0,
            'frontend_css' => 0,
            'snippet' => 'FormIt',
            'hooks' => 'nospam,FormItSaveForm,email,amoCRMAddContact',
            'form' => '@INLINE <form enctype="multipart/form-data" action="" method="post" class="modal-form js-validateForm_Policy"><input type="hidden" name="cf-page" value="'~$_modx->resource.id~'"><input type="hidden" name="workemail" value="">
            <div class="modal-form__title">Заказать звонок</div>
            <div class="modal-form__subtitle">Оставьте ваши данные и наш менеджер свяжется с вами</div>
            <div class="form-field">
              <input type="text" name="name" id="formModalCallback_name" class="form-input modal-form__input" placeholder="Ваше имя" data-req>
              <div class="form-field__prompt">Заполните поле</div>
            </div>
            <div class="form-field">
              <input type="tel" name="phone" id="formModalCallback_phone" class="form-input modal-form__input" placeholder="+7 ___ ___ __ __" data-req>
              <div class="form-field__prompt">Заполните поле</div>
            </div>
            <div class="form-policy">
              <input type="checkbox" name="policy_check" id="formModalCallback_policy" class="form-policy__checkbox" value="1" checked data-req>
              <label for="formModalCallback_policy" class="form-policy__checkbox-label">
                Я согласен с <a href="{5|url}" target="_blank" class="form-policy__link">обработкой персональных данных</a>
              </label>
            </div>
            <input type="hidden" name="formModalCallback_hidden" class="form-input--hide">
            <button type="submit" name="frm_modalcallback_submit" class="button button-primary modal-form__submit">Перезвоните мне</button>
          </form>',
            'formSelector' => 'frm_modalcallback_submit',
            'emailTpl' => 'cf-contacts',
            'emailSubject' => 'Заказ обратного звонка',
            'emailTo' => $_modx->config['email_form']?:$_modx->config['email'],
            'emailFrom' => ('emailsender'|option),
            'emailUseFieldForSubject' => 1,
            'validate' => 'phone:required',
            'amoCRMmodxAmoFieldsEq' => 'name||phone',
            'formFields' => 'name,phone,cf-page',
    	    'fieldNames' => 'name==Имя,phone==Телефон,cf-page==ID страницы',
    	    'formName' => 'Заказ обратного звонка',
    	    'validationErrorMessage' => 'Проверьте правильность заполнения формы'
            ]}
        </div>
      </div>
    </div>
  </div>
  <div id="modalGetExample" class="modal">
    <div class="modal-overlay">
      <div class="modal-content">
        <div class="modal-wrap">
          <div class="modal-close js-modalClose">
            <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cross.svg" alt="" class="modal-close__ico">
          </div>
          <div class="modal-form__image">
            <img src="{$_modx->config['img_example']?:'assets/templates/cuptrade/public/assets/cuptrade/img/modal_form_img.jpg'}" alt="" class="modal-form__img">
          </div>
          {'!AjaxFormCustom' | snippet : [
            'frontend_js' => 0,
            'frontend_css' => 0,
            'snippet' => 'FormIt',
            'hooks' => 'nospam,FormItSaveForm,email,amoCRMAddContact',
            'form' => '@INLINE <form enctype="multipart/form-data" action="" method="post" class="modal-form js-validateForm_Policy"><input type="hidden" name="cf-page" value="'~$_modx->resource.id~'"><input type="hidden" name="workemail" value="">
            <div class="modal-form__title">Получить образцы</div>
            <div class="modal-form__subtitle">Оставьте ваши данные и наш менеджер свяжется с вами, чтобы уточнить адрес доставки образцов</div>
            <div class="form-field">
              <input type="text" name="name" id="formModalGetExample_name" class="form-input modal-form__input" placeholder="Ваше имя" data-req>
              <div class="form-field__prompt">Заполните поле</div>
            </div>
            <div class="form-field">
              <input type="tel" name="phone" id="formModalGetExample_phone" class="form-input modal-form__input" placeholder="+7 ___ ___ __ __" data-req>
              <div class="form-field__prompt">Заполните поле</div>
            </div>
            <div class="form-policy">
              <input type="checkbox" name="policy_check" id="formModalGetExample_policy" class="form-policy__checkbox" value="1" checked data-req>
              <label for="formModalGetExample_policy" class="form-policy__checkbox-label">
                Я согласен с <a href="{5|url}" target="_blank" class="form-policy__link">обработкой персональных данных</a>
              </label>
            </div>
            <input type="hidden" name="formModalGetExample_hidden" class="form-input--hide">
            <button type="submit" name="frm_modalgetexample_submit" class="button button-primary modal-form__submit">Получить образцы</button>
          </form>',
            'formSelector' => 'frm_modalgetexample_submit',
            'emailTpl' => 'cf-contacts',
            'emailSubject' => 'Заказ образцов',
            'emailTo' => $_modx->config['email_form']?:$_modx->config['email'],
            'emailFrom' => ('emailsender'|option),
            'emailUseFieldForSubject' => 1,
            'validate' => 'phone:required',
            'formFields' => 'name,phone,cf-page',
            'amoCRMmodxAmoFieldsEq' => 'name||phone',
    	    'fieldNames' => 'name==Имя,phone==Телефон,cf-page==ID страницы',
    	    'formName' => 'Заказ образцов',
    	    'validationErrorMessage' => 'Проверьте правильность заполнения формы'
            ]}
        </div>
      </div>
    </div>
  </div>
  <div id="modalGetOptprice" class="modal">
    <div class="modal-overlay">
      <div class="modal-content">
        <div class="modal-wrap">
          <div class="modal-close js-modalClose">
            <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cross.svg" alt="" class="modal-close__ico">
          </div>
          <div class="modal-form__image">
            <img src="{$_modx->config['img_optprice']?:'assets/templates/cuptrade/public/assets/cuptrade/img/modal_form_img.jpg'}" alt="" class="modal-form__img">
          </div>
          {'!AjaxFormCustom' | snippet : [
            'frontend_js' => 0,
            'frontend_css' => 0,
            'snippet' => 'FormIt',
            'hooks' => 'nospam,FormItSaveForm,email,amoCRMAddContact',
            'form' => '@INLINE <form enctype="multipart/form-data" action="" method="post" class="modal-form js-validateForm_Policy"><input type="hidden" name="cf-page" value="'~$_modx->resource.id~'"><input type="hidden" name="workemail" value="">
            <div class="modal-form__title">Получить прайс-лист</div>
            <div class="modal-form__subtitle">Оставьте ваши данные, чтобы мы смогли направить вам наш прайс-лист</div>
            <div class="form-field">
              <input type="text" name="name" id="formModalGetOptprice_name" class="form-input modal-form__input" placeholder="Ваше имя" data-req>
              <div class="form-field__prompt">Заполните поле</div>
            </div>
            <div class="form-field">
              <input type="tel" name="phone" id="formModalGetOptprice_phone" class="form-input modal-form__input" placeholder="+7 ___ ___ __ __" data-req>
              <div class="form-field__prompt">Заполните поле</div>
            </div>
            <div class="form-policy">
              <input type="checkbox" name="policy_check" id="formModalGetOptprice_policy" class="form-policy__checkbox" value="1" checked data-req>
              <label for="formModalGetOptprice_policy" class="form-policy__checkbox-label">
                Я согласен с <a href="{5|url}" target="_blank" class="form-policy__link">обработкой персональных данных</a>
              </label>
            </div>
            <input type="hidden" name="formModalGetOptprice_hidden" class="form-input--hide">
            <button type="submit" name="frm_modalgetoptprice_submit" class="button button-primary modal-form__submit">Отправить заявку</button>
          </form>',
            'formSelector' => 'frm_modalgetoptprice_submit',
            'emailTpl' => 'cf-contacts',
            'emailSubject' => 'Заказ прайс-листа',
            'emailTo' => $_modx->config['email_form']?:$_modx->config['email'],
            'emailFrom' => ('emailsender'|option),
            'emailUseFieldForSubject' => 1,
            'validate' => 'phone:required',
            'formFields' => 'name,phone,cf-page',
            'amoCRMmodxAmoFieldsEq' => 'name||phone',
    	    'fieldNames' => 'name==Имя,phone==Телефон,cf-page==ID страницы',
    	    'formName' => 'Заказ прайс-листа',
    	    'validationErrorMessage' => 'Проверьте правильность заполнения формы'
            ]}
        </div>
      </div>
    </div>
  </div>
{/set}
{$modals | lazy:true}
  {if $top = json_decode((1|resource:"top"), true)[0]}
    <div class="offer-top-section section" style="background-color: #{$top.bg?:'000100'}; {if $.cookie.offerTop}display:none;{/if}">
      <div class="section-wrapper">
        <div class="offer-top">
          <div class="offer-top__info">
            {if $top.desc}<div class="offer-top__text">
              {$top.desc}
            </div>{/if}
            {if $top.link}<a href="{$top.link|url}" class="button button-outlined offer-top__button">
              {$top.btn_text?:'Подробнее'}
            </a>{/if}
          </div>
          {if $top.img}<img src="{$top.img}" alt="{$top.desc|htmlent}" class="offer-top__img">{/if}
          <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cross-white.svg" alt="" class="offer-top__close js-offerTop_Close">
        </div>
      </div>
    </div>
  {/if}

  <div class="header-section section">
    <div class="section-wrapper">
      <div class="header">
        <a href="/" class="header-logo">
          <img src="assets/templates/cuptrade/public/assets/cuptrade/img/logo-cuptrade.svg" alt="" class="header-logo__img">
          <div class="header-logo__text">
            <span>
              Одноразовая посуда
            </span>
            <span>
              из эко-материалов
            </span>
          </div>
        </a>
        {if $_modx->config['insta']}
          <a href="{$_modx->config['insta']}" rel="nofollow" target="_blank" class="header-socnet header-instagram">
            <svg width="27" height="26" viewbox="0 0 27 26" fill="none" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
              <path d="M4.487.682a6.376 6.376 0 00-2.304 1.501 6.357 6.357 0 00-1.501 2.3c-.323.824-.54 1.77-.603 3.155C.016 9.023 0 9.467 0 12.998c0 3.53.016 3.974.08 5.359.063 1.385.285 2.33.602 3.155.333.857.777 1.58 1.501 2.305a6.375 6.375 0 002.304 1.501c.825.323 1.771.54 3.156.603 1.385.063 1.829.079 5.36.079 3.53 0 3.974-.016 5.36-.08 1.384-.063 2.33-.285 3.155-.602a6.375 6.375 0 002.304-1.501 6.374 6.374 0 001.501-2.305c.323-.824.54-1.77.603-3.155.063-1.385.08-1.829.08-5.36 0-3.53-.017-3.974-.08-5.36-.063-1.384-.285-2.33-.603-3.155a6.41 6.41 0 00-1.495-2.299A6.376 6.376 0 0021.523.682c-.825-.323-1.77-.54-3.155-.603C16.983.016 16.538 0 13.008 0c-3.53 0-3.975.016-5.36.08-1.39.057-2.336.28-3.16.602zm13.77 1.734c1.268.058 1.955.27 2.415.449a4.05 4.05 0 011.496.972c.454.455.735.888.973 1.496.18.46.39 1.147.449 2.416.063 1.369.074 1.781.074 5.254 0 3.472-.016 3.885-.074 5.254-.058 1.268-.27 1.955-.45 2.415a4.05 4.05 0 01-.972 1.496 4.05 4.05 0 01-1.496.973c-.46.18-1.147.39-2.415.449-1.37.063-1.782.074-5.254.074-3.473 0-3.885-.016-5.254-.074-1.269-.058-1.956-.27-2.416-.45a4.05 4.05 0 01-1.496-.972 4.05 4.05 0 01-.972-1.496c-.18-.46-.391-1.147-.45-2.415-.063-1.37-.073-1.782-.073-5.254 0-3.473.015-3.885.074-5.254.058-1.269.27-1.956.449-2.416a4.05 4.05 0 01.972-1.496 4.05 4.05 0 011.496-.972c.46-.18 1.147-.391 2.416-.45 1.369-.063 1.781-.073 5.254-.073 3.472 0 3.885.01 5.254.074z" fill="url(#paint0_radial)" />
              <path d="M6.327 13.003a6.676 6.676 0 1013.352 0 6.676 6.676 0 00-13.352 0zm11.01 0a4.333 4.333 0 01-4.334 4.334 4.333 4.333 0 01-4.334-4.334 4.333 4.333 0 014.334-4.335 4.333 4.333 0 014.334 4.335z" fill="url(#paint1_radial)" />
              <path d="M19.948 7.622a1.56 1.56 0 100-3.119 1.56 1.56 0 000 3.119z" fill="#654C9F" />
              <defs>
                <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientunits="userSpaceOnUse" gradienttransform="rotate(-3 451.221 2.775) scale(37.5974 31.9574)">
                  <stop stop-color="#FED576" />
                  <stop offset=".263" stop-color="#F47133" />
                  <stop offset=".609" stop-color="#BC3081" />
                  <stop offset="1" stop-color="#4C63D2" />
                </radialGradient>
                <radialGradient id="paint1_radial" cx="0" cy="0" r="1" gradientunits="userSpaceOnUse" gradienttransform="rotate(-3 355.64 -116.204) scale(19.3068 16.4106)">
                  <stop stop-color="#FED576" />
                  <stop offset=".263" stop-color="#F47133" />
                  <stop offset=".609" stop-color="#BC3081" />
                  <stop offset="1" stop-color="#4C63D2" />
                </radialGradient>
              </defs>
            </svg>
          </a>
        {/if}
        {if $_modx->config['whatsapp']}
          <a target="_blank" href="https://api.whatsapp.com/send?phone={$_modx->config['whatsapp']}" rel="nofollow" class="header-socnet header-whatsapp">
            <svg width="26" height="27" viewbox="0 0 26 27" fill="#75C067" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
              <path d="M22.2015 3.79687C21.0042 2.58841 19.5782 1.63033 18.0069 0.978471C16.4355 0.326611 14.7502 -0.00599449 13.049 3.47481e-05C5.92052 3.47481e-05 0.110999 5.80792 0.10447 12.938C0.10447 15.2217 0.701908 17.4433 1.82986 19.4103L0 26.1176L6.86238 24.3187C8.7608 25.3518 10.8876 25.8934 13.049 25.8939H13.0555C20.1856 25.8939 25.9935 20.086 26 12.9494C26.0016 11.2486 25.6667 9.56425 25.0145 7.99343C24.3623 6.42261 23.4057 4.99637 22.1999 3.79687H22.2015ZM13.049 23.7033C11.1212 23.704 9.22879 23.1852 7.57082 22.2016L7.17905 21.9665L3.10799 23.0341L4.19513 19.0626L3.94048 18.6528C2.86282 16.9394 2.29287 14.9556 2.29671 12.9315C2.29671 7.01259 7.12355 2.18411 13.0555 2.18411C14.4687 2.18158 15.8684 2.45879 17.1739 2.99978C18.4795 3.54077 19.665 4.33483 20.6622 5.33617C21.6629 6.33361 22.4562 7.51931 22.9964 8.82487C23.5365 10.1304 23.8128 11.53 23.8094 12.9429C23.8029 18.883 18.976 23.7033 13.049 23.7033ZM18.9499 15.6493C18.6283 15.4877 17.0401 14.7058 16.7413 14.5948C16.4442 14.4887 16.2271 14.4332 16.0149 14.7564C15.7978 15.078 15.1775 15.8109 14.9915 16.0215C14.8054 16.2386 14.6128 16.2631 14.2895 16.1031C13.968 15.9399 12.9249 15.6004 11.6909 14.4953C10.7278 13.6383 10.083 12.5773 9.89038 12.2557C9.70429 11.9325 9.87242 11.7594 10.034 11.5978C10.176 11.4542 10.3556 11.2191 10.5172 11.0331C10.6804 10.847 10.7343 10.7099 10.8404 10.4944C10.9465 10.2756 10.8959 10.0896 10.8159 9.92796C10.7343 9.76636 10.0895 8.17155 9.81692 7.52841C9.55575 6.89343 9.28968 6.98157 9.09053 6.97341C8.90444 6.96199 8.68734 6.96199 8.47024 6.96199C8.30629 6.96606 8.14496 7.00398 7.99636 7.07336C7.84776 7.14275 7.71511 7.2421 7.60673 7.36518C7.30964 7.68838 6.47878 8.47027 6.47878 10.0651C6.47878 11.6599 7.63774 13.1926 7.80098 13.4098C7.96095 13.6269 10.0765 16.8899 13.3232 18.2937C14.0904 18.6284 14.6944 18.8259 15.1661 18.976C15.9415 19.2242 16.6418 19.1866 17.2 19.1066C17.8203 19.012 19.1115 18.3231 19.3841 17.5673C19.6518 16.8099 19.6518 16.1635 19.5702 16.028C19.4902 15.8909 19.2731 15.8109 18.9499 15.6493Z" />
            </svg>
            <span class="header-whatsapp__text">
              WhatsApp
              <br>
              консультант
            </span>
          </a>
        {/if}
        <div class="header-call">
          <a href="tel:{$_modx->config['phone']}" class="hidden-desktop button button-outlined header-phone">
            <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-phone_cell-white.svg" alt="" class="header-phone__ico-mob">
            <span class="header-phone__text js-phone">
              {$_modx->config['phone'] | replace:"+7":""}
            </span>
          </a>
          <a href="javascript:void(0);" class="visible-desktop button button-outlined header-phone js-modalCall" data-modal-called="modalCallback">
            <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-phone_cell-white.svg" alt="" class="header-phone__ico-mob">
            <span class="header-phone__text js-phone">
              {$_modx->config['phone'] | replace:"+7":""}
            </span>
          </a>
          <div class="header-callback js-modalCall" data-modal-called="modalCallback">
            Заказать
            <br>
            звонок
          </div>
        </div>
        <div class="header-connect__item js-modalCall" data-modal-called="modalGetExample">
          <span class="header-connect__ico">
            <svg width="19" height="20" viewbox="0 0 19 20" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M.333 3.42A3 3 0 013.303 0h12.391a3 3 0 012.97 3.424l-.174 1.217-1.98-.282.174-1.218A1 1 0 0015.694 2H3.304a1 1 0 00-.99 1.14l1.983 14a1 1 0 00.99.86h8.496a1 1 0 00.984-.82l.25-1.36 1.967.36-.25 1.361A3 3 0 0113.784 20H5.286a3 3 0 01-2.97-2.58l-1.984-14z" />
              <path fill-rule="evenodd" clip-rule="evenodd" d="M15 13V7h2v6h-2z" />
              <path fill-rule="evenodd" clip-rule="evenodd" d="M13 9h6v2h-6V9z" />
            </svg>
          </span>
          <span class="header-connect__item-text">
            Получить
            <br>
            образцы
          </span>
        </div>
        <div class="header-connect__item header-connect__item-second js-modalCall" data-modal-called="modalGetOptprice">
          <span class="header-connect__ico">
            <svg width="20" height="14" viewbox="0 0 20 14" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M2 6.8v4a1 1 0 001 1h14a1 1 0 001-1v-4h2v4a3 3 0 01-3 3H3a3 3 0 01-3-3v-4h2z" />
              <path fill-rule="evenodd" clip-rule="evenodd" d="M4.307 1.721L5.693.28 10 4.414 14.307.279l1.386 1.442L10 7.186 4.307 1.721z" />
            </svg>
          </span>
          <span class="header-connect__item-text">
            Получить
            <br>
            прайс-лист
          </span>
        </div>
        <a href="{24|url}" class="header-market-mob">
          <div class="header-market__ico">
            <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cart-red.svg" alt="Корзина товаров">
            <div class="header-market__count js-minicart">
              {''|getcart}
            </div>
          </div>
        </a>
        <div class="header-burger js-sitenavMob_Call">
          <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-burger.svg" alt="" class="header-burger__ico">
        </div>
      </div>
    </div>
  </div>

  <div class="header-nav-section section">
    <div class="section-wrapper">
      <div class="header-nav">
        <div class="header-nav__item header-nav__catalog js-dropdownMenu_Toggle">
          Каталог
          <svg width="12" height="8" viewbox="0 0 12 8" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg header-nav__item-control">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M.04 1.989L1.425.546l4.307 4.135L10.04.546l1.385 1.443-5.693 5.465L.04 1.989z" />
          </svg>
          <div class="header-dropdown header-dropdown-catalog">
            <div class="header-dropdown-wide">
              <div class="header-dropdown__items">
                {set $menu = '!pdoResources' | snippet : [
                  'tplWrapper' => '@INLINE {$output}',
                  'parents' => 2,
                  'limit' => 0,
                  'includeTVs' => 'category',
                  'sortby' => 'menuindex',
                  'depth' => 0,
                  'class_key' => 'msCategory',
                  'sortdir' => 'asc',
                  'tpl' => '@INLINE 
                  {if $category = $_pls[\'tv.category\'][0]}
                  {set $tmp = $_modx->getPlaceholder(\'_menu_img\')?:\'\'}
                  {set $img}
                  <img src="{$category.img_menu}" alt="{($menutitle?:$pagetitle)|htmlent}" id="navImg_{$id}" class="header-dropdown__image animated fast">
                  {/set}
                  {set $tmp = $tmp ~ $img}
                  {$_modx->setPlaceholder(\'_menu_img\', $tmp)}
                  {/if}
                  <a href="{$uri}" class="header-dropdown__item" data-headernav-img="navImg_{$id}" data-replace>{$menutitle?:$pagetitle}</a>',
                  'cache' => 1,
                  'cacheTime' => 86400
                  ]}
                {$menu}
                {$_modx->setPlaceholder('_menu', $menu)}
              </div>
              <div class="header-dropdown__images">
                {set $menu_img_block}
                <img src="{$_modx->config['img_menu_catalog']}" alt="" class="header-dropdown__image-main header-dropdown__image--active">
                {$_modx->getPlaceholder('_menu_img')}
                {/set}
                {$menu_img_block | lazy:true}
              </div>
            </div>
          </div>
        </div>
        <div class="header-nav__item header-nav__item-dropdown js-dropdownMenu_Toggle">
          О нас
          <svg width="12" height="8" viewbox="0 0 12 8" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg header-nav__item-control">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M.04 1.989L1.425.546l4.307 4.135L10.04.546l1.385 1.443-5.693 5.465L.04 1.989z" />
          </svg>
          <div class="header-dropdown header-dropdown-about">
            <div class="header-dropdown__items">
              <a href="{4|url}" class="header-dropdown__item">О компании</a>
              <a href="{10|url}" class="header-dropdown__item">Доставка и оплата</a>
              <!--<a href="{7|url}" class="header-dropdown__item">Контакты</a>-->
            </div>
          </div>
        </div>
        <a href="{8|url}" class="header-nav__item">
          Производство
        </a>
        <a href="{9|url}" class="header-nav__item">
          Ваш дизайн
        </a>
        <a href="{81|url}" class="header-nav__item">
          Ваш логотип
        </a>
        <a href="{7|url}" class="header-nav__item">
          Контакты
        </a>
        <form action="{25 | url}" method="GET" id="headerSearchForm" class="header-search">
          <input type="search" name="query" value="{$.get.query}" id="headerSearchField" class="header-search__input" placeholder="Я ищу...">
          <input type="submit" name="headerSearch_submit" id="headerSearchButton" class="header-search__submit" value="">
          <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-search.svg" alt="" class="header-search__submit-ico">
        </form>
        <div class="header-search__ico js-searchDesktop_Call">
          <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-search.svg" alt="">
        </div>
        <a href="{24|url}" class="header-market">
          <div class="header-market__ico">
            <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cart-red.svg" alt="Корзина товаров">
            <div class="header-market__count js-minicart">
              {''|getcart}
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>

  <div id="sitenavMob" class="sitenav animated faster">
    <div class="sitenav-wrapper">
      <div class="sitenav-closing">
        <div class="sitenav-close js-sitenavClose">
          <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cross.svg" alt="" class="sitenav-close__ico">
        </div>
      </div>
      <div class="sitenav-menu__module">
        <a href="{2|url}" class="sitenav-menu__module-heading">Каталог<span class="sitenav-menu__module-heading-control js-sitenavSet_Toggle"><img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_down.svg" alt="" class="sitenav-menu__module-heading-ico"></span></a>
        <div class="sitenav-set">
          {$menu | replace:"header-dropdown__item":"sitenav-set__item" | replace:"</a>":"<img src=\"assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_right.svg\" alt=\"\" class=\"sitenav-set__item-ico\"></a>"}
        </div>
      </div>
      <div class="sitenav-menu__module">
        <div class="sitenav-menu__module-heading">О нас<span class="sitenav-menu__module-heading-control js-sitenavSet_Toggle"><img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_down.svg" alt="" class="sitenav-menu__module-heading-ico"></span></div>
        <div class="sitenav-set">
          <a href="{4|url}" class="sitenav-set__item">О компании<img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_right.svg" alt="" class="sitenav-set__item-ico"></a>
          <a href="{10|url}" class="sitenav-set__item">Доставка и оплата<img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_right.svg" alt="" class="sitenav-set__item-ico"></a>
          <a href="{7|url}" class="sitenav-set__item">Контакты<img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_right.svg" alt="" class="sitenav-set__item-ico"></a>
        </div>
      </div>
      <a href="{8|url}" class="sitenav-menu__section">Производство</a>
      <a href="{9|url}" class="sitenav-menu__section">Ваш дизайн</a>
      <a href="{81|url}" class="sitenav-menu__section">Ваш логотип</a>
      <form action="{25 | url}" method="GET" class="sitenav-search">
        <input type="search" name="query" value="{$.get.query}" id="sitenavSearchField" class="sitenav-search__input" placeholder="Я ищу...">
        <input type="submit" name="sitenavSearch_submit" id="sitenavSearchButton" class="sitenav-search__submit" value="">
        <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-search.svg" alt="" class="sitenav-search__ico">
      </form>
      <div class="sitenav-contacts">
        <div class="sitenav-contacts__title">Контакты</div>
        <div class="sitenav-contacts__item">{$_modx->config['address']}</div>
        <a href="tel:{$_modx->config['phone']}" class="sitenav-contacts__item js-phone">{$_modx->config['phone'] | replace:"+7":""}</a>
        <a href="mailto:{$_modx->config['email']}" class="sitenav-contacts__item">{$_modx->config['email']}</a>
        <button class="button button-primary sitenav__button js-modalCall_Mob" data-modal-called="modalCallback">Заказать звонок</button>
      </div>
      <div class="sitenav-socnet">
        {if $_modx->config['insta']}
          <a href="{$_modx->config['insta']}" rel="nofollow" class="sitenav-socnet__item">
            <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-inst-gradient.svg" alt="" class="sitenav-socnet__ico">
          </a>
        {/if}
        {if $_modx->config['whatsapp']}
          <a href="https://api.whatsapp.com/send?phone={$_modx->config['whatsapp']}" rel="nofollow" class="sitenav-socnet__item">
            <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-whatsapp-green.svg" alt="" class="sitenav-socnet__ico">
          </a>
        {/if}
        {if $_modx->config['telegram']}
          <a href="{$_modx->config['telegram']}" rel="nofollow" class="sitenav-socnet__item">
            <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-telegram-color.svg" alt="" class="sitenav-socnet__ico">
          </a>
        {/if}
      </div>
      <div class="sitenav-connect">
        <div class="sitenav-connect__item sitenav-connect__item-first js-modalCall_Mob" data-modal-called="modalGetExample">
          <span class="sitenav-connect__ico">
            <svg width="19" height="20" viewbox="0 0 19 20" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M.333 3.42A3 3 0 013.303 0h12.391a3 3 0 012.97 3.424l-.174 1.217-1.98-.282.174-1.218A1 1 0 0015.694 2H3.304a1 1 0 00-.99 1.14l1.983 14a1 1 0 00.99.86h8.496a1 1 0 00.984-.82l.25-1.36 1.967.36-.25 1.361A3 3 0 0113.784 20H5.286a3 3 0 01-2.97-2.58l-1.984-14z" />
              <path fill-rule="evenodd" clip-rule="evenodd" d="M15 13V7h2v6h-2z" />
              <path fill-rule="evenodd" clip-rule="evenodd" d="M13 9h6v2h-6V9z" />
            </svg>
          </span>
          <span class="sitenav-connect__item-text">
            Получить образцы
          </span>
        </div>
        <div class="sitenav-connect__item js-modalCall_Mob" data-modal-called="modalGetOptprice">
          <span class="sitenav-connect__ico">
            <svg width="20" height="14" viewbox="0 0 20 14" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M2 6.8v4a1 1 0 001 1h14a1 1 0 001-1v-4h2v4a3 3 0 01-3 3H3a3 3 0 01-3-3v-4h2z" />
              <path fill-rule="evenodd" clip-rule="evenodd" d="M4.307 1.721L5.693.28 10 4.414 14.307.279l1.386 1.442L10 7.186 4.307 1.721z" />
            </svg>
          </span>
          <span class="sitenav-connect__item-text">
            Получить прайс-лист
          </span>
        </div>
      </div>
    </div>
  </div>

  <div id="sitenavOverlay" class="sitenav-overlay"></div>