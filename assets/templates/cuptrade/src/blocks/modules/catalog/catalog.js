//== Slider for services Also
// var carouselservicesAlso = new Swiper('.services-carousel .swiper-container', {
//   loop: true,
//   // slidesPerView : 'auto',
//   spaceBetween: 20,
//   slidesPerView: 4,
//   initialSlide: 0,
//   // Navigation arrows
//   navigation: {
//     nextEl: '.services-carousel__next',
//     prevEl: '.services-carousel__prev',
//   },
//   breakpoints: {
//     // when window width is >= 560px etc
//     290: {
//       slidesPerView: 1,
//     },
//     561: {
//       slidesPerView: 2,
//     },
//     861: {
//       slidesPerView: 3,
//     },
//   },
// });

$('.catalog-sorting__control').click(function (e) {
  e.preventDefault();
  e.stopPropagation();
  $(this).toggleClass('catalog-sorting__control-expanded');
  // $('#' + $(e.target).attr('for')).prop('checked', true);
});
$('.catalog-sorting__menu-label').click(function (e) {
  $('#' + $(e.target).attr('for')).attr('checked', true);
  //console.log($('#' + $(e.target).attr('for')).attr('value'));
  $(this)
    .closest('.catalog-sorting__control')
    .find('.catalog-sorting__control-text')
    .text($('#' + $(e.target).attr('for')).attr('data-title'));
  $(this)
    .closest('.catalog-sorting__control')
    .find('.catalog-sorting__control-value')
    .attr('value', $('#' + $(e.target).attr('for')).attr('value'));
  $(this)
    .closest('.catalog-sorting__control')
    .find('.catalog-sorting__control-value').trigger('change');
});

$(document).click(function () {
  $('.catalog-sorting__control').removeClass(
    'catalog-sorting__control-expanded'
  );
});

//== Carousel for catalog card
//== this function called in a file: script-closing
function catalogCard_Carousel() {
  $('.catalog-card__carousel').each(function (index, element) {
    const $this = $(this);
    $slide = $this.find('.swiper-slide');
    if($slide.length < 2){
      return;
    }
    $this
      .find('.catalog-card__carousel-pagination')
      .addClass('catalog-card__carousel-pagination-' + index);
    $this.find('.swiper-container').addClass('catalog-card__carousel-' + index);

    let swiper = new Swiper('.catalog-card__carousel-' + index, {
      // loop: true,
      autoplay: false,
      pagination: {
        el: '.catalog-card__carousel-pagination-' + index,
        type: 'bullets',
        clickable: true,
      },
    });
  });
}

$(document).on( "mse2_load", function() {
  catalogCard_Carousel()
});

// $(function () {
//   if ($('.catalog-card__carousel').length > 0) {
//     //some-slider-wrap-in
//     catalogCard_Carousel();
//     $('.catalog-card__carousel-pagination>.swiper-pagination-bullet').hover(
//       function () {
//         $(this).trigger('click');
//       }
//     );
//   }
// });
