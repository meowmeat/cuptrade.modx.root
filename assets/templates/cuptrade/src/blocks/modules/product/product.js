//==== product ====//
//== Carousel mobile for Product Page
function carouselProductRecommend_Mob() {
  if ($(window).width() <= 480) {
    $('.product-recommend').find('.catalog-card').addClass('swiper-slide');
    let carouselProduct_Recommend = new Swiper(
      '.product-recommend .swiper-container',
      {
        loop: true,
        slidesPerView: 1,
        navigation: {
          nextEl: '.product-recommend__next',
          prevEl: '.product-recommend__prev',
        },
      }
    );
  } else {
    $('.product-recommend').find('.catalog-card').removeClass('swiper-slide');
  }
}
function carouselProductSimilar_Mob() {
  if ($(window).width() <= 480) {
    $('.product-similar').find('.catalog-card').addClass('swiper-slide');
    let carouselProduct_Similar = new Swiper(
      '.product-similar .swiper-container',
      {
        loop: true,
        slidesPerView: 1,
        navigation: {
          nextEl: '.product-similar__next',
          prevEl: '.product-similar__prev',
        },
      }
    );
  } else {
    $('.product-similar').find('.catalog-card').removeClass('swiper-slide');
  }
}

var carouselProduct_Image = new Swiper('.product-carousel .swiper-container', {
  spaceBetween: 10,
  centeredSlides: true,
  slidesPerView: 'auto',
  // loop: true,
  loopedSlides: 4,
  resizeReInit: true,
  navigation: {
    nextEl: '.product-carousel__next',
    prevEl: '.product-carousel__prev',
  },
  pagination: {
    el: '.product-carousel__pagination',
    type: 'bullets',
    clickable: true,
  },
});

$().fancybox({
  selector:
    '.product-carousel .swiper-container .swiper-slide:not(.swiper-slide-duplicate), .product-view a[data-fancybox]',
  thumbs: false,
  hash: false,
  toolbar: false,
  smallBtn: true,
  backFocus: false,
  beforeClose: function (instance) {
    if (instance.group.length > 1) {
      carouselProduct_Image.slideTo(instance.currIndex, 0);
    }
  },
});

var carouselProductImage_Thumbs = new Swiper(
  '.product-preview .swiper-container',
  {
    spaceBetween: 20,
    centeredSlides: true,
    slidesPerView: 'auto',
    touchRatio: 0.2,
    slideToClickedSlide: true,
    // loop: true,
    loopedSlides: 4,
    breakpoints: {
      561: {
        spaceBetween: 10,
      },
      861: {
        spaceBetween: 20,
      },
    },
  }
);
//== this function called in a file: script-closing
function productThumbs_BindControls() {
  if ($(window).width() > 560) {
    carouselProduct_Image.controller.control = carouselProductImage_Thumbs;
    carouselProductImage_Thumbs.controller.control = carouselProduct_Image;
  }
}

//== Product Counter Processing
$(document).on('keyup', '.js-productCount_Number', function () {
  $(this).trigger('change');
  $(this).find('+.js-productCount').trigger('click', ['not_change']);
});
//== Product Page: Change Count and Price of product
$(document).on('click touch', '.js-productCount', function (e, prop) {
  let $button = $(this);
  // let costField = $button.parents('.popup').find('input[name="cost"]');
  let productPrice_Single = $button
    .closest('[data-product-counter]')
    .siblings('[data-product-price-wrap]')
    .find('[data-product-price]');
  let productPriceDisplayed = productPrice_Single.find('[data-product-cost]');
  let productCount = $button.siblings('.js-productCount_Number');
  let productCountVal = parseFloat(productCount.val()?productCount.val():1);
  let productCountMin = parseFloat(productCount.attr('min'));
  let productCountMax = parseFloat(productCount.attr('max'));
  let productCountStep = parseFloat(productCount.attr('step'));
  let newVal = parseFloat(productCountVal);
  if(typeof prop != 'undefined' && prop == 'not_change'){
  }
  else{
    if ($button.hasClass('product-count__plus')) {
      newVal = newVal + productCountStep;
      productPriceDisplayed.text(
        (productPrice_Single.attr('data-product-price') * newVal).toLocaleString(
          'ru'
        )
      );
      if (productCountVal >= productCountMax) {
        newVal = newVal - productCountStep;
        productPriceDisplayed.text(
          (
            productPrice_Single.attr('data-product-price') * newVal
          ).toLocaleString('ru')
        );
      }
    } else if (productCountVal > productCountMin) {
      newVal = newVal - productCountStep;
      productPriceDisplayed.text(
        (productPrice_Single.attr('data-product-price') * newVal).toLocaleString(
          'ru'
        )
      );
    }
  }
  productCount.val(newVal);
  $('.js-type-pack:checked').trigger('change');
  productCount.trigger('change');
  // let $form = $(this).parents('form.ms2_form');
  // if ($form.length) {
  //   let formData = $form.serializeArray(),
  //     name = miniShop2.actionName,
  //     action = formData.name;
  //   miniShop2.sendData = {
  //     $form: $form,
  //     action: action,
  //     formData: formData,
  //   };
  //   miniShop2.Cart.change();
  // }
});
