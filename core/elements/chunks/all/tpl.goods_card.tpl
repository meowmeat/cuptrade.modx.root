<a href="{$uri}" title="{$pagetitle|htmlent}" class="catalog-card">
  <div class="catalog-card__image catalog-card__carousel">
    {if $popular}<div class="catalog-card__image-label">хит</div>{/if}
    {if $new}<div class="catalog-card__image-label">new</div>{/if}
    {$_modx->runSnippet('msGallery', [
      'limit' => 0,
      'product' => $id,
      'tpl' => '@INLINE 
            {if $files?}
              {if ($files | length) > 1}
                <div class="swiper-container">
                  <div class="swiper-wrapper">
                    {foreach $files as $file}
                      <div class="swiper-slide">
                        <img data-src="{$file.url?:\'assets/templates/cuptrade/public/assets/cuptrade/img/logo-cuptrade.svg\'}" src="assets/templates/cuptrade/public/assets/cuptrade/img/preloader.gif" alt="{($file.description?:$pagetitle)|htmlent}" class="catalog-card__img lazy">
                      </div>
                    {/foreach}
                  </div>
                  <div class="catalog-card__carousel-pagination"></div>
                </div>
              {else}
                <img data-src="{$files.0.url?:\'assets/templates/cuptrade/public/assets/cuptrade/img/logo-cuptrade.svg\'}" src="assets/templates/cuptrade/public/assets/cuptrade/img/preloader.gif" alt="{($files.0.description?:$pagetitle)|htmlent}" class="catalog-card__img lazy">
              {/if}
            {else}
              <img data-src="assets/templates/cuptrade/public/assets/cuptrade/img/logo-cuptrade.svg" src="assets/templates/cuptrade/public/assets/cuptrade/img/preloader.gif" alt="{$pagetitle|htmlent}" class="catalog-card__img lazy">
            {/if}'
    ])}
  </div>
  <div class="catalog-card__available">
    {if $_modx->resource.searchable}
      <span class="catalog-card__available-text">На складе</span>
    {else}
      <span class="catalog-card__available-text" style="color:#df312f">Под заказ</span>
    {/if}
    <span class="catalog-card__available-line"></span>
  </div>
  <h3 class="catalog-card__title">{$pagetitle}</h3>
  <div class="catalog-card__text">{$introtext?:($content | striptags | truncate : 80 : '...' : true : false)}</div>
  <div class="catalog-card__footer">
    {set $price_position = ($price|replace:" ":""|replace:",":".")}
    {set $price_discount = ($price_position - ($price_position*($_modx->getPlaceholder('_discount')?:0)/100)) | round:"2"}
    <div class="catalog-card__price">{if $opt_price = $id|resource:"opt_price"}от <span class="js-price">{$opt_price[0]}</span>{else}<span class="js-price">{$price}</span>{/if} ₽</div>
    <div class="catalog-card__forward">Купить <svg width="8" height="12" viewBox="0 0 8 12" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg catalog-card__forward-ico">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M1.721 11.693L.28 10.307 4.414 6 .279 1.693 1.72.307 7.186 6l-5.465 5.693z" />
      </svg></div>
  </div>
</a>