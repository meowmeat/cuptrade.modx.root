{extends 'file:templates/index.tpl'}
{block 'class'}order-page{/block}
{block 'main'}
  <div class="order-section section">
    <img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_lines_square.svg" alt="" class="order-section__shape">
    <img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_lines_circle.svg" alt="" class="order-section__shape2">
    <div class="section-wrapper">
      <div class="section-heading">
        <a href="{2|url}" class="shopward-link"><span class="shopward-link__ico"><svg width="6" height="11" viewBox="0 0 8 12" fill="#989798" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M6.079 11.693l1.442-1.386L3.386 6l4.135-4.307L6.08.307.614 6l5.465 5.693z" />
            </svg></span>Продолжить покупки</a>
        <h1 class="section-heading__title">Оформить заказ</h1>
      </div>
      <div class="section-twin">
        {'!msCart' | snippet : [
          'tpl' => '@FILE chunks/all/tpl.msCart.cart_order.tpl'
        ]}

        {'!msOrder' | snippet : [
        	'userFields' => '{"receiver":"fullname","phone":"mobilephone","email":"email","address":"address"}',
        	'tpl' => '@FILE chunks/all/tpl.MymsOrder.tpl'
        ]}

      </div> <!-- end section-twin -->

    </div>
  </div>
{/block}