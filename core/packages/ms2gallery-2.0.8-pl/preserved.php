<?php return array (
  '047ebd094b2d4e11286e88bb8ce4e1d7' => 
  array (
    'criteria' => 
    array (
      'name' => 'ms2gallery',
    ),
    'object' => 
    array (
      'name' => 'ms2gallery',
      'path' => '{core_path}components/ms2gallery/',
      'assets_path' => '',
    ),
  ),
  'c3aaf80fe761eda8fc178dc0e209575b' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_duplicate_check',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_duplicate_check',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_resource',
      'editedon' => NULL,
    ),
  ),
  '2f1c26b9b6452d7935b374fb158c55c2' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_source_default',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_source_default',
      'value' => '3',
      'xtype' => 'modx-combo-source',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_resource',
      'editedon' => '2021-06-29 02:38:08',
    ),
  ),
  '2a43589fc7853f93d44a585955244309' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_date_format',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_date_format',
      'value' => '%d.%m.%y %H:%M',
      'xtype' => 'textfield',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_resource',
      'editedon' => NULL,
    ),
  ),
  'f057605e74b825dfa00b9729d53eb16a' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_page_size',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_page_size',
      'value' => '50',
      'xtype' => 'textfield',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_resource',
      'editedon' => NULL,
    ),
  ),
  'a8710dde1410398f7b0845a1683901b1' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_disable_for_templates',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_disable_for_templates',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_resource',
      'editedon' => NULL,
    ),
  ),
  'a9745a410ce35d9e405f6576992db8db' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_new_tab_mode',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_new_tab_mode',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_resource',
      'editedon' => NULL,
    ),
  ),
  '31f554a5d54b627607f73b25d3749de3' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_disable_for_ms2',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_disable_for_ms2',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_resource',
      'editedon' => NULL,
    ),
  ),
  'b7cb59403dda63c6d3db01c59a9c9821' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_sync_ms2',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_sync_ms2',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_resource',
      'editedon' => NULL,
    ),
  ),
  'e3bc1111fd5d2ea91a3305ca5c967547' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_sync_tickets',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_sync_tickets',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_resource',
      'editedon' => NULL,
    ),
  ),
  'aecc5f3fbfb97168c2899e77698a7f0d' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_set_placeholders',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_set_placeholders',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_frontend',
      'editedon' => NULL,
    ),
  ),
  '2bf2675a6491748a7a5fb390e7c38483' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_placeholders_prefix',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_placeholders_prefix',
      'value' => 'ms2g.',
      'xtype' => 'textfield',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_frontend',
      'editedon' => NULL,
    ),
  ),
  '77fed5b06a042b0f8bb4b119e2acd0f7' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_placeholders_tpl',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_placeholders_tpl',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_frontend',
      'editedon' => NULL,
    ),
  ),
  '61dc0cb70a2d008b344c16a0f209ef3f' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_placeholders_for_templates',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_placeholders_for_templates',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_frontend',
      'editedon' => NULL,
    ),
  ),
  '9569543ad9f9a432e0437be4219faaf7' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_placeholders_thumbs',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_placeholders_thumbs',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_frontend',
      'editedon' => NULL,
    ),
  ),
  '7b4e35e3f8e0c3d9b7f3bae12a172c4c' => 
  array (
    'criteria' => 
    array (
      'category' => 'ms2Gallery',
    ),
    'object' => 
    array (
      'id' => 13,
      'parent' => 0,
      'category' => 'ms2Gallery',
      'rank' => 0,
    ),
  ),
  'e1564e291498002b92eb6d272bce290b' => 
  array (
    'criteria' => 
    array (
      'name' => 'tpl.ms2Gallery',
    ),
    'object' => 
    array (
      'id' => 28,
      'source' => 1,
      'property_preprocess' => 0,
      'name' => 'tpl.ms2Gallery',
      'description' => '',
      'editor_type' => 0,
      'category' => 13,
      'cache_type' => 0,
      'snippet' => '<div class="ms2Gallery">
    {if $files?}
        <div class="fotorama"
             data-nav="thumbs"
             data-thumbheight="45"
             data-allowfullscreen="true"
             data-swipe="true"
             data-autoplay="5000">
            {foreach $files as $file}
                <a href="{$file[\'url\']}" target="_blank">
                    <img src="{$file[\'small\']}" alt="{$file[\'name\']}" title="{$file[\'name\']}">
                </a>
            {/foreach}
        </div>
    {else}
        <img src="{(\'assets_url\' | option) ~ \'components/ms2gallery/img/web/ms2_medium.png\'}" alt="" title=""/>
    {/if}
</div>',
      'locked' => 0,
      'properties' => NULL,
      'static' => 0,
      'static_file' => 'core/components/ms2gallery/elements/chunks/chunk.ms2gallery.tpl',
      'content' => '<div class="ms2Gallery">
    {if $files?}
        <div class="fotorama"
             data-nav="thumbs"
             data-thumbheight="45"
             data-allowfullscreen="true"
             data-swipe="true"
             data-autoplay="5000">
            {foreach $files as $file}
                <a href="{$file[\'url\']}" target="_blank">
                    <img src="{$file[\'small\']}" alt="{$file[\'name\']}" title="{$file[\'name\']}">
                </a>
            {/foreach}
        </div>
    {else}
        <img src="{(\'assets_url\' | option) ~ \'components/ms2gallery/img/web/ms2_medium.png\'}" alt="" title=""/>
    {/if}
</div>',
    ),
  ),
  '188509774c3d18b64b5bc2c7955624b3' => 
  array (
    'criteria' => 
    array (
      'name' => 'ms2Gallery',
    ),
    'object' => 
    array (
      'id' => 54,
      'source' => 1,
      'property_preprocess' => 0,
      'name' => 'ms2Gallery',
      'description' => '',
      'editor_type' => 0,
      'category' => 13,
      'cache_type' => 0,
      'snippet' => '/** @var array $scriptProperties */
/** @var ms2Gallery $ms2Gallery */
$ms2Gallery = $modx->getService(\'ms2gallery\', \'ms2Gallery\', MODX_CORE_PATH . \'components/ms2gallery/model/ms2gallery/\');
/** @var pdoFetch $pdoFetch */
if (!$modx->loadClass(\'pdofetch\', MODX_CORE_PATH . \'components/pdotools/model/pdotools/\', false, true)) {
    return false;
}
$pdoFetch = new pdoFetch($modx, $scriptProperties);
$extDir = $modx->getOption(\'extensionsDir\', $scriptProperties, \'components/ms2gallery/img/mgr/extensions/\', true);
$fenom = !empty($scriptProperties[\'tpl\']);

// Register styles and scripts on frontend
$config = $pdoFetch->makePlaceholders($ms2Gallery->config);
$css = $modx->getOption(\'frontend_css\', $scriptProperties, \'frontend_css\');
if (!empty($css) && preg_match(\'#\\.css#i\', $css)) {
    $modx->regClientCSS(str_replace($config[\'pl\'], $config[\'vl\'], $css));
}
$js = $modx->getOption(\'frontend_js\', $scriptProperties, \'frontend_js\');
if (!empty($js) && preg_match(\'#\\.js#i\', $js)) {
    $tmp = json_encode(array(
        \'cssUrl\' => $ms2Gallery->config[\'cssUrl\'] . \'web/\',
        \'jsUrl\' => $ms2Gallery->config[\'jsUrl\'] . \'web/\',
    ));
    $modx->regClientScript(\'<script type="text/javascript">ms2GalleryConfig=\' . $tmp . \';</script>\');
    $modx->regClientScript(str_replace($config[\'pl\'], $config[\'vl\'], $js));
}
if (empty($outputSeparator)) {
    $outputSeparator = "\\n";
}
if (empty($tagsSeparator)) {
    $tagsSeparator = \',\';
}

$where = array(
    \'File.parent\' => 0,
);

// Define where parameters
if ($scriptProperties[\'parents\'] == \'\' && empty($scriptProperties[\'resources\'])) {
    $resources = !empty($resource)
        ? $resource
        : $modx->resource->get(\'id\');
    $scriptProperties[\'resources\'] = $resources;
}

if (!empty($filetype)) {
    $where[\'File.type:IN\'] = array_map(\'trim\', explode(\',\', $filetype));
}

if (empty($showInactive)) {
    $where[\'File.active\'] = 1;
}

$innerJoin = array(
    \'File\' => array(
        \'class\' => \'msResourceFile\',
        \'on\' => \'File.resource_id = modResource.id\',
    ),
);
if (!empty($tagsVar) && isset($_REQUEST[$tagsVar])) {
    $tags = $modx->stripTags($_REQUEST[$tagsVar]);
}
if (!empty($tags)) {
    $tags = array_map(\'trim\', explode(\',\', str_replace(\'"\', \'\', $tags)));
    $tags = implode(\'","\', $tags);
    $innerJoin[\'Tag\'] = array(
        \'class\' => \'msResourceFileTag\',
        \'on\' => \'Tag.file_id = File.id AND Tag.tag IN ("\' . $tags . \'")\',
    );
}

$select = array(
    \'File\' => \'*\',
);

// Set default sort by File table
if (!empty($sortby)) {
    $sortby = array_map(\'trim\', explode(\',\', $sortby));
    foreach ($sortby as &$value) {
        if (strpos($value, \'.\') === false && strpos($value, \'(\') === false) {
            $value = \'File.\' . $value;
        }
    }
    $scriptProperties[\'sortby\'] = implode(\', \', $sortby);
}

// Process additional query params
foreach (array(\'where\', \'innerJoin\', \'select\') as $v) {
    if (!empty($scriptProperties[$v])) {
        $tmp = json_decode($scriptProperties[$v], true);
        if (is_array($tmp)) {
            $$v = array_merge($$v, $tmp);
        }
    }
    unset($scriptProperties[$v]);
}

if (empty($limit) && !empty($offset)) {
    $scriptProperties[\'limit\'] = 10000;
}

// Default parameters
$default = array(
    \'class\' => \'modResource\',
    \'innerJoin\' => $innerJoin,
    \'where\' => json_encode($where),
    \'select\' => $select,
    \'limit\' => 10,
    \'sortby\' => \'rank\',
    \'sortdir\' => \'ASC\',
    \'fastMode\' => false,
    \'groupby\' => \'File.id\',
    \'return\' => \'data\',
    \'nestedChunkPrefix\' => \'ms2gallery_\',
);

// Merge all properties and run!
if (empty($scriptProperties[\'tpl\']) && !empty($tplRow)) {
    $scriptProperties[\'tpl\'] = $tplRow;
}
$pdoFetch->setConfig(array_merge($default, $scriptProperties));
$rows = $pdoFetch->run();

if (!empty($rows)) {
    $tmp = current($rows);
    $resolution = array();
    /** @var modMediaSource $mediaSource */
    if ($mediaSource = $modx->getObject(\'sources.modMediaSource\', $tmp[\'source\'])) {
        $properties = $mediaSource->getProperties();
        if (isset($properties[\'thumbnails\'][\'value\'])) {
            $fileTypes = json_decode($properties[\'thumbnails\'][\'value\'], true);
            foreach ($fileTypes as $k => $v) {
                if (!is_numeric($k)) {
                    $resolution[] = $k;
                } elseif (!empty($v[\'name\'])) {
                    $resolution[] = $v[\'name\'];
                } else {
                    $resolution[] = @$v[\'w\'] . \'x\' . @$v[\'h\'];
                }
            }
        }
    }
}

// Processing rows
$output = null;
$images = array();
foreach ($rows as $k => $row) {
    $row[\'idx\'] = $pdoFetch->idx++;
    $row[\'tags\'] = \'\';

    if (!empty($row[\'type\'])) {
        if ($row[\'type\'] == \'image\') {
            $previews = $pdoFetch->getCollection(\'msResourceFile\', array(\'parent\' => $row[\'id\']));
            foreach ($previews as $preview) {
                if (preg_match("#/{$preview[\'resource_id\']}/(.*?)/#", $preview[\'url\'], $size)) {
                    $row[$size[1]] = !empty($getPreviewProperties)
                        ? $preview
                        : $preview[\'url\'];
                }
            }
        } else {
            $row[\'thumbnail\'] = file_exists(MODX_ASSETS_PATH . $extDir . $row[\'type\'] . \'.png\')
                ? MODX_ASSETS_URL . $extDir . $row[\'type\'] . \'.png\'
                : MODX_ASSETS_URL . $extDir . \'other.png\';
            if (!empty($resolution)) {
                foreach ($resolution as $v) {
                    $row[$v] = $row[\'thumbnail\'];
                }
            }
        }
    }


    if (!empty($getTags)) {
        $q = $modx->newQuery(\'msResourceFileTag\', array(\'file_id\' => $row[\'id\']));
        $q->select(\'tag\');
        $tstart = microtime(true);
        if ($q->prepare() && $q->stmt->execute()) {
            $modx->queryTime += microtime(true) - $tstart;
            $modx->executedQueries++;
            $row[\'tags\'] = implode($tagsSeparator, $q->stmt->fetchAll(PDO::FETCH_COLUMN));
        }
    }

    $images[] = $row;
}
$pdoFetch->addTime(!empty($getTags) ? \'Thumbnails and tags was retrieved\' : \'Thumbnails was retrieved\');

// Processing chunks
$output = array();
if ($fenom) {
    $output[] = $pdoFetch->getChunk($tpl, array(
        \'files\' => $images,
        \'images\' => $images,
    ));
} else {
    foreach ($images as $row) {
        $tpl = $pdoFetch->defineChunk($row);

        $output[] = empty($tpl)
            ? \'<pre>\' . $pdoFetch->getChunk(\'\', $row) . \'</pre>\'
            : $pdoFetch->getChunk($tpl, $row, $pdoFetch->config[\'fastMode\']);
    }
}
$pdoFetch->addTime(\'Rows was templated\');

// Return output
$log = \'\';
if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $log .= \'<pre class="ms2GalleryLog">\' . print_r($pdoFetch->getTime(), 1) . \'</pre>\';
}

if (!$fenom) {
    if (!empty($toSeparatePlaceholders)) {
        $output[\'log\'] = $log;
        $modx->setPlaceholders($output, $toSeparatePlaceholders);

        return;
    }
    if (count($output) === 1 && !empty($tplSingle)) {
        $output = $pdoFetch->getChunk($tplSingle, array_shift($images));
    } else {
        $output = implode($outputSeparator, $output);

        if (!empty($output)) {
            $arr = array_shift($images);
            $arr[\'rows\'] = $output;
            $arr[\'images\'] = $images;
            if (!empty($tplOuter)) {
                $output = $pdoFetch->getChunk($tplOuter, $arr);
            }
        } else {
            $output = !empty($tplEmpty)
                ? $pdoFetch->getChunk($tplEmpty)
                : \'\';
        }
    }
}

if (is_array($output)) {
    $output = implode($outputSeparator, $output);
}
$output .= $log;
if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $output);
} else {
    return $output;
}',
      'locked' => 0,
      'properties' => 'a:19:{s:7:"parents";a:7:{s:4:"name";s:7:"parents";s:4:"desc";s:23:"ms2gallery_prop_parents";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:9:"resources";a:7:{s:4:"name";s:9:"resources";s:4:"desc";s:25:"ms2gallery_prop_resources";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:7:"showLog";a:7:{s:4:"name";s:7:"showLog";s:4:"desc";s:23:"ms2gallery_prop_showLog";s:4:"type";s:13:"combo-boolean";s:7:"options";a:0:{}s:5:"value";b:0;s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:13:"toPlaceholder";a:7:{s:4:"name";s:13:"toPlaceholder";s:4:"desc";s:29:"ms2gallery_prop_toPlaceholder";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:3:"tpl";a:7:{s:4:"name";s:3:"tpl";s:4:"desc";s:19:"ms2gallery_prop_tpl";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:14:"tpl.ms2Gallery";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:5:"limit";a:7:{s:4:"name";s:5:"limit";s:4:"desc";s:21:"ms2gallery_prop_limit";s:4:"type";s:11:"numberfield";s:7:"options";a:0:{}s:5:"value";i:0;s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:6:"offset";a:7:{s:4:"name";s:6:"offset";s:4:"desc";s:22:"ms2gallery_prop_offset";s:4:"type";s:11:"numberfield";s:7:"options";a:0:{}s:5:"value";i:0;s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:5:"where";a:7:{s:4:"name";s:5:"where";s:4:"desc";s:21:"ms2gallery_prop_where";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:8:"filetype";a:7:{s:4:"name";s:8:"filetype";s:4:"desc";s:24:"ms2gallery_prop_filetype";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:12:"showInactive";a:7:{s:4:"name";s:12:"showInactive";s:4:"desc";s:28:"ms2gallery_prop_showInactive";s:4:"type";s:13:"combo-boolean";s:7:"options";a:0:{}s:5:"value";b:0;s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:6:"sortby";a:7:{s:4:"name";s:6:"sortby";s:4:"desc";s:22:"ms2gallery_prop_sortby";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:4:"rank";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:7:"sortdir";a:7:{s:4:"name";s:7:"sortdir";s:4:"desc";s:23:"ms2gallery_prop_sortdir";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:3:"ASC";s:5:"value";s:3:"ASC";}i:1;a:2:{s:4:"text";s:4:"DESC";s:5:"value";s:4:"DESC";}}s:5:"value";s:3:"ASC";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:12:"frontend_css";a:7:{s:4:"name";s:12:"frontend_css";s:4:"desc";s:28:"ms2gallery_prop_frontend_css";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:26:"[[+cssUrl]]web/default.css";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:11:"frontend_js";a:7:{s:4:"name";s:11:"frontend_js";s:4:"desc";s:27:"ms2gallery_prop_frontend_js";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:24:"[[+jsUrl]]web/default.js";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:4:"tags";a:7:{s:4:"name";s:4:"tags";s:4:"desc";s:20:"ms2gallery_prop_tags";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:7:"tagsVar";a:7:{s:4:"name";s:7:"tagsVar";s:4:"desc";s:23:"ms2gallery_prop_tagsVar";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:7:"getTags";a:7:{s:4:"name";s:7:"getTags";s:4:"desc";s:23:"ms2gallery_prop_getTags";s:4:"type";s:13:"combo-boolean";s:7:"options";a:0:{}s:5:"value";b:0;s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:13:"tagsSeparator";a:7:{s:4:"name";s:13:"tagsSeparator";s:4:"desc";s:29:"ms2gallery_prop_tagsSeparator";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:1:",";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:20:"getPreviewProperties";a:7:{s:4:"name";s:20:"getPreviewProperties";s:4:"desc";s:36:"ms2gallery_prop_getPreviewProperties";s:4:"type";s:13:"combo-boolean";s:7:"options";a:0:{}s:5:"value";b:0;s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}}',
      'moduleguid' => '',
      'static' => 0,
      'static_file' => 'core/components/ms2gallery/elements/snippets/snippet.ms2gallery.php',
      'content' => '/** @var array $scriptProperties */
/** @var ms2Gallery $ms2Gallery */
$ms2Gallery = $modx->getService(\'ms2gallery\', \'ms2Gallery\', MODX_CORE_PATH . \'components/ms2gallery/model/ms2gallery/\');
/** @var pdoFetch $pdoFetch */
if (!$modx->loadClass(\'pdofetch\', MODX_CORE_PATH . \'components/pdotools/model/pdotools/\', false, true)) {
    return false;
}
$pdoFetch = new pdoFetch($modx, $scriptProperties);
$extDir = $modx->getOption(\'extensionsDir\', $scriptProperties, \'components/ms2gallery/img/mgr/extensions/\', true);
$fenom = !empty($scriptProperties[\'tpl\']);

// Register styles and scripts on frontend
$config = $pdoFetch->makePlaceholders($ms2Gallery->config);
$css = $modx->getOption(\'frontend_css\', $scriptProperties, \'frontend_css\');
if (!empty($css) && preg_match(\'#\\.css#i\', $css)) {
    $modx->regClientCSS(str_replace($config[\'pl\'], $config[\'vl\'], $css));
}
$js = $modx->getOption(\'frontend_js\', $scriptProperties, \'frontend_js\');
if (!empty($js) && preg_match(\'#\\.js#i\', $js)) {
    $tmp = json_encode(array(
        \'cssUrl\' => $ms2Gallery->config[\'cssUrl\'] . \'web/\',
        \'jsUrl\' => $ms2Gallery->config[\'jsUrl\'] . \'web/\',
    ));
    $modx->regClientScript(\'<script type="text/javascript">ms2GalleryConfig=\' . $tmp . \';</script>\');
    $modx->regClientScript(str_replace($config[\'pl\'], $config[\'vl\'], $js));
}
if (empty($outputSeparator)) {
    $outputSeparator = "\\n";
}
if (empty($tagsSeparator)) {
    $tagsSeparator = \',\';
}

$where = array(
    \'File.parent\' => 0,
);

// Define where parameters
if ($scriptProperties[\'parents\'] == \'\' && empty($scriptProperties[\'resources\'])) {
    $resources = !empty($resource)
        ? $resource
        : $modx->resource->get(\'id\');
    $scriptProperties[\'resources\'] = $resources;
}

if (!empty($filetype)) {
    $where[\'File.type:IN\'] = array_map(\'trim\', explode(\',\', $filetype));
}

if (empty($showInactive)) {
    $where[\'File.active\'] = 1;
}

$innerJoin = array(
    \'File\' => array(
        \'class\' => \'msResourceFile\',
        \'on\' => \'File.resource_id = modResource.id\',
    ),
);
if (!empty($tagsVar) && isset($_REQUEST[$tagsVar])) {
    $tags = $modx->stripTags($_REQUEST[$tagsVar]);
}
if (!empty($tags)) {
    $tags = array_map(\'trim\', explode(\',\', str_replace(\'"\', \'\', $tags)));
    $tags = implode(\'","\', $tags);
    $innerJoin[\'Tag\'] = array(
        \'class\' => \'msResourceFileTag\',
        \'on\' => \'Tag.file_id = File.id AND Tag.tag IN ("\' . $tags . \'")\',
    );
}

$select = array(
    \'File\' => \'*\',
);

// Set default sort by File table
if (!empty($sortby)) {
    $sortby = array_map(\'trim\', explode(\',\', $sortby));
    foreach ($sortby as &$value) {
        if (strpos($value, \'.\') === false && strpos($value, \'(\') === false) {
            $value = \'File.\' . $value;
        }
    }
    $scriptProperties[\'sortby\'] = implode(\', \', $sortby);
}

// Process additional query params
foreach (array(\'where\', \'innerJoin\', \'select\') as $v) {
    if (!empty($scriptProperties[$v])) {
        $tmp = json_decode($scriptProperties[$v], true);
        if (is_array($tmp)) {
            $$v = array_merge($$v, $tmp);
        }
    }
    unset($scriptProperties[$v]);
}

if (empty($limit) && !empty($offset)) {
    $scriptProperties[\'limit\'] = 10000;
}

// Default parameters
$default = array(
    \'class\' => \'modResource\',
    \'innerJoin\' => $innerJoin,
    \'where\' => json_encode($where),
    \'select\' => $select,
    \'limit\' => 10,
    \'sortby\' => \'rank\',
    \'sortdir\' => \'ASC\',
    \'fastMode\' => false,
    \'groupby\' => \'File.id\',
    \'return\' => \'data\',
    \'nestedChunkPrefix\' => \'ms2gallery_\',
);

// Merge all properties and run!
if (empty($scriptProperties[\'tpl\']) && !empty($tplRow)) {
    $scriptProperties[\'tpl\'] = $tplRow;
}
$pdoFetch->setConfig(array_merge($default, $scriptProperties));
$rows = $pdoFetch->run();

if (!empty($rows)) {
    $tmp = current($rows);
    $resolution = array();
    /** @var modMediaSource $mediaSource */
    if ($mediaSource = $modx->getObject(\'sources.modMediaSource\', $tmp[\'source\'])) {
        $properties = $mediaSource->getProperties();
        if (isset($properties[\'thumbnails\'][\'value\'])) {
            $fileTypes = json_decode($properties[\'thumbnails\'][\'value\'], true);
            foreach ($fileTypes as $k => $v) {
                if (!is_numeric($k)) {
                    $resolution[] = $k;
                } elseif (!empty($v[\'name\'])) {
                    $resolution[] = $v[\'name\'];
                } else {
                    $resolution[] = @$v[\'w\'] . \'x\' . @$v[\'h\'];
                }
            }
        }
    }
}

// Processing rows
$output = null;
$images = array();
foreach ($rows as $k => $row) {
    $row[\'idx\'] = $pdoFetch->idx++;
    $row[\'tags\'] = \'\';

    if (!empty($row[\'type\'])) {
        if ($row[\'type\'] == \'image\') {
            $previews = $pdoFetch->getCollection(\'msResourceFile\', array(\'parent\' => $row[\'id\']));
            foreach ($previews as $preview) {
                if (preg_match("#/{$preview[\'resource_id\']}/(.*?)/#", $preview[\'url\'], $size)) {
                    $row[$size[1]] = !empty($getPreviewProperties)
                        ? $preview
                        : $preview[\'url\'];
                }
            }
        } else {
            $row[\'thumbnail\'] = file_exists(MODX_ASSETS_PATH . $extDir . $row[\'type\'] . \'.png\')
                ? MODX_ASSETS_URL . $extDir . $row[\'type\'] . \'.png\'
                : MODX_ASSETS_URL . $extDir . \'other.png\';
            if (!empty($resolution)) {
                foreach ($resolution as $v) {
                    $row[$v] = $row[\'thumbnail\'];
                }
            }
        }
    }


    if (!empty($getTags)) {
        $q = $modx->newQuery(\'msResourceFileTag\', array(\'file_id\' => $row[\'id\']));
        $q->select(\'tag\');
        $tstart = microtime(true);
        if ($q->prepare() && $q->stmt->execute()) {
            $modx->queryTime += microtime(true) - $tstart;
            $modx->executedQueries++;
            $row[\'tags\'] = implode($tagsSeparator, $q->stmt->fetchAll(PDO::FETCH_COLUMN));
        }
    }

    $images[] = $row;
}
$pdoFetch->addTime(!empty($getTags) ? \'Thumbnails and tags was retrieved\' : \'Thumbnails was retrieved\');

// Processing chunks
$output = array();
if ($fenom) {
    $output[] = $pdoFetch->getChunk($tpl, array(
        \'files\' => $images,
        \'images\' => $images,
    ));
} else {
    foreach ($images as $row) {
        $tpl = $pdoFetch->defineChunk($row);

        $output[] = empty($tpl)
            ? \'<pre>\' . $pdoFetch->getChunk(\'\', $row) . \'</pre>\'
            : $pdoFetch->getChunk($tpl, $row, $pdoFetch->config[\'fastMode\']);
    }
}
$pdoFetch->addTime(\'Rows was templated\');

// Return output
$log = \'\';
if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $log .= \'<pre class="ms2GalleryLog">\' . print_r($pdoFetch->getTime(), 1) . \'</pre>\';
}

if (!$fenom) {
    if (!empty($toSeparatePlaceholders)) {
        $output[\'log\'] = $log;
        $modx->setPlaceholders($output, $toSeparatePlaceholders);

        return;
    }
    if (count($output) === 1 && !empty($tplSingle)) {
        $output = $pdoFetch->getChunk($tplSingle, array_shift($images));
    } else {
        $output = implode($outputSeparator, $output);

        if (!empty($output)) {
            $arr = array_shift($images);
            $arr[\'rows\'] = $output;
            $arr[\'images\'] = $images;
            if (!empty($tplOuter)) {
                $output = $pdoFetch->getChunk($tplOuter, $arr);
            }
        } else {
            $output = !empty($tplEmpty)
                ? $pdoFetch->getChunk($tplEmpty)
                : \'\';
        }
    }
}

if (is_array($output)) {
    $output = implode($outputSeparator, $output);
}
$output .= $log;
if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $output);
} else {
    return $output;
}',
    ),
  ),
  '443833470148978364ab5b69a213e40b' => 
  array (
    'criteria' => 
    array (
      'name' => 'ms2GalleryResources',
    ),
    'object' => 
    array (
      'id' => 55,
      'source' => 1,
      'property_preprocess' => 0,
      'name' => 'ms2GalleryResources',
      'description' => '',
      'editor_type' => 0,
      'category' => 13,
      'cache_type' => 0,
      'snippet' => '/** @var array $scriptProperties */
$class = $modx->getOption(\'class\', $scriptProperties, \'modResource\', true);
$snippet = $modx->getOption(\'snippet\', $scriptProperties, \'pdoResources\', true);

// Load model
if (empty($loadModels)) {
    $scriptProperties[\'loadModels\'] = \'ms2gallery\';
} else {
    $loadModels = array_map(\'trim\', explode(\',\', $loadModels));
    if (!in_array(\'ms2gallery\', $loadModels)) {
        $loadModels[] = \'ms2gallery\';
    }
    $scriptProperties[\'loadModels\'] = $loadModels;
}

// Type of join
if (empty($typeOfJoin)) {
    $typeOfJoin = \'left\';
}
switch (strtolower(trim($typeOfJoin))) {
    case \'right\':
        $join = \'rightJoin\';
        break;
    case \'inner\':
        $join = \'innerJoin\';
        break;
    default:
        $join = \'leftJoin\';
        break;
}

// Select modResource
$columns = array_keys($modx->getFieldMeta($class));
if (empty($includeContent) && empty($useWeblinkUrl)) {
    $key = array_search(\'content\', $columns);
    unset($columns[$key]);
}
$select = array(
    $class => implode(\',\', $columns),
);

// Include Thumbnails
${$join} = array();
if (empty($includeThumbs)) {
    $includeThumbs = \'small\';
}
$thumbs = array_map(\'trim\', explode(\',\', $includeThumbs));
if (!empty($thumbs[0])) {
    foreach ($thumbs as $thumb) {
        ${$join}[] = array(
            \'class\' => \'msResourceFile\',
            \'alias\' => $thumb,
            \'on\' => implode(\' AND \', array(
                "`$thumb`.`resource_id` = `$class`.`id`",
                "`$thumb`.`parent` != 0",
                "`$thumb`.`path` LIKE \'%/$thumb/%\'",
                "`$thumb`.`active` = 1",
                "`$thumb`.`rank` = 0",
            )),
        );
        $select[$thumb] = implode(\',\', array(
            "`$thumb`.`url` as `$thumb`",
            "`$thumb`.`name` as `$thumb.name`",
            "`$thumb`.`description` as `$thumb.description`",
            "`$thumb`.`createdon` as `$thumb.createdon`",
            "`$thumb`.`createdby` as `$thumb.createdby`",
            "`$thumb`.`properties` as `$thumb.properties`",
            "`$thumb`.`alt` as `$thumb.alt`",
            "`$thumb`.`add` as `$thumb.add`",
        ));

        if (!empty($includeOriginal)) {
            ${$join}[] = array(
                \'class\' => \'msResourceFile\',
                \'alias\' => $thumb . \'o\',
                \'on\' => "`{$thumb}o`.`id` = `$thumb`.`parent`",
            );
            $select[$thumb] .= ", `{$thumb}o`.`url` as `$thumb.original`";
        }
    }
}

foreach (array(\'leftJoin\', \'innerJoin\', \'rightJoin\', \'select\') as $v) {
    if (!empty($scriptProperties[$v])) {
        $tmp = json_decode($scriptProperties[$v], true);
        if (is_array($tmp)) {
            $$v = array_merge($$v, $tmp);
        }
    }
    unset($scriptProperties[$v]);
}
$scriptProperties[\'select\'] = json_encode($select);
//$scriptProperties[\'groupby\'] = $class . \'.id\';
$scriptProperties[$join] = json_encode(${$join});

return $modx->runSnippet($snippet, $scriptProperties);',
      'locked' => 0,
      'properties' => 'a:3:{s:10:"typeOfJoin";a:7:{s:4:"name";s:10:"typeOfJoin";s:4:"desc";s:26:"ms2gallery_prop_typeOfJoin";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:4:"left";s:5:"value";s:4:"left";}i:1;a:2:{s:4:"text";s:5:"inner";s:5:"value";s:5:"inner";}}s:5:"value";s:4:"left";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:13:"includeThumbs";a:7:{s:4:"name";s:13:"includeThumbs";s:4:"desc";s:29:"ms2gallery_prop_includeThumbs";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:5:"small";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:15:"includeOriginal";a:7:{s:4:"name";s:15:"includeOriginal";s:4:"desc";s:31:"ms2gallery_prop_includeOriginal";s:4:"type";s:13:"combo-boolean";s:7:"options";a:0:{}s:5:"value";b:0;s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}}',
      'moduleguid' => '',
      'static' => 0,
      'static_file' => 'core/components/ms2gallery/elements/snippets/snippet.ms2gallery_resources.php',
      'content' => '/** @var array $scriptProperties */
$class = $modx->getOption(\'class\', $scriptProperties, \'modResource\', true);
$snippet = $modx->getOption(\'snippet\', $scriptProperties, \'pdoResources\', true);

// Load model
if (empty($loadModels)) {
    $scriptProperties[\'loadModels\'] = \'ms2gallery\';
} else {
    $loadModels = array_map(\'trim\', explode(\',\', $loadModels));
    if (!in_array(\'ms2gallery\', $loadModels)) {
        $loadModels[] = \'ms2gallery\';
    }
    $scriptProperties[\'loadModels\'] = $loadModels;
}

// Type of join
if (empty($typeOfJoin)) {
    $typeOfJoin = \'left\';
}
switch (strtolower(trim($typeOfJoin))) {
    case \'right\':
        $join = \'rightJoin\';
        break;
    case \'inner\':
        $join = \'innerJoin\';
        break;
    default:
        $join = \'leftJoin\';
        break;
}

// Select modResource
$columns = array_keys($modx->getFieldMeta($class));
if (empty($includeContent) && empty($useWeblinkUrl)) {
    $key = array_search(\'content\', $columns);
    unset($columns[$key]);
}
$select = array(
    $class => implode(\',\', $columns),
);

// Include Thumbnails
${$join} = array();
if (empty($includeThumbs)) {
    $includeThumbs = \'small\';
}
$thumbs = array_map(\'trim\', explode(\',\', $includeThumbs));
if (!empty($thumbs[0])) {
    foreach ($thumbs as $thumb) {
        ${$join}[] = array(
            \'class\' => \'msResourceFile\',
            \'alias\' => $thumb,
            \'on\' => implode(\' AND \', array(
                "`$thumb`.`resource_id` = `$class`.`id`",
                "`$thumb`.`parent` != 0",
                "`$thumb`.`path` LIKE \'%/$thumb/%\'",
                "`$thumb`.`active` = 1",
                "`$thumb`.`rank` = 0",
            )),
        );
        $select[$thumb] = implode(\',\', array(
            "`$thumb`.`url` as `$thumb`",
            "`$thumb`.`name` as `$thumb.name`",
            "`$thumb`.`description` as `$thumb.description`",
            "`$thumb`.`createdon` as `$thumb.createdon`",
            "`$thumb`.`createdby` as `$thumb.createdby`",
            "`$thumb`.`properties` as `$thumb.properties`",
            "`$thumb`.`alt` as `$thumb.alt`",
            "`$thumb`.`add` as `$thumb.add`",
        ));

        if (!empty($includeOriginal)) {
            ${$join}[] = array(
                \'class\' => \'msResourceFile\',
                \'alias\' => $thumb . \'o\',
                \'on\' => "`{$thumb}o`.`id` = `$thumb`.`parent`",
            );
            $select[$thumb] .= ", `{$thumb}o`.`url` as `$thumb.original`";
        }
    }
}

foreach (array(\'leftJoin\', \'innerJoin\', \'rightJoin\', \'select\') as $v) {
    if (!empty($scriptProperties[$v])) {
        $tmp = json_decode($scriptProperties[$v], true);
        if (is_array($tmp)) {
            $$v = array_merge($$v, $tmp);
        }
    }
    unset($scriptProperties[$v]);
}
$scriptProperties[\'select\'] = json_encode($select);
//$scriptProperties[\'groupby\'] = $class . \'.id\';
$scriptProperties[$join] = json_encode(${$join});

return $modx->runSnippet($snippet, $scriptProperties);',
    ),
  ),
  '58d58fee509b942d4c6ee2e4ecf98515' => 
  array (
    'criteria' => 
    array (
      'name' => 'ms2Gallery',
    ),
    'object' => 
    array (
      'id' => 13,
      'source' => 1,
      'property_preprocess' => 0,
      'name' => 'ms2Gallery',
      'description' => 'Main plugin for ms2Gallery',
      'editor_type' => 0,
      'category' => 13,
      'cache_type' => 0,
      'plugincode' => '/** @var modX $modx */
/** @var array $scriptProperties */
switch ($modx->event->name) {

    case \'OnDocFormRender\':
        /** @var modResource $resource */
        $templates = array_map(\'trim\', explode(\',\', $modx->getOption(\'ms2gallery_disable_for_templates\')));
        $disable = $mode == \'new\' ||
            ($templates[0] != \'\' && in_array($resource->get(\'template\'), $templates)) ||
            ($resource->class_key == \'msProduct\' &&
                $modx->getOption(\'ms2gallery_disable_for_ms2\', null, true) &&
                !$modx->getOption(\'ms2gallery_sync_ms2\', null, false)
            );
        if (!$disable) {
            /** @var ms2Gallery $ms2Gallery */
            $ms2Gallery = $modx->getService(\'ms2gallery\', \'ms2Gallery\',
                MODX_CORE_PATH . \'components/ms2gallery/model/ms2gallery/\');
            if ($ms2Gallery) {
                $ms2Gallery->loadManagerFiles($modx->controller, $resource);
            }
        }
        break;

    case \'OnBeforeDocFormSave\':
        if ($source_id = $resource->get(\'media_source\')) {
            $resource->setProperties(array(\'media_source\' => $source_id), \'ms2gallery\');
        }
        break;

    case \'OnLoadWebDocument\':
        if (!$modx->getOption(\'ms2gallery_set_placeholders\', null, false, true)) {
            return;
        }
        $tstart = microtime(true);
        /** @var pdoFetch $pdoFetch */
        $pdoFetch = $modx->getService(\'pdoFetch\');
        $plTemplates = array_map(\'trim\', explode(\',\', $modx->getOption(\'ms2gallery_placeholders_for_templates\')));
        if (!empty($plTemplates[0]) && !in_array($modx->resource->get(\'template\'), $plTemplates)) {
            return;
        }
        $plPrefix = $modx->getOption(\'ms2gallery_placeholders_prefix\', null, \'ms2g.\', true);
        $plThumbs = array_map(\'trim\', explode(\',\', $modx->getOption(\'ms2gallery_placeholders_thumbs\')));
        $tplName = $modx->getOption(\'ms2gallery_placeholders_tpl\');

        // Check for assigned TV
        $q = $modx->newQuery(\'modTemplateVarTemplate\');
        $q->innerJoin(\'modTemplateVar\', \'TemplateVar\');
        $q->innerJoin(\'modTemplate\', \'Template\');
        $q->where(array(
            \'TemplateVar.name\' => $tplName,
            \'Template.id\' => $modx->resource->get(\'template\'),
        ));
        $q->select(\'TemplateVar.id\');
        $tpl = $modx->getCount(\'modTemplateVarTemplate\', $q)
            ? \'@INLINE \' . $modx->resource->getTVValue($tplName)
            : $tplName;

        $options = array(\'loadModels\' => \'ms2gallery\');
        $where = array(\'resource_id\' => $modx->resource->id, \'parent\' => 0);

        $parents = $pdoFetch->getCollection(\'msResourceFile\', $where, $options);
        $options[\'select\'] = \'url\';
        foreach ($parents as &$parent) {
            $where = array(\'parent\' => $parent[\'id\']);
            if (!empty($plThumbs[0])) {
                $where[\'path:IN\'] = array();
                foreach ($plThumbs as $thumb) {
                    $where[\'path:IN\'][] = $parent[\'path\'] . $thumb . \'/\';
                }
            }
            if ($children = $pdoFetch->getCollection(\'msResourceFile\', $where, $options)) {
                foreach ($children as $child) {
                    if (preg_match("#/{$parent[\'resource_id\']}/(.*?)/#", $child[\'url\'], $size)) {
                        $parent[$size[1]] = $child[\'url\'];
                    }
                }
            }
            $pls = $pdoFetch->makePlaceholders($parent, $plPrefix . $parent[\'rank\'] . \'.\', \'[[+\', \']]\', false);
            $pls[\'vl\'][$plPrefix . $parent[\'rank\']] = $pdoFetch->getChunk($tpl, $parent);
            $modx->setPlaceholders($pls[\'vl\']);
        }

        $modx->log(modX::LOG_LEVEL_INFO, \'[ms2Gallery] Set image placeholders for page id = \' . $modx->resource->id .
            \' in \' . number_format(microtime(true) - $tstart, 7) . \' sec.\');
        break;

    case \'OnBeforeEmptyTrash\':
        if (empty($scriptProperties[\'ids\']) || !is_array($scriptProperties[\'ids\'])) {
            return;
        }
        if (!$modx->addPackage(\'ms2gallery\', MODX_CORE_PATH . \'components/ms2gallery/model/\')) {
            return;
        }
        $resources = $modx->getIterator(\'modResource\', array(\'id:IN\' => $scriptProperties[\'ids\']));
        /** @var modResource $resource */
        foreach ($resources as $resource) {
            $properties = $resource->getProperties(\'ms2gallery\');
            if (!empty($properties[\'media_source\'])) {
                /** @var modMediaSource $source */
                $source = $modx->getObject(\'modMediaSource\', $properties[\'media_source\']);
                $resource_id = $resource->get(\'id\');
                if ($source) {
                    $source->set(\'ctx\', $resource->get(\'context_key\'));
                    $source->initialize();
                }
                $images = $modx->getIterator(\'msResourceFile\', array(\'resource_id\' => $resource_id, \'parent\' => 0));
                /** @var msResourceFile $image */
                foreach ($images as $image) {
                    $prepare = $image->prepareSource($source);
                    if ($prepare === true) {
                        $image->remove();
                    } else {
                        $modx->log(modX::LOG_LEVEL_ERROR, "[ms2Gallery] {$prepare}.");
                    }
                }
                if ($source) {
                    $source->removeContainer($source->getBasePath() . $resource_id);
                }
            }
        }
        break;

}',
      'locked' => 0,
      'properties' => NULL,
      'disabled' => 0,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => 'core/components/ms2gallery/elements/plugins/plugin.ms2gallery.php',
      'content' => '/** @var modX $modx */
/** @var array $scriptProperties */
switch ($modx->event->name) {

    case \'OnDocFormRender\':
        /** @var modResource $resource */
        $templates = array_map(\'trim\', explode(\',\', $modx->getOption(\'ms2gallery_disable_for_templates\')));
        $disable = $mode == \'new\' ||
            ($templates[0] != \'\' && in_array($resource->get(\'template\'), $templates)) ||
            ($resource->class_key == \'msProduct\' &&
                $modx->getOption(\'ms2gallery_disable_for_ms2\', null, true) &&
                !$modx->getOption(\'ms2gallery_sync_ms2\', null, false)
            );
        if (!$disable) {
            /** @var ms2Gallery $ms2Gallery */
            $ms2Gallery = $modx->getService(\'ms2gallery\', \'ms2Gallery\',
                MODX_CORE_PATH . \'components/ms2gallery/model/ms2gallery/\');
            if ($ms2Gallery) {
                $ms2Gallery->loadManagerFiles($modx->controller, $resource);
            }
        }
        break;

    case \'OnBeforeDocFormSave\':
        if ($source_id = $resource->get(\'media_source\')) {
            $resource->setProperties(array(\'media_source\' => $source_id), \'ms2gallery\');
        }
        break;

    case \'OnLoadWebDocument\':
        if (!$modx->getOption(\'ms2gallery_set_placeholders\', null, false, true)) {
            return;
        }
        $tstart = microtime(true);
        /** @var pdoFetch $pdoFetch */
        $pdoFetch = $modx->getService(\'pdoFetch\');
        $plTemplates = array_map(\'trim\', explode(\',\', $modx->getOption(\'ms2gallery_placeholders_for_templates\')));
        if (!empty($plTemplates[0]) && !in_array($modx->resource->get(\'template\'), $plTemplates)) {
            return;
        }
        $plPrefix = $modx->getOption(\'ms2gallery_placeholders_prefix\', null, \'ms2g.\', true);
        $plThumbs = array_map(\'trim\', explode(\',\', $modx->getOption(\'ms2gallery_placeholders_thumbs\')));
        $tplName = $modx->getOption(\'ms2gallery_placeholders_tpl\');

        // Check for assigned TV
        $q = $modx->newQuery(\'modTemplateVarTemplate\');
        $q->innerJoin(\'modTemplateVar\', \'TemplateVar\');
        $q->innerJoin(\'modTemplate\', \'Template\');
        $q->where(array(
            \'TemplateVar.name\' => $tplName,
            \'Template.id\' => $modx->resource->get(\'template\'),
        ));
        $q->select(\'TemplateVar.id\');
        $tpl = $modx->getCount(\'modTemplateVarTemplate\', $q)
            ? \'@INLINE \' . $modx->resource->getTVValue($tplName)
            : $tplName;

        $options = array(\'loadModels\' => \'ms2gallery\');
        $where = array(\'resource_id\' => $modx->resource->id, \'parent\' => 0);

        $parents = $pdoFetch->getCollection(\'msResourceFile\', $where, $options);
        $options[\'select\'] = \'url\';
        foreach ($parents as &$parent) {
            $where = array(\'parent\' => $parent[\'id\']);
            if (!empty($plThumbs[0])) {
                $where[\'path:IN\'] = array();
                foreach ($plThumbs as $thumb) {
                    $where[\'path:IN\'][] = $parent[\'path\'] . $thumb . \'/\';
                }
            }
            if ($children = $pdoFetch->getCollection(\'msResourceFile\', $where, $options)) {
                foreach ($children as $child) {
                    if (preg_match("#/{$parent[\'resource_id\']}/(.*?)/#", $child[\'url\'], $size)) {
                        $parent[$size[1]] = $child[\'url\'];
                    }
                }
            }
            $pls = $pdoFetch->makePlaceholders($parent, $plPrefix . $parent[\'rank\'] . \'.\', \'[[+\', \']]\', false);
            $pls[\'vl\'][$plPrefix . $parent[\'rank\']] = $pdoFetch->getChunk($tpl, $parent);
            $modx->setPlaceholders($pls[\'vl\']);
        }

        $modx->log(modX::LOG_LEVEL_INFO, \'[ms2Gallery] Set image placeholders for page id = \' . $modx->resource->id .
            \' in \' . number_format(microtime(true) - $tstart, 7) . \' sec.\');
        break;

    case \'OnBeforeEmptyTrash\':
        if (empty($scriptProperties[\'ids\']) || !is_array($scriptProperties[\'ids\'])) {
            return;
        }
        if (!$modx->addPackage(\'ms2gallery\', MODX_CORE_PATH . \'components/ms2gallery/model/\')) {
            return;
        }
        $resources = $modx->getIterator(\'modResource\', array(\'id:IN\' => $scriptProperties[\'ids\']));
        /** @var modResource $resource */
        foreach ($resources as $resource) {
            $properties = $resource->getProperties(\'ms2gallery\');
            if (!empty($properties[\'media_source\'])) {
                /** @var modMediaSource $source */
                $source = $modx->getObject(\'modMediaSource\', $properties[\'media_source\']);
                $resource_id = $resource->get(\'id\');
                if ($source) {
                    $source->set(\'ctx\', $resource->get(\'context_key\'));
                    $source->initialize();
                }
                $images = $modx->getIterator(\'msResourceFile\', array(\'resource_id\' => $resource_id, \'parent\' => 0));
                /** @var msResourceFile $image */
                foreach ($images as $image) {
                    $prepare = $image->prepareSource($source);
                    if ($prepare === true) {
                        $image->remove();
                    } else {
                        $modx->log(modX::LOG_LEVEL_ERROR, "[ms2Gallery] {$prepare}.");
                    }
                }
                if ($source) {
                    $source->removeContainer($source->getBasePath() . $resource_id);
                }
            }
        }
        break;

}',
    ),
  ),
  '69d7142c42dc14e091fedcb69ae4b28d' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 13,
      'event' => 'OnDocFormRender',
    ),
    'object' => 
    array (
      'pluginid' => 13,
      'event' => 'OnDocFormRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '8136e810d6af10d9a62eecb7f441f5b4' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 13,
      'event' => 'OnLoadWebDocument',
    ),
    'object' => 
    array (
      'pluginid' => 13,
      'event' => 'OnLoadWebDocument',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '0ac92133a8b8d190fa7b7fbecc2129a3' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 13,
      'event' => 'OnBeforeDocFormSave',
    ),
    'object' => 
    array (
      'pluginid' => 13,
      'event' => 'OnBeforeDocFormSave',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'ba910041d77488d3f096841f094f0d48' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 13,
      'event' => 'OnBeforeEmptyTrash',
    ),
    'object' => 
    array (
      'pluginid' => 13,
      'event' => 'OnBeforeEmptyTrash',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
);