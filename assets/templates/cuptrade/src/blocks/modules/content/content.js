//== Slider for Content Page
function content_Carousel() {
  const $this = $('.content-carousel');
  let navContainer = $this.find('.content-carousel__sum');
  let contentCarousel_Image = $this.find('.content-carousel__img');
  // console.log(contentCarousel_Image.innerHeight());
  navContainer.css(
    'top',
    contentCarousel_Image.innerHeight() - navContainer.innerHeight()
  );
  let contentCarousel = new Swiper('.content-carousel .swiper-container', {
    loop: true,
    slidesPerView: 1,
    // centeredSlides: true,
    // autoplay: {
    //   delay: 4000,
    // },
    pagination: {
      el: '.content-carousel__total',
      type: 'fraction',
    },
    // Navigation arrows
    navigation: {
      nextEl: '.content-carousel__next',
      prevEl: '.content-carousel__prev',
    },
    breakpoints: {
      // when window width is >= 860px
      220: {
        pagination: {
          el: '.content-carousel__pagination',
          type: 'bullets',
          dynamicBullets: true,
          clickable: true,
        },
      },
      861: {
        pagination: {
          el: '.content-carousel__total',
          type: 'fraction',
        },
        navigation: {
          nextEl: '.content-carousel__next',
          prevEl: '.content-carousel__prev',
        },
      },
    },
  });
}
