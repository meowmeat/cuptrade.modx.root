<?php
//шлём заголовки
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json; charset=utf-8');
if (!isset($modx)) {
    define('MODX_API_MODE', true);
    while (!isset($modx) && ($i = isset($i) ? --$i : 10)) {
        if (($file = dirname(!empty($file) ? dirname($file) : __FILE__) . '/index.php') AND !file_exists($file)) {
            continue;
        }
        require_once $file;
    }
    if (!is_object($modx)) {
        exit('{"success":false,"message":"Access denied"}');
    }
    $modx->getService('error', 'error.modError');
    $modx->getRequest();
    $modx->setLogLevel(modX::LOG_LEVEL_ERROR);
    $modx->setLogTarget('FILE');
    $modx->error->message = null;
}
$ctx = !empty($_REQUEST['ctx']) ? $_REQUEST['ctx'] : $modx->context->get('key');
if ($ctx != $modx->context->get('key')) {
    $modx->switchContext($ctx);
}

$user_id = $modx->user->id;
$product_key = (string)$_POST['product_key'];
$action = (string)$_POST['action'];
$data = ['html' => ''];
$is_success = false;

if($product_key){
    switch($action){
        case 'remove':
            $tpl = '<div class="cart-position__cancel">
                  <span class="cart-position__cancel-head">Вы удалили</span>
                  <span class="cart-position__cancel-info">[[+name]]</span>
                  <div class="cart-position__cancel-action js-cancel-remove" data-key="[[+key]]">Отменить
                    <div class="cart-position__cancel-img"><img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cross-white.svg" alt="" class="cart-position__cancel-ico"></div>
                  </div>
                </div>';
            $data['key'] = $product_key;
            $_SESSION['minishop2']['cart_remove'][$product_key] = $_SESSION['minishop2']['cart'][$product_key];
            $data['name'] = '';
            if($res = $modx->getObject('msProduct', $_SESSION['minishop2']['cart'][$product_key]['id'])){
                $data['name'] = $res->pagetitle;
            }
            $data['html'] = str_replace(['[[+name]]', '[[+key]]'], [$data['name']?:'Товар', $product_key], $tpl);
            $is_success = true;
        break;
        case 'cancel':
            $data['key'] = $product_key;
            if(isset($_SESSION['minishop2']['cart_remove'][$product_key])){
                $_SESSION['minishop2']['cart'][$product_key] = $_SESSION['minishop2']['cart_remove'][$product_key];
                unset($_SESSION['minishop2']['cart_remove'][$product_key]);
                $data['name'] = '';
                if($res = $modx->getObject('msProduct', array('id' => $_SESSION['minishop2']['cart'][$product_key]['id'], 'published' => 1, 'deleted' => 0))){
                    $data['name'] = $res->pagetitle;
                    $is_success = true;
                    $message = 'Позиция добавлена в корзину';
                }
                else{
                    unset($_SESSION['minishop2']['cart'][$product_key]);
                    $message = 'Товар отсутствует';
                }
            }
            else{
                $message = 'Ошибка восстановления позиции в корзине';
            }
        break;
    }
}
else{
    $is_success = false;
    $message = 'Не могу сохранить удаляемую позицию';
}
$out = array('success' => $is_success, 'message' => $message?:'Не могу сохранить удаляемую позицию', 'data' => $data);
die(json_encode($out));