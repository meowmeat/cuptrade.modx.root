<?php
include_once 'setting.inc.php';

$_lang['amocrm'] = 'amocrm';
//$_lang['amocrm_menu_desc'] = 'A sample Extra to develop from.';
//$_lang['amocrm_intro_msg'] = 'You can select multiple items by holding Shift or Ctrl button.';
//
//$_lang['amocrm_items'] = 'Items';
//$_lang['amocrm_item_id'] = 'Id';
//$_lang['amocrm_item_name'] = 'Name';
//$_lang['amocrm_item_description'] = 'Description';
//$_lang['amocrm_item_active'] = 'Active';
//
//$_lang['amocrm_item_create'] = 'Create Item';
//$_lang['amocrm_item_update'] = 'Update Item';
//$_lang['amocrm_item_enable'] = 'Enable Item';
//$_lang['amocrm_items_enable'] = 'Enable Items';
//$_lang['amocrm_item_disable'] = 'Disable Item';
//$_lang['amocrm_items_disable'] = 'Disable Items';
//$_lang['amocrm_item_remove'] = 'Remove Item';
//$_lang['amocrm_items_remove'] = 'Remove Items';
//$_lang['amocrm_item_remove_confirm'] = 'Are you sure you want to remove this Item?';
//$_lang['amocrm_items_remove_confirm'] = 'Are you sure you want to remove this Items?';
//
//$_lang['amocrm_item_err_name'] = 'You must specify the name of Item.';
//$_lang['amocrm_item_err_ae'] = 'An Item already exists with that name.';
//$_lang['amocrm_item_err_nf'] = 'Item not found.';
//$_lang['amocrm_item_err_ns'] = 'Item not specified.';
//$_lang['amocrm_item_err_remove'] = 'An error occurred while trying to remove the Item.';
//$_lang['amocrm_item_err_save'] = 'An error occurred while trying to save the Item.';
//
//$_lang['amocrm_grid_search'] = 'Search';
//$_lang['amocrm_grid_actions'] = 'Actions';