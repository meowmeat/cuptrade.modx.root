//== This section of code should be at the end of main script-file
$(document).ready(function () {
  // console.log( 'Height of screen is ' + currentScreenHeight() );
  currentScreenHeight();
  // equalHeight_Units('.main-page-carousel .swiper-slide');
  // if( $(window).width() > 420 ) {
  //   equalHeight_Units('.artisans__items .artisans-item');
  //   equalHeight_Units('.category__items .category-item__info');
  //   equalHeight_Units('.search__items .search-item__info');
  // }
  // headingContent_NearOffer();
  mainAboutAccent_remove();
  if ($('.catalog-card__carousel').length > 0) {
    catalogCard_Carousel();
    $('.catalog-card__carousel-pagination>.swiper-pagination-bullet').hover(
      function () {
        $(this).trigger('click');
      }
    );
  }
  content_Carousel();
  // navMob_Call();
  gallery_hideElementsTablet();
  productThumbs_BindControls();
  carouselProductRecommend_Mob();
  carouselProductSimilar_Mob();
  // about_Carousel();
  // galleryLeftImages_Count();
  // productImagesCarousel_Mobile();
});

$(window).resize(function () {
  // console.log( 'Height of screen is ' + currentScreenHeight() );
  currentScreenHeight();
  productThumbs_BindControls();
  // equalHeight_Units('.main-page-carousel .swiper-slide');
  // if( $(window).width() > 420 ) {
  //   equalHeight_Units('.artisans__items .artisans-item');
  //   equalHeight_Units('.category__items .category-item__info');
  //   equalHeight_Units('.search__items .search-item__info');
  // }
  // headingContent_NearOffer();
  mainAboutAccent_remove();
  $(function () {
    if ($('.catalog-card__carousel').length > 0) {
      //some-slider-wrap-in
      catalogCard_Carousel();
      $('.catalog-card__carousel-pagination>.swiper-pagination-bullet').hover(
        function () {
          $(this).trigger('click');
        }
      );
    }
  });
  content_Carousel();
  // navMob_Call();
  gallery_hideElementsTablet();
  productThumbs_BindControls();
  carouselProductRecommend_Mob();
  carouselProductSimilar_Mob();
  // about_Carousel();
  // galleryLeftImages_Count();
  // productImagesCarousel_Mobile();
  // modalOverlayHeight_OnResize();
});
