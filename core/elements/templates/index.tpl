<!DOCTYPE html>
<html lang="ru">
{include 'file:chunks/all/head.tpl'}
<body class="{block 'class'}index-page{/block}">
{include 'file:chunks/all/header.tpl'}
{block 'main'}
  <div class="main-nav-section section">
    <div class="section-wrapper">
      <div class="main-nav-wrap">
        {if $header_block = json_decode($_modx->resource.header_block, true)}
          {foreach $header_block as $item}
            {if $item@index == 2}
            <div class="main-nav-wide main-nav-wrap__order2w">
              <div class="main-nav-wide__links">
                <div class="main-nav-wide__title">Продаем в розницу</div>
                {$_modx->getPlaceholder('_menu') | replace:"header-dropdown__item":"main-nav-wide__link" | replace:"data-headernav-img":"data-mainnav-img" | replace:"data-replace>":"data-replace><span class=\"main-nav-wide__link-marker\">•</span> " | replace:"data-mainnav-img=\"":"data-mainnav-img=\"main"}
              </div>
              <div class="main-nav-wide__images">
                {set $main_img_block}
                <img src="assets/templates/cuptrade/public/assets/cuptrade/img/cuosnewmp2.png" alt="" class="main-nav-wide__image-main main-nav-wide__image--active">
                {$_modx->getPlaceholder('_menu_img') | replace:"header-dropdown__image":"main-nav-wide__image" | replace:"id=\"":"id=\"main"}
                {/set}
                {$main_img_block | lazy:true}
              </div>
            </div>
            {/if}
            <a href="{$item.link|url}" class="main-nav-wrap__order{$item@index} main-nav{if $item@index<=1}-flip {if $item@index < 1}main-nav-flip--red{else}main-nav-flip--dark{/if}{/if}">
              <div class="main-nav__title">{$item.title}</div>
              <div class="main-nav__text">{$item.desc|strip_tags:"<b><br><em><s><i><strong><u>"}</div>
              <img src="{$item.img}" alt="{$item.title|htmlent}" class="main-nav__bg">
            </a>
          {/foreach}
        {/if}
      </div>
    </div>
  </div>
  {set $our_block}
  {if $our = json_decode($_modx->resource.our, true)[0]}
  <div class="main-cups-section section">
    <div class="section-wrapper">
      <div class="main-cups__heading">
        <div class="main-cups__heading-title">
          <h1 class="title main-cups__title" style="width: 100%;">{$our.title}</h1>
          <div class="main-cups__subtitle">{$our.desc | strip_tags:"<b><i><em><strong><s><u><br>"}</div>
        </div>
        {if $our.img}
        <div class="main-cups__image-mob">
          <img src="{$our.img}" alt="{$our.title|htmlent}" class="main-cups__img-mob">
        </div>
        {/if}
      </div>
        <div class="main-cups-wrap">
          <div class="main-cups__col">
            {if $our.img}
            <div class="main-cups__image">
              <img src="{$our.img}" alt="{$our.title|htmlent}" class="main-cups__img">
            </div>
            {/if}
          </div>
          <div class="main-cups__col">
            <div class="main-cups">
              {if $items = json_decode($our.items, true)}
                {foreach $items as $item}
                  <div class="main-cups__item">
                    <img src="{$item.img}" class="main-cups__item-ico">
                    <div class="main-cups__item-title">{$item.title}</div>
                    <div class="main-cups__item-text">{$item.desc}</div>
                  </div>
                {/foreach}
              {/if}
            </div>
          </div>
        </div>
    </div>
  </div>
  {/if}
  {/set}
  {$our_block | lazy:true}

  {'!msProductsCustom' | snippet : [
      'wrapIfEmpty' => 0,
      'parents' => 2,
      'sortby' => 'RAND()'
      'limit' => 16,
      'where' => '{"Data.new:=":1}',
      'includeContent' => 1,
      'tplWrapper' => '@INLINE <div class="main-novelty-section section">
    <div class="section-wrapper">
      <div class="section-heading-compound">
        <div class="section-heading__title">Новинки</div>
        <a href="{2|url}" class="main-novelty__more">Все новинки в каталоге<img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_right.svg" alt="" class="main-novelty__more-ico"></a>
        <a href="{2|url}" class="main-novelty__more-mob">Весь каталог<img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_right.svg" alt="" class="main-novelty__more-ico"></a>
      </div>
      <div class="main-novelty">
        <div class="swiper-container">
          <div class="swiper-wrapper">{$output}</div>
        </div>
        <div class="main-novelty__controls">
          <div class="main-novelty__nav main-novelty__prev">
            <!-- <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_left.svg" alt="" class="main-novelty__prev-ico"> -->
            <svg width="8" height="12" viewBox="0 0 8 12" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg main-novelty__prev-ico">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M6.079 11.693l1.442-1.386L3.386 6l4.135-4.307L6.08.307.614 6l5.465 5.693z" />
            </svg>
          </div>
          <div class="main-novelty__nav main-novelty__next">
            <!-- <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_right.svg" alt="" class="main-novelty__next-ico"> -->
            <svg width="8" height="12" viewBox="0 0 8 12" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg main-novelty__next-ico">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M1.721 11.693L.28 10.307 4.414 6 .279 1.693 1.72.307 7.186 6l-5.465 5.693z" />
            </svg>
          </div>
        </div>
      </div>
    </div>
  </div>',
      'tpl' => '@FILE chunks/all/tpl.goods_card.tpl'
  ] | replace:"catalog-card\"":"catalog-card swiper-slide\""}
  
            {*<a href="product.html" class="catalog-card swiper-slide">
              <div class="catalog-card__image">
                <img src="assets/templates/cuptrade/public/assets/cuptrade/img/products/prv-stakanchik_dvuhsloynyy_chernyy_250ml_d03cc.png" alt="" class="catalog-card__img">
              </div>
              <div class="catalog-card__available"><span class="catalog-card__available-text">На складе</span></div>
              <h3 class="catalog-card__title">Черный бумажный стакан 250 мл</h3>
              <div class="catalog-card__text">Собственная логистика, бесплатно* возим по Москве и до терминалов ТК</div>
              <div class="catalog-card__footer">
                <div class="catalog-card__price">от 1,82 ₽</div>
                <div class="catalog-card__forward">Купить <svg width="8" height="12" viewBox="0 0 8 12" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg catalog-card__forward-ico">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M1.721 11.693L.28 10.307 4.414 6 .279 1.693 1.72.307 7.186 6l-5.465 5.693z" />
                  </svg></div>
              </div>
            </a>
            <a href="product.html" class="catalog-card swiper-slide">
              <div class="catalog-card__image">
                <div class="catalog-card__image-label">хит</div>
                <img src="assets/templates/cuptrade/public/assets/cuptrade/img/products/prv-stakanchik_dvuhsloynyy_chernyy_300ml_111ad.png" alt="" class="catalog-card__img">
              </div>
              <div class="catalog-card__available"><span class="catalog-card__available-text">На складе</span></div>
              <h3 class="catalog-card__title">Черный бумажный стакан 300 мл</h3>
              <div class="catalog-card__text">Собственная логистика, бесплатно* возим по Москве и до терминалов ТК</div>
              <div class="catalog-card__footer">
                <div class="catalog-card__price">от 5,80 ₽</div>
                <div class="catalog-card__forward">Купить <svg width="8" height="12" viewBox="0 0 8 12" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg catalog-card__forward-ico">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M1.721 11.693L.28 10.307 4.414 6 .279 1.693 1.72.307 7.186 6l-5.465 5.693z" />
                  </svg></div>
              </div>
            </a>
            <a href="product.html" class="catalog-card swiper-slide">
              <div class="catalog-card__image">
                <div class="catalog-card__image-label">хит</div>
                <img src="assets/templates/cuptrade/public/assets/cuptrade/img/products/prv-stakanchik_odnosloynyy_chernyy_400ml_dd5fa.png" alt="" class="catalog-card__img">
              </div>
              <div class="catalog-card__available"><span class="catalog-card__available-text">На складе</span></div>
              <h3 class="catalog-card__title">Черный бумажный стакан 400 мл</h3>
              <div class="catalog-card__text">Собственная логистика, бесплатно* возим по Москве и до терминалов ТК</div>
              <div class="catalog-card__footer">
                <div class="catalog-card__price">от 7,40 ₽</div>
                <div class="catalog-card__forward">Купить <svg width="8" height="12" viewBox="0 0 8 12" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg catalog-card__forward-ico">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M1.721 11.693L.28 10.307 4.414 6 .279 1.693 1.72.307 7.186 6l-5.465 5.693z" />
                  </svg></div>
              </div>
            </a>
            <a href="product.html" class="catalog-card swiper-slide">
              <div class="catalog-card__image">
                <img src="assets/templates/cuptrade/public/assets/cuptrade/img/products/prv-stakanchik_dvuhsloynyy_kraft_250ml_3e2f0.png" alt="" class="catalog-card__img">
              </div>
              <div class="catalog-card__available"><span class="catalog-card__available-text">На складе</span></div>
              <h3 class="catalog-card__title">Бумажный крафт стакан 250 мл</h3>
              <div class="catalog-card__text">Собственная логистика, бесплатно* возим </div>
              <div class="catalog-card__footer">
                <div class="catalog-card__price">от 4,30 ₽</div>
                <div class="catalog-card__forward">Купить <svg width="8" height="12" viewBox="0 0 8 12" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg catalog-card__forward-ico">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M1.721 11.693L.28 10.307 4.414 6 .279 1.693 1.72.307 7.186 6l-5.465 5.693z" />
                  </svg></div>
              </div>
            </a>*}
          

  {include 'file:chunks/all/advantages.tpl'}
  {*'ms2Gallery' | snippet : [
      'frontend_css' => 0,
      'frontend_js' => 0,
      'resources' => $_modx->resource.id,
      'limit' => 8,
      'setMeta' => 0,
      'tpl' => '@INLINE {if $files?}
      <div class="main-gallery-section section">
      <div class="section-wrapper">
        <div class="section-heading">
          <div class="section-heading__title">Немного кейсов</div>
        </div>
        <div class="gallery">
        {foreach $files as $file}
            <a href="{$file.url}" data-caption="{$file.alt|htmlent}" rel="nofollow" data-fancybox="gallery" class="gallery-preview {if $file@index>5}gallery-preview--hidetablet{/if}">
              <img src="assets/templates/cuptrade/public/assets/cuptrade/img/preloader.gif" data-src="{$file.gallery_origin}" alt="{$file.alt|htmlent}" class="gallery-preview__img lazy">
            </a>
        {/foreach}
        </div>
      </div>
    </div>
      {/if}'
  ]*}
  
  <div class="main-gallery-section section" id="pdopage">
    <div class="section-wrapper">
      <div class="section-info">
        <div class="section-heading__title">Немного кейсов</div>
      </div>
      <div class="gallery-section">
        <div class="section-wrapper">
          <div class="gallery js-gallery-wrap">
      {'!pdoPageCustom' | snippet : [
          'ajaxTplMore' => '@INLINE <div class="flex-centered"><button class="button button-primary design-section__button js-more-btn">Показать ещё</button></div>',
          'ajaxElemMore' => '#pdopage .js-more-btn',
          'ajaxElemRows' => '#pdopage .js-gallery-wrap',
          'element' => 'ms2Gallery@ms2Gallery',
          'resources' => $_modx->resource.id,
          'limit' => 8,
          'ajax' => 1,
          'ajaxMode' => 'button',
          'setMeta' => 0,
          'tplPageWrapper' => '@INLINE <div class="pagination"><ul class="pagination">{$pages}</ul></div>',
          'tpl' => '@INLINE {if $files?}
            {foreach $files as $file}
                <a href="{$file.url}" data-caption="{$file.alt|htmlent}" rel="nofollow" data-fancybox="gallery" class="gallery-preview">
                  <img src="assets/templates/cuptrade/public/assets/cuptrade/img/preloader.gif" data-src="{$file.gallery_origin}" alt="{$file.alt|htmlent}" class="gallery-preview__img lazy">
                </a>
            {/foreach}
          {/if}'
      ]}
          </div>
        </div>
      </div>
        {$_modx->getPlaceholder('page.nav')}
    </div>
  </div>

{set $maincallback = $_modx->getChunk('@FILE chunks/all/maincallback.tpl')}
{$maincallback | lazy:true}
{set $contacts = $_modx->getChunk('@FILE chunks/all/contacts.tpl')}
{$contacts | lazy:true}
{/block}
{set $footer = $_modx->getChunk('@FILE chunks/all/footer.tpl')}
{$footer | lazy:true}
</body>
</html>