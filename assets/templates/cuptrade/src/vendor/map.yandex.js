function getYaMap() {
  if ($('#map').length) {
    ymaps.ready(init);

    var myMap,
      myPlacemark,
      //coordOffice = [55.841347, 37.450998],
      //placemarkCoord = [55.841347, 37.450998];
      coordOffice = COORDS,
      placemarkCoord = COORDS;

    function init() {
      myMap = new ymaps.Map('map', {
        center: placemarkCoord,
        zoom: 16,
        controls: [],
      });

      myMap.behaviors.disable('scrollZoom');
      myMap.controls.add('zoomControl');
      myPlacemark = new ymaps.GeoObject(
        {
          geometry: {
            type: 'Point',
            coordinates: coordOffice,
          },
          properties: {hintContent: ADDRESS,
          balloonContentHeader: ADDRESS,
          balloonContentBody: 'Одноразовая посуда из эко-материалов',
          },
        },
        {
          iconLayout: 'default#image',
          // iconImageHref: 'img/map_location.svg',
          iconImageHref: '/assets/templates/cuptrade/public/assets/cuptrade/img/map_location.svg',
          iconImageSize: [98, 98],
          iconImageOffset: [-29, -75],
        }
      );

      if ($(window).outerWidth() <= 1024) {
        myMap.behaviors.disable('drag');
      }

      myMap.geoObjects.add(myPlacemark);
    }
  }
}
