<?php return array (
  'manifest-version' => '1.1',
  'manifest-attributes' => 
  array (
    'changelog' => 'Changelog for amoCRM.

1.9.0-pl
==============
- Полностью обновленная версия инсталятора

1.8.5-pl
==============
- Исправлена ошибка связанная с обращением к пустому итератору объекта msOrderStatus

1.8.4-pl
==============
- В запрос дополнительных полей добавлена поддержка параметра page и обход пагинации для поддержки большого количества полей.

1.8.3-pl
==============
- Удалены ссылки на объекты, как legacy код
- Весь код отформатирован по стандарту PSR-12
- Оптимизирован скрипт запуска cron задач для SQ
- Добавлена привязка контакта к заявке в формах

1.8.2-pl
==============
- Обновлен код вызова события
- Исправлена поддержка дополнительных полей при вызове события amocrmOnBeforeOrderSend

1.8.1-pl
==============
- Исправлена ошибка кэширования токена, приводящая к ошибке авторизации
- Исправлена ошибка отправки поля name напрямую из формы

1.8.0-pl
==============
- Добавлена системная настройка amocrm_token_field
- Изменена логика хранения токена авторизации.  Сведения по токену перенесены из кэша в системную настройку
- Работа с авторизацией вынесена в отдельный контроллер
- Оптимизирован плагин amocrm. Решена ошибка постоянных попыток авторизации при незаполненных авторизационных данных
- Добавлено автоматическое создание контакта и его привязка к заказу
- Улучшен поиск клиента по телефону

1.7.0-pl
==============
- Логика работы со сделками вынесена в отдельный контроллер
- Упрощена логика работы главного контроллера
- Доработаны методы контроллера контактов

1.6.1-pl
==============
- Исправлена ошибка синхронизации статусов заказа в случае отсутствия miniShop2

1.6.0-pl
==============
- Работа с контактами вынесена в отдельный контроллер
- Доработано взаимодействие с API
- Доработано взаимодействие с хуками

1.5.0-pl
==============
- Версия API повышена со 2 до 4

1.4.0-pl
==============
- Добавлен обновленный способ авторизации через oauth

1.3.4-pl
==============
- Добавлен плагин-пример модификации сделок
- Улучшено определение родителей товаров

1.3.3-pl
==============
- Исправлена обработка дополнительных полей заказа, возвращаемых из плагинов

1.3.2-pl
==============
- Исправлена обработка дополнительных полей заказа, возвращаемых из плагинов
- Добавлен плагин-пример для переноса названия оплаты в заказе в дополнительное поле с типом ENUM

1.3.1-pl2
==============
- Исправлена передача артикулов в сделку
- Обновлен плагин захвата UTM-меток

1.3.1-pl
==============
- Добавлена передача артикулов товаров в сделку

1.3.0-beta
==============
- Добавлена возможность обновления всех свойств заказа в amoCRM при изменении статуса на сайте
- Добавлена передача ответа от amoCRM в плагины после передачи данных
- Добавлен плагин-пример для изменения ответственного
- Добавлены поля по умолчанию для пользователей (контактов)
- Скорректировано описание системной настройки amocrm_default_responsible_user_id

1.2.2-beta2
==============
- Добавлено поле visitor_uid к стандартным полям сделок

1.2.2-beta
==============
- Актуализирован метод смены статуса заказа
- Добавлен плагин-пример для переименования сделок
- Добавлен пропуск заданий на синхронизацию после 5 неудачных ошибок обработки
- Изменена проверка активного режима вебхука в плагине-примере
- Изменено время задержки для обработки заданий от вебхука
- Исправлен возврат значений из плагина
- Улучшена работа с полями почты и телефона
- Улучшено обновление пользователя

1.2.1-beta
==============
- Добавлен скрипт для установки статусов сделок заказам ms2
- Добавлен учет контекста при поиске воронок и других опций по категории
- Добавлена возможность установки дополнительных полей контактам через extended поля пользователей
- Добавлена поддержка очереди simpleQueue для вебхука
- Добавлена сортировка при выборке заданий на отправку
- Добавлено сохранение воронки для amoCRMLead при отправке формы
- Изменен механизм выставления и проверки работы в режиме вебхука
- Изменена выборка заданий с коллекции на единичные объекты
- Исключено пустое событие OnHandleRequest для основного плагина
- Исправлена ошибка добавления контакта с отсутствующими телефоном и почтой
- Отключено сохранение связи со сделкой, если от amoCRM не получен ID сделки
- Улучшен плагин-пример отлова UTM-меток для их сохранения в профиль пользователя
- Улучшен скрипт-пример для консоли, добавляющий контакт и выводящий ссылки на страницы пользователей
- Улучшена обработка полей контактов, сохраняемых в extended пользователей

1.2.0-beta
==============
- Добавлен плагин для захвата и передачи UTM-меток
- Исправлен метод amoCRM::changeOrderStatusInAmo для случая отсутствующего ID сделки
- Обновлен плагин-пример для работы с amoCRMTools
- Улучшена совместимость с Office плагина сохранения множественных полей

1.2.0-alpha
==============
- Добавлен вспомогательный класс amoCRMTools, все служебные методы перенесены в него
- Добавлен метод нормализации российских мобильных телефонных номеров
- Добавлен скрипт для нормализации телефонов и удаления дублирующихся значений полей в контактах amoCRM
- Добавлен скрипт для консоли, добавляющий контакт и выводящий ссылки на страницы пользователей
- Добавлен скрипт очистки дополнительных полей от дублей и нормализации номеров телефонов
- Добавлена возможность указания произвольных полей для контакта при оформлении заказа
- Добавлена системная настройка для указания префикса полей адреса заказа
- Добавлена системная настройка amocrm_user_fields_glue_amo_values, значение которой используется при склейке множественных значений полей контактов при получении из amoCRM
- Добавлено внесение пакета amoCRM в системную настройку extension_packages при установке
- Добавлено обновление аккаунта, если массив дополнительных пустой
- Замена array_merge на array_replace для поддержки указания ID полей
- Исключено дублирование почты и телефона для контактов при добавлении заказа
- Исправлен механизм создания связи "Пользователь MODX" -- "Контакт amoCRM"
- Исправлен механизм смены статуса для удаленной в amoCRM сделки
- Исправлен возвращаемого значения метода добавления формы, если не включено создание сделок
- Исправлена подготовка контактов из пользователей, если не передано поле name
- Исправлено создание дублирующихся значений в дополнительных полях
- Оптимизирован основной плагин
- Улучшен механизм подготовки полей пользователя
- Улучшен механизм разбора полей
- Улучшена защита полей контактов amoCRM от изменения
- Улучшена проверка существования дополнительных полей для сделок
- Улучшено применение стоимости сделки при отправке формы
- Улучшены методы получения ID дополнительных полей

1.1.6-beta
==============
- Добавлен скрипт сохранения зависимых от категорий параметров в заказе при работе в контексте отличном от WEB
- Добавлена настройка для списка полей контактов, значения которых в amo не обновляются значениями с сайта
- Улучшено сохранение ID контактов в БД
- Метод updateContact помечен "устаревшим"

1.1.5-beta2
==============
- Добавлен вызов событий при создании сделок из формы
- Исправлена передача данных при добавлении формы

1.1.5-beta
==============
- Добавлена возможность передачи особых параметров $amoCRM в скрипт фоновой отправки данных
- Добавлена возможность принудительной авторизации
- Исправлено создание отдельных экземпляров amoCRM в скрипте фоновой отправки
- Улучшена обработка возвращаемых из плагинов данных
- Добавлена передача ID контактов amoCRM в плагины
- Метод обновления контакта изменен на актуальную версию API
- Добавлен пример плагина для сохранения в amoCRM имеющихся значений ENUM полей
- Добавлена системная настройка для указания ответственного по умолчанию

1.1.4-beta1
==============
- Добавлена передача экземпляра класса (amoCRM и amoCRMWebhook) в вызываемые события
- Добавлена поддержка очередей simpleQueue и отправки данных в отдельном потоке
- Добавлена прозрачная авторизация
- Добавлена системная настройка для пропуска пустых полей при передаче данных
- Добавлены примеры дополнительных скриптов
- Добавлены события отправки пользователей
- Исправлено обновление пользователя из контакта (добавлена передача username в процессор)

1.1.3-beta1
==============
- Исправлена проверка воронки сделки при изменении статуса заказа в MODX
- Возвращено поле товаров в заказе в список передаваемых полей сделки
- Добавлена возможность смены статуса заказа из сайта в AMO в любой воронке
- Исправлено добавление сделки к контакту при отправке формы
- Добавлены методы работы с вебхуками в amoCRM: подписка, отписка, получение
- Добавлены вспомогательные скрипты

1.1.2-beta1
==============
- Добавлены события при отправке сделки и контакта, получении данных при вызове вебхука

1.1.1-rc5
==============
- Добавлена отправка пользователя при сохранении профиля $profile->save()
- Улучшено сохранение сделок для существующих контактов в amoCRM
- Исправлен поиск воронок и статусов по категории товаров - при отсутствии по категории не затираются ранее выставленные значения
- Исправлена передача пользователя при сохранении в админке
- Исправлена смена статуса MS2 для нового заказа при получении данных из amoCRM
- Скорректирован возврат результата работы метода обновления воронки в amoCRM
- Незначительные корректировки отладочного логирования

1.1.1-rc4
==============
- Исправлено обновление контакта при поиске по телефону
- Исправлена обработка полей адреса заказа
- Исключенио поле goods из списка обрабатываемых полей заказа (добавляется всегда независимо от списка)

1.1.1-rc3
==============
- Изменен формат полей-перечислений по умолчанию (исключен array_flip)
- Исправлено формирование запроса для поиска существующего контакта в amoCRM
- Улучшено определение ID статуса amoCRM для нового заказа

1.1.1-rc2
==============
- Улучшен механизм обработки списков полей
- Улучшен механизм определения ENUM полей

1.1.1-rc1
==============
- Добавлен возврат результата добавления контакта из формы
- Добавлен метод получения всех сделок по ID
- Добавлен механизм автообновлений воронок и их статусов
- Добавлена возможность включения/отключения автоматического создания несуществующих в amoCRM дополнительных полей контактов и сделок
- Добавлена возможность включения/отключения передачи пользователей из MODX в amoCRM при редактировании в админке
- Добавлена возможность использования любых полей в массиве properties заказа в качестве значения дополнительных полей для сделок
- Добавлена возможность синхронизации контактов из amoCRM
- Добавлена возможность указания воронки, статуса и ответственного для сделки на основании категории одного из товаров
- Добавлена возможность указания параметров подключения к amoCRM для хука FormIt
- Добавлена возможность указания передаваемых дополнительных полей контакта
- Добавлена возможность указания списков полей в 4-х форматах: прежний строковый (f==a||ff==aa), PHP-массив, JSON-массив, перечисление через запятую
- Добавлена поддержка ENUM для полей контактов
- Добавлена работа с компаниями в amoCRM
- Добавлена системная настройка для автообновления воронок
- Добавлена системная настройка для включения/отключения большего приоритета ответственного из свойств категории над указанным в properties заказа
- Добавлено отключение синхронизации пользователя при сохранении в админке или активном приеме данных из amoCRM через Webhook
- Добавлено поле goods в список дополнительных полей заказа для передачи купленных товаров
- Добавлено сохранение ID воронки для объекта amoCRMLead
- Обновлен список стандартных полей контактов и сделок, включая тэги
- Обновлено взаимодействие с API amoCRM по многим запросам
- Улучшена проверка существования контакта в amoCRM
- Улучшена работа с дополнительными полями
- Улучшена работа с полями формы в хуке FormIt
- Улучшено определение воронки и статуса для новой сделки
- Улучшено получение статусов воронки
- Исправлено выставление воронки и ее статуса при изменении статуса заказа в MS2
- Исправлено получение сделок существующего контакта
- Исправлено формирование названия новой сделки из формы
- Иные незначительные исправления и улучшения

1.0.9-pl3
==============
- Добавлена защита дополнения

1.0.8-pl3
==============
- Фикс бага при создании заказа авторизованным пользователем

1.0.7-pl3
==============
- Добавлена поддержка тэгов в качестве полей сделки

1.0.7-pl2
==============
- Исправлена ошибка проверки заполненности обязательных для форм полей

1.0.7-pl
==============
- Добавлена проверка на заполненность обязательных полей формы до создания контакта

1.0.6-pl
==============
- Добавление responsible_user_id в список разрешенных полей для контактов и сделок
- Добавление сниппета-хука amoCRMAddContactGeo для примера взаимодействия с другими компонентами и добавления ответственных в заявки на основе геопозиции

1.0.5-pl2
==============
- Оптимизация работы с cookie
- Корректировка работы с полями по ID
- Устранение излишнего логирования

1.0.5-pl
==============
- Изменение способа хранения cookie
- Корректировка получения имени контакта (username, если пусто fullname)
- Добавление подбора дополнительного поля по ID

1.0.4-beta
==============
- Добавление заявок из форм в качестве сделок
- Устранение предупреждения PHP

1.0.3-pl
==============
- Исправление работы hook\'а для FormIt

1.0.2-pl
==============
- Добавлена проверка контекста в плагине перед созданием контакта

1.0.1-pl
==============
- Добавлена передача телефона контакту при создании сделки
- Исправление ошибки в сниппете amoCRMAddContact

1.0.0-pl
==============
- Добавлены списки полей адреса и заказа для передачи в amoCRM при создании сделки

1.0.0-beta2
==============
- Исправлено создание системной настройки для нового заказа amocrm_new_order_status_id

1.0.0-beta
==============
- Первая публичная версия
',
    'license' => 'GNU GENERAL PUBLIC LICENSE
   Version 2, June 1991
--------------------------

Copyright (C) 1989, 1991 Free Software Foundation, Inc.
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Everyone is permitted to copy and distribute verbatim copies
of this license document, but changing it is not allowed.

Preamble
--------

  The licenses for most software are designed to take away your
freedom to share and change it.  By contrast, the GNU General Public
License is intended to guarantee your freedom to share and change free
software--to make sure the software is free for all its users.  This
General Public License applies to most of the Free Software
Foundation\'s software and to any other program whose authors commit to
using it.  (Some other Free Software Foundation software is covered by
the GNU Library General Public License instead.)  You can apply it to
your programs, too.

  When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure that you
have the freedom to distribute copies of free software (and charge for
this service if you wish), that you receive source code or can get it
if you want it, that you can change the software or use pieces of it
in new free programs; and that you know you can do these things.

  To protect your rights, we need to make restrictions that forbid
anyone to deny you these rights or to ask you to surrender the rights.
These restrictions translate to certain responsibilities for you if you
distribute copies of the software, or if you modify it.

  For example, if you distribute copies of such a program, whether
gratis or for a fee, you must give the recipients all the rights that
you have.  You must make sure that they, too, receive or can get the
source code.  And you must show them these terms so they know their
rights.

  We protect your rights with two steps: (1) copyright the software, and
(2) offer you this license which gives you legal permission to copy,
distribute and/or modify the software.

  Also, for each author\'s protection and ours, we want to make certain
that everyone understands that there is no warranty for this free
software.  If the software is modified by someone else and passed on, we
want its recipients to know that what they have is not the original, so
that any problems introduced by others will not reflect on the original
authors\' reputations.

  Finally, any free program is threatened constantly by software
patents.  We wish to avoid the danger that redistributors of a free
program will individually obtain patent licenses, in effect making the
program proprietary.  To prevent this, we have made it clear that any
patent must be licensed for everyone\'s free use or not licensed at all.

  The precise terms and conditions for copying, distribution and
modification follow.


GNU GENERAL PUBLIC LICENSE
TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
---------------------------------------------------------------

  0. This License applies to any program or other work which contains
a notice placed by the copyright holder saying it may be distributed
under the terms of this General Public License.  The "Program", below,
refers to any such program or work, and a "work based on the Program"
means either the Program or any derivative work under copyright law:
that is to say, a work containing the Program or a portion of it,
either verbatim or with modifications and/or translated into another
language.  (Hereinafter, translation is included without limitation in
the term "modification".)  Each licensee is addressed as "you".

Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running the Program is not restricted, and the output from the Program
is covered only if its contents constitute a work based on the
Program (independent of having been made by running the Program).
Whether that is true depends on what the Program does.

  1. You may copy and distribute verbatim copies of the Program\'s
source code as you receive it, in any medium, provided that you
conspicuously and appropriately publish on each copy an appropriate
copyright notice and disclaimer of warranty; keep intact all the
notices that refer to this License and to the absence of any warranty;
and give any other recipients of the Program a copy of this License
along with the Program.

You may charge a fee for the physical act of transferring a copy, and
you may at your option offer warranty protection in exchange for a fee.

  2. You may modify your copy or copies of the Program or any portion
of it, thus forming a work based on the Program, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) You must cause the modified files to carry prominent notices
    stating that you changed the files and the date of any change.

    b) You must cause any work that you distribute or publish, that in
    whole or in part contains or is derived from the Program or any
    part thereof, to be licensed as a whole at no charge to all third
    parties under the terms of this License.

    c) If the modified program normally reads commands interactively
    when run, you must cause it, when started running for such
    interactive use in the most ordinary way, to print or display an
    announcement including an appropriate copyright notice and a
    notice that there is no warranty (or else, saying that you provide
    a warranty) and that users may redistribute the program under
    these conditions, and telling the user how to view a copy of this
    License.  (Exception: if the Program itself is interactive but
    does not normally print such an announcement, your work based on
    the Program is not required to print an announcement.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Program,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Program, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Program.

In addition, mere aggregation of another work not based on the Program
with the Program (or with a work based on the Program) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

  3. You may copy and distribute the Program (or a work based on it,
under Section 2) in object code or executable form under the terms of
Sections 1 and 2 above provided that you also do one of the following:

    a) Accompany it with the complete corresponding machine-readable
    source code, which must be distributed under the terms of Sections
    1 and 2 above on a medium customarily used for software interchange; or,

    b) Accompany it with a written offer, valid for at least three
    years, to give any third party, for a charge no more than your
    cost of physically performing source distribution, a complete
    machine-readable copy of the corresponding source code, to be
    distributed under the terms of Sections 1 and 2 above on a medium
    customarily used for software interchange; or,

    c) Accompany it with the information you received as to the offer
    to distribute corresponding source code.  (This alternative is
    allowed only for noncommercial distribution and only if you
    received the program in object code or executable form with such
    an offer, in accord with Subsection b above.)

The source code for a work means the preferred form of the work for
making modifications to it.  For an executable work, complete source
code means all the source code for all modules it contains, plus any
associated interface definition files, plus the scripts used to
control compilation and installation of the executable.  However, as a
special exception, the source code distributed need not include
anything that is normally distributed (in either source or binary
form) with the major components (compiler, kernel, and so on) of the
operating system on which the executable runs, unless that component
itself accompanies the executable.

If distribution of executable or object code is made by offering
access to copy from a designated place, then offering equivalent
access to copy the source code from the same place counts as
distribution of the source code, even though third parties are not
compelled to copy the source along with the object code.

  4. You may not copy, modify, sublicense, or distribute the Program
except as expressly provided under this License.  Any attempt
otherwise to copy, modify, sublicense or distribute the Program is
void, and will automatically terminate your rights under this License.
However, parties who have received copies, or rights, from you under
this License will not have their licenses terminated so long as such
parties remain in full compliance.

  5. You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Program or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Program (or any work based on the
Program), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Program or works based on it.

  6. Each time you redistribute the Program (or any work based on the
Program), the recipient automatically receives a license from the
original licensor to copy, distribute or modify the Program subject to
these terms and conditions.  You may not impose any further
restrictions on the recipients\' exercise of the rights granted herein.
You are not responsible for enforcing compliance by third parties to
this License.

  7. If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Program at all.  For example, if a patent
license would not permit royalty-free redistribution of the Program by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Program.

If any portion of this section is held invalid or unenforceable under
any particular circumstance, the balance of the section is intended to
apply and the section as a whole is intended to apply in other
circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system, which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

  8. If the distribution and/or use of the Program is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Program under this License
may add an explicit geographical distribution limitation excluding
those countries, so that distribution is permitted only in or among
countries not thus excluded.  In such case, this License incorporates
the limitation as if written in the body of this License.

  9. The Free Software Foundation may publish revised and/or new versions
of the General Public License from time to time.  Such new versions will
be similar in spirit to the present version, but may differ in detail to
address new problems or concerns.

Each version is given a distinguishing version number.  If the Program
specifies a version number of this License which applies to it and "any
later version", you have the option of following the terms and conditions
either of that version or of any later version published by the Free
Software Foundation.  If the Program does not specify a version number of
this License, you may choose any version ever published by the Free Software
Foundation.

  10. If you wish to incorporate parts of the Program into other free
programs whose distribution conditions are different, write to the author
to ask for permission.  For software which is copyrighted by the Free
Software Foundation, write to the Free Software Foundation; we sometimes
make exceptions for this.  Our decision will be guided by the two goals
of preserving the free status of all derivatives of our free software and
of promoting the sharing and reuse of software generally.

NO WARRANTY
-----------

  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS
TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,
REPAIR OR CORRECTION.

  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING
OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED
TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER
PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES.

---------------------------
END OF TERMS AND CONDITIONS',
    'readme' => '--------------------
amoCRM
--------------------
Author: Mikhail Voevodskiy
Support:  Nikolay Savin info@megawebs.kz
--------------------

Module integration between AmoCRM and Revolution.',
  ),
  'manifest-vehicles' => 
  array (
    0 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOFileVehicle',
      'class' => 'xPDOFileVehicle',
      'guid' => '33f84ba715fa9a8b8596116cdaa467cf',
      'native_key' => '33f84ba715fa9a8b8596116cdaa467cf',
      'filename' => 'xPDOFileVehicle/b5311ca59ee11de8afbf8f0dc7343d8c.vehicle',
    ),
    1 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOScriptVehicle',
      'class' => 'xPDOScriptVehicle',
      'guid' => 'f03f59012162f22c769bfb5141631508',
      'native_key' => 'f03f59012162f22c769bfb5141631508',
      'filename' => 'xPDOScriptVehicle/ae1074826c85c52313b9c6faf48f9fec.vehicle',
    ),
    2 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modNamespace',
      'guid' => '8a3ea97734babf249c18115018f37394',
      'native_key' => 'amocrm',
      'filename' => 'modNamespace/7c1ff671ea866e252bed477c2f9b076a.vehicle',
      'namespace' => 'amocrm',
    ),
    3 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'c1d47000bcce48d4c717e670ca2b04fa',
      'native_key' => 'amocrm_account',
      'filename' => 'modSystemSetting/e65c804e02725d6993d6ba58d08ed570.vehicle',
      'namespace' => 'amocrm',
    ),
    4 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '1fd48be87bed28cc6add60c03911bec1',
      'native_key' => 'amocrm_client_id',
      'filename' => 'modSystemSetting/36ac1bf58e5cb398a93c319819493302.vehicle',
      'namespace' => 'amocrm',
    ),
    5 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'e8296f37f8c1f08a6a24abf04eb5891d',
      'native_key' => 'amocrm_client_secret',
      'filename' => 'modSystemSetting/5245fc5de96ca0333d2c3d84a134c0be.vehicle',
      'namespace' => 'amocrm',
    ),
    6 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'bb166f9d72e871303c331f937a8e121d',
      'native_key' => 'amocrm_client_code',
      'filename' => 'modSystemSetting/ba7a7af02ee0bbcc57acc25cac89a89f.vehicle',
      'namespace' => 'amocrm',
    ),
    7 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'c68e5c10c0713500ea88ea6eeff23848',
      'native_key' => 'amocrm_form_as_lead',
      'filename' => 'modSystemSetting/b172681eaadb8ac2d784063538b9772d.vehicle',
      'namespace' => 'amocrm',
    ),
    8 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '6c9c90650296f9d27eaa153b70ca45b0',
      'native_key' => 'amocrm_save_user_in_mgr',
      'filename' => 'modSystemSetting/244193b1c4af799bf92b3b5b99abe51e.vehicle',
      'namespace' => 'amocrm',
    ),
    9 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '3d6bcf1740ffd463955127a93d469c69',
      'native_key' => 'amocrm_save_user_by_profile',
      'filename' => 'modSystemSetting/4ca34bc7132c0abe3b2d17069eb7d0b5.vehicle',
      'namespace' => 'amocrm',
    ),
    10 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'd04c7484040613634ad9c7df04c99293',
      'native_key' => 'amocrm_use_simple_queue',
      'filename' => 'modSystemSetting/c4939f40f8213d20834c829f858b202c.vehicle',
      'namespace' => 'amocrm',
    ),
    11 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'd0e23d7d99c90893e91682d747b8e3da',
      'native_key' => 'amocrm_token_field',
      'filename' => 'modSystemSetting/7cf7e5d30df9c89062cf775ca35cf8fa.vehicle',
      'namespace' => 'amocrm',
    ),
    12 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '1351efe717c7324e847b34279ae17102',
      'native_key' => 'amocrm_pipeline_id',
      'filename' => 'modSystemSetting/33c1e9ddc887be01de8dcc33b5eb3db4.vehicle',
      'namespace' => 'amocrm',
    ),
    13 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '769197020db29824afbd3ea9ab383ef9',
      'native_key' => 'amocrm_form_pipeline_id',
      'filename' => 'modSystemSetting/83011de3f4507059c297bc109ca56ea3.vehicle',
      'namespace' => 'amocrm',
    ),
    14 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '101b707dea717f0cd72884241975dac1',
      'native_key' => 'amocrm_new_order_status_id',
      'filename' => 'modSystemSetting/5358256f17e63a10bd5665b70ad25729.vehicle',
      'namespace' => 'amocrm',
    ),
    15 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'd545372e06f48ad07543fd72f6aad2c1',
      'native_key' => 'amocrm_form_status_new',
      'filename' => 'modSystemSetting/5910a167e9e3c052fb3f44c05c5e4a25.vehicle',
      'namespace' => 'amocrm',
    ),
    16 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'e89c6c5d7b0e3cbfb4af2c2101d5743d',
      'native_key' => 'amocrm_auto_update_pipelines',
      'filename' => 'modSystemSetting/66b7ca562988108125b409884b6576db.vehicle',
      'namespace' => 'amocrm',
    ),
    17 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'd6d2349a2005f810a136d813eed287ba',
      'native_key' => 'amocrm_form_filled_fields',
      'filename' => 'modSystemSetting/7cb7f2aaea9cadb036b4e68e27edbe3e.vehicle',
      'namespace' => 'amocrm',
    ),
    18 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '57d080be72278da5ccf327a14dc41e49',
      'native_key' => 'amocrm_order_fields',
      'filename' => 'modSystemSetting/925fb0a587284e30a81b3ab556b6f2a9.vehicle',
      'namespace' => 'amocrm',
    ),
    19 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '4e58323a4879204a8a8a07b74becbc8e',
      'native_key' => 'amocrm_order_properties_element',
      'filename' => 'modSystemSetting/ba24d34b8637a622814b7d3a4a7dba00.vehicle',
      'namespace' => 'amocrm',
    ),
    20 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '20755bff0d5e54e65335275f7fbacf53',
      'native_key' => 'amocrm_order_address_fields',
      'filename' => 'modSystemSetting/f000dab260353502e916193d72c3ff89.vehicle',
      'namespace' => 'amocrm',
    ),
    21 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '4d5c963a74523280163d5668d7abfc0d',
      'native_key' => 'amocrm_order_address_fields_prefix',
      'filename' => 'modSystemSetting/88f4e657f5e5dc58f032c3253d238ab2.vehicle',
      'namespace' => 'amocrm',
    ),
    22 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '9a1b9ca3baf82bf94b6588d5c67726a5',
      'native_key' => 'amocrm_responsible_id_priority_category',
      'filename' => 'modSystemSetting/66e61a9da2036d0bc1c66eecd572f5a1.vehicle',
      'namespace' => 'amocrm',
    ),
    23 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '43f54e8cc461415902218d4980d70e32',
      'native_key' => 'amocrm_user_fields',
      'filename' => 'modSystemSetting/88fd15552bdd347737f5e23caf7e0423.vehicle',
      'namespace' => 'amocrm',
    ),
    24 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '705378265636bf68a2512e0aff49e1e2',
      'native_key' => 'amocrm_categories_pipelines',
      'filename' => 'modSystemSetting/4789c6fb9101f49d09839952c04880b3.vehicle',
      'namespace' => 'amocrm',
    ),
    25 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'dd42999d39385bc4da25654d86cdc426',
      'native_key' => 'amocrm_auto_create_users_fields',
      'filename' => 'modSystemSetting/6a4136e852f68ccca0ad435a1e8b52cf.vehicle',
      'namespace' => 'amocrm',
    ),
    26 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '63a57563db70095fbc2364df60eeb600',
      'native_key' => 'amocrm_auto_create_orders_fields',
      'filename' => 'modSystemSetting/c89ae7520a6723373fa30f9844b9c4ff.vehicle',
      'namespace' => 'amocrm',
    ),
    27 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '30176e669100a1e2a751962852ef9fe7',
      'native_key' => 'amocrm_user_enum_fields',
      'filename' => 'modSystemSetting/7eb7ab994226c0364ff8611e6e8a2d7a.vehicle',
      'namespace' => 'amocrm',
    ),
    28 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'f06beab71f551dba59841fd7945d8da6',
      'native_key' => 'amocrm_user_readonly_fields',
      'filename' => 'modSystemSetting/a3a0719f2d8f08753e0401c635d77822.vehicle',
      'namespace' => 'amocrm',
    ),
    29 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '8b87e24f2d3911a877d7e4100f408376',
      'native_key' => 'amocrm_user_fields_glue_amo_values',
      'filename' => 'modSystemSetting/1bf26a341ef82197234b2e541ff9b877.vehicle',
      'namespace' => 'amocrm',
    ),
    30 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '5f1dc1b545da589ad753ad1127f6f209',
      'native_key' => 'amocrm_skip_empty_fields',
      'filename' => 'modSystemSetting/aa0f87f098e6a87a91166715d08a005f.vehicle',
      'namespace' => 'amocrm',
    ),
    31 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '0c5e85527e6813089f27ff75875fcd7f',
      'native_key' => 'amocrm_default_responsible_user_id',
      'filename' => 'modSystemSetting/3472a1a17a924e4f66eb22a2aa5bd3e2.vehicle',
      'namespace' => 'amocrm',
    ),
    32 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '6c90b863436ca61a03aae6be905429ef',
      'native_key' => 'amocrm_update_order_on_change_status',
      'filename' => 'modSystemSetting/785f216256ea19dd41a68cb7d6430059.vehicle',
      'namespace' => 'amocrm',
    ),
    33 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'encryptedVehicle',
      'class' => 'modCategory',
      'guid' => '5fc6d39bf832214164dec0462c0b4c37',
      'native_key' => NULL,
      'filename' => 'modCategory/f78e1297e2651a4be7f2189ed0e502d0.vehicle',
      'namespace' => 'amocrm',
    ),
    34 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOScriptVehicle',
      'class' => 'xPDOScriptVehicle',
      'guid' => '71b44063382f0c6f3cde82a8bbdfeb2f',
      'native_key' => '71b44063382f0c6f3cde82a8bbdfeb2f',
      'filename' => 'xPDOScriptVehicle/61bb1b47effc294d7ac88508fac640a1.vehicle',
      'namespace' => 'amocrm',
    ),
  ),
);