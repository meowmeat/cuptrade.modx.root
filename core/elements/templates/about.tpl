{extends 'file:templates/index.tpl'}
{block 'class'}about-page{/block}
{block 'main'}
  {set $about = json_decode($_modx->resource.about, true)[0]}
  {set $bread = $_modx->getChunk('@FILE chunks/all/breadcrumbs.tpl')}
  {if $about.slide}
    {set $slide = json_decode($about.slide, true)[0]}
    <div class="about-promo-section section" {if $slide.img}style="background:url('{$slide.img}') 50% 50% no-repeat;background-size: cover;"{/if}>
      <img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_lines_square_white.svg" alt="" class="about-promo__shape">
      <img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_lines_square_white.svg" alt="" class="about-promo__shape2">
      {$bread | replace:"class=\"breadcrumbs\"":"class=\"breadcrumbs breadcrumbs--white section\"" | replace:"grey":"white"}
      <div class="section-wrapper">
        <div class="about-promo">
          <h1 class="title promo-title">{$slide.title}</h1>
          <div class="promo-subtitle">{$slide.desc | strip_tags:"<br><b><i><em><s><u><strong>"}</div>
        </div>
      </div>
    </div>
  {else}
    {$bread}
  {/if}
  {if $blocks = json_decode($about.block1, true)}
    <div class="about-components-section section">
      {if $about.slide}<img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_linnes_red_about.svg" alt="" class="about-components__shape">{/if}
      <div class="section-wrapper">
        {foreach $blocks as $block}
          <div class="section-info">
            <div class="title section-info__title">{$block.title}</div>
            {$block.desc}
          </div>
          {if $items = json_decode($block.items, true)}
            <div class="about-components">
              {foreach $items as $item}
                <div class="about-component">
                  <div class="about-component__number">{$item.title}</div>
                  <div class="about-component__title">{$item.desc}</div>
                </div>
              {/foreach}
            </div>
          {/if}
        {/foreach}
      </div>
    </div>
  {/if}
  
  {include 'file:chunks/all/advantages.tpl'}
  
  {if $blocks = json_decode($about.block2, true)}
    <div class="about-today-section section">
      <img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_lines_phase.svg" alt="" class="about-today__shape">
      <div class="section-wrapper">
        {foreach $blocks as $block}
          <div class="section-info">
            <div class="title section-info__title">{$block.title}</div>
            {$block.desc}
          </div>
          {if $items = json_decode($block.items, true)}
            <div class="about-components">
              {foreach $items as $item}
                <div class="about-component">
                  <div class="about-component__number">{$item.title}</div>
                  <div class="about-component__title">{$item.desc}</div>
                </div>
              {/foreach}
            </div>
          {/if}
        {/foreach}
      </div>
    </div>
  {/if}

  {'ms2Gallery' | snippet : [
      'frontend_css' => 0,
      'frontend_js' => 0,
      'limit' => 0,
      'tpl' => '@INLINE {if $files?}
            <div class="about-carousel-section section" style="position: relative; z-index: 2;">
    <div class="section-wrapper">
      <div class="about-carousel">
        <div class="swiper-container">
          <div class="swiper-wrapper">
              {foreach $files as $file}
                <div class="about-carousel__slide swiper-slide">
                  <img src="{$file.about_origin}" alt="{$file.alt|htmlent}" class="about-carousel__img">
                </div>
              {/foreach}
          </div>
          <div class="about-carousel__controls">
            <div class="about-carousel__nav about-carousel__prev">
              <svg width="8" height="12" viewBox="0 0 8 12" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg about-carousel__prev-ico">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M6.079 11.693l1.442-1.386L3.386 6l4.135-4.307L6.08.307.614 6l5.465 5.693z" />
              </svg>
            </div>
            <div class="about-carousel__nav about-carousel__next">
              <svg width="8" height="12" viewBox="0 0 8 12" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg about-carousel__next-ico">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M1.721 11.693L.28 10.307 4.414 6 .279 1.693 1.72.307 7.186 6l-5.465 5.693z" />
              </svg>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
      {/if}'
  ]}
  
  {set $maincallback = $_modx->getChunk('@FILE chunks/all/maincallback.tpl')}
  {$maincallback | lazy:true}

  {set $contacts = $_modx->getChunk('@FILE chunks/all/contacts.tpl')}
  {$contacts | lazy:true}
{/block}