{'pdoCrumbs'|snippet:[
  'fastMode' => 1,
  'tplWrapper' => '@INLINE <div class="breadcrumbs"><div class="section-wrapper">{$output}</div></div>',
  'wrapIfEmpty' => 0,
  'showHome' => 1,
  'showCurrent' => 1,
  'tplHome' => '@INLINE <a href="/" class="breadcrumbs__item">Главная<img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_small-grey.svg" alt="" class="breadcrumbs__pointer"></a>',
  'tpl' => '@INLINE <a href="{$link}" class="breadcrumbs__item">{$menutitle}<img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_small-grey.svg" alt="" class="breadcrumbs__pointer"></a>',
  'tplCurrent' => '@INLINE <span class="breadcrumbs__item breadcrumbs__item--current">{$menutitle}</span>'
  ]}