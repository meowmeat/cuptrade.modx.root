//== Validation Form on Send
var req_rules = {
  phone: /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/,
  email: /^[\w0-9\._-]+@\w+\.\w{2,4}$/,
};
$(document).on('submit', '.js-validateForm', function (e) {
  // e.preventDefault();
  var form = $(this);
  var form_input_hide = form.find('.form-input--hide');

  form.find('*[data-req]').each(function () {
    var name = $(this).attr('name'),
      field = $(this).closest('.form-field'),
      value = $(this).val();

    if (req_rules[name] === undefined) {
      req_rules[name] = /^.+$/; // any symbol
    }
    if (req_rules[name].test(value) & !form_input_hide.val()) {
      field.removeClass('form-field--invalid');
    } else {
      field.addClass('form-field--invalid');
    }
  });

  if (form.find('.form-field--invalid').length) {
    return false;
  }
});
// Validation Form on Input
$('.js-validateForm').on(
  'keydown, change',
  '.form-field--invalid *[data-req]',
  function () {
    var name = $(this).attr('name'),
      value = $(this).val(),
      field = $(this).closest('.form-field');
    if (req_rules[name].test(value)) {
      field.removeClass('form-field--invalid');
    }
  }
);

//== Validation Form with PolicyCheck on Send
$(document).on('submit', '.js-validateForm_Policy', function (e) {
  // e.preventDefault();
  var form = $(this);
  var form_input_hide = form.find('.form-input--hide');
  // var form_policy_check = form.find(".form-input--hide").attr("checked");

  var req_or_fields = {}

  form.find('[data-req-or]').each(function () {
    var name = $(this).attr('name')
    var name_relation_name = $(this).attr('data-req-or')
    var name_relation = form.find('[name="'+ name_relation_name +'"')

    req_or_fields[name] = !!name_relation.val() // если true - можно не подсвечивать красным
  })

  form.find('*[data-req]').each(function () {
    var name = $(this).attr('name'),
      field = $(this).closest('.form-field'),
      value = $(this).val();

    field.removeClass('form-field--invalid');
    if (req_rules[name] === undefined) {
      req_rules[name] = /^.+$/; // any symbol
    }

    if (req_rules[name].test(value) & !form_input_hide.val()) {
      field.removeClass('form-field--invalid');
    } else {
      if (!req_or_fields[name]) {
        field.addClass('form-field--invalid');
      }
    }
  });

  afValidated = true;
  if (form.find('.form-field--invalid').length) {
    afValidated = false;
    return false;
  }
  if (!form.find('.form-policy__checkbox').prop('checked')) {
    window.alert('Дайте свое согласие на обработку данных!');
    afValidated = false;
    return false;
  }
  // or with Else: } else return true;
});
// Validation Form on Input
$('.js-validateForm_Policy').on(
  'keydown, change',
  '.form-field--invalid *[data-req]',
  function () {
    var name = $(this).attr('name'),
      value = $(this).val(),
      field = $(this).closest('.form-field');
    if (req_rules[name].test(value)) {
      field.removeClass('form-field--invalid');
    }
  }
);
