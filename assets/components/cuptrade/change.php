<?php
//шлём заголовки
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json; charset=utf-8');
if (!isset($modx)) {
    define('MODX_API_MODE', true);
    while (!isset($modx) && ($i = isset($i) ? --$i : 10)) {
        if (($file = dirname(!empty($file) ? dirname($file) : __FILE__) . '/index.php') AND !file_exists($file)) {
            continue;
        }
        require_once $file;
    }
    if (!is_object($modx)) {
        exit('{"success":false,"message":"Access denied"}');
    }
    $modx->getService('error', 'error.modError');
    $modx->getRequest();
    $modx->setLogLevel(modX::LOG_LEVEL_ERROR);
    $modx->setLogTarget('FILE');
    $modx->error->message = null;
}
$ctx = !empty($_REQUEST['ctx']) ? $_REQUEST['ctx'] : $modx->context->get('key');
if ($ctx != $modx->context->get('key')) {
    $modx->switchContext($ctx);
}

$user_id = $modx->user->id;
$product_key = (string)$_POST['product_key'];
$value = $_POST['value'];
$action = (string)$_POST['action'];
$data = ['html' => ''];
$is_success = false;

switch($action){
  case 'addopts':
  case 'removeopts':
    //print_r($product_key);
    //print_r($value);
    if($product_key && isset($_SESSION['minishop2']['cart'][$product_key])){
      $options = &$_SESSION['minishop2']['cart'][$product_key]['options']?:[];
      //print_r($_SESSION['minishop2']['cart'][$product_key]);
      if($action == 'addopts'){
        if($options['opts']){
          if (!in_array($value, $options['opts'])) {
            $options['opts'][] = $value;
          }
        }
        else{
          $options['opts'][] = $value;
        }
      }
      else{
        if($options['opts']){
          $key = array_search($value, $options['opts']);
          if ($key !== false) {
            unset($options['opts'][$key]);
          }
          if(count($options['opts']) <= 0){
            unset($options['opts']);
          }
        }
      }
      ksort($options);
      $is_success = true;
    }
  break;
  case 'getcost':
    $modx->pdoTools = $modx->pdoTools?:$modx->getService('pdoTools');
    $modx->pdoTools->runSnippet('@FILE snippets/revaluation.php');
    $cart_wrap = $modx->pdoTools->runSnippet('msCart', [
          'tpl' => '@FILE chunks/all/tpl.msCart.cart.tpl'
        ]);
    $summ_cart = 0;
    $summ_cost = 0;
    $total_discount = 0;
    foreach($_SESSION['minishop2']['cart'] as $pos){
      $summ_cart = $summ_cart + (($pos['old_price']?:$pos['price'])*$pos['count']);
      $summ_cost = $summ_cost + ($pos['price']*$pos['count']);
      $total_discount = $total_discount + $pos['discount_cost'];
    }
    $data = ['html' => $cart_wrap, 'data' => ['total_cart' => $summ_cart, 'total_cost' => $summ_cost, 'total_discount' => $total_discount]];
    $is_success = true;
  break;
}
$out = array('success' => $is_success, 'message' => $message?:'Не могу выполнить запрос', 'data' => $data);
die(json_encode($out));