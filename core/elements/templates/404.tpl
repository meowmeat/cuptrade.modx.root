{extends 'file:templates/index.tpl'}
{block 'class'}notfound-page{/block}
{block 'main'}
  <div class="notfound section">
    <div class="notfound-wrapper">
      <div class="notfound__title">К сожалению, страница не найдена.</div>
      <a href="/" class="button button-primary notfound-button">
        <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_left-white.svg" class="notfound-button__ico">
        Вернуться на главную
      </a>
    </div>
  </div>
{/block}