<?php
include_once 'setting.inc.php';

$_lang['amocrm'] = 'amoCRM';
$_lang['amocrm_form_status_new'] = 'Новая';
$_lang['amocrm_form_name_new'] = 'Новая заявка [[+date]]';

// Переместить в ошибки

$_lang['amocrm_error_auth'] = 'Неправильный логин или пароль';
$_lang['amocrm_error_auth_desc'] = 'Общая ошибка авторизации.';
$_lang['amocrm_error_lead_nf'] = 'Сделка [[+id]] не найдена.';

$_lang['amocrm_order_name'] = 'Заказ с сайта [[+num]]';
$_lang['amocrm_order_product_row'] = '[[+idx]]. ([[+article]]) [[+name]] [[+price]] [[+currency]] * [[+count]] [[+unit]] = [[+cost]] [[+currency]]
    [[+options]]';


$_lang['amocrm_menu_desc'] = 'Управление связями AMO-MODX';
$_lang['amocrm_intro_msg'] = 'Вы можете выделять сразу несколько строк при помощи Shift или Ctrl.';
//
//$_lang['amocrm_items'] = 'Предметы';
//$_lang['amocrm_item_id'] = 'Id';
//$_lang['amocrm_item_name'] = 'Название';
//$_lang['amocrm_item_description'] = 'Описание';
//$_lang['amocrm_item_active'] = 'Активно';
//
//$_lang['amocrm_item_create'] = 'Создать предмет';
//$_lang['amocrm_item_update'] = 'Изменить Предмет';
//$_lang['amocrm_item_enable'] = 'Включить Предмет';
//$_lang['amocrm_items_enable'] = 'Включить Предметы';
//$_lang['amocrm_item_disable'] = 'Отключить Предмет';
//$_lang['amocrm_items_disable'] = 'Отключить Предметы';
//$_lang['amocrm_item_remove'] = 'Удалить Предмет';
//$_lang['amocrm_items_remove'] = 'Удалить Предметы';
//$_lang['amocrm_item_remove_confirm'] = 'Вы уверены, что хотите удалить этот Предмет?';
//$_lang['amocrm_items_remove_confirm'] = 'Вы уверены, что хотите удалить эти Предметы?';
//$_lang['amocrm_item_active'] = 'Включено';
//
//$_lang['amocrm_item_err_name'] = 'Вы должны указать имя Предмета.';
//$_lang['amocrm_item_err_ae'] = 'Предмет с таким именем уже существует.';
//$_lang['amocrm_item_err_nf'] = 'Предмет не найден.';
//$_lang['amocrm_item_err_ns'] = 'Предмет не указан.';
//$_lang['amocrm_item_err_remove'] = 'Ошибка при удалении Предмета.';
//$_lang['amocrm_item_err_save'] = 'Ошибка при сохранении Предмета.';

$_lang['amocrm_grid_search'] = 'Поиск';
$_lang['amocrm_grid_actions'] = 'Действия';