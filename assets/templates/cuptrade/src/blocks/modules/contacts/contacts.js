//== Contacts form switchers toggle
$(document).on('click touch', '.js-contactsFormSwitcher_Toggle', function () {
  let contactsFormSwitcher = $(this)
    .closest('.contacts-form__switchers')
    .find('.contacts-form__switcher');
  let contactsFormSwitcher_Active = $(this);
  if (contactsFormSwitcher.hasClass('contacts-form__switcher--active')) {
    contactsFormSwitcher
      .not(contactsFormSwitcher_Active)
      .removeClass('contacts-form__switcher--active');
    contactsFormSwitcher_Active.addClass('contacts-form__switcher--active');
  } else {
    contactsFormSwitcher.removeClass('contacts-form__switcher--active');
  }

  let contactsFormField_mail = contactsFormSwitcher_Active
    .closest('.contacts-form')
    .find('.contacts-form__field-mail');
  let contactsFormField_phone = contactsFormSwitcher_Active
    .closest('.contacts-form')
    .find('.contacts-form__field-phone');
  if (
    contactsFormSwitcher_Active.data('contacts-fieldhide') ===
    'contasts-form-mail'
  ) {
    contactsFormField_mail.addClass('contacts-form__field--hidden');
    contactsFormField_mail.find('.contacts-form__input').removeAttr('data-req');
    contactsFormField_phone.removeClass('contacts-form__field--hidden');
    contactsFormField_phone.find('.contacts-form__input').attr('data-req', '');
  }
  if (
    contactsFormSwitcher_Active.data('contacts-fieldhide') ===
    'contasts-form-phone'
  ) {
    contactsFormField_phone.addClass('contacts-form__field--hidden');
    contactsFormField_phone
      .find('.contacts-form__input')
      .removeAttr('data-req');
    contactsFormField_mail.removeClass('contacts-form__field--hidden');
    contactsFormField_mail.find('.contacts-form__input').attr('data-req', '');
  }

  if (contactsFormSwitcher_Active.data('switcher-note') === 'telephone') {
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('.contacts-form__switchers-note')
      .text('Свяжемся по телефону.');
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('input[name="type"]')
      .val('Свяжемся по телефону.');
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('input[name="name_form"]')
      .val('Связаться по телефону');
  }
  if (contactsFormSwitcher_Active.data('switcher-note') === 'email') {
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('.contacts-form__switchers-note')
      .text('Отправим электронное письмо.');
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('input[name="type"]')
      .val('Отправим электронное письмо.');
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('input[name="name_form"]')
      .val('Связаться по почте');
  }
  if (contactsFormSwitcher_Active.data('switcher-note') === 'whatsapp') {
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('.contacts-form__switchers-note')
      .text('Напишем в WhatsApp, никаких звонков!');
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('input[name="type"]')
      .val('Напишем в WhatsApp, никаких звонков!');
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('input[name="name_form"]')
      .val('Связаться по WhatsApp');
  }
});
