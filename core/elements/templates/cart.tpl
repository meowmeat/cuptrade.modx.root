{extends 'file:templates/index.tpl'}
{block 'class'}cart-page{/block}
{block 'main'}
  <div class="cart-section section">
    <div class="section-wrapper">
      <div class="section-heading">
        <a href="{2 | url}" class="shopward-link"><span class="shopward-link__ico"><svg width="6" height="11" viewBox="0 0 8 12" fill="#989798" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M6.079 11.693l1.442-1.386L3.386 6l4.135-4.307L6.08.307.614 6l5.465 5.693z" />
            </svg></span>Продолжить покупки</a>
        <h1 class="section-heading__title">Корзина</h1>
      </div>
      {'!msCart' | snippet : [
        'tpl' => '@FILE chunks/all/tpl.msCart.cart.tpl'
      ]}
    </div>
  </div>
{/block}