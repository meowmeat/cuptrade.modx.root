var about = $('.main-about');
var about_el_count = about.find('.main-about__item').length;
var about_current_step = 0;
var about_interval;

function randomAboutItem() {
  var num = Math.floor((Math.random() * 10) % about_el_count);

  while (num == about_current_step) {
    num = Math.floor((Math.random() * 10) % about_el_count);
  }

  return num;
}

function showAboutItem() {
  var el_prev = about.find('.main-about__item').eq(about_current_step);
  if (el_prev) el_prev.find('.main-about__item-fade').css('display', '');
  about_current_step = randomAboutItem();
  var el = about.find('.main-about__item').eq(about_current_step);
  el.find('.main-about__item-fade').show(400);
}

function mainAboutInterval() {
  if (!about_el_count || window.screen.width <= 1030) return;
  clearInterval(about_interval);
  showAboutItem();
  about_interval = setInterval(function () {
    showAboutItem();
  }, 4000);
}

about.on('mouseenter', function () {
  clearInterval(about_interval);
  var el_prev = about.find('.main-about__item').eq(about_current_step);
  if (el_prev) el_prev.find('.main-about__item-fade').css('display', '');
});
about.on('mouseleave', function () {
  mainAboutInterval();
});
mainAboutInterval();
