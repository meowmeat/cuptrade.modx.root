//== Slider for About Page
// function about_Carousel() {
// const $this = $('.about-carousel');
// let navContainer = $this.find('.content-carousel__sum');
// let contentCarousel_Image = $this.find('.content-carousel__img');
// console.log(contentCarousel_Image.innerHeight());
// navContainer.css(
//   'top',
//   contentCarousel_Image.innerHeight() - navContainer.innerHeight()
// );
let aboutCarousel = new Swiper('.about-carousel .swiper-container', {
  loop: true,
  slidesPerView: 2,
  centeredSlides: true,
  // spaceBetween: 30,
  navigation: {
    nextEl: '.about-carousel__next',
    prevEl: '.about-carousel__prev',
  },
  breakpoints: {
    // when window width is >= 860px
    220: {
      slidesPerView: 1,
    },
    861: {
      slidesPerView: 2,
    },
  },
});
// }
