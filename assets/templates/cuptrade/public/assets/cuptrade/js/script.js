//== this function called in a file: script-closing
function currentScreenHeight() {
  let screenHeight = $(window).innerHeight();
  return screenHeight;
}

function changeLogo(th) {
  filepath = th.value.split('\\').pop().split('/').pop();
  const lastDot = filepath.lastIndexOf('.');

  const fileName = filepath.substring(0, lastDot);
  const ext = filepath.substring(lastDot + 1);
  
  $('#file_upl_value')[0].value = fileName;

  $(th).prev().hide() // hide button
  $(th).next().show()
}
function clearLogo(th) {
  $('[for="file_upl"]').show();
  $('#uploaded-logo').hide();
  $('#file_upl_value')[0].value = null;
}


//== Phone Links: remove phone attribute on desktop
// if (screen.width > 1030) $('a[href^="tel:"]').removeAttr('href');

//== Masked Phone input
$('input[type="tel"], .js-phone').inputmask({
  mask: '+7 999 999 99 99',
  clearMaskOnLostFocus: true,
});

//== Validation Form on Send
var req_rules = {
  phone: /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/,
  email: /^[\w0-9\._-]+@\w+\.\w{2,4}$/,
};
$(document).on('submit', '.js-validateForm', function (e) {
  // e.preventDefault();
  var form = $(this);
  var form_input_hide = form.find('.form-input--hide');

  form.find('*[data-req]').each(function () {
    var name = $(this).attr('name'),
      field = $(this).closest('.form-field'),
      value = $(this).val();

    if (req_rules[name] === undefined) {
      req_rules[name] = /^.+$/; // any symbol
    }
    if (req_rules[name].test(value) & !form_input_hide.val()) {
      field.removeClass('form-field--invalid');
    } else {
      field.addClass('form-field--invalid');
    }
  });

  if (form.find('.form-field--invalid').length) {
    return false;
  }
});
// Validation Form on Input
$('.js-validateForm').on(
  'keydown, change',
  '.form-field--invalid *[data-req]',
  function () {
    var name = $(this).attr('name'),
      value = $(this).val(),
      field = $(this).closest('.form-field');
    if (req_rules[name].test(value)) {
      field.removeClass('form-field--invalid');
    }
  }
);

//== Validation Form with PolicyCheck on Send
$(document).on('submit', '.js-validateForm_Policy', function (e) {
  // e.preventDefault();
  var form = $(this);
  var form_input_hide = form.find('.form-input--hide');
  var button = form.find('[type="submit"]');
  
  let button_text = button.val() ? button.val() : button.text()
  
  button.attr('disabled', true)
  button.text('Отправка..')
  button.val('Отправка..')
  // var form_policy_check = form.find(".form-input--hide").attr("checked");

  var req_or_fields = {}

  form.find('[data-req-or]').each(function () {
    var name = $(this).attr('name')
    var name_relation_name = $(this).attr('data-req-or')
    var name_relation = form.find('[name="'+ name_relation_name +'"')

    req_or_fields[name] = !!name_relation.val() // если true - можно не подсвечивать красным
  })

  form.find('*[data-req]').each(function () {
    var name = $(this).attr('name'),
      field = $(this).closest('.form-field'),
      value = $(this).val();

    field.removeClass('form-field--invalid');
    if (req_rules[name] === undefined) {
      req_rules[name] = /^.+$/; // any symbol
    }

    if (req_rules[name].test(value) & !form_input_hide.val()) {
      field.removeClass('form-field--invalid');
    } else {
      if (!req_or_fields[name]) {
        field.addClass('form-field--invalid');
      }
    }
  });

  afValidated = true;
  if (form.find('.form-field--invalid').length) {
    afValidated = false;
  }
  if (!form.find('.form-policy__checkbox').prop('checked')) {
    window.alert('Дайте свое согласие на обработку данных!');
    afValidated = false;
  }
  
  if (!afValidated) {
    location.hash = "#";
    location.hash = "#form";
    button.attr('disabled', false)
    button.text(button_text)
    button.val(button_text)
    return false;
  }
  
  
  // or with Else: } else return true;
});
// Validation Form on Input
$('.js-validateForm_Policy').on(
  'keydown, change',
  '.form-field--invalid *[data-req]',
  function () {
    var name = $(this).attr('name'),
      value = $(this).val(),
      field = $(this).closest('.form-field');
    if (req_rules[name].test(value)) {
      field.removeClass('form-field--invalid');
    }
  }
);


//== Check number format for input - calling in input
function checkingNumberFormat(e) {
  e = e || window.event;
  var charCode = e.which ? e.which : e.keyCode;
  return /\d/.test(String.fromCharCode(charCode));
}

$.fancybox.defaults.loop = true;

//== Modal Call
$(document).on('click touch', '.js-modalCall', function () {
  $('body').addClass('no-scroll');
  let modalCalled = this.getAttribute('data-modal-called');
  let modalActive = $('#' + modalCalled);
  let modalActive_Content = modalActive.find('.modal-content');
  let modalActive_Overlay = modalActive.find('.modal-overlay');
  $('#sitenavMob').hide();
  $('#sitenavMob').removeClass('sitenav-overlay--active');
  $('#sidenavOverlay').removeClass('sitenav-overlay--active');

  modalActive.show();

  // console.log( currentScreenHeight() );
  // console.log( 'Height of modalActive '+modalActive.height() );
  modalActive_Overlay.css('height', currentScreenHeight());
  // console.log( modalActive_Content.height() );
  if (modalActive_Content.height() >= currentScreenHeight()) {
    modalActive.css('height', currentScreenHeight());
    modalActive_Overlay.css('height', 'auto');
  }
});

$(document).on('click touch', '.js-modalCall_Mob', function () {
  $('#sitenavMob').removeClass('slideInRight').addClass('slideOutRight');
  setTimeout(function () {
    $('#sitenavMob').css('height', '');
  }, 500);
  $('#sitenavOverlay').removeClass('sitenav-overlay--active').css('height', '');
  $('body').addClass('no-scroll');
  let modalCalled = this.getAttribute('data-modal-called');
  let modalActive = $('#' + modalCalled);
  let modalActive_Content = modalActive.find('.modal-content');
  let modalActive_Overlay = modalActive.find('.modal-overlay');
  modalActive.show();
  // console.log( currentScreenHeight() );
  // console.log( 'Height of modalActive '+modalActive.height() );
  modalActive_Overlay.css('height', currentScreenHeight());
  // console.log( modalActive_Content.height() );
  if (modalActive_Content.height() >= currentScreenHeight()) {
    modalActive.css('height', currentScreenHeight());
    modalActive_Overlay.css('height', 'auto');
  }
});

//== this function called in a file: script-closing
function modalOverlayHeight_OnResize() {
  let modalInstance = $('body').find('.modal');
  let modalInstance_Overlay = modalInstance.find('.modal-overlay');
  let modalInstance_Content = modalInstance.find('.modal-content');
  if (
    modalInstance.is(':visible') &&
    modalInstance_Content.outerHeight() < currentScreenHeight()
  ) {
    modalInstance.css('height', 'auto');
    modalInstance_Overlay.css('height', currentScreenHeight());
  }
  if (
    modalInstance.is(':visible') &&
    modalInstance_Content.outerHeight() >= currentScreenHeight()
  ) {
    modalInstance.css('height', currentScreenHeight());
    modalInstance_Overlay.css('height', 'auto');
  }
}

//== Call Nested Modals forms
// $('.js-modalFormNested_Call').click(function () {
//   $('#sitenavMob').hide();
//   $('#sitenavOverlay').removeClass('sitenav-overlay--active');
//   $('.modal').hide();
//   $('body').addClass('no-scroll');
//   let modalFormNested = this.getAttribute('data-modal-called');
//   $('#' + modalFormNested).show();
// });

//== Modal Close
$(document).on('click touch', '.js-modalClose', function () {
  $(this).closest('.modal').hide().css('height', '');
  $(this).closest('.modal-overlay').css({ height: '' });
  $('body').removeClass('no-scroll');
});
$(document).on('click touch', '.modal-overlay', function (e) {
  let modalContent = $('.modal-content');
  if (!modalContent.is(e.target) && modalContent.has(e.target).length === 0) {
    $(this).closest('.modal').hide().css('height', '');
    $(this).css({ height: '' });
    $('body').removeClass('no-scroll');
  }
});

//== Hide Offer Top Section
$('.js-offerTop_Close').click(function () {
  $(this).closest('.offer-top-section').hide();
});

//== Header nav toggle dropdown menu
$(document).on('click touch', '.js-dropdownMenu_Toggle', function () {
  $(this)
    .find('.header-nav__item-control')
    .toggleClass('header-nav__item-control--rotate');
  $(this).toggleClass('header-nav-item__dropdown--active');
  $(this).find('.header-dropdown').slideToggle();
  if (
    $('.js-dropdownMenu_Toggle').hasClass('header-nav-item__dropdown--active')
  ) {
    $('.js-dropdownMenu_Toggle')
      .not($(this))
      .removeClass('header-nav-item__dropdown--active')
      .find('.header-dropdown')
      .slideUp();
  }
});

//== Change images in header dropdown menu
$('.header-dropdown-catalog .header-dropdown__item').on({
  mouseenter: function () {
    let dropdownItem_Image = this.getAttribute('data-headernav-img');
    let dropdownItem_Image_Active = $('#' + dropdownItem_Image);
    $('.header-dropdown__image-main').removeClass(
      'header-dropdown__image--active'
    );
    $(this).addClass('header-dropdown__item--active');
    dropdownItem_Image_Active.toggleClass('header-dropdown__image--active');
  },
  mouseleave: function () {
    let dropdownItem_Image = this.getAttribute('data-headernav-img');
    let dropdownItem_Image_Active = $('#' + dropdownItem_Image);
    $(this).removeClass('header-dropdown__item--active');
    dropdownItem_Image_Active.removeClass('header-dropdown__image--active');
    setTimeout(function () {
      if (
        !$('.header-dropdown__item').hasClass('header-dropdown__item--active')
      ) {
        $('.header-dropdown__image-main').addClass(
          'header-dropdown__image--active'
        );
      }
    }, 200);
  },
});

//== Header Search Call
$(document).on('click touch', '.js-searchDesktop_Call', function (e) {
  e.stopPropagation();
  $(this)
    .siblings('#headerSearchForm')
    .addClass('header-search--active')
    .show(('slide', { direction: 'left' }, 400));
});

$(document).on('click touch', function (e) {
  if (
    $('.header-search').hasClass('header-search--active') &&
    !$('#headerSearchForm').is(e.target) &&
    $('#headerSearchForm').has(e.target).length === 0
  ) {
    $('#headerSearchForm')
      .removeClass('header-search--active')
      .hide(('slide', { direction: 'right' }, 400));
  }
});

//== Sitenav Mobile: slide toggle On Tablet
$(document).on('click touch', '.js-sitenavMob_Call', function () {
  $('body').addClass('no-scroll');
  $('#sitenavMob').removeClass('slideOutRight').addClass('slideInRight');
  $('#sitenavMob').show();
  if ($(window).width() > 560) {
    $('#sitenavOverlay').addClass('sitenav-overlay--active');
    $('.sitenav-overlay--active').css('height', $(document).height());
    if ($('#sitenavOverlay').hasClass('sitenav-overlay--active') != true) {
      $('#sitenavOverlay').css('height', '');
    }
  }
});
$(document).on('click touch', '#sitenavOverlay', function () {
  $('body').removeClass('no-scroll');
  $('#sitenavMob').removeClass('slideInRight').addClass('slideOutRight');
  $('#sitenavCatalog_Mob')
    .removeClass('slideInRight')
    .addClass('slideOutRight');
  $(this).removeClass('sitenav-overlay--active');
  $(this).css('height', '');
});

//== Sitenav Mobile Close
$(document).on('click touch', '.js-sitenavClose', function () {
  $('body').removeClass('no-scroll');
  $('#sitenavMob').removeClass('slideInRight').addClass('slideOutRight');
  $('#sitenavOverlay').removeClass('sitenav-overlay--active').css('height', '');
});

//== Sitenav Set of inner links togle
$(document).on('click touch', '.js-sitenavSet_Toggle', function (e) {
  e.preventDefault();
  // e.stopPropagation();
  $(this)
    .closest('.sitenav-menu__module-heading')
    .toggleClass('sitenav-menu__module-heading--expanded');
  $(this).closest('.sitenav-menu__module').find('.sitenav-set').slideToggle();
});

//== Sitenav Mobile: toggle menu set
// function sitenav_toggleSet() {
//   const sitenav = $('#sitenavMob');
//   if (!sitenav.length) return;
//   sitenav.on('click', '.js-sitenavChildSet_Call', function () {
//     let sitenav_module = $(this);
//     let sitenav_set = sitenav_module.siblings('.sitenav-set');
//     sitenav_set.slideToggle();
//     $('.js-sitenavChildSet_Call')
//       .not(sitenav_module)
//       .siblings('.sitenav-set')
//       .slideUp();
//   });
// }
// sitenav_toggleSet();

//== Change images in main nav menu
$('.main-nav-wide__link').on({
  mouseenter: function () {
    let mainNavLink_Image = this.getAttribute('data-mainnav-img');
    let mainNavLink_Image_Active = $('#' + mainNavLink_Image);
    $('.main-nav-wide__image-main').removeClass('main-nav-wide__image--active');
    $(this).addClass('main-nav-wide__link--active');
    mainNavLink_Image_Active.toggleClass('main-nav-wide__image--active');
    // mainNavLink_Image_Active
    //   .addClass('slideInRight');
  },
  mouseleave: function () {
    let mainNavLink_Image = this.getAttribute('data-mainnav-img');
    let mainNavLink_Image_Active = $('#' + mainNavLink_Image);
    $(this).removeClass('main-nav-wide__link--active');
    // mainNavLink_Image_Active
    //   .removeClass('slideInRight');
    mainNavLink_Image_Active.removeClass('main-nav-wide__image--active');
    setTimeout(function () {
      if (!$('.main-nav-wide__link').hasClass('main-nav-wide__link--active')) {
        $('.main-nav-wide__image-main').addClass(
          'main-nav-wide__image--active'
        );
      }
    }, 200);
  },
});

//== Carousel for Product Novelty
var mainNoveltyCarousel = new Swiper('.main-novelty > .swiper-container', {
  loop: false,
  slidesPerView: 4,
  spaceBetween: 30,
  navigation: {
    nextEl: '.main-novelty__next',
    prevEl: '.main-novelty__prev',
  },
  breakpoints: {
    // when window width is >= value px etc
    1025: {
      grid: {
        rows: 2,
        fill: 'row',
      },
      enabled: false
    },
    761: {
      slidesPerView: 3,
      spaceBetween: 20,
    },
    421: {
      slidesPerView: 2,
      spaceBetween: 14,
    },
    319: {
      slidesPerView: 1,
      spaceBetween: 10,
    },
  },
});

//== Main About section - remove accented-class items
//== this function called in a file: script-closing
function mainAboutAccent_remove() {
  let mainAboutItem = $('.main-about').find('.main-about__item');
  if ($(window).width() <= 560) {
    if (mainAboutItem.hasClass('main-about__item--accent')) {
      mainAboutItem.removeClass('main-about__item--accent');
    } else {
      mainAboutItem.addClass('main-about__item--accent');
    }
  }
}

//== Main page Callback Form switchers toggle
$(document).on('click touch', '.js-callbackFormSwitcher_Toggle', function () {
  let callbackFormSwitcher = $(this)
    .closest('.main-callback-form__switchers')
    .find('.main-callback-form__switcher');
  let callbackFormSwitcher_Active = $(this);
  if (callbackFormSwitcher.hasClass('main-callback-form__switcher--active')) {
    callbackFormSwitcher
      .not(callbackFormSwitcher_Active)
      .removeClass('main-callback-form__switcher--active');
    callbackFormSwitcher_Active.addClass(
      'main-callback-form__switcher--active'
    );
  } else {
    callbackFormSwitcher.removeClass('main-callback-form__switcher--active');
  }

  let callbackFormField_mail = callbackFormSwitcher_Active
    .closest('.main-callback-form')
    .find('.main-callback-form__field-mail');
  let callbackFormField_phone = callbackFormSwitcher_Active
    .closest('.main-callback-form')
    .find('.main-callback-form__field-phone');
  if (
    callbackFormSwitcher_Active.data('contacts-fieldhide') ===
    'contasts-form-mail'
  ) {
    callbackFormField_mail.addClass('main-callback-form__field--hidden');
    callbackFormField_mail
      .find('.main-callback-form__input')
      .removeAttr('data-req');
    callbackFormField_phone.removeClass('main-callback-form__field--hidden');
    callbackFormField_phone
      .find('.main-callback-form__input')
      .attr('data-req', '');
  }
  if (
    callbackFormSwitcher_Active.data('contacts-fieldhide') ===
    'contasts-form-phone'
  ) {
    callbackFormField_phone.addClass('main-callback-form__field--hidden');
    callbackFormField_phone
      .find('.main-callback-form__input')
      .removeAttr('data-req');
    callbackFormField_mail.removeClass('main-callback-form__field--hidden');
    callbackFormField_mail
      .find('.main-callback-form__input')
      .attr('data-req', '');
  }

  if (callbackFormSwitcher_Active.data('switcher-note') === 'telephone') {
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('.main-callback-form__switchers-note')
      .text('Свяжемся по телефону.');
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('input[name="type"]')
      .val('Свяжемся по телефону.');
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('input[name="name_form"]')
      .val('Связаться по телефону');
  }
  if (callbackFormSwitcher_Active.data('switcher-note') === 'email') {
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('.main-callback-form__switchers-note')
      .text('Отправим электронное письмо.');
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('input[name="type"]')
      .val('Отправим электронное письмо.');
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('input[name="name_form"]')
      .val('Связаться по почте');
  }
  if (callbackFormSwitcher_Active.data('switcher-note') === 'whatsapp') {
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('.main-callback-form__switchers-note')
      .text('Напишем в WhatsApp, никаких звонков!');
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('input[name="type"]')
      .val('Напишем в WhatsApp, никаких звонков!');
    callbackFormSwitcher_Active
      .closest('.main-callback-form__switchers')
      .siblings('input[name="name_form"]')
      .val('Связаться по WhatsApp');
  }
});

//== hide gallery-elements for tablet
//== this function called in a file: script-closing
function gallery_hideElementsTablet() {
  let galleryElement_toHide = $('.gallery').find(
    '.gallery-preview--hidetablet'
  );
  if ($(window).width() <= 860) {
    galleryElement_toHide
      .find('.gallery-preview__img')
      .addClass('gallery-preview__img-hidden');
  } else {
    galleryElement_toHide
      .find('.gallery-preview__img')
      .removeClass('gallery-preview__img-hidden');
  }
}

//== Gallery Toggle hidden previews
$('.js-galleryToggleHidden').on('click touch', function (e) {
  // e.preventDefault();
  let galleryPreviewImg_Hidden = $(this)
    .closest('.section-wrapper')
    .find('.gallery-section')
    .find('.gallery-preview__img-hidden');
  let galleryPreviewImages = $(this)
    .closest('.section-wrapper')
    .find('.gallery-preview')
    .find('.gallery-preview__img');
  if ($(window).width() > 860) {
    if (galleryPreviewImg_Hidden.length >= 1) {
      // $(this).text() == 'Показать ещё';
      galleryPreviewImg_Hidden
        .slice(0, 4)
        .slideDown()
        .removeClass('gallery-preview__img-hidden');
    } else if (galleryPreviewImg_Hidden.length == 0) {
      // $(this).text('Скрыть');
      galleryPreviewImages.slice(8).slideUp();
      setTimeout(function () {
        galleryPreviewImages.slice(8).addClass('gallery-preview__img-hidden');
      }, 600);
    }
  }

  if ($(window).width() > 560 && $(window).width() <= 860) {
    if (galleryPreviewImg_Hidden.length >= 1) {
      // $(this).text() == 'Показать ещё';
      galleryPreviewImg_Hidden
        .slice(0, 3)
        .slideDown()
        .removeClass('gallery-preview__img-hidden');
    } else if (galleryPreviewImg_Hidden.length == 0) {
      // $(this).text('Скрыть');
      galleryPreviewImages.slice(6).slideUp();
      setTimeout(function () {
        galleryPreviewImages.slice(6).addClass('gallery-preview__img-hidden');
      }, 600);
    }
  }

  if ($(window).width() >= 320 && $(window).width() <= 560) {
    if (galleryPreviewImg_Hidden.length >= 1) {
      // $(this).text() == 'Показать ещё';
      galleryPreviewImg_Hidden
        .slice(0, 2)
        .slideDown()
        .removeClass('gallery-preview__img-hidden');
    } else if (galleryPreviewImg_Hidden.length == 0) {
      // $(this).text('Скрыть');
      galleryPreviewImages.slice(6).slideUp();
      setTimeout(function () {
        galleryPreviewImages.slice(6).addClass('gallery-preview__img-hidden');
      }, 600);
    }
  }
});

//== Slider for services Also
// var carouselservicesAlso = new Swiper('.services-carousel .swiper-container', {
//   loop: true,
//   // slidesPerView : 'auto',
//   spaceBetween: 20,
//   slidesPerView: 4,
//   initialSlide: 0,
//   // Navigation arrows
//   navigation: {
//     nextEl: '.services-carousel__next',
//     prevEl: '.services-carousel__prev',
//   },
//   breakpoints: {
//     // when window width is >= 560px etc
//     290: {
//       slidesPerView: 1,
//     },
//     561: {
//       slidesPerView: 2,
//     },
//     861: {
//       slidesPerView: 3,
//     },
//   },
// });

$('.catalog-sorting__control').click(function (e) {
  e.preventDefault();
  e.stopPropagation();
  $(this).toggleClass('catalog-sorting__control-expanded');
  // $('#' + $(e.target).attr('for')).prop('checked', true);
});
$('.catalog-sorting__menu-label').click(function (e) {
  $('#' + $(e.target).attr('for')).attr('checked', true);
  //console.log($('#' + $(e.target).attr('for')).attr('value'));
  $(this)
    .closest('.catalog-sorting__control')
    .find('.catalog-sorting__control-text')
    .text($('#' + $(e.target).attr('for')).attr('data-title'));
  $(this)
    .closest('.catalog-sorting__control')
    .find('.catalog-sorting__control-value')
    .attr('value', $('#' + $(e.target).attr('for')).attr('value'));
  $(this)
    .closest('.catalog-sorting__control')
    .find('.catalog-sorting__control-value').trigger('change');
});

$(document).click(function () {
  $('.catalog-sorting__control').removeClass(
    'catalog-sorting__control-expanded'
  );
});

//== Carousel for catalog card
//== this function called in a file: script-closing
function catalogCard_Carousel() {
  $('.catalog-card__carousel').each(function (index, element) {
    const $this = $(this);
    $slide = $this.find('.swiper-slide');
    if($slide.length < 2){
      return;
    }
    $this
      .find('.catalog-card__carousel-pagination')
      .addClass('catalog-card__carousel-pagination-' + index);
    $this.find('.swiper-container').addClass('catalog-card__carousel-' + index);

    let swiper = new Swiper('.catalog-card__carousel-' + index, {
      // loop: true,
      autoplay: false,
      pagination: {
        el: '.catalog-card__carousel-pagination-' + index,
        type: 'bullets',
        clickable: true,
      },
    });
  });
}

$(document).on( "mse2_load", function() {
  catalogCard_Carousel()
});

// $(function () {
//   if ($('.catalog-card__carousel').length > 0) {
//     //some-slider-wrap-in
//     catalogCard_Carousel();
//     $('.catalog-card__carousel-pagination>.swiper-pagination-bullet').hover(
//       function () {
//         $(this).trigger('click');
//       }
//     );
//   }
// });

//==== product ====//
//== Carousel mobile for Product Page
function carouselProductRecommend_Mob() {
  if ($(window).width() <= 480) {
    $('.product-recommend').find('.catalog-card').addClass('swiper-slide');
    let carouselProduct_Recommend = new Swiper(
      '.product-recommend .swiper-container',
      {
        loop: true,
        slidesPerView: 1,
        navigation: {
          nextEl: '.product-recommend__next',
          prevEl: '.product-recommend__prev',
        },
      }
    );
  } else {
    $('.product-recommend').find('.catalog-card').removeClass('swiper-slide');
  }
}
function carouselProductSimilar_Mob() {
  if ($(window).width() <= 480) {
    $('.product-similar').find('.catalog-card').addClass('swiper-slide');
    let carouselProduct_Similar = new Swiper(
      '.product-similar .swiper-container',
      {
        loop: true,
        slidesPerView: 1,
        navigation: {
          nextEl: '.product-similar__next',
          prevEl: '.product-similar__prev',
        },
      }
    );
  } else {
    $('.product-similar').find('.catalog-card').removeClass('swiper-slide');
  }
}

var carouselProduct_Image = new Swiper('.product-carousel .swiper-container', {
  spaceBetween: 10,
  centeredSlides: true,
  slidesPerView: 'auto',
  // loop: true,
  loopedSlides: 4,
  resizeReInit: true,
  navigation: {
    nextEl: '.product-carousel__next',
    prevEl: '.product-carousel__prev',
  },
  pagination: {
    el: '.product-carousel__pagination',
    type: 'bullets',
    clickable: true,
  },
});

$().fancybox({
  selector:
    '.product-carousel .swiper-container .swiper-slide:not(.swiper-slide-duplicate), .product-view a[data-fancybox]',
  thumbs: false,
  hash: false,
  toolbar: false,
  smallBtn: true,
  backFocus: false,
  beforeClose: function (instance) {
    if (instance.group.length > 1) {
      carouselProduct_Image.slideTo(instance.currIndex, 0);
    }
  },
});

var carouselProductImage_Thumbs = new Swiper(
  '.product-preview .swiper-container',
  {
    spaceBetween: 20,
    centeredSlides: true,
    slidesPerView: 'auto',
    touchRatio: 0.2,
    slideToClickedSlide: true,
    // loop: true,
    loopedSlides: 4,
    breakpoints: {
      561: {
        spaceBetween: 10,
      },
      861: {
        spaceBetween: 20,
      },
    },
  }
);
//== this function called in a file: script-closing
function productThumbs_BindControls() {
  if ($(window).width() > 560) {
    carouselProduct_Image.controller.control = carouselProductImage_Thumbs;
    carouselProductImage_Thumbs.controller.control = carouselProduct_Image;
  }
}

//== Product Counter Processing
$(document).on('keyup', '.js-productCount_Number', function () {
  $(this).trigger('change');
  $(this).find('+.js-productCount').trigger('click', ['not_change']);
});
//== Product Page: Change Count and Price of product
$(document).on('click touch', '.js-productCount', function (e, prop) {
  let $button = $(this);
  // let costField = $button.parents('.popup').find('input[name="cost"]');
  let productPrice_Single = $button
    .closest('[data-product-counter]')
    .siblings('[data-product-price-wrap]')
    .find('[data-product-price]');
  let productPriceDisplayed = productPrice_Single.find('[data-product-cost]');
  let productCount = $button.siblings('.js-productCount_Number');
  let productCountVal = parseFloat(productCount.val()?productCount.val():1);
  let productCountMin = parseFloat(productCount.attr('min'));
  let productCountMax = parseFloat(productCount.attr('max'));
  let productCountStep = parseFloat(productCount.attr('step'));
  let newVal = parseFloat(productCountVal);
  if(typeof prop != 'undefined' && prop == 'not_change'){
  }
  else{
    if ($button.hasClass('product-count__plus')) {
      newVal = newVal + productCountStep;
      productPriceDisplayed.text(
        (productPrice_Single.attr('data-product-price') * newVal).toLocaleString(
          'ru'
        )
      );
      if (productCountVal >= productCountMax) {
        newVal = newVal - productCountStep;
        productPriceDisplayed.text(
          (
            productPrice_Single.attr('data-product-price') * newVal
          ).toLocaleString('ru')
        );
      }
    } else if (productCountVal > productCountMin) {
      newVal = newVal - productCountStep;
      productPriceDisplayed.text(
        (productPrice_Single.attr('data-product-price') * newVal).toLocaleString(
          'ru'
        )
      );
    }
  }
  productCount.val(newVal);
  $('.js-type-pack:checked').trigger('change');
  productCount.trigger('change');
  // let $form = $(this).parents('form.ms2_form');
  // if ($form.length) {
  //   let formData = $form.serializeArray(),
  //     name = miniShop2.actionName,
  //     action = formData.name;
  //   miniShop2.sendData = {
  //     $form: $form,
  //     action: action,
  //     formData: formData,
  //   };
  //   miniShop2.Cart.change();
  // }
});

$('[data-fancybox="manufacture"]').fancybox({
	thumbs: false,
  hash: false,
  toolbar: false,
  smallBtn: true,
  backFocus: false,
});
//== Slider for Content Page
function content_Carousel() {
  const $this = $('.content-carousel');
  let navContainer = $this.find('.content-carousel__sum');
  let contentCarousel_Image = $this.find('.content-carousel__img');
  // console.log(contentCarousel_Image.innerHeight());
  navContainer.css(
    'top',
    contentCarousel_Image.innerHeight() - navContainer.innerHeight()
  );
  let contentCarousel = new Swiper('.content-carousel .swiper-container', {
    loop: true,
    slidesPerView: 1,
    // centeredSlides: true,
    // autoplay: {
    //   delay: 4000,
    // },
    pagination: {
      el: '.content-carousel__total',
      type: 'fraction',
    },
    // Navigation arrows
    navigation: {
      nextEl: '.content-carousel__next',
      prevEl: '.content-carousel__prev',
    },
    breakpoints: {
      // when window width is >= 860px
      220: {
        pagination: {
          el: '.content-carousel__pagination',
          type: 'bullets',
          dynamicBullets: true,
          clickable: true,
        },
      },
      861: {
        pagination: {
          el: '.content-carousel__total',
          type: 'fraction',
        },
        navigation: {
          nextEl: '.content-carousel__next',
          prevEl: '.content-carousel__prev',
        },
      },
    },
  });
}

//==== Sidebar for Categories ====//
//== this function called in a file: script-closing
function sidebarListSingle_Unmargined() {
  if ($('.sidebar').find('.sidebar-list').length < 2) {
    $('.sidebar-list').addClass('sidebar-list--single');
  }
}

//== Show more categories side list
$('.js-sidebarList_More').click(function () {
  var categoriesHidden_Accordion = $(this).siblings('.sidebar-list--hidden');
  categoriesHidden_Accordion.slideToggle('slow');
  // $('.js-sidebarList_More').text($(this).text() == 'Скрыть' ? 'Ещё' : 'Скрыть');
  $(this).toggleClass('sidebar-list__more--rotate');
  var categoriesMore_Text = $(this).find('.sidebar-list__more-text');
  categoriesMore_Text.text(
    categoriesMore_Text.text() == 'Скрыть' ? 'Ещё' : 'Скрыть'
  );
});

//== sidebarMob show
$('.js-sidebarMob_Call').click(function () {
  $(this).toggleClass('sidebar-mob--active');
  var sidebarMobConntent = $(this).siblings('.sidebar-mob');
  sidebarMobConntent.slideToggle();
});

//== Slider for About Page
// function about_Carousel() {
// const $this = $('.about-carousel');
// let navContainer = $this.find('.content-carousel__sum');
// let contentCarousel_Image = $this.find('.content-carousel__img');
// console.log(contentCarousel_Image.innerHeight());
// navContainer.css(
//   'top',
//   contentCarousel_Image.innerHeight() - navContainer.innerHeight()
// );
let aboutCarousel = new Swiper('.about-carousel .swiper-container', {
  loop: true,
  slidesPerView: 2,
  centeredSlides: true,
  // spaceBetween: 30,
  navigation: {
    nextEl: '.about-carousel__next',
    prevEl: '.about-carousel__prev',
  },
  breakpoints: {
    // when window width is >= 860px
    220: {
      slidesPerView: 1,
    },
    861: {
      slidesPerView: 2,
    },
  },
});
// }

//== Contacts form switchers toggle
$(document).on('click touch', '.js-contactsFormSwitcher_Toggle', function () {
  let contactsFormSwitcher = $(this)
    .closest('.contacts-form__switchers')
    .find('.contacts-form__switcher');
  let contactsFormSwitcher_Active = $(this);
  if (contactsFormSwitcher.hasClass('contacts-form__switcher--active')) {
    contactsFormSwitcher
      .not(contactsFormSwitcher_Active)
      .removeClass('contacts-form__switcher--active');
    contactsFormSwitcher_Active.addClass('contacts-form__switcher--active');
  } else {
    contactsFormSwitcher.removeClass('contacts-form__switcher--active');
  }

  let contactsFormField_mail = contactsFormSwitcher_Active
    .closest('.contacts-form')
    .find('.contacts-form__field-mail');
  let contactsFormField_phone = contactsFormSwitcher_Active
    .closest('.contacts-form')
    .find('.contacts-form__field-phone');
  if (
    contactsFormSwitcher_Active.data('contacts-fieldhide') ===
    'contasts-form-mail'
  ) {
    contactsFormField_mail.addClass('contacts-form__field--hidden');
    contactsFormField_mail.find('.contacts-form__input').removeAttr('data-req');
    contactsFormField_phone.removeClass('contacts-form__field--hidden');
    contactsFormField_phone.find('.contacts-form__input').attr('data-req', '');
  }
  if (
    contactsFormSwitcher_Active.data('contacts-fieldhide') ===
    'contasts-form-phone'
  ) {
    contactsFormField_phone.addClass('contacts-form__field--hidden');
    contactsFormField_phone
      .find('.contacts-form__input')
      .removeAttr('data-req');
    contactsFormField_mail.removeClass('contacts-form__field--hidden');
    contactsFormField_mail.find('.contacts-form__input').attr('data-req', '');
  }

  if (contactsFormSwitcher_Active.data('switcher-note') === 'telephone') {
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('.contacts-form__switchers-note')
      .text('Свяжемся по телефону.');
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('input[name="type"]')
      .val('Свяжемся по телефону.');
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('input[name="name_form"]')
      .val('Связаться по телефону');
  }
  if (contactsFormSwitcher_Active.data('switcher-note') === 'email') {
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('.contacts-form__switchers-note')
      .text('Отправим электронное письмо.');
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('input[name="type"]')
      .val('Отправим электронное письмо.');
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('input[name="name_form"]')
      .val('Связаться по почте');
  }
  if (contactsFormSwitcher_Active.data('switcher-note') === 'whatsapp') {
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('.contacts-form__switchers-note')
      .text('Напишем в WhatsApp, никаких звонков!');
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('input[name="type"]')
      .val('Напишем в WhatsApp, никаких звонков!');
    contactsFormSwitcher_Active
      .closest('.contacts-form__switchers')
      .siblings('input[name="name_form"]')
      .val('Связаться по WhatsApp');
  }
});

var about = $('.main-about');
var about_el_count = about.find('.main-about__item').length;
var about_current_step = 0;
var about_interval;

function randomAboutItem() {
  var num = Math.floor((Math.random() * 10) % about_el_count);

  while (num == about_current_step) {
    num = Math.floor((Math.random() * 10) % about_el_count);
  }

  return num;
}

function showAboutItem() {
  var el_prev = about.find('.main-about__item').eq(about_current_step);
  if (el_prev) el_prev.find('.main-about__item-fade').css('display', '');
  about_current_step = randomAboutItem();
  var el = about.find('.main-about__item').eq(about_current_step);
  el.find('.main-about__item-fade').show(400);
}

function mainAboutInterval() {
  if (!about_el_count || window.screen.width <= 1030) return;
  clearInterval(about_interval);
  showAboutItem();
  about_interval = setInterval(function () {
    showAboutItem();
  }, 4000);
}

about.on('mouseenter', function () {
  clearInterval(about_interval);
  var el_prev = about.find('.main-about__item').eq(about_current_step);
  if (el_prev) el_prev.find('.main-about__item-fade').css('display', '');
});
about.on('mouseleave', function () {
  mainAboutInterval();
});
mainAboutInterval();


/*
 *
 *
 */
//== This section of code should be at the end of main script-file
$(document).ready(function () {
  // console.log( 'Height of screen is ' + currentScreenHeight() );
  currentScreenHeight();
  // equalHeight_Units('.main-page-carousel .swiper-slide');
  // if( $(window).width() > 420 ) {
  //   equalHeight_Units('.artisans__items .artisans-item');
  //   equalHeight_Units('.category__items .category-item__info');
  //   equalHeight_Units('.search__items .search-item__info');
  // }
  // headingContent_NearOffer();
  mainAboutAccent_remove();
  if ($('.catalog-card__carousel').length > 0) {
    catalogCard_Carousel();
    $('.catalog-card__carousel-pagination>.swiper-pagination-bullet').hover(
      function () {
        $(this).trigger('click');
      }
    );
  }
  content_Carousel();
  // navMob_Call();
  gallery_hideElementsTablet();
  productThumbs_BindControls();
  carouselProductRecommend_Mob();
  carouselProductSimilar_Mob();
  // about_Carousel();
  // galleryLeftImages_Count();
  // productImagesCarousel_Mobile();
});

$(window).resize(function () {
  // console.log( 'Height of screen is ' + currentScreenHeight() );
  currentScreenHeight();
  productThumbs_BindControls();
  // equalHeight_Units('.main-page-carousel .swiper-slide');
  // if( $(window).width() > 420 ) {
  //   equalHeight_Units('.artisans__items .artisans-item');
  //   equalHeight_Units('.category__items .category-item__info');
  //   equalHeight_Units('.search__items .search-item__info');
  // }
  // headingContent_NearOffer();
  mainAboutAccent_remove();
  $(function () {
    if ($('.catalog-card__carousel').length > 0) {
      //some-slider-wrap-in
      catalogCard_Carousel();
      $('.catalog-card__carousel-pagination>.swiper-pagination-bullet').hover(
        function () {
          $(this).trigger('click');
        }
      );
    }
  });
  content_Carousel();
  // navMob_Call();
  gallery_hideElementsTablet();
  productThumbs_BindControls();
  carouselProductRecommend_Mob();
  carouselProductSimilar_Mob();
  // about_Carousel();
  // galleryLeftImages_Count();
  // productImagesCarousel_Mobile();
  // modalOverlayHeight_OnResize();
});

