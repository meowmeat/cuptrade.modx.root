{extends 'file:templates/index.tpl'}
{block 'class'}design-page{/block}
{block 'main'}
  {set $about = json_decode($_modx->resource.design, true)[0]}
  {set $bread = $_modx->getChunk('@FILE chunks/all/breadcrumbs.tpl')}
  {if $about.slide}
    {set $slide = json_decode($about.slide, true)[0]}
    <div class="design-promo-section section">
      <div class="section-wrapper">
        <div class="design-promo">
          <div class="design-promo__content">
            <h1 class="title design-promo__title">{$slide.title?:$_modx->resource.pagetitle}</h1>
            <div class="design-promo__text">{$slide.desc | strip_tags:"<b><i><em><strong><br><u><s>"}</div>
            {if $about.slide_btn}<button class="button button-primary js-modalCall" data-modal-called="modalOrder">{$about.slide_btn}</button>{/if}
          </div>
          <div class="design-promo__image">
            {if $slide.img}<img src="{$slide.img}" alt="{$slide.title|htmlent}" class="design-promo__img">{/if}
          </div>
        </div>
      </div>
    </div>
  {/if}
  {if $block1 = json_decode($about.block1, true)}
    <div class="design-components-section section">
      <div class="section-wrapper">
        <div class="design-components">
          {foreach $block1 as $b}
          <div class="design-component">
            <div class="design-component__number">{$b.title}</div>
            <div class="design-component__title">{$b.desc}</div>
          </div>
          {/foreach}
        </div>
      </div>
    </div>
  {/if}

  {* высокотехнологичное производство / постпечатная обработка *}
  {if $_modx->resource.id == 9}
    {set $manufacture = json_decode(8|resource:"manufacture", true)[0]}
    {if $blocks = json_decode($manufacture.block2, true)}
      <div class="manufacture-produces-section section">
        <div class="section-wrapper">
          {foreach $blocks as $block}
          <div class="section-info">
            <div class="title section-info__title">{$block.title}</div>
            {$block.desc}
          </div>
          {if $items = json_decode($block.items, true)}
          <div class="manufacture-produces">
            {foreach $items as $item}
            <div class="manufacture-produce">
              <img src="{$item.img}" alt="" class="manufacture-produce__ico">
              <div class="manufacture-produce__info">
                <div class="manufacture-produce__title">{$item.title}</div>
                <div class="manufacture-produce__text">{$item.desc}</div>
              </div>
            </div>
            {/foreach}
          </div>
          {/if}
          {/foreach}
        </div>
      </div>
    {/if}
    {if $blocks = json_decode($manufacture.block3, true)}
      {set $block3}
      <div class="manufacture-processing-section section">
        <div class="section-wrapper">
          <div class="section-heading">
            <div class="section-heading__title">{$manufacture.block3_title}</div>
          </div>
          <div class="manufacture-processings">
            {foreach $blocks as $block}
              <div class="manufacture-processing" href="{$block.big_img}" data-fancybox="manufacture" data-caption="{$block.desc}">
                <div class="manufacture-processing__title">{$block.title}</div>
                <div class="manufacture-processing__text">{$block.desc}</div>
                <img src="{$block.img}" alt="" class="manufacture-processing__bg">
              </div>
            {/foreach}
          </div>
        </div>
      </div>
      {/set}
      {$block3 | lazy:true}
    {/if}
  {/if}

  {if $about.prices}
  <div class="design-price-section section">
    <div class="section-wrapper">
      <div class="design-price__heading">
        {if $about.prices_title}<div class="title design-price__title">{$about.prices_title}</div>{/if}
        {if $about.prices_desc}<div class="design-price__note">{$about.prices_desc}</div>{/if}
      </div>
      <div class="table-wrapper design-price__table">
        {$_modx->runSnippet('TVTable', ['tdTpl' => '@INLINE {if $val | preg_match : \'/(.+)(|\\/)(.+)(header)/isu\'}<th>{$val}</th>{else}<td>{$val}</td>{/if}', 'tableClass' => 'price-table', 'input' => $about.prices]) | trim_inner_td}
      </div>
      {if $about.prices_btn}<div class="flex-centered"><button class="button button-primary design-section__button js-modalCall" data-modal-called="modalOrder">{$about.prices_btn}</button></div>{/if}
    </div>
  </div>
  {/if}
  {*if $_modx->resource.id == 81*}
    {include 'file:chunks/all/advantages.tpl'}
  {*/if*}
  <div class="design-portfolio-section section" id="pdopage">
    <div class="section-wrapper">
      <div class="section-info">
        {if $about.title1}<div class="title section-info__title">{$about.title1}</div>{/if}
        {$about.text1 | lazy}
      </div>
      <div class="gallery-section">
        <div class="section-wrapper">
          <div class="gallery js-gallery-wrap">
      {'!pdoPageCustom' | snippet : [
          'frontend_css' => 0,
          'frontend_js' => 0,
          'ajaxTplMore' => '@INLINE <div class="flex-centered"><button class="button button-primary design-section__button js-more-btn">Показать ещё</button></div>',
          'ajaxElemMore' => '#pdopage .js-more-btn',
          'ajaxElemRows' => '#pdopage .js-gallery-wrap',
          'element' => 'ms2Gallery@ms2Gallery',
          'resources' => $_modx->resource.id,
          'limit' => 8,
          'ajax' => 1,
          'ajaxMode' => 'button',
          'setMeta' => 0,
          'tplPageWrapper' => '@INLINE <div class="pagination"><ul class="pagination">{$pages}</ul></div>',
          'tpl' => '@INLINE {if $files?}
            {foreach $files as $file}
                <a href="{$file.url}" data-caption="{$file.alt|htmlent}" rel="nofollow" data-fancybox="gallery" class="gallery-preview">
                  <img src="assets/templates/cuptrade/public/assets/cuptrade/img/preloader.gif" data-src="{$file.gallery_origin}" alt="{$file.alt|htmlent}" class="gallery-preview__img lazy">
                </a>
            {/foreach}
          {/if}'
      ]}
          </div>
        </div>
      </div>
        {$_modx->getPlaceholder('page.nav')}
    </div>
  </div>
  {if $content = $_modx->resource.content}
    <div class="design-section section">
      <img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_lines_waves.svg" alt="" class="design-section__shape">
      <div class="section-wrapper">
        <div class="section-info">
          {$content | lazy}
        </div>
      </div>
    </div>
  {/if}
{/block}