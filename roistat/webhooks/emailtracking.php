<?php

// https://www.cuptrade.ru/roistat/webhooks/emailtracking.php

const ROISTAT_KEY       = 'b063fdf044ee29435bad07f51c0eecb3';
const ROISTAT_PROJECT   = 169419;

const AMO_SUBDOMAIN     = 'cuptrade';
const AMO_CLIENT_ID     = 'bd04b82d-dd15-46e7-ab60-9a8cfcdaa2b8';
const AMO_CLIENT_SECRET = 'BpD15lbMEhNQD6D5hgZ9VeSI8ZiXO2lbQDykXaCMz56cMENF3wpIuAd27Tn5iUck';
const AMO_REDIRECT_URL  = 'https://www.cuptrade.ru/roistat/amo/redirect_url.php';

$data = json_decode(trim(file_get_contents('php://input')), true);

if(empty($data)) {
    die('No data');
}

writeToLog($data, 'Webhook data');

$visit   = !empty($data['visit_id']) ? $data['visit_id'] : $data['marker'];
$email   = $data['email_from'];
$emailTo = $data['email_to'];
$title   = $data['subject'];
$name    = 'Письмо от '.$email;
$comment = $data['text'];
$customFields = array(
    '611311' => $data['landing_page'],
    '611313' => $data['marker'],
    '611315' => $data['city'],
    '611317' => $data['utm_source'],
    '611319' => $data['utm_medium'],
    '611321' => $data['utm_campaign'],
    '611323' => $data['utm_term'],
    '611325' => $data['utm_content'],
    '612637' => strval($visit),
);
$customFieldsValues = array_map(function ($field, $value) {
    return array(
        'field_id' => $field,
        'values'   => array(
            array(
                'value' => $value
            ),
        ),
    );
}, array_keys($customFields), $customFields);

// -----------------------------------------------------------------------------

$token = getAmoAccessToken();

$contacts = getContactsByEmail($token, $email);

if(!empty($contacts)) {
    $contact = $contacts->_embedded->contacts[0];

    if(empty($contact->_embedded->leads)) {
        $lead = createLead($token, array(
            'name'                 => $title,
            'pipeline_id'          => 2032951,
            'status_id'            => 29735686,
            'custom_fields_values' => $customFieldsValues,
        ));

        $lead = array_shift($lead->_embedded->leads);

        if($contact !== null && $lead !== null) {
            createNote($token, $lead, $comment);

            $link = linkContactToLead($token, $contact, $lead);

            writeToLog(array(
                'Status'    => 'First Lead',
                'ContactID' => $contact->id,
                'LeadID'    => $lead->id,
            ), 'New Lead');
        }

        die(json_encode(array(
            'Status'    => 'First Lead',
            'ContactID' => $contact->id,
            'LeadID'    => $lead->id,
        )));
    }

    $lastLead = getLead($token, array_pop($contact->_embedded->leads));

    $openedStatuses = roistatOpenedStatuses(roistatStatuses(), ['progress']);
    $openedStatuses = array_map(function ($item) {
        return $item['id'];
    }, $openedStatuses);

    if(!in_array($lastLead->status_id, $openedStatuses)) {
        writeToLog($lastLead, 'Create. Lead close');

        $lead = createLead($token, array(
            'name'                 => $title,
            'pipeline_id'          => 2032951,
            'status_id'            => 29735686,
            'custom_fields_values' => $customFieldsValues,
        ));

        $lead = array_shift($lead->_embedded->leads);

        if($contact !== null && $lead !== null) {
            createNote($token, $lead, $comment);

            $link = linkContactToLead($token, $contact, $lead);

            writeToLog(array(
                'ContactID' => $contact->id,
                'LeadID'    => $lead->id,
            ), 'New Lead');
        }

        die(json_encode(array(
            'Status'    => 'Next Lead',
            'ContactID' => $contact->id,
            'LeadID'    => $lead->id,
        )));
    } else {
        createNote($token, $lastLead, $comment);

        writeToLog(array(
            'statuses' => $openedStatuses,
            'lead'     => $lastLead
        ), 'Ignore. Lead ID '.$lastLead->id.' open');

        die('Ignore. Lead ID '.$lastLead->id.' open');
    }
} else {
    writeToLog($contacts, 'Create. No Contacts');

    $lead = createLead($token, array(
        'name'                 => $title,
        'pipeline_id'          => 2032951,
        'status_id'            => 29735686,
        'custom_fields_values' => $customFieldsValues,
    ));

    $lead = array_shift($lead->_embedded->leads);

    $contact = createContact($token, array(
        'name' => $name,
        'custom_fields_values' => array(
            array(
                'field_id' => 376481,
                'values'   => array(
                    array(
                        'value'     => $email,
                        'enum_code' => 'WORK'
                    ),
                ),
            ),
        )
    ));

    $contact = array_shift($contact->_embedded->contacts);

    if($contact !== null && $lead !== null) {
        createNote($token, $lead, $comment);

        $link = linkContactToLead($token, $contact, $lead);

        writeToLog(array(
            'ContactID' => $contact->id,
            'LeadID'    => $lead->id,
        ), 'First contact and Lead');
    }

    die(json_encode(array(
        'Status'    => 'New Lead',
        'ContactID' => $contact->id,
        'LeadID'    => $lead->id,
    )));
}

// ----------------------------------------------------------------------------

function createLead($token, $fields)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://'.AMO_SUBDOMAIN.'.amocrm.ru/api/v4/leads',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode(array($fields)),
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token,
            'Content-Type: application/json',
        ),
    ));
    $out = curl_exec($curl);
    curl_close($curl);

    return json_decode($out);
}

function createContact($token, $fields)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://'.AMO_SUBDOMAIN.'.amocrm.ru/api/v4/contacts',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode(array($fields)),
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token,
            'Content-Type: application/json',
        ),
    ));
    $out = curl_exec($curl);
    curl_close($curl);

    return json_decode($out);
}

function createNote($token, $lead, $text)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://'.AMO_SUBDOMAIN.'.amocrm.ru/api/v4/leads/'.$lead->id.'/notes',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode(array(array(
            'note_type' => 'common',
            'params'    => array(
                'text' => $text,
            ),
        ))),
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token,
            'Content-Type: application/json',
        ),
    ));
    $out = curl_exec($curl);
    curl_close($curl);

    return json_decode($out);
}

function linkContactToLead($token, $contact, $lead)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://'.AMO_SUBDOMAIN.'.amocrm.ru/api/v4/contacts/'.$contact->id.'/link',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode(array(array(
            'to_entity_id'   => $lead->id,
            'to_entity_type' => 'leads',
        ))),
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token,
            'Content-Type: application/json',
        ),
    ));
    $out = curl_exec($curl);
    curl_close($curl);

    return json_decode($out);
}

function getLead($token, $lead)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://'.AMO_SUBDOMAIN.'.amocrm.ru/api/v4/leads/'.$lead->id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token,
            'Content-Type: application/json',
        ),
    ));
    $out = curl_exec($curl);
    curl_close($curl);

    return json_decode($out);
}

function getAmoAccessToken()
{
    if(time() > getToken('time')) {
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-oAuth-client/1.0');
        curl_setopt($curl,CURLOPT_URL, 'https://'.AMO_SUBDOMAIN.'.amocrm.ru/oauth2/access_token');
        curl_setopt($curl,CURLOPT_HTTPHEADER,['Content-Type:application/json']);
        curl_setopt($curl,CURLOPT_HEADER, false);
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl,CURLOPT_POSTFIELDS, json_encode(array(
            'client_id'     => AMO_CLIENT_ID,
            'client_secret' => AMO_CLIENT_SECRET,
            'grant_type'    => 'refresh_token',
            'refresh_token' => getToken('refresh'),
            'redirect_uri'  => AMO_REDIRECT_URL,
        )));
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 2);
        $out = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($out);

        setToken('time', time() + $response->expires_in);
        setToken('access', $response->access_token);
        setToken('refresh', $response->refresh_token);
    }

    return getToken('access');
}

function getContactsByEmail($token, $email)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://'.AMO_SUBDOMAIN.'.amocrm.ru/api/v4/contacts?with=leads&query='.$email,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token,
            'Content-Type: application/json',
        ),
    ));
    $out = curl_exec($curl);
    curl_close($curl);

    return json_decode($out);
}

function roistatStatuses()
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'https://cloud.roistat.com/api/v1/project/integration/status/list?is_need_unused=1&key='.ROISTAT_KEY.'&project='.ROISTAT_PROJECT);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    $out = curl_exec($curl);
    curl_close($curl);

    return json_decode($out, true);
}

function roistatOpenedStatuses($statuses, $closedGroup)
{
    $statusesByGroups = [];
    foreach($statuses['data'] as $status) {
        $statusesByGroups[$status['type']][] = [
            'id'   => $status['id'],
            'name' => $status['name'],
        ];
    }

    $closedByGroups = array_filter($statusesByGroups, function ($item) use ($closedGroup) {
        return in_array($item, $closedGroup);
    }, ARRAY_FILTER_USE_KEY);

    return call_user_func_array('array_merge', $closedByGroups);
}

function getToken($name)
{
    return file_get_contents(__DIR__.'/../amo/.amo_'.$name.'_token');
}

function setToken($name, $value)
{
    file_put_contents(__DIR__.'/../amo/.amo_'.$name.'_token', $value);
}

function writeToLog($data, $title = '')
{
    $log = "\n------------------------\n";
    $log .= date("d.m.Y G:i:s") . "\n";
    $log .= (strlen($title) > 0 ? $title : 'DEBUG') . "\n";
    $log .= print_r($data, 1);
    $log .= "\n------------------------\n";
    // file_put_contents(getcwd() . '/log_data_emailtracking.log', $log, FILE_APPEND);
    return true;
}