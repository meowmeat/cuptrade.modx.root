<div class="section-twin__side">
  <div class="order-purchase">
    <div class="order-purchase__header">
      <div class="order-purchase__title">Ваш заказ</div>
      {if $_modx->config['discount']}<div class="order-purchase__note">Приобретая товар кратно коробке, вы можете сэкономить до 15%!</div>{/if}
    </div>
    {if $products}
      {foreach $products as $product}
        {if $packs = ($product.id | resource:"sizes")}
          {if $product.options.pack}
            {foreach json_decode($packs, true) as $pack}
              {if $pack.MIGX_id == $product.options.pack}
                {set $pack_count = $pack.count}
                {set $pack_type = $pack.pack_type~' ('~$pack.count~' шт.)'}
              {/if}
            {/foreach}
          {/if}
        {/if}
        <div class="order-purchase__set">
          <div class="order-purchase__set-name"><a href="{$product.uri}">{$product.pagetitle}</a></div>
          <div class="order-purchase__row">
            <div class="order-purchase__row-name">Кол.</div>
            <div class="order-purchase__row-value">{($pack_count?:1) * $product.count} шт.</div>
          </div>
          <div class="order-purchase__row">
            <div class="order-purchase__row-name">Стоимость</div>
            <div class="order-purchase__row-value"><span class="js-price">{($.session.minishop2.cart[$product.key].price * $.session.minishop2.cart[$product.key].count)}</span> ₽</div>
          </div>
        </div>
      {/foreach}
      {set $summ_cost = 0}
      {foreach $.session.minishop2.cart as $pos}
        {set $summ_cost = $summ_cost + ($pos.price*$pos.count)}
      {/foreach}
      
      <div class="cart-purchase__position" style="margin-top:20px">
        <div class="cart-purchase__position-name">Доставка</div>
        <div class="cart-purchase__position-price"><span class="js-delivery_cost js-price">{$.cookie.delivery_price?:0}</span> ₽</div>
      </div>
      
      <div class="order-purchase__summary">
        <div class="order-purchase__summary-name">Всего</div>
        <div class="order-purchase__summary-price"><span class="js-total_cost js-price" data-cost="{$summ_cost}">{$summ_cost}</span> ₽</div>
      </div>
    {/if}
      <a href="{24 | url}" class="button button-secondary order-purchase__button">Изменить заказ</a>
  </div>
</div>