<?php
/**
 * English permissions Lexicon Entries for amocrm
 *
 * @package amocrm
 * @subpackage lexicon
 */
$_lang['amocrm_save'] = 'Permission for save/update data.';