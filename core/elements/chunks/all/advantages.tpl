{if $advantages = json_decode(1|resource:"advantages", true)}
  <div class="main-about-section section">
    <img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_lines_circle.svg" alt="" class="main-about__shape">
    <div class="section-wrapper">
      <div class="section-heading">
        <div class="section-heading__title">Это про нас</div>
      </div>
      <div class="main-about">
        {foreach $advantages as $adv}
        {set $idx = $adv@index+1}
          <div class="main-about__item {if $idx in list [2, 3, 6, 7, 10, 11, 14, 15, 18, 19]}main-about__item--accent{/if} {cycle ["main-about__item--even", ""] index=$idx}">
            <div class="main-about__item-fade">
              <div class="main-about__item-fade__number">{if $idx <= 9}0{$idx}{else}{$idx}{/if}</div>
              <div class="main-about__item-fade__text">{$adv.desc}</div>
            </div>
            <div class="main-about__item-number">{if $idx <= 9}0{$idx}{else}{$idx}{/if}</div>
            <div class="main-about__item-title">{$adv.title}</div>
            <div class="main-about__item-text">{$adv.desc}</div>
            <div class="main-about__item-ico">+</div>
          </div>
        {/foreach}
      </div>
    </div>
  </div>
  {/if}