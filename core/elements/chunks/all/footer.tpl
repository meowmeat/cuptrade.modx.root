  <div class="footer-section section">
    <div class="section-wrapper">
      <div class="footer">
        <div class="footer-col footer-nav footer-nav-first">
          <a href="{2|url}" class="footer-nav__heading">
            Каталог
          </a>
          <a href="{16|url}" class="footer-nav__item">
            Однослойные стаканы
          </a>
          <a href="{17|url}" class="footer-nav__item">
            Двухслойные стаканы
          </a>
          <a href="{18|url}" class="footer-nav__item">
            Крышки
          </a>
          <a href="{19|url}" class="footer-nav__item">
            Сопутствующие товары
          </a>
          <a href="{20|url}" class="footer-nav__item">
            Пластиковые стаканы и крышки
          </a>
          <a href="{21|url}" class="footer-nav__item">
            Упаковка из бумаги
          </a>
        </div>
        <div class="footer-col footer-nav footer-nav-second">
          <a href="{4|url}" class="footer-nav__heading">
            О нас
          </a>
          <a href="{4|url}" class="footer-nav__item">
            О компании
          </a>
          <a href="{10|url}" class="footer-nav__item">
            Доставка и оплата
          </a>
          <a href="{7|url}" class="footer-nav__item">
            Контакты
          </a>
          <div class="footer-nav__sections">
            <a href="{9|url}" class="footer-nav__section">
              Ваш дизайн
            </a>
            <a href="{81|url}" class="footer-nav__section">
              Ваш логотип
            </a>
            <a href="{8|url}" class="footer-nav__section">
              Производство
            </a>
          </div>
        </div>
        <div class="footer-col footer-contacts">
          <a href="{7|url}" class="footer-nav__heading">
            Контакты
          </a>
          <div class="footer-contact footer-address">
            {$_modx->config['address']}
          </div>
          <a href="tel:{$_modx->config['phone']}" class="hidden-desktop footer-contact footer-phone js-phone">
            {$_modx->config['phone'] | replace:"+7":""}
          </a>
          <a href="javascript:void(0);" class="visible-desktop js-modalCall footer-contact footer-phone js-phone" data-modal-called="modalCallback">
            {$_modx->config['phone'] | replace:"+7":""}
          </a>
          <a href="mailto:{$_modx->config['email']}" class="footer-contact footer-mail">
            {$_modx->config['email']}
          </a>
          <button class="button button-primary js-modalCall" data-modal-called="modalCallback">
            Заказать звонок
          </button>

          <div class="footer-socnet">
            {if $_modx->config['insta']}
              <a target="_blank" href="{$_modx->config['insta']}" rel="nofollow" class="footer-socnet__item">
                <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-inst-gradient.svg" alt="" class="footer-socnet__ico">
              </a>
            {/if}
            {if $_modx->config['whatsapp']}
              <a target="_blank" href="https://api.whatsapp.com/send?phone={$_modx->config['whatsapp']}" rel="nofollow" class="footer-socnet__item">
                <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-whatsapp-green.svg" alt="" class="footer-socnet__ico">
              </a>
            {/if}
            {if $_modx->config['telegram']}
              <a target="_blank" href="{$_modx->config['telegram']}" rel="nofollow" class="footer-socnet__item">
                <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-telegram-color.svg" alt="" class="footer-socnet__ico">
              </a>
            {/if}
          </div>
        </div>
        <div class="footer-col footer-connect">
          <div class="footer-connect__item footer-connect__item-first js-modalCall" data-modal-called="modalGetExample">
            <span class="footer-connect__ico">
              <svg width="19" height="20" viewbox="0 0 19 20" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M.333 3.42A3 3 0 013.303 0h12.391a3 3 0 012.97 3.424l-.174 1.217-1.98-.282.174-1.218A1 1 0 0015.694 2H3.304a1 1 0 00-.99 1.14l1.983 14a1 1 0 00.99.86h8.496a1 1 0 00.984-.82l.25-1.36 1.967.36-.25 1.361A3 3 0 0113.784 20H5.286a3 3 0 01-2.97-2.58l-1.984-14z" />
                <path fill-rule="evenodd" clip-rule="evenodd" d="M15 13V7h2v6h-2z" />
                <path fill-rule="evenodd" clip-rule="evenodd" d="M13 9h6v2h-6V9z" />
              </svg>
            </span>
            <span class="footer-connect__item-text">
              Получить образцы
            </span>
          </div>
          <div class="footer-connect__item js-modalCall" data-modal-called="modalGetOptprice">
            <span class="footer-connect__ico">
              <svg width="20" height="14" viewbox="0 0 20 14" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M2 6.8v4a1 1 0 001 1h14a1 1 0 001-1v-4h2v4a3 3 0 01-3 3H3a3 3 0 01-3-3v-4h2z" />
                <path fill-rule="evenodd" clip-rule="evenodd" d="M4.307 1.721L5.693.28 10 4.414 14.307.279l1.386 1.442L10 7.186 4.307 1.721z" />
              </svg>
            </span>
            <span class="footer-connect__item-text">
              Получить прайс-лист
            </span>
          </div>
          <br>
          <div class="footer-yaorg-rate">
            {set $yandex_sprav}
            <iframe src="https://yandex.ru/sprav/widget/rating-badge/20968097212" width="150" height="50" frameborder="0"></iframe>
            {/set}
            {$yandex_sprav | lazy:true}
          </div>
        </div>

        <div class="footer-site">
          <a href="/" class="footer-logo footer-site__col">
            <img src="assets/templates/cuptrade/public/assets/cuptrade/img/logo-cuptrade.svg" alt="" class="footer-logo__img">
            <div class="footer-logo__text">
              <span>
                Одноразовая посуда
              </span>
              <span>
                из эко-материалов
              </span>
            </div>
          </a>
          <div class="footer-owner footer-site__col">
            © 2017–{'' | date:"Y"} "CupTrade" — одноразовая посуда и сопутствующие товары
          </div>
          <div class="footer-infolaw footer-site__col">
            {if $_modx->config['org_name']}
              <div class="footer-infolaw__item">
                {$_modx->config['org_name']}
              </div>
            {/if}
            {if $_modx->config['org_inn']}
              <div class="footer-infolaw__item">
                ИНН: {$_modx->config['org_inn']}<br />
              {if $_modx->config['org_ogrn']}
                ОГРН: {$_modx->config['org_ogrn']}
              {/if}
              </div>
            {/if}
            
          </div>
          {if $_modx->resource.id == 1}
          <a href="https://ne-prosto.ru" target="_blank" class="footer-developer footer-site__col">
            Разработка сайта – Непросто
          </a>
          {/if}
        </div>

      </div>
    </div>
  </div>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
COORDS = [{$_modx->config['coords']}];
ADDRESS = '{$_modx->config['address']}';
ID_PAGE = {$_modx->resource.id};
HTTP_HOST = '{$_modx->config['http_host']}';
DISCOUNT_CODE = '{$.session.cuptrade_code}';
SUCCESS_SUMBIT_URL = '{6|url}';
CART_LINK = '{24|url}';
{if $discount_summ = $_modx->getPlaceholder('_discount_summ')}
  DISCOUNT_SUMM = {$discount_summ};
  DISCOUNT = '{$_modx->getPlaceholder('_discount')?:0}';
{/if}
{if $_modx->resource.id == 7}
window.addEventListener('DOMContentLoaded', () => {
  window.scrollTo(window.scrollX, window.scrollY + 1);
  window.scrollTo(window.scrollX, window.scrollY - 1);
});
{/if}
var fired1 = false;
window.addEventListener('scroll', () => {
if (fired1 === false) {
    fired1 = true;
    setTimeout(() => {
      {if $_modx->resource.template != 15}
        var script   = document.createElement("script");
        script.type  = "text/javascript";
        script.src   = "//api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=2f65af83-e582-4222-87cf-2e7ee56c45c5&onload=getYaMap";
        script.async  = 1;
        document.body.appendChild(script);
      {/if}
    }, 200)
}
});
</script>
{'!MinifyX' | snippet : [
    'minifyJs' => 1,
    'jsTpl' => '<script async src="[[+file]]" onload="initAjaxForm();"></script>',
    'jsSources' => '
    assets/templates/cuptrade/public/assets/cuptrade/js/jquery-3.4.1.min.js,
    assets/templates/cuptrade/public/assets/cuptrade/js/map.yandex.js,
    assets/templates/cuptrade/public/assets/cuptrade/js/jquery.inputmask.min.js,
    assets/templates/cuptrade/public/assets/cuptrade/js/swiper-bundle.min.js,
    assets/templates/cuptrade/public/assets/cuptrade/js/fancybox.min.js,
    assets/components/ajaxform/js/lib/jquery.form.min.js,
    assets/components/ajaxform/js/script.js,
    assets/templates/cuptrade/public/assets/cuptrade/js/script.js,
    assets/templates/cuptrade/public/assets/cuptrade/js/custom.js,
    assets/components/pdotools/js/mypdopage.js
    '
]}

<!-- Yandex.Metrika counter -->
<script type="text/javascript">(function(m,e,t,r,i,k,a){ m[i]=m[i]||function(){ (m[i].a=m[i].a||[]).push(arguments) }; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a) }) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(45043370, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, ecommerce:"dataLayer" }); </script>
<noscript><div><img src="https://mc.yandex.ru/watch/45043370" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init?referrer="+encodeURIComponent(d.location.href);
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, 'script', 'cloud.roistat.com', 'd46d242768df4f3a8f7587759d63a538');
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{ if(f.fbq)return;n=f.fbq=function(){ n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments) };
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s) }(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '140616907353866'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" src="https://www.facebook.com/tr?id=140616907353866&ev=PageView&noscript=1" alt=""/>
</noscript>
<!-- End Facebook Pixel Code -->
<!-- Begin LeadBack code -->
<script>
  if (window.screen.width >= 720) {
    var _emv = _emv || [];
    _emv['campaign'] = 'fd605345abc1824ce4011e6b';
    
    (function() {
        var em = document.createElement('script'); em.type = 'text/javascript'; em.async = true;
        em.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'leadback.ru/js/leadback.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(em, s);
    })();
  }
</script>
<!-- End LeadBack code -->

{if $_modx->resource.template == 15}
<script src="//api-maps.yandex.ru/2.1/?apikey=2f65af83-e582-4222-87cf-2e7ee56c45c5&lang=ru_RU&mode=debug" defer></script>
<script src="/assets/templates/cuptrade/public/assets/cuptrade/js/calc_delivery.js" defer></script>
{/if}
{$_modx->getPlaceholder('MinifyX.javascript')}