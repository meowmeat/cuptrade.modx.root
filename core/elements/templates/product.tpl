{extends 'file:templates/index.tpl'}
{block 'class'}product-page{/block}
{block 'main'}
  <div class="product-section section">
    {include 'file:chunks/all/breadcrumbs.tpl'}
    <div class="section-wrapper">
      <h1>{$_modx->resource.pagetitle}</h1>
      <div class="product-contain">
        <div class="product-view" data-image-previews>
          {$_modx->runSnippet('msGallery', [
            'limit' => 0,
            'tpl' => '@INLINE 
                  {if $files?}
                  {if $files | length > 1}
                  <div class="product-carousel">
                    <div class="swiper-container">
                      <div class="product-carousel__slides swiper-wrapper">
                        {foreach $files as $file}
                          <a class="product-carousel__slide swiper-slide" href="{$file.gallery}">
                            <img src="{$file.gallery_card}" alt="{($file.description?:$_modx->resource.pagetitle)|htmlent}">
                          </a>
                        {/foreach}
                      </div>
                    <div class="product-carousel__pagination"></div>
                  </div>
                  <div class="product-carousel__nav product-carousel__prev"><svg width="8" height="12" viewBox="0 0 8 12" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M6.079 11.693l1.442-1.386L3.386 6l4.135-4.307L6.08.307.614 6l5.465 5.693z" />
                    </svg></div>
                  <div class="product-carousel__nav product-carousel__next"><svg width="8" height="12" viewBox="0 0 8 12" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M1.721 11.693L.28 10.307 4.414 6 .279 1.693 1.72.307 7.186 6l-5.465 5.693z" />
                    </svg></div>
                  </div>
                  <div class="product-preview">
                    <div class="swiper-container">
                      <div class="swiper-wrapper">
                        {foreach $files as $file}
                          <img src="{$file.preview}" alt="{($file.description?:$_modx->resource.pagetitle)|htmlent}" class="product-preview__img swiper-slide">
                        {/foreach}
                      </div>
                    </div>
                    <div class="product-preview__shadow"></div>
                  </div>
                  {else}
                    {foreach $files as $file}
                      <a data-fancybox href="{$file.gallery}">
                        <img src="{$file.gallery_card}" alt="{($file.description?:$_modx->resource.pagetitle)|htmlent}">
                      </a>
                    {/foreach}
                  {/if}
                  {else}
                    {set $notimg}
                    <img src="assets/templates/cuptrade/public/assets/cuptrade/img/404_2.png" alt="{$_modx->resource.pagetitle|htmlent}">
                    {/set}
                    {$notimg|lazy:true}
                  {/if}'
          ])}
        </div>
        {set $sizes = json_decode($_modx->resource.sizes, true)}
        {set $price_position = ($price|replace:" ":""|replace:",":".")}
        {set $price_discount = ($_modx->resource.opt_price[0]|replace:" ":""|replace:",":".")}

        {*set $price_discount = ($price_position - ($price_position*($_modx->getPlaceholder('_discount')?:0)/100)) | round:"2"}
        {set $summ_cart = 0}
        {foreach $.session.minishop2.cart as $pos}
          {set $summ_cart = $summ_cart + ($pos.price*$pos.count)}
        {/foreach}
        {if $_modx->getPlaceholder('_discount_summ') && ($summ_cart >= $_modx->getPlaceholder('_discount_summ'))}
          {set $price_position = $price_discount}
        {/if*}
        <div class="product-stats">
          <div class="product__note">{$_modx->resource.introtext}</div>
          <div class="product-take">
            {if $_modx->resource.searchable}<div class="product-take__available">На складе</div>{else}<div class="product-take__available" style="color:#df312f">Под заказ</div>{/if}
            {if $article}<div class="product-take__model">Артикул: {$article}</div>{/if}
          </div>
          <form method="post" id="productOptionsForm" class="product-form ms2_form">
            <input type="hidden" name="id" value="{$_modx->resource.id}">
            {if $sizes}
              <div class="product-form__switchers">
                {foreach $sizes as $size}
                  {set $price_sizes[$size.MIGX_id] = $size.MIGX_id == 1 ? ($size.count?:1)*$price_position : ($size.count?:1)*$price_discount}
                  <input data-pack-count="{$size.count?:1}" data-price="{$price_sizes[$size.MIGX_id]|price_format}" type="radio" class="product-form__switcher js-type-pack" id="size{$size.MIGX_id}" name="options[pack]" value="{$size.MIGX_id}" {if $size@first}checked{/if}>
                  <label class="product-form__switcher-name" for="size{$size.MIGX_id}">{$size.pack_type} ({$size.count?:1} шт.)</label>
                {/foreach}
              </div>
            {/if}

            <div class="product-form__set" data-product-counter>
              <div class="product-form__price"><span class="js-price-current js-price">{$price}</span> ₽ <span class="product-form__price-measure">за шт.</span></div>
              <div class="product-form__counter">
                <div class="product-count">
                  <div class="product-count__control product-count__minus js-productCount" onselectstart="return false;"><span class="product-count__control-sign">&ndash;</span></div>
                  <input type="number" value="1" min="1" step="1" name="count" onkeypress="return checkingNumberFormat(event);" data-price="{$price_position}" data-discount="{$price_discount | price_format}" class="product-count__number js-productCount_Number">
                  <div class="product-count__control product-count__plus js-productCount" onselectstart="return false;"><span class="product-count__control-sign">+</span></div>
                </div>
              </div>
            </div>

            <!--div class="product-form__options">
              <script>
              var OPTS = [];
              </script>
              {if $opts = json_decode($_modx->resource.opts, true)}
                {foreach $opts as $opt}
                  {set $opt_prices = json_decode($opt.prices, true)}
                  {set $opt_min_price = 0}
                  {set $opt_max_price = 0}
                  {set $checked = false}
                  {foreach $opt_prices as $opt_price}
                    {if !$opt_min_price || $opt_min_price > $opt_price.price}
                      {set $opt_min_price = $opt_price.price}
                    {/if}
                    {if !$opt_max_price || $opt_max_price < $opt_price.price}
                      {set $opt_max_price = $opt_price.price}
                    {/if}
                    <script>
                      if(typeof OPTS[{$opt.MIGX_id}] == 'undefined'){
                        OPTS[{$opt.MIGX_id}] = [];
                      }
                      OPTS[{$opt.MIGX_id}][{$opt_price.count}] = {$opt_price.price};
                    </script>
                  {/foreach}
                  {if $opt_min_price}
                    <div class="product-form__checkboxer">
                      <input type="checkbox" name="options[opts][]" value="{$opt.MIGX_id}" id="formProductLogo{$opt.MIGX_id}" class="product-form__checkbox js-opts">
                      <label for="formProductLogo{$opt.MIGX_id}" class="product-form__checkbox-label">{$opt.option_type} <span class="js-price-opts" data-opt-price="от {$opt_min_price}">от {$opt_min_price|price_format}</span> ₽ {$opt.postfix}</label>
                    </div>
                  {/if}
                {/foreach}
              {/if}
              {*<script>
                console.log(OPTS);
              </script>*}
              {*<div class="product-form__checkboxer">
                <input type="checkbox" name="product_logo_add" id="formProductLogo_add" class="product-form__checkbox">
                <label for="formProductLogo_add" class="product-form__checkbox-label">Добавить логотип 1 цвет от 1.8 ₽ за стакан</label>
              </div>
              <div class="product-form__checkboxer">
                <input type="checkbox" name="product_logo_add" id="formProductLogo_add_2" class="product-form__checkbox">
                <label for="formProductLogo_add_2" class="product-form__checkbox-label">Добавить логотип 2 цвета от 2.16 ₽ за стакан</label>
              </div>*}
            </div-->
            <div class="product-form__footer" data-product-price-wrap>
              <div class="product-form__summary">
                <div class="product-form__summary-title">Итого</div>
                <div class="product-form__summary-cost js-cost" data-product-price="{$price_sizes[1]}"><span data-product-cost>{$price_sizes[1]}</span></div>
                <div class="product-form__summary-currency">₽</div>
              </div>
              <button type="submit" name="ms2_action" value="cart/add" class="button button-primary product-form__submit">В корзину</button>
            </div>
          </form>
        </div>
      </div>
      
      <script>
        window.dataLayer.push({
            "ecommerce": {
                "detail": {
                    "products": [
                        {
                            "id": {$_modx->resource.id},
                            "name" : "{$_modx->resource.pagetitle}",
                            "price": {$price_sizes[1]}
                        }
                    ]
                }
            }
        });
        function dataLayerPushCart(total_cost, total_count) {
          dataLayer.push({
              "ecommerce": {
                  "add": {
                      "products": [
                          {
                              "id": {$_modx->resource.id},
                              "name" : "{$_modx->resource.pagetitle}",
                              "price": total_cost,
                              "quantity": total_count
                          }
                      ]
                  }
              }
          });
        }
      </script>


      <div class="product-profile">
        {if $_modx->resource.content}
          <div class="product-description">
            <h2>Описание</h2>
            {$_modx->resource.content|lazy:true}
          </div>
        {/if}

        <div class="product-params">
          {if $props = json_decode($_modx->resource.props, true)[0]}
            <h2>Характеристики</h2>
            <div class="product-param__items">
              {if $props.value}
              <div class="product-param">
                <div class="product-param__image">
                  <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cup-amount.svg" alt="" class="product-param__ico">
                </div>
                <div class="product-param__name">Обьём</div>
                <div class="product-param__value">{$props.value} мл</div>
              </div>
              {/if}
              {if $props.dia_out}
              <div class="product-param">
                <div class="product-param__image">
                  <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cup-width-out.svg" alt="" class="product-param__ico">
                </div>
                <div class="product-param__name">Диаметр верх (наруж.)</div>
                <div class="product-param__value">{$props.dia_out} мм</div>
              </div>
              {/if}
              {if $props.dia_in}
              <div class="product-param">
                <div class="product-param__image">
                  <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cup-width-in.svg" alt="" class="product-param__ico">
                </div>
                <div class="product-param__name">Диаметр верх (внутр.)</div>
                <div class="product-param__value">{$props.dia_in} мм</div>
              </div>
              {/if}
              {if $props.dia_bottom}
              <div class="product-param">
                <div class="product-param__image">
                  <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cup-width-bottom.svg" alt="" class="product-param__ico">
                </div>
                <div class="product-param__name">Диаметр дна</div>
                <div class="product-param__value">{$props.dia_bottom} мм</div>
              </div>
              {/if}
              {if $props.height}
              <div class="product-param">
                <div class="product-param__image">
                  <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cup-height.svg" alt="" class="product-param__ico">
                </div>
                <div class="product-param__name">Высота</div>
                <div class="product-param__value">{$props.height} мм</div>
              </div>
              {/if}
              {if $other = json_decode($props.props, true)}
                {foreach $other as $o}
                  <div class="product-param">
                    <div class="product-param__name">{$o.name}</div>
                    <div class="product-param__value">{$o.value}</div>
                  </div>
                {/foreach}
              {/if}
            </div>
          {/if}
          {if $sizes}
            <div class="product-packing">
              <h2>Упаковка</h2>
              <div class="product-packing__items">
                {foreach $sizes as $s}
                  <div class="product-packing__item">
                    <div class="product-packing__image">
                      {if $s.pack_type|preg_match : '/туб|рукав/iu'}
                        <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-tube.svg" alt="" class="product-packing__ico">
                      {else}
                        <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-box.svg" alt="" class="product-packing__ico">
                      {/if}
                    </div>
                    <div class="product-packing__params">
                      <div class="product-packing__name">{$s.pack_type}</div>
                      {if $s.count}<div class="product-packing__param">{$s.count} шт.</div>{/if}
                      {if $s.pack_size}<div class="product-packing__param">{$s.pack_size} см</div>{/if}
                      {if $s.pack_value}<div class="product-packing__param">{$s.pack_value} м&sup3;</div>{/if}
                      {if $s.pack_weight}<div class="product-packing__param">{if $s.pack_weight < 1}{$s.pack_weight*1000} г{else}{$s.pack_weight} кг{/if}</div>{/if}
                    </div>
                  </div>
                {/foreach}
              </div>
            </div>
          {/if}
        </div>

        <div class="product-delipay">
          {set $deliveries = $_modx->runSnippet('@FILE snippets/getDelivery.php')}
          {if $deliveries}
          <div class="product-delivery">
            <h2>Доставка</h2>
            {foreach $deliveries as $deliv}
              {if $deliv.name | preg_match : '/доста/iu'}
                <div class="product-delivery__item">
                  <div class="product-delivery__info">{$deliv.name | preg_replace : '/^доставка/iu': ''}</div>
                  <div class="product-delivery__note">{$deliv.description}</div>
                </div>
              {/if}
            {/foreach}
          </div>
          {/if}
          {set $payments = $_modx->runSnippet('@FILE snippets/getPayment.php')}
          {if $payments}
            <div class="product-payment">
              <h2>Оплата</h2>
              {foreach $payments as $pay}
                <div class="product-delivery__item">
                  <div class="product-delivery__info">{$pay.name | lcase}</div>
                </div>
              {/foreach}
            </div>
          {/if}
        </div>
      </div>
    </div>
  </div>

  {'!msProductsCustom' | snippet : [
      'wrapIfEmpty' => 0,
      'parents' => 0,
      'link' => 1,
      'master' => $_modx->resource.id,
      'limit' => 0,
      'includeContent' => 1,
      'tplWrapper' => '@INLINE <div class="product-recommend-section section">
    <img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_lines_square.svg" alt="" class="product-recommend__shape">
    <div class="section-wrapper">
      <h3 class="product-section__title">С этим товаром покупают</h3>
      <div class="product-recommend">
        <div class="swiper-container swiper-container-recommend">
          <div class="swiper-wrapper swiper-wrapper-recommend">{$output}</div>
          <div class="product-recommend__controls">
            <div class="product-recommend__nav product-recommend__prev"><img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_left.svg" alt="" class="product-recommend__prev-ico"></div>
            <div class="product-recommend__nav product-recommend__next"><img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_right.svg" alt="" class="product-recommend__next-ico"></div>
          </div>
        </div>
      </div>
    </div>
  </div>',
      'tpl' => '@FILE chunks/all/tpl.goods_card_h4.tpl'
  ] | replace:"catalog-card\"":"catalog-card swiper-slide\""}

  {'!msProductsCustom' | snippet : [
      'wrapIfEmpty' => 0,
      'parents' => $_modx->resource.parent,
      'resources' => '-'~$_modx->resource.id,
      'limit' => 8,
      'includeContent' => 1,
      'tplWrapper' => '@INLINE <div class="product-similar-section section">
    <img src="assets/templates/cuptrade/public/assets/cuptrade/img/bg_lines_square.svg" alt="" class="product-similar__shape">
    <div class="section-wrapper">
      <h3 class="product-section__title">Другие {$_modx->resource.parent | resource:"pagetitle"}</h2>
      <div class="product-similar">
        <div class="swiper-container swiper-container-similar">
          <div class="swiper-wrapper swiper-wrapper-similar">{$output}</div>
          <div class="product-similar__controls">
            <div class="product-similar__nav product-similar__prev"><img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_left.svg" alt="" class="product-similar__prev-ico"></div>
            <div class="product-similar__nav product-similar__next"><img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-chevron_right.svg" alt="" class="product-similar__next-ico"></div>
          </div>
        </div>
      </div>
    </div>
  </div>',
      'tpl' => '@FILE chunks/all/tpl.goods_card_h4.tpl'
  ] | replace:"catalog-card\"":"catalog-card swiper-slide\""}
{/block}