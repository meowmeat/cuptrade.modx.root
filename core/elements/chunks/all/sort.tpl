<div class="catalog-sorting js-sorting" id="mse2_sort">
            <div class="catalog-sorting__title">Сортировать</div>
            <div class="catalog-sorting__controls">
              <div class="catalog-sorting__control js-value-wrapper">
                <input type="text" name="sort" id="catalogSortAmount_value" class="catalog-sorting__control-value">
                <div class="catalog-sorting__control-text">по объёму</div>
                <span class="catalog-sorting__control-icon"><svg width="12" height="8" viewBox="0 0 12 8" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg catalog-sorting__control-ico">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M.04 1.989L1.425.546l4.307 4.135L10.04.546l1.385 1.443-5.693 5.465L.04 1.989z" />
                  </svg></span>
                <div class="catalog-sorting__menu">
                  <input type="radio" name="sortValue" value="value:desc" id="catalogSortAmount_descent" data-title="от большего объёма" class="catalog-sorting__menu-input">
                  <label for="catalogSortAmount_descent" class="catalog-sorting__menu-label">от большего<span class="catalog-sorting__menu-ico"><svg width="10" height="6" viewBox="0 0 12 8" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M.04 1.989L1.425.546l4.307 4.135L10.04.546l1.385 1.443-5.693 5.465L.04 1.989z" />
                      </svg></span></label>
                  <input type="radio" name="sortValue" value="value:asc" id="catalogSortAmount_ascent" data-title="от меньшего объёма" class="catalog-sorting__menu-input">
                  <label for="catalogSortAmount_ascent" class="catalog-sorting__menu-label">от меньшего<span class="catalog-sorting__menu-ico"><svg width="10" height="6" viewBox="0 0 12 8" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M.375 6.011L1.76 7.454l4.308-4.135 4.307 4.135L11.76 6.01 6.068.546.375 6.011z" />
                      </svg></span></label>
                </div>
              </div>
              <div class="catalog-sorting__control">
                <input type="text" name="sort" id="catalogSortAmount_value" class="catalog-sorting__control-value">
                <div class="catalog-sorting__control-text">по цене</div>
                <span class="catalog-sorting__control-icon"><svg width="12" height="8" viewBox="0 0 12 8" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg catalog-sorting__control-ico">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M.04 1.989L1.425.546l4.307 4.135L10.04.546l1.385 1.443-5.693 5.465L.04 1.989z" />
                  </svg></span>
                <div class="catalog-sorting__menu">
                  <input type="radio" name="sortPrice" value="price:desc" id="catalogSortPrice_descent" data-title="от большей цены" class="catalog-sorting__menu-input">
                  <label for="catalogSortPrice_descent" class="catalog-sorting__menu-label">от большей<span class="catalog-sorting__menu-ico"><svg width="10" height="6" viewBox="0 0 12 8" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M.04 1.989L1.425.546l4.307 4.135L10.04.546l1.385 1.443-5.693 5.465L.04 1.989z" />
                      </svg></span></label>
                  <input type="radio" name="sortPrice" value="price:asc" id="catalogSortPrice_ascent" data-title="от меньшей цены" class="catalog-sorting__menu-input">
                  <label for="catalogSortPrice_ascent" class="catalog-sorting__menu-label">от меньшей<span class="catalog-sorting__menu-ico"><svg width="10" height="6" viewBox="0 0 12 8" fill="#000100" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M.375 6.011L1.76 7.454l4.308-4.135 4.307 4.135L11.76 6.01 6.068.546.375 6.011z" />
                      </svg></span></label>
                </div>
              </div>
            </div>
          </div>