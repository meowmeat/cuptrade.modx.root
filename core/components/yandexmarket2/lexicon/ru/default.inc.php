<?php

$_lang = array_merge($_lang ?? [], [
    'yandexmarket2'           => 'YandexMarket2',
    'yandexmarket2_menu_desc' => 'Экспорт предложений в XML',

    'ym2_pricelist'                 => 'Прайс-лист',
    'ym2_field'                     => 'Элемент',
    'ym2_attribute'                 => 'Атрибут',
    'ym2_category'                  => 'Категория',
    'ym2_condition'                 => 'Условие',
    'ym2_pricelist_err_nf'          => 'Прайс-лист не найден',
    'ym2_attribute_err_nf'          => 'Атрибут не найден',
    'ym2_category_err_nf'           => 'Категория не найдена',
    'ym2_condition_err_nf'          => 'Условие не найдено',
    'ym2_field_err_nf'              => 'Элемент не найден',
    'ym2_category_err_ae'           => 'Категория уже существует',
    'ym2_condition_column_err_ns'   => 'Нужно указать столбец',
    'ym2_condition_operator_err_ns' => 'Нужно выбрать оператор',
    'ym2_pricelist_name_err_ns'     => 'Нужно ввести название',
    'ym2_pricelist_type_err_ns'     => 'Нужно выбрать тип прайс-листа',
    'ym2_pricelist_file_err_ae'     => 'Прайс-лист с таким файлом уже существует',
    'ym2_field_err_valid'           => 'Невалидные значения элемента',
    'ym2_attribute_err_valid'       => 'Невалидное значение атрибута',
    'ym2_pricelist_err_method'      => 'Неизвестный метод',

    'ym2_field_type_0'  => 'текст (без обработки)',
    'ym2_field_type_1'  => 'корневой элемент',
    'ym2_field_type_2'  => 'элемент магазина',
    'ym2_field_type_3'  => 'список валют',
    'ym2_field_type_4'  => 'список категорий',
    'ym2_field_type_5'  => 'список предложений',
    'ym2_field_type_6'  => 'элемент предложения',
    'ym2_field_type_7'  => 'элемент категории',
    'ym2_field_type_10' => 'родительский элемент',
    'ym2_field_type_11' => 'значение из столбца',
    'ym2_field_type_12' => 'столбец в CDATA (с html тегами)',
    'ym2_field_type_13' => 'изображения товара',
    'ym2_field_type_14' => 'прозрачная обёртка для категорий',
    'ym2_field_type_15' => 'прозрачная обёртка для товаров',
    'ym2_field_type_19' => 'сырой XML (без обработки)',
    'ym2_field_type_20' => 'только атрибуты (пустой элемент)',

    'ym2_marketplace_yandex.market' => 'Яндекс Маркет',
    'ym2_marketplace_google.rss20' => 'Google RSS 2.0 (Merchant center, FB)',
    'ym2_msoptionsprice_options' => 'Массив опций со значениями'
]);
