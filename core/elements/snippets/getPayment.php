<?php
$out = [];
$c = $modx->newQuery('msPayment', array('active' => 1));
$c->select(array('msPayment.*'));
if ($c->prepare() && $c->stmt->execute()) {
	while ($row = $c->stmt->fetch(PDO::FETCH_ASSOC)) {
		$out[] = $row;
	}
}
return $out;