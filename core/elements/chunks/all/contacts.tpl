  <div class="contacts-section section">
    <div class="section-wrapper">
{if $_modx->resource.id == 7}
      <div class="section-heading">
        <h1 class="section-heading__title">{$_modx->resource.pagetitle}</h1>
      </div>
{/if}
      <div class="contacts">
        <div class="contacts-modules">
          <div class="contacts-module contacts-module--1">
            <div class="contacts__title">Склад, шоу-рум, офис</div>
            <div class="contacts__text">{$_modx->config['address']}</div>
          </div>
          <div class="contacts-module contacts-module--2">
            <div class="contacts__title">Отдел продаж</div>
            <a href="tel:{$_modx->config['phone']}" class="contacts__text js-phone">{$_modx->config['phone'] | replace:"+7":""}</a>
          </div>
          <div class="contacts-module contacts-module--3">
            <div class="contacts__title">Почта</div>
            <a href="mailto:{$_modx->config['email']}" class="contacts__text">{$_modx->config['email']}</a>
          </div>
          <div class="contacts-module contacts-module--4">
            <div class="contacts__title">Работаем в будние дни</div>
            <div class="contacts__text">{$_modx->config['worktime']}</div>
          </div>
        </div>
      </div>
      <div id="map" class="contacts__map"></div>
    </div>
  </div>