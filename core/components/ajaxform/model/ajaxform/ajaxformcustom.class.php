<?php
require_once 'ajaxform.class.php';
class AjaxFormCustom extends AjaxForm
{
    /**
     * Initializes AjaxForm into different contexts.
     *
     * @deprecated
     *
     * @return boolean
     */
    public function initialize()
    {
        $this->loadJsCss();

        return true;
    }
    /**
     * Independent registration of css and js
     *
     * @param string $objectName Name of object to initialize in javascript
     */
    public function loadJsCss($objectName = 'AjaxForm')
    {
        if ($css = trim($this->config['frontend_css'])) {
            if (preg_match('/\.css/i', $css)) {
                $this->modx->regClientCSS(str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $css));
            }
        }
        if ($js = trim($this->config['frontend_js'])) {
            if (preg_match('/\.js/i', $js)) {
                $this->modx->regClientScript(str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $js));
            }
        }

        $config = $this->modx->toJSON(array(
            'assetsUrl' => $this->config['assetsUrl'],
            'actionUrl' => str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $this->config['actionUrl']),
            'closeMessage' => $this->config['closeMessage'],
            'formSelector' => "form.{$this->config['formSelector']}",
            'pageId' => !empty($this->modx->resource)
                ? $this->modx->resource->get('id')
                : 0,
        ));
        $objectName = trim($objectName);
        $this->modx->setPlaceholder('_AjaxFormInit', ($this->modx->getPlaceholder('_AjaxFormInit')?:0)+1 );
        $idx = $this->modx->getPlaceholder('_AjaxFormInit');
        $this->modx->regClientScript(
            "<script>function initAjaxForm{$idx}(){ {$objectName}.initialize({$config});}</script>", true
        );
        $script = '';
        for ($i = 1; $i <= $idx; $i++) {
            $script .= "initAjaxForm{$i}();";
        }
        $this->modx->regClientScript(
            "<script>function initAjaxForm(){ {$script}}</script>", true
        );
    }
}
