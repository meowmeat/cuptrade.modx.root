<div class="section-twin" id="msCart">
<div class="section-twin__main">
          {if $products}
          <div class="cart-set">
            <div class="cart-head">
              <div class="cart-head__title cart-head__title-good">Товар</div>
              <div class="cart-head__title cart-head__title-pack">Упаковок</div>
              <div class="cart-head__title cart-head__title-logo">Логотип</div>
              <div class="cart-head__title cart-head__title-number">Единиц</div>
              <div class="cart-head__title cart-head__title-price">Цена за шт., ₽</div>
              <div class="cart-head__title cart-head__title-summary">Сумма, ₽</div>
            </div>
            {foreach $products as $product}
              {if $packs = ($product.id | resource:"sizes")}
                {if $product.options.pack}
                  {foreach json_decode($packs, true) as $pack}
                    {if $pack.MIGX_id == $product.options.pack}
                      {set $pack_count = $pack.count}
                      {set $pack_type = $pack.pack_type~' ('~$pack.count~' шт.)'}
                    {/if}
                  {/foreach}
                {/if}
              {/if}
              {set $ress = $_modx->getParentIds($product.id)}
              {foreach $ress as $res}
                {set $r = ($res|resource)}
                {if $r.published}
                  {set $title_cat = $r.pagetitle}
                  {break}
                {/if}
              {/foreach}
            <div class="cart-position" id="{$product.key}">
              <a href="{$product.uri}"><div class="cart-position__image">
                <img src="{$product.thumb?($product.thumb|replace:"small/":"card/"):'assets/templates/cuptrade/public/assets/cuptrade/img/logo-cuptrade.svg'}" alt="" class="cart-position__img">
              </div></a>
              <div class="cart-position__content">
                <div class="cart-position__info cart-position__col">
                  <div class="cart-position__name"><a href="{$product.uri}">{$product.pagetitle}</a></div>
                  {if $product.introtext}<div class="cart-position__desc">{$product.introtext}</div>{/if}
                  {if $product.article}<div class="cart-position__property">Артикул: <span class="cart-position__property-value">{$product.article}</span></div>{/if}
                  {if $title_cat}<div class="cart-position__property">Тип: <span class="cart-position__property-value">{$title_cat | lcase}</span></div>{/if}
                  {if $product.options.pack}<div class="cart-position__property">{$pack_type}</div>{/if}
                </div>
                <div class="cart-position__counter cart-position__col" data-product-counter>
                  <div class="cart-position__title">Упаковок</div>
                  <form method="post" class="ms2_form">
                    <input type="hidden" name="key" value="{$product.key}">
                    <input type="hidden" name="ms2_action" value="cart/change">
                    <div class="product-count">
                      <div class="product-count__control product-count__minus js-productCount" onselectstart="return false;"><span class="product-count__control-sign">&ndash;</span></div>
                      <input type="number" value="{$product.count?:1}" name="count" min="0" step="1" onkeypress="return checkingNumberFormat(event);" class="product-count__number js-productCount_Number">
                      <div class="product-count__control product-count__plus js-productCount" onselectstart="return false;"><span class="product-count__control-sign">+</span></div>
                    </div>
                  </form>
                </div>
                <div class="cart-position__logo cart-position__col">
                  {if $opts = json_decode($product.id|resource:"opts", true)}
                    <div class="cart-position__title">Логотип</div>
                    {foreach $opts as $opt}
                      {set $opt_prices = json_decode($opt.prices, true)}
                      {set $opt_min_price = 0}
                      {set $opt_max_price = 0}
                      {set $checked = false}
                      {foreach $opt_prices as $opt_price}
                        {if !$opt_min_price || $opt_min_price > $opt_price.price}
                          {set $opt_min_price = $opt_price.price}
                        {/if}
                        {if !$opt_max_price || $opt_max_price < $opt_price.price}
                          {set $opt_max_price = $opt_price.price}
                        {/if}
                      {/foreach}
                      {foreach $product.options.opts as $op}
                        {if $op == $opt.MIGX_id}
                          {set $checked = true}
                        {/if}
                      {/foreach}
                      <div class="cart-position__logo-control">
                        <input type="checkbox" name="options[opts][]" value="{$opt.MIGX_id}" id="cartProductLogo_{$product.key}_{$opt.MIGX_id}" class="cart-position__checkbox js-opts" {if $checked}checked{/if}>
                        <label for="cartProductLogo_{$product.key}_{$opt.MIGX_id}" class="cart-position__checkbox-label">{$opt.option_type} от {$opt_min_price} ₽ {$opt.postfix}</label>
                      </div>
                    {/foreach}
                  {/if}
                </div>
                <div class="cart-position__amount cart-position__col">
                  <div class="cart-position__title">Единиц</div>
                  {($pack_count?:1) * $product.count}
                </div>
                <div class="cart-position__singlecost cart-position__col">
                  <div class="cart-position__title">Цена за шт., ₽</div>
                  <span class="js-price">{$.session.minishop2.cart[$product.key].price/($pack_count?:1)}</span>
                </div>
                <div class="cart-position__price-wrap cart-position__col" data-product-price-wrap>
                  <div class="cart-position__title">Сумма, ₽</div>
                  <div class="cart-position__price" data-product-price="{($.session.minishop2.cart[$product.key].price * $.session.minishop2.cart[$product.key].count)}"><span class="js-price" data-product-cost>{($.session.minishop2.cart[$product.key].price * $.session.minishop2.cart[$product.key].count)}</span></div>
                </div>
                <form method="post" class="ms2_form">
                  <input type="hidden" name="key" value="{$product.key}">
                  <div class="cart-position__delete js-cart-delete">
                    <!-- <img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cross-grey.svg" alt="" class="cart-position__delete-img"> -->
                    <svg width="14" height="14" viewBox="0 0 14 14" fill="#989798" xmlns="http://www.w3.org/2000/svg" class="ico-svg">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M12.646 13.353l-12-12 .707-.707 12 12-.707.707z" />
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M.647 12.646l12-12 .707.707-12 12-.707-.707z" />
                    </svg>
                  </div>
                </form>
              </div>
            </div>
            {/foreach}
            {foreach $.session.minishop2.cart_remove as $k => $rm}
                {set $res = ($rm.id|resource)}
                <div class="cart-position__cancel">
                  <span class="cart-position__cancel-head">Вы удалили</span>
                  <span class="cart-position__cancel-info">{$res.pagetitle}</span>
                  <div class="cart-position__cancel-action js-cancel-remove" data-key="{$k}">Отменить
                    <div class="cart-position__cancel-img"><img src="assets/templates/cuptrade/public/assets/cuptrade/img/ico/ico-cross-white.svg" alt="" class="cart-position__cancel-ico"></div>
                  </div>
                </div>
            {/foreach}
          </div>
          {/if}
        </div>
        <div class="section-twin__side">
          <div class="cart-purchase">
            <div class="cart-purchase__header">
              <div class="cart-purchase__title">Итого</div>
              {if $_modx->config['discount']}<div class="cart-purchase__note">Приобретая товар кратно коробке, вы можете сэкономить до 15%!</div>{/if}
            </div>
            {set $summ_cart = 0}
            {set $summ_cost = 0}
            {set $total_discount = 0}
            {*$.session.minishop2.cart | print*}
            {foreach $.session.minishop2.cart as $pos}
              {set $summ_cart = $summ_cart + (($pos.old_price?:$pos.price)*$pos.count)}
              {set $summ_cost = $summ_cost + ($pos.price*$pos.count)}
              {set $total_discount = $total_discount + $pos.discount_cost}
            {/foreach}
            {*$.session.minishop2 | print*}
            <div class="cart-purchase__position">
              <div class="cart-purchase__position-name">Товары</div>
              <div class="cart-purchase__position-price"><span class="js-total_cart js-price">{$summ_cart}</span> ₽</div>
            </div>
            <div class="cart-purchase__position" style="color:#df312f">
              <div class="cart-purchase__position-name">Скидка</div>
              <div class="cart-purchase__position-price"><span class="js-total_discount js-price">{$total_discount}</span> ₽</div>
            </div>
            {'!msOrder' | snippet : [
            	'tpl' => '@INLINE <div class="cart-purchase__position">
                  <!-- <div class="cart-purchase__position-name">Доставка</div> -->
                  <!-- <div class="cart-purchase__position-price"><span class="js-delivery_cost js-price">{$.cookie.delivery_price?:($order.delivery_cost?:0)}</span> ₽</div> -->
                </div>
                <div class="cart-purchase__summary">
                  <div class="cart-purchase__summary-name">Всего</div>
                 <!--  <div class="cart-purchase__summary-price"><span class="js-total_cost js-price">{*$summ_cost*}{($order.cost|replace:" ":""|replace:",":".")+$.cookie.delivery_price-$order.delivery_cost}</span> -->
                  <div class="cart-purchase__summary-price"><span class="js-total_cost js-price">{*$summ_cost*}{($order.cart_cost|replace:" ":""|replace:",":".")}</span> ₽</div>
                </div>'
            ]}
            <a href="{27 | url}" class="button button-primary cart-purchase__button">Оформить заказ</a>
          </div>
        </div>
</div> <!-- end section-twin -->