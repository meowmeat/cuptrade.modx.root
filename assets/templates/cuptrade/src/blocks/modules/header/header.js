//== Header nav toggle dropdown menu
$(document).on('click touch', '.js-dropdownMenu_Toggle', function () {
  $(this)
    .find('.header-nav__item-control')
    .toggleClass('header-nav__item-control--rotate');
  $(this).toggleClass('header-nav-item__dropdown--active');
  $(this).find('.header-dropdown').slideToggle();
  if (
    $('.js-dropdownMenu_Toggle').hasClass('header-nav-item__dropdown--active')
  ) {
    $('.js-dropdownMenu_Toggle')
      .not($(this))
      .removeClass('header-nav-item__dropdown--active')
      .find('.header-dropdown')
      .slideUp();
  }
});

//== Change images in header dropdown menu
$('.header-dropdown-catalog .header-dropdown__item').on({
  mouseenter: function () {
    let dropdownItem_Image = this.getAttribute('data-headernav-img');
    let dropdownItem_Image_Active = $('#' + dropdownItem_Image);
    $('.header-dropdown__image-main').removeClass(
      'header-dropdown__image--active'
    );
    $(this).addClass('header-dropdown__item--active');
    dropdownItem_Image_Active.toggleClass('header-dropdown__image--active');
  },
  mouseleave: function () {
    let dropdownItem_Image = this.getAttribute('data-headernav-img');
    let dropdownItem_Image_Active = $('#' + dropdownItem_Image);
    $(this).removeClass('header-dropdown__item--active');
    dropdownItem_Image_Active.removeClass('header-dropdown__image--active');
    setTimeout(function () {
      if (
        !$('.header-dropdown__item').hasClass('header-dropdown__item--active')
      ) {
        $('.header-dropdown__image-main').addClass(
          'header-dropdown__image--active'
        );
      }
    }, 200);
  },
});

//== Header Search Call
$(document).on('click touch', '.js-searchDesktop_Call', function (e) {
  e.stopPropagation();
  $(this)
    .siblings('#headerSearchForm')
    .addClass('header-search--active')
    .show(('slide', { direction: 'left' }, 400));
});

$(document).on('click touch', function (e) {
  if (
    $('.header-search').hasClass('header-search--active') &&
    !$('#headerSearchForm').is(e.target) &&
    $('#headerSearchForm').has(e.target).length === 0
  ) {
    $('#headerSearchForm')
      .removeClass('header-search--active')
      .hide(('slide', { direction: 'right' }, 400));
  }
});
