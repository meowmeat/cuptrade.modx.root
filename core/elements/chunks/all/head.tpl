<head>
  <base href="{'site_url'|option}">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  {$_modx->runSnippet('SeoSuiteMeta', ['toPlaceholders' => true])}
  {$_modx->getPlaceholder('ss_meta.robots')}
  {$_modx->getPlaceholder('ss_meta.keywords')}
  {$_modx->getPlaceholder('ss_meta.meta_description')}
  {$_modx->getPlaceholder('ss_meta.og_image')}
  <meta name="description" content="{$_modx->resource.description | htmlent}" />
  <meta property="og:title" content="{($_modx->resource.longtitle?:$_modx->resource.pagetitle) | htmlent}" />
  <meta property="og:description" content="{$_modx->resource.description | htmlent}" />
  <meta property="og:url" content="{$_modx->resource.id | url : ['scheme' => 'full']}" />
  <meta property="og:site_name" content="{'site_name'|option}" />
  <meta name="yandex-verification" content="4b00d36aea7e9299" />
  <link type="image/x-icon" rel="shortcut icon" href="/favicon.ico">
  <link rel="canonical" href="{$_modx->resource.id | url : ['scheme' => 'full']}" />
  {'!MinifyX' | snippet : [
      'minifyCss' => 1,
      'cssPlaceholder' => 'HeadMinifyX.css',
      'cssSources' => '
      assets/templates/cuptrade/public/assets/cuptrade/css/normalize.css,
      assets/templates/cuptrade/public/assets/cuptrade/css/swiper-bundle.min.css,
      assets/templates/cuptrade/public/assets/cuptrade/css/fancybox.min.css,
      assets/templates/cuptrade/public/assets/cuptrade/css/style.css
      ',
      'cssTpl' => '<link rel="stylesheet" rel="preload" as="style" onload="this.onload=null;this.rel=\'stylesheet\'" href="[[+file]]">'
  ]}
  {$_modx->getPlaceholder('HeadMinifyX.css')}
  <title>{($_modx->resource.longtitle?:$_modx->resource.pagetitle) | htmlent}</title>
  {if $discount = $_modx->config['discount'] | split:":"}
    {if $discount[0]}
      {$_modx->setPlaceholder('_discount_summ', ($discount[0] | replace:" ":"" | replace:",":"."))}
      {$_modx->setPlaceholder('_discount', ($discount[1] | replace:" ":"" | replace:",":"." | replace:"%":""))}
    {/if}
  {/if}
  <script>window.dataLayer = window.dataLayer || [];</script>
  {*$_modx->runSnippet('@FILE snippets/revaluation.php')*}
</head>