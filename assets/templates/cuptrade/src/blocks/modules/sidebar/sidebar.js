//==== Sidebar for Categories ====//
//== this function called in a file: script-closing
function sidebarListSingle_Unmargined() {
  if ($('.sidebar').find('.sidebar-list').length < 2) {
    $('.sidebar-list').addClass('sidebar-list--single');
  }
}

//== Show more categories side list
$('.js-sidebarList_More').click(function () {
  var categoriesHidden_Accordion = $(this).siblings('.sidebar-list--hidden');
  categoriesHidden_Accordion.slideToggle('slow');
  // $('.js-sidebarList_More').text($(this).text() == 'Скрыть' ? 'Ещё' : 'Скрыть');
  $(this).toggleClass('sidebar-list__more--rotate');
  var categoriesMore_Text = $(this).find('.sidebar-list__more-text');
  categoriesMore_Text.text(
    categoriesMore_Text.text() == 'Скрыть' ? 'Ещё' : 'Скрыть'
  );
});
