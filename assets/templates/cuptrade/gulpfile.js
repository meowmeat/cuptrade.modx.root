let preprocessor = 'less';

const { src, dest, parallel, series, watch } = require('gulp');
const changedFiles = require('gulp-changed-in-place');
const del = require('del');
const htmlPart = require('gulp-file-include');
const includeJs = require('gulp-include');
const beautify = require('gulp-jsbeautifier');
const gcssmq = require('gulp-group-css-media-queries');
const concat = require('gulp-concat');
const less = require('gulp-less');
const newer = require('gulp-newer');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');

const path = {
  dev: {
    less: './src/styles/*.less',
    html: './src/view/page/*.html',
    vendor: './src/vendor/',
    modules: './src/blocks/modules/**/',
    fonts: './src/fonts/**/*',
    js: './src/js/**/*',
    img: './src/img/**/*',
    video: './src/video/*',
    lesswatch: './src/styles/**/*.less',
  },
  build: {
    css: './build/assets/cuptrade/css/',
    dest: './build/',
    fonts: './build/assets/fonts/',
    js: './build/assets/cuptrade/js/',
    img: './build/assets/cuptrade/img/',
    video: './build/assets/cuptrade/video/',
  },
  public: './public/',
};

async function cleanPublic() {
  return del.sync(path.public, { force: true });
}

async function clean() {
  return del.sync(path.build.dest, { force: true });
}

function browsersync() {
  browserSync.init({
    server: {
      baseDir: './build/',
      serveStaticOptions: {
        extensions: ['html'],
      },
    },
    port: 8090,
    ui: { port: 8091 },
    open: false,
  });
}

function scripts() {
  return src(path.dev.js)
    .pipe(
      includeJs({
        includePaths: path.dev.modules,
      })
    )
    .pipe(
      changedFiles({
        firstPass: true,
      })
    )
    .pipe(dest(path.build.js))
    .pipe(browserSync.stream());
}

function styles() {
  return src(path.dev.less)
    .pipe(eval(preprocessor)())
    .pipe(
      plumber({
        errorHandler: notify.onError(function (err) {
          return {
            title: 'Styles',
            message: err.message,
          };
        }),
      })
    )
    .pipe(autoprefixer({ grid: true, overrideBrowserslist: ['> 0.1%'] }))
    .pipe(gcssmq())
    .pipe(dest(path.build.css))
    .pipe(
      changedFiles({
        firstPass: true,
      })
    )
    .pipe(browserSync.stream()); // Сделаем инъекцию в браузер
}

function pages() {
  return src(path.dev.html)
    .pipe(
      htmlPart({
        prefix: '@@',
        basepath: './src/',
      })
    )
    .pipe(
      plumber({
        errorHandler: notify.onError(function (err) {
          return {
            title: 'Pages',
            message: err.message,
          };
        }),
      })
    )
    .pipe(dest(path.build.dest))
    .pipe(
      changedFiles({
        firstPass: true,
      })
    )
    .pipe(
      browserSync.reload({
        stream: true,
      })
    );
}

function htmlPrettify() {
  return src(path.build.dest + '*.html')
    .pipe(
      beautify({
        indent_size: 2,
        indent_with_tabs: false,
        preserve_newlines: true,
      })
    )
    .pipe(dest(path.build.dest));
}

function cssPrettify() {
  return src(path.build.css + 'style.css')
    .pipe(
      beautify({
        indent_size: 2,
        indent_char: ' ',
        indent_with_tabs: false,
        newline_between_rules: false,
        preserve_newlines: false,
      })
    )
    .pipe(dest(path.build.css));
}

function copyFonts() {
  return src(path.dev.fonts)
    .pipe(newer(path.build.fonts))
    .pipe(dest(path.build.fonts));
}
function copyImages() {
  return src(path.dev.img)
    .pipe(newer(path.build.img))
    .pipe(dest(path.build.img));
}
function copyVideos() {
  return src(path.dev.video)
    .pipe(newer(path.build.video))
    .pipe(dest(path.build.video));
}
function copyFavicon() {
  return src('./src/view/' + '*.ico')
    .pipe(newer(path.build.dest + '*.ico'))
    .pipe(dest(path.build.dest));
}
function copyVendorJS() {
  return src(path.dev.vendor + '*.js')
    .pipe(newer(path.build.js))
    .pipe(dest(path.build.js));
}
function copyVendorCss() {
  return src(path.dev.vendor + '*.css')
    .pipe(newer(path.build.css))
    .pipe(dest(path.build.css));
}

async function copyResources() {
  copyFonts();
  copyImages();
  copyVideos();
  copyFavicon();
  copyVendorJS();
  copyVendorCss();
}

function copyBuildToPublic() {
  return src(path.build.dest + '/**/*').pipe(dest(path.public));
}

function startwatch() {
  watch([path.dev.modules + '*.js', path.dev.js], scripts);
  watch([path.dev.lesswatch, path.dev.modules + '*.less'], styles).on(
    'change',
    browserSync.reload
  );
  watch([path.dev.html, path.dev.modules + '*.html'], pages).on(
    'change',
    browserSync.reload
  );
  watch('./src/**/*', copyResources);
}

exports.browsersync = browsersync;
exports.scripts = scripts;
exports.styles = styles;
exports.pages = pages;
exports.clean = clean;
exports.cleanPublic = cleanPublic;
exports.copyResources = copyResources;
exports.copyBuildToPublic = copyBuildToPublic;
exports.htmlPrettify = htmlPrettify;
exports.cssPrettify = cssPrettify;

exports.build = series(
  clean,
  styles,
  scripts,
  copyResources,
  pages,
  htmlPrettify,
  cssPrettify
);
exports.public = series(cleanPublic, copyBuildToPublic);
exports.default = parallel(
  styles,
  scripts,
  copyResources,
  pages,
  browsersync,
  startwatch
);
