<?php
$out = [];
$c = $modx->newQuery('msDelivery', array('active' => 1));
$c->select(array('msDelivery.*'));
if ($c->prepare() && $c->stmt->execute()) {
	while ($row = $c->stmt->fetch(PDO::FETCH_ASSOC)) {
		$out[] = $row;
	}
}
return $out;